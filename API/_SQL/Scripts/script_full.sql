USE [master]
GO
/****** Object:  Database [YouVita]    Script Date: 30/05/2019 20:52:51 ******/
CREATE DATABASE [YouVita]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'YouVita', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\YouVita.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'YouVita_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\YouVita_log.ldf' , SIZE = 1280KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [YouVita] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [YouVita].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [YouVita] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [YouVita] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [YouVita] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [YouVita] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [YouVita] SET ARITHABORT OFF 
GO
ALTER DATABASE [YouVita] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [YouVita] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [YouVita] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [YouVita] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [YouVita] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [YouVita] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [YouVita] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [YouVita] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [YouVita] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [YouVita] SET  DISABLE_BROKER 
GO
ALTER DATABASE [YouVita] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [YouVita] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [YouVita] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [YouVita] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [YouVita] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [YouVita] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [YouVita] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [YouVita] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [YouVita] SET  MULTI_USER 
GO
ALTER DATABASE [YouVita] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [YouVita] SET DB_CHAINING OFF 
GO
ALTER DATABASE [YouVita] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [YouVita] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [YouVita] SET DELAYED_DURABILITY = DISABLED 
GO
USE [YouVita]
GO
/****** Object:  Table [dbo].[AddressType]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AddressType](
	[AddressTypeId] [int] IDENTITY(1,1) NOT NULL,
	[AddressTypeDescription] [varchar](40) NULL,
	[AddressTypeEnabled] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
 CONSTRAINT [pk_AddressType] PRIMARY KEY CLUSTERED 
(
	[AddressTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Clients]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Clients](
	[ClientId] [int] IDENTITY(1,1) NOT NULL,
	[ClientName] [varchar](60) NOT NULL,
	[ClientFantasyName] [varchar](60) NOT NULL,
	[CNPJ] [varchar](14) NOT NULL,
	[Address] [varchar](60) NOT NULL,
	[AddressNumber] [varchar](10) NOT NULL,
	[Neighborhood] [varchar](50) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[ZipCode] [varchar](8) NOT NULL,
	[StateId] [int] NOT NULL,
	[ResponsibleContact] [varchar](50) NULL,
	[Occupation] [varchar](50) NULL,
	[Email] [varchar](60) NOT NULL,
	[Phone] [varchar](20) NULL,
	[Ramal] [varchar](10) NULL,
	[CellPhone] [varchar](20) NULL,
	[ClientLogin] [varchar](60) NOT NULL,
	[ClientPassword] [varchar](40) NULL,
	[ConfirmPassword] [varchar](40) NULL,
	[ClientTypeId] [int] NOT NULL,
	[ClientStatus] [varchar](1) NOT NULL,
	[Comments] [varchar](200) NULL,
	[ClientsEnabled] [int] NOT NULL,
 CONSTRAINT [pk_Clients] PRIMARY KEY CLUSTERED 
(
	[ClientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ClientsContacts]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClientsContacts](
	[ContactId] [int] IDENTITY(1,1) NOT NULL,
	[ClientId] [int] NOT NULL,
	[ContactName] [varchar](40) NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[Password] [varchar](20) NULL,
	[Occupation] [varchar](20) NULL,
	[Phone] [varchar](20) NOT NULL,
	[Ramal] [varchar](10) NULL,
	[CellPhone] [varchar](20) NULL,
	[ContactTypeId] [int] NOT NULL,
	[ClientsContactsEnabled] [int] NOT NULL,
 CONSTRAINT [pk_ClientsContacts] PRIMARY KEY CLUSTERED 
(
	[ContactId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ClientType]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClientType](
	[ClientTypeId] [int] IDENTITY(1,1) NOT NULL,
	[ClientTypeDescription] [varchar](40) NULL,
	[ClientTypeenabled] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
 CONSTRAINT [pk_ClientType] PRIMARY KEY CLUSTERED 
(
	[ClientTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Configurations]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Configurations](
	[ConfigurationId] [int] IDENTITY(1,1) NOT NULL,
	[ConfigurationName] [varchar](100) NOT NULL,
	[ConfigurationValue] [varchar](400) NOT NULL,
	[ConfigurationEnabled] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
 CONSTRAINT [pk_Configurations] PRIMARY KEY CLUSTERED 
(
	[ConfigurationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactType]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactType](
	[ContactTypeId] [int] IDENTITY(1,1) NOT NULL,
	[ContactDescription] [varchar](40) NOT NULL,
	[ContactTypeEnabled] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
 CONSTRAINT [pk_ContactType] PRIMARY KEY CLUSTERED 
(
	[ContactTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DispensationPeriod]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DispensationPeriod](
	[DispensationId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](20) NOT NULL,
	[QtyDays] [int] NOT NULL,
	[DispensationPeriodEnabled] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
 CONSTRAINT [pk_DispensationPeriod] PRIMARY KEY CLUSTERED 
(
	[DispensationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DoctorAddresses]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DoctorAddresses](
	[DoctorAddressesId] [int] IDENTITY(1,1) NOT NULL,
	[DoctorId] [int] NOT NULL,
	[AddressTypeId] [int] NOT NULL,
	[Address] [varchar](60) NOT NULL,
	[AddressNumber] [varchar](10) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[Neighborhood] [varchar](50) NOT NULL,
	[StateId] [int] NOT NULL,
	[ZipCode] [varchar](8) NOT NULL,
	[Complement] [varchar](200) NULL,
	[DoctorAddressesEnabled] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
 CONSTRAINT [pk_DoctorAddresses] PRIMARY KEY CLUSTERED 
(
	[DoctorAddressesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Doctors]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Doctors](
	[DoctorID] [int] IDENTITY(1,1) NOT NULL,
	[DoctorName] [varchar](60) NULL,
	[CRM] [varchar](10) NULL,
	[UF_CRM] [varchar](2) NULL,
	[CPF] [varchar](14) NULL,
	[Address] [varchar](60) NULL,
	[Number] [varchar](10) NULL,
	[Complement] [varchar](60) NULL,
	[City] [varchar](60) NULL,
	[Neighborhood] [varchar](50) NULL,
	[StateID] [int] NULL,
	[ZipCode] [varchar](9) NULL,
	[PhoneComercial] [varchar](20) NULL,
	[CellPhone] [varchar](20) NULL,
	[FocalPoint] [varchar](20) NULL,
	[email] [varchar](100) NULL,
	[login] [varchar](20) NULL,
	[password] [varchar](40) NULL,
	[Comments] [varchar](200) NULL,
	[Status] [varchar](10) NULL,
	[RepresentativeID] [int] NULL,
	[DoctorEnabled] [int] NOT NULL,
	[filter] [varchar](200) NULL,
	[ramal] [varchar](10) NULL,
 CONSTRAINT [pk_doctor] PRIMARY KEY CLUSTERED 
(
	[DoctorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Laboratories]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Laboratories](
	[LaboratoryID] [int] IDENTITY(1,1) NOT NULL,
	[LaboratoryName] [varchar](60) NULL,
	[LaboratoryFantasyName] [varchar](60) NULL,
	[CNPJ] [varchar](20) NULL,
	[Address] [varchar](60) NULL,
	[Number] [varchar](10) NULL,
	[Neighborhood] [varchar](60) NULL,
	[City] [varchar](60) NULL,
	[StateID] [int] NULL,
	[Complement] [varchar](60) NULL,
	[ZipCode] [varchar](10) NULL,
	[PABX] [varchar](20) NULL,
	[Responsible] [varchar](30) NULL,
	[DeptResponsible] [varchar](30) NULL,
	[Email] [varchar](100) NULL,
	[ComercialPhone] [varchar](20) NULL,
	[CellPhone] [varchar](20) NULL,
	[Login] [varchar](20) NULL,
	[Password] [varchar](40) NULL,
	[Status] [int] NULL,
	[Comments] [varchar](200) NULL,
	[LaboratoryEnabled] [int] NOT NULL,
	[filter] [varchar](500) NULL,
 CONSTRAINT [pk_laboratory] PRIMARY KEY CLUSTERED 
(
	[LaboratoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Medicines]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Medicines](
	[MedicineId] [int] IDENTITY(1,1) NOT NULL,
	[EAN] [varchar](13) NOT NULL,
	[MedicineName] [varchar](100) NULL,
	[LabId] [varchar](10) NULL,
	[LabName] [varchar](100) NULL,
	[ActivePrincipleId] [varchar](10) NULL,
	[ActivePrincipleName] [varchar](100) NULL,
	[Presentation] [varchar](100) NULL,
	[UnitId] [varchar](10) NULL,
	[UnitName] [varchar](100) NULL,
	[Dosage] [int] NULL,
	[QtyTotal] [int] NULL,
	[MedicineType] [varchar](20) NULL,
	[Comments] [varchar](200) NULL,
	[MedicineEnabled] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
	[CodeTOTVS] [varchar](20) NULL,
	[SystemId] [int] NULL,
 CONSTRAINT [pk_MedicineId] PRIMARY KEY CLUSTERED 
(
	[MedicineId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MedicinesPMC]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MedicinesPMC](
	[MedicienPMCId] [int] IDENTITY(1,1) NOT NULL,
	[MedicineId] [int] NOT NULL,
	[Value] [numeric](10, 2) NULL,
	[Aliquot] [numeric](5, 2) NULL,
	[MedicinesPMCEnabled] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Filter] [varchar](200) NULL,
 CONSTRAINT [pk_MedicienPMCId] PRIMARY KEY CLUSTERED 
(
	[MedicienPMCId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderAvaliable]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderAvaliable](
	[OrderAvaliableID] [int] IDENTITY(1,1) NOT NULL,
	[DoctorId] [int] NULL,
	[startValidity] [datetime] NULL,
	[endValidity] [datetime] NULL,
	[frequence] [int] NULL,
	[dateToSend] [datetime] NULL,
	[laboratoryID] [int] NULL,
	[OrderAvaliableEnabled] [int] NULL,
	[status] [varchar](1) NULL,
 CONSTRAINT [pk_OrderAvaliable] PRIMARY KEY CLUSTERED 
(
	[OrderAvaliableID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderAvaliableRepresentative]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderAvaliableRepresentative](
	[OrderAvaliableRepresentativeID] [int] IDENTITY(1,1) NOT NULL,
	[RepresentativeID] [int] NULL,
	[startValidity] [datetime] NULL,
	[endValidity] [datetime] NULL,
	[frequence] [int] NULL,
	[dateToSend] [datetime] NULL,
	[laboratoryID] [int] NULL,
	[OrderAvaliableRepresentativeEnabled] [int] NULL,
	[status] [varchar](1) NULL,
 CONSTRAINT [pk_OrderAvaliableRepresentative] PRIMARY KEY CLUSTERED 
(
	[OrderAvaliableRepresentativeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderItem]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderItem](
	[OrderItemId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[ReleaseOrderItemId] [int] NOT NULL,
	[ReleaseOrderId] [int] NOT NULL,
	[MedicineId] [int] NOT NULL,
	[Dosage] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[OrderItemEnabled] [int] NOT NULL,
 CONSTRAINT [pk_OrderItemId] PRIMARY KEY CLUSTERED 
(
	[OrderItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Orders]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderId] [int] IDENTITY(1,1) NOT NULL,
	[ReleaseOrderId] [int] NOT NULL,
	[OrderDate] [datetime] NOT NULL,
	[PatientId] [int] NOT NULL,
	[PatientAddressId] [int] NOT NULL,
	[PatientAuthorization] [varchar](20) NOT NULL,
	[OrderStatus] [varchar](1) NULL DEFAULT ('P'),
	[OrderObservation] [varchar](200) NULL,
	[OrdersEnabled] [int] NOT NULL,
 CONSTRAINT [pk_OrderId] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrdersDoctor]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrdersDoctor](
	[OrderDoctorID] [int] IDENTITY(1,1) NOT NULL,
	[DoctorID] [int] NOT NULL,
	[Status] [varchar](1) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[OrderDoctorDateSend] [datetime] NOT NULL,
	[OrderDoctorEnabled] [int] NOT NULL,
	[OrderAvaliableID] [int] NULL,
 CONSTRAINT [pk_OrderDoctor] PRIMARY KEY CLUSTERED 
(
	[OrderDoctorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrdersRepresentative]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrdersRepresentative](
	[OrdersRepresentativeID] [int] IDENTITY(1,1) NOT NULL,
	[RepresentativeID] [int] NOT NULL,
	[Status] [varchar](1) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[OrderRepresentativeDateSend] [datetime] NOT NULL,
	[OrderRepresentativeEnabled] [int] NOT NULL,
	[OrderRepresentativeAvaliableID] [int] NULL,
 CONSTRAINT [pk_OrdersDoctorRepresentative] PRIMARY KEY CLUSTERED 
(
	[OrdersRepresentativeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pathologies]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pathologies](
	[pathologyId] [int] IDENTITY(1,1) NOT NULL,
	[pathologyDescription] [varchar](50) NULL,
	[PathologiesEnabled] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
 CONSTRAINT [pk_Pathologies] PRIMARY KEY CLUSTERED 
(
	[pathologyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PatientAddresses]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PatientAddresses](
	[PatientAddressesId] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[AddressTypeId] [int] NOT NULL,
	[Address] [varchar](60) NOT NULL,
	[AddressNumber] [varchar](10) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[Neighborhood] [varchar](50) NOT NULL,
	[StateId] [int] NOT NULL,
	[ZipCode] [varchar](8) NOT NULL,
	[Complement] [varchar](200) NULL,
	[PatientAddressesEnabled] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
 CONSTRAINT [pk_PatientAddresses] PRIMARY KEY CLUSTERED 
(
	[PatientAddressesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PatientPahologies]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PatientPahologies](
	[PatientPathologiesId] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[pathologyId] [int] NULL,
	[PatientPathologiesEnabled] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
 CONSTRAINT [pk_PatientPahologies] PRIMARY KEY CLUSTERED 
(
	[PatientPathologiesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PatientPathologies]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PatientPathologies](
	[PatientPathologiesId] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[pathologyId] [int] NULL,
	[PatientPathologiesEnabled] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
 CONSTRAINT [pk_PatientPathologies] PRIMARY KEY CLUSTERED 
(
	[PatientPathologiesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PatientPhones]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PatientPhones](
	[PatientPhoneId] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[PhoneNumber] [varchar](12) NOT NULL,
	[Ramal] [varchar](10) NULL,
	[PhoneTypeId] [int] NULL,
	[PatientPhonesEnabled] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
 CONSTRAINT [pk_PatientPhones] PRIMARY KEY CLUSTERED 
(
	[PatientPhoneId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Patients]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Patients](
	[PatientId] [int] IDENTITY(1,1) NOT NULL,
	[ClientId] [int] NOT NULL,
	[PatientName] [varchar](60) NOT NULL,
	[BirthDate] [datetime] NULL,
	[CPF] [varchar](11) NOT NULL,
	[RG] [varchar](15) NOT NULL,
	[IdentificationOnClient] [varchar](20) NOT NULL,
	[Responsible] [varchar](60) NULL,
	[BestContactPeriod] [varchar](20) NOT NULL,
	[Comments] [varchar](200) NULL,
	[PatientStatus] [varchar](1) NOT NULL,
	[CID1] [varchar](20) NULL,
	[CID2] [varchar](20) NULL,
	[CID3] [varchar](20) NULL,
	[PatientEnabled] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
 CONSTRAINT [pk_Patients] PRIMARY KEY CLUSTERED 
(
	[PatientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PatientsMedicines]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PatientsMedicines](
	[PatientsMedicinesId] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NULL,
	[EAN] [varchar](13) NULL,
	[PatienteAuthorization] [varchar](20) NULL,
	[Qty] [int] NULL,
	[DailyDosage] [int] NULL,
	[Dispensation] [int] NULL,
 CONSTRAINT [pk_PatientsMedicines] PRIMARY KEY CLUSTERED 
(
	[PatientsMedicinesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Permissions]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Permissions](
	[PermissionId] [int] IDENTITY(1,1) NOT NULL,
	[PermissionName] [varchar](150) NOT NULL,
	[PermissionEnabled] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
 CONSTRAINT [pk_Permissions] PRIMARY KEY CLUSTERED 
(
	[PermissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PhoneType]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhoneType](
	[PhoneTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NULL,
	[PhoneTypeEnabled] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
 CONSTRAINT [pk_PhoneType] PRIMARY KEY CLUSTERED 
(
	[PhoneTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProcessLog]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProcessLog](
	[ProcessLogDate] [datetime] NOT NULL,
	[ProcessLogUser] [varchar](40) NOT NULL,
	[ProcessLogName] [varchar](100) NOT NULL,
	[ProcessLogTable] [varchar](100) NOT NULL,
	[ProcessLogField] [varchar](100) NOT NULL,
	[ProcessLogValue] [varchar](100) NOT NULL,
	[Filter] [varchar](200) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Product]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product](
	[productID] [int] IDENTITY(1,1) NOT NULL,
	[productName] [varchar](100) NULL,
	[productCode] [varchar](20) NULL,
	[productEnabled] [int] NULL,
	[laboratoryID] [int] NULL,
 CONSTRAINT [pk_Product] PRIMARY KEY CLUSTERED 
(
	[productID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductsAvaliable]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductsAvaliable](
	[ProductAvaliableID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NOT NULL,
	[Qty] [int] NOT NULL,
	[ProductsAvaliableEnabled] [int] NOT NULL,
	[OrderAvaliableID] [int] NULL,
 CONSTRAINT [pk_ProductsAvaliable] PRIMARY KEY CLUSTERED 
(
	[ProductAvaliableID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductsAvaliableRepresentative]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductsAvaliableRepresentative](
	[ProductsAvaliableRepresentativeID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NOT NULL,
	[Qty] [int] NOT NULL,
	[ProductsAvaliableRepresentativeEnabled] [int] NOT NULL,
	[OrderAvaliableRepresentativeID] [int] NULL,
 CONSTRAINT [pk_ProductsAvaliableRepresentative] PRIMARY KEY CLUSTERED 
(
	[ProductsAvaliableRepresentativeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductsOrderDoctor]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductsOrderDoctor](
	[ProductsOrderDoctorID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NULL,
	[Qty] [int] NULL,
	[OrdersDoctorID] [int] NULL,
 CONSTRAINT [pk_ProductsOrderDoctor] PRIMARY KEY CLUSTERED 
(
	[ProductsOrderDoctorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductsOrderRepresentative]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductsOrderRepresentative](
	[ProductsOrderDoctorRepresentativeID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NULL,
	[Qty] [int] NULL,
	[OrdersRepresentativeID] [int] NULL,
 CONSTRAINT [pk_ProductsOrderDoctorRepresentative] PRIMARY KEY CLUSTERED 
(
	[ProductsOrderDoctorRepresentativeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReleaseOrder]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReleaseOrder](
	[ReleaseOrderId] [int] IDENTITY(1,1) NOT NULL,
	[ReleaseOrderDate] [datetime] NOT NULL,
	[PatientId] [int] NOT NULL,
	[PatientAuthorization] [varchar](20) NOT NULL,
	[DispensationId] [int] NOT NULL,
	[ReleaseOrderStatus] [varchar](1) NULL DEFAULT ('P'),
	[ReleaseOrderObservation] [varchar](200) NULL,
	[ReleaseOrderEnabled] [int] NOT NULL,
 CONSTRAINT [pk_ReleaseOrder] PRIMARY KEY CLUSTERED 
(
	[ReleaseOrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReleaseOrderItem]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReleaseOrderItem](
	[ReleaseOrderItemId] [int] IDENTITY(1,1) NOT NULL,
	[ReleaseOrderId] [int] NOT NULL,
	[MedicineId] [int] NOT NULL,
	[Dosage] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[ReleaseOrderItemEnabled] [int] NOT NULL,
 CONSTRAINT [pk_ReleaseOrderItem] PRIMARY KEY CLUSTERED 
(
	[ReleaseOrderItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Representatives]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Representatives](
	[RepresentativeID] [int] IDENTITY(1,1) NOT NULL,
	[RepresentativeName] [varchar](50) NULL,
	[email] [varchar](100) NULL,
	[login] [varchar](20) NULL,
	[password] [varchar](40) NULL,
	[Status] [varchar](10) NULL,
	[Comments] [varchar](200) NULL,
	[RepresentativeEnabled] [int] NOT NULL,
	[laboratoryID] [int] NULL,
	[cpf] [varchar](11) NULL,
	[address] [varchar](200) NULL,
	[addressNumber] [varchar](10) NULL,
	[complement] [varchar](100) NULL,
	[city] [varchar](100) NULL,
	[Neighborhood] [varchar](50) NULL,
	[stateID] [int] NULL,
	[zipcode] [varchar](8) NULL,
	[phoneComercial] [varchar](20) NULL,
	[ramal] [varchar](10) NULL,
	[cellPhone] [varchar](20) NULL,
 CONSTRAINT [pk_RepresentativeID] PRIMARY KEY CLUSTERED 
(
	[RepresentativeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[States]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[States](
	[StateId] [int] IDENTITY(1,1) NOT NULL,
	[StateName] [varchar](50) NULL,
	[StateUF] [varchar](2) NULL,
	[StateEnabled] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
 CONSTRAINT [pk_States] PRIMARY KEY CLUSTERED 
(
	[StateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Systems]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Systems](
	[SystemId] [int] IDENTITY(1,1) NOT NULL,
	[SystemName] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](40) NOT NULL,
	[UserLoginName] [varchar](40) NOT NULL,
	[UserPassword] [varchar](40) NOT NULL,
	[UserEnabled] [int] NOT NULL,
	[UserType] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
	[ClientId] [int] NULL,
	[laboratoryID] [int] NULL,
 CONSTRAINT [pk_Users] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserType]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserType](
	[UserTypeID] [int] IDENTITY(1,1) NOT NULL,
	[UserDescription] [varchar](30) NULL,
 CONSTRAINT [pk_UserType] PRIMARY KEY CLUSTERED 
(
	[UserTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[viewAddressType]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewAddressType]
as
Select * from AddressType where AddressTypeEnabled = 1

GO
/****** Object:  View [dbo].[viewClients]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[viewClients] as 
SELECT [ClientId]
      ,[ClientName]
      ,[ClientFantasyName]
      ,[CNPJ]
      ,[Address]
      ,[AddressNumber]
      ,[Neighborhood]
      ,[City]
      ,[ZipCode]
      ,[StateId]
      ,[ResponsibleContact]
      ,[Occupation]
      ,[Email]
      ,[Phone]
      ,[Ramal]
      ,[CellPhone]
      ,[ClientLogin]
      ,[ClientPassword]
      ,[ConfirmPassword]
      ,[ClientTypeId]
      ,[ClientStatus]
      ,[Comments]
      ,[ClientsEnabled]
  FROM [Clients] where Clients.ClientsEnabled = 1;

GO
/****** Object:  View [dbo].[viewClientsContacts]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[viewClientsContacts] as 
SELECT [ContactId]
      ,[ClientId]
      ,[ContactName]
      ,[Email]
      ,[Password]
      ,[Occupation]
      ,[Phone]
      ,[Ramal]
      ,[CellPhone]
      ,[ContactTypeId]
      ,[ClientsContactsEnabled]
  FROM [ClientsContacts]
where ClientsContacts.ClientsContactsEnabled = 1

GO
/****** Object:  View [dbo].[viewClientType]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[viewClientType] as 
Select 
	ClientType.ClientTypeId,
	ClientType.ClientTypeDescription,
	ClientType.ClientTypeEnabled,
	ClientType.Filter
from ClientType where ClientType.ClientTypeEnabled = 1

GO
/****** Object:  View [dbo].[viewConfigurations]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[viewConfigurations] as 
Select 
	Configurations.ConfigurationId,
	Configurations.ConfigurationName,
	Configurations.ConfigurationValue,
	Configurations.ConfigurationEnabled,
	Configurations.filter
from Configurations where Configurations.ConfigurationEnabled = 1

GO
/****** Object:  View [dbo].[viewContactType]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[viewContactType] as 
Select 
	ContactType.ContactTypeId,
	ContactType.ContactDescription,
	ContactType.ContactTypeEnabled,
	ContactType.Filter
from ContactType where ContactType.ContactTypeEnabled = 1;

GO
/****** Object:  View [dbo].[viewDispensationPeriod]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[viewDispensationPeriod] as 
Select 
	DispensationPeriod.DispensationId,
	DispensationPeriod.Description,
	DispensationPeriod.QtyDays,
	DispensationPeriod.DispensationPeriodEnabled,
	DispensationPeriod.Filter
from DispensationPeriod where DispensationPeriod.DispensationPeriodEnabled = 1;


GO
/****** Object:  View [dbo].[viewDoctorAddresses]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewDoctorAddresses] as
select * from DoctorAddresses where DoctorAddressesEnabled = 1

GO
/****** Object:  View [dbo].[viewDoctors]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[viewDoctors]
AS
SELECT        DoctorID, DoctorName, CRM, UF_CRM, CPF, Address, Number, Complement, City, Neighborhood, StateID, ZipCode, PhoneComercial, CellPhone, FocalPoint, email, login, password, Comments, Status, 
                         RepresentativeID, DoctorEnabled, filter, ramal
FROM            dbo.Doctors
WHERE        (DoctorEnabled = 1)


GO
/****** Object:  View [dbo].[viewLaboratories]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewLaboratories] as select * from Laboratories where LaboratoryEnabled = 1

GO
/****** Object:  View [dbo].[viewMedicines]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[viewMedicines] as 
select * from Medicines where MedicineEnabled = 1

GO
/****** Object:  View [dbo].[viewMedicinesPMC]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewMedicinesPMC] as 
select * from MedicinesPMC where MedicinesPMCEnabled = 1


GO
/****** Object:  View [dbo].[viewOrderAvaliable]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewOrderAvaliable] as select * from OrderAvaliable

GO
/****** Object:  View [dbo].[viewOrderAvaliableRepresentative]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewOrderAvaliableRepresentative] as select * from OrderAvaliableRepresentative where OrderAvaliableRepresentativeEnabled = 1

GO
/****** Object:  View [dbo].[viewOrdersDoctor]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewOrdersDoctor] as select * from OrdersDoctor

GO
/****** Object:  View [dbo].[viewOrdersRepresentative]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewOrdersRepresentative] as select * from OrdersRepresentative where OrderRepresentativeEnabled = 1

GO
/****** Object:  View [dbo].[viewPathologies]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[viewPathologies] as 
Select 
	Pathologies.PathologyId,
	Pathologies.PathologyDescription,
	Pathologies.PathologiesEnabled,
	Pathologies.Filter
from Pathologies where Pathologies.PathologiesEnabled = 1

GO
/****** Object:  View [dbo].[viewPatientAddresses]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewPatientAddresses] as
select * from PatientAddresses where PatientAddressesEnabled = 1

GO
/****** Object:  View [dbo].[viewPatientPathologies]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[viewPatientPathologies] as 
Select 
	PatientPathologies.PatientPathologiesId,
	PatientPathologies.PatientId,
	PatientPathologies.PathologyId,
	PatientPathologies.PatientPathologiesEnabled,
	PatientPathologies.Filter
from PatientPathologies where PatientPathologies.PatientPathologiesEnabled = 1


GO
/****** Object:  View [dbo].[viewPatientPhones]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[viewPatientPhones] as 
Select 
	PatientPhones.PatientPhoneId,
	PatientPhones.PatientId,
	PatientPhones.PhoneNumber,
	PatientPhones.Ramal,
	PatientPhones.PhoneTypeId,
	PatientPhones.PatientPhonesEnabled,
	PatientPhones.filter
from PatientPhones where PatientPhones.PatientPhonesEnabled = 1

GO
/****** Object:  View [dbo].[viewPatients]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[viewPatients] as 
SELECT [PatientId]
      ,[ClientId]
      ,[PatientName]
      ,[BirthDate]
      ,[CPF]
      ,[RG]
      ,[IdentificationOnClient]
      ,[Responsible]
      ,[BestContactPeriod]
      ,[Comments]
      ,[PatientStatus]
      ,[CID1]
      ,[CID2]
      ,[CID3]
      ,[PatientEnabled]
	  ,Filter
  FROM [dbo].[Patients]
	WHERE
		PatientEnabled = 1

GO
/****** Object:  View [dbo].[viewPermissions]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[viewPermissions] as 
Select 
	Permissions.PermissionId,
	Permissions.PermissionName,
	Permissions.PermissionEnabled,
	Permissions.filter
from Permissions where Permissions.PermissionEnabled = 1

GO
/****** Object:  View [dbo].[viewPhoneType]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[viewPhoneType] as 
Select 
	PhoneType.PhoneTypeId,
	PhoneType.Description,
	PhoneType.PhoneTypeEnabled,
	PhoneType.Filter
from PhoneType where PhoneType.PhoneTypeEnabled = 1

GO
/****** Object:  View [dbo].[viewProduct]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewProduct] as select * from Product where productEnabled = 1 

GO
/****** Object:  View [dbo].[viewProductsAvaliable]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewProductsAvaliable] as 
select * from ProductsAvaliable where ProductsAvaliableEnabled = 1

GO
/****** Object:  View [dbo].[viewProductsAvaliableRepresentative]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewProductsAvaliableRepresentative] as select * from ProductsAvaliableRepresentative where ProductsAvaliableRepresentativeEnabled = 1

GO
/****** Object:  View [dbo].[viewProductsOrderDoctor]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewProductsOrderDoctor] as select * from ProductsOrderDoctor

GO
/****** Object:  View [dbo].[viewProductsOrderRepresentative]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewProductsOrderRepresentative] as select * from ProductsOrderRepresentative 

GO
/****** Object:  View [dbo].[viewReleaseOrder]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewReleaseOrder] as select * from ReleaseOrder where ReleaseOrderEnabled = 1
GO
/****** Object:  View [dbo].[viewReleaseOrderItem]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewReleaseOrderItem] as select * from ReleaseOrderItem where ReleaseOrderItemEnabled = 1
GO
/****** Object:  View [dbo].[viewRepresentatives]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewRepresentatives] as select * from Representatives where RepresentativeEnabled = 1

GO
/****** Object:  View [dbo].[viewStates]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[viewStates] as 
Select 
	States.StateId,
	States.StateName,
	States.StateUF,
	States.StateEnabled,
	States.Filter
from States where States.StateEnabled = 1;


GO
/****** Object:  View [dbo].[viewUsers]    Script Date: 30/05/2019 20:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[viewUsers] as select * from Users where UserEnabled = 1

GO
SET IDENTITY_INSERT [dbo].[AddressType] ON 

INSERT [dbo].[AddressType] ([AddressTypeId], [AddressTypeDescription], [AddressTypeEnabled], [Filter]) VALUES (1, N'Residencial', 1, NULL)
INSERT [dbo].[AddressType] ([AddressTypeId], [AddressTypeDescription], [AddressTypeEnabled], [Filter]) VALUES (2, N'Comercial', 1, NULL)
INSERT [dbo].[AddressType] ([AddressTypeId], [AddressTypeDescription], [AddressTypeEnabled], [Filter]) VALUES (3, N'Faturamento', 1, NULL)
SET IDENTITY_INSERT [dbo].[AddressType] OFF
SET IDENTITY_INSERT [dbo].[Clients] ON 

INSERT [dbo].[Clients] ([ClientId], [ClientName], [ClientFantasyName], [CNPJ], [Address], [AddressNumber], [Neighborhood], [City], [ZipCode], [StateId], [ResponsibleContact], [Occupation], [Email], [Phone], [Ramal], [CellPhone], [ClientLogin], [ClientPassword], [ConfirmPassword], [ClientTypeId], [ClientStatus], [Comments], [ClientsEnabled]) VALUES (1, N'CLIENTE 1', N'FANTASIA 1', N'11111111111111', N'LOGADOURO 1', N'NUMERO 1', N'BAIRRO 1', N'CIDADE 1', N'99999999', 1, N'CONTATO 1', N'CARGO 1', N'EMAIL@EMAIL.COM', N'11111111111', N'RAMAL 1', N'2342434232', N'LOGIN1', N'SENHA1', N'SENHA1', 1, N'1', NULL, 1)
INSERT [dbo].[Clients] ([ClientId], [ClientName], [ClientFantasyName], [CNPJ], [Address], [AddressNumber], [Neighborhood], [City], [ZipCode], [StateId], [ResponsibleContact], [Occupation], [Email], [Phone], [Ramal], [CellPhone], [ClientLogin], [ClientPassword], [ConfirmPassword], [ClientTypeId], [ClientStatus], [Comments], [ClientsEnabled]) VALUES (2, N'CLIENTE 1', N'FANTASIA 1', N'11111111111111', N'LOGADOURO 1', N'NUMERO 1', N'BAIRRO 1', N'CIDADE 1', N'99999999', 1, N'CONTATO 1', N'CARGO 1', N'EMAIL@EMAIL.COM', N'11111111111', N'RAMAL 1', N'11111111111', N'LOGIN1', N'SENHA1', N'SENHA1', 1, N'1', NULL, 1)
INSERT [dbo].[Clients] ([ClientId], [ClientName], [ClientFantasyName], [CNPJ], [Address], [AddressNumber], [Neighborhood], [City], [ZipCode], [StateId], [ResponsibleContact], [Occupation], [Email], [Phone], [Ramal], [CellPhone], [ClientLogin], [ClientPassword], [ConfirmPassword], [ClientTypeId], [ClientStatus], [Comments], [ClientsEnabled]) VALUES (3, N'CLIENTE 3', N'FANTASIA 3', N'12457855555500', N'LOGADOURO 3', N'NUMERO 3', N'BAIRRO 3', N'CIDADE 3', N'33459933', 1, N'CONTATO 3', N'CARGO 3', N'EMAIL@EMAIL3.COM', N'1233333333', N'RAMAL 3', N'11933333333', N'LOGIN3', N'SENHA1', N'SENHA1', 1, N'1', NULL, 1)
SET IDENTITY_INSERT [dbo].[Clients] OFF
SET IDENTITY_INSERT [dbo].[ClientsContacts] ON 

INSERT [dbo].[ClientsContacts] ([ContactId], [ClientId], [ContactName], [Email], [Password], [Occupation], [Phone], [Ramal], [CellPhone], [ContactTypeId], [ClientsContactsEnabled]) VALUES (1, 2, N'ABCSAF DS', N'EMAIL@EMAIL.COM', N'SENHA', N'CARGO', N'11125555555', N'RAM', N'19999999999', 1, 1)
INSERT [dbo].[ClientsContacts] ([ContactId], [ClientId], [ContactName], [Email], [Password], [Occupation], [Phone], [Ramal], [CellPhone], [ContactTypeId], [ClientsContactsEnabled]) VALUES (2, 3, N'ABCSAF DS', N'EMAIL@EMAIL.COM', N'SENHA', N'CARGO', N'11125555555', N'RAM', N'19999999999', 1, 0)
INSERT [dbo].[ClientsContacts] ([ContactId], [ClientId], [ContactName], [Email], [Password], [Occupation], [Phone], [Ramal], [CellPhone], [ContactTypeId], [ClientsContactsEnabled]) VALUES (3, 3, N'NOVO CONTATO 2', N'EMAIL@CONTA.COM', N'SENHA', N'CARGO', N'12345555555', N'RAM', N'11999333333', 3, 0)
INSERT [dbo].[ClientsContacts] ([ContactId], [ClientId], [ContactName], [Email], [Password], [Occupation], [Phone], [Ramal], [CellPhone], [ContactTypeId], [ClientsContactsEnabled]) VALUES (4, 3, N'NOVO OUTRO CONTATO', N'EMAIL@EMAIL.COM', N'TESTE', N'CARGO', N'12345788855', N'RAMAL 1', N'11855995555', 2, 0)
INSERT [dbo].[ClientsContacts] ([ContactId], [ClientId], [ContactName], [Email], [Password], [Occupation], [Phone], [Ramal], [CellPhone], [ContactTypeId], [ClientsContactsEnabled]) VALUES (5, 3, N'SUPER CONTATO', N'EMAIL@EMAIL.COM', N'SENAHS', N'CARGO', N'12554888855', N'RAMA', N'11588855885', 2, 1)
INSERT [dbo].[ClientsContacts] ([ContactId], [ClientId], [ContactName], [Email], [Password], [Occupation], [Phone], [Ramal], [CellPhone], [ContactTypeId], [ClientsContactsEnabled]) VALUES (6, 3, N'NOME CONTATO NOVO', N'EMAIL2@TESTE.COM', N'e10adc3949ba59abbe56', N'CARGO NOVO', N'11485585585', N'RAMAL X', N'1199855633', 1, 1)
SET IDENTITY_INSERT [dbo].[ClientsContacts] OFF
SET IDENTITY_INSERT [dbo].[ClientType] ON 

INSERT [dbo].[ClientType] ([ClientTypeId], [ClientTypeDescription], [ClientTypeenabled], [Filter]) VALUES (1, N'OPERADORA DE SAÚDE', 1, NULL)
INSERT [dbo].[ClientType] ([ClientTypeId], [ClientTypeDescription], [ClientTypeenabled], [Filter]) VALUES (2, N'DISTRIBUIDORA', 1, NULL)
INSERT [dbo].[ClientType] ([ClientTypeId], [ClientTypeDescription], [ClientTypeenabled], [Filter]) VALUES (3, N'LABORATÓRIO', 1, NULL)
SET IDENTITY_INSERT [dbo].[ClientType] OFF
SET IDENTITY_INSERT [dbo].[ContactType] ON 

INSERT [dbo].[ContactType] ([ContactTypeId], [ContactDescription], [ContactTypeEnabled], [Filter]) VALUES (1, N'RH', 1, NULL)
INSERT [dbo].[ContactType] ([ContactTypeId], [ContactDescription], [ContactTypeEnabled], [Filter]) VALUES (2, N'FINANCEIRO', 1, NULL)
INSERT [dbo].[ContactType] ([ContactTypeId], [ContactDescription], [ContactTypeEnabled], [Filter]) VALUES (3, N'FATURAMENTO', 1, NULL)
SET IDENTITY_INSERT [dbo].[ContactType] OFF
SET IDENTITY_INSERT [dbo].[DispensationPeriod] ON 

INSERT [dbo].[DispensationPeriod] ([DispensationId], [Description], [QtyDays], [DispensationPeriodEnabled], [Filter]) VALUES (1, N'30 DIAS', 30, 1, NULL)
INSERT [dbo].[DispensationPeriod] ([DispensationId], [Description], [QtyDays], [DispensationPeriodEnabled], [Filter]) VALUES (2, N'60 DIAS', 60, 1, NULL)
INSERT [dbo].[DispensationPeriod] ([DispensationId], [Description], [QtyDays], [DispensationPeriodEnabled], [Filter]) VALUES (3, N'90 DIAS', 90, 1, NULL)
SET IDENTITY_INSERT [dbo].[DispensationPeriod] OFF
SET IDENTITY_INSERT [dbo].[DoctorAddresses] ON 

INSERT [dbo].[DoctorAddresses] ([DoctorAddressesId], [DoctorId], [AddressTypeId], [Address], [AddressNumber], [City], [Neighborhood], [StateId], [ZipCode], [Complement], [DoctorAddressesEnabled], [Filter]) VALUES (1, 1, 0, N'ASFSDFDS', N'ASFASF', N'ASFDSFDS', N'ASFSDF', 2, N'65465465', N'ASFSD', 1, N'1|ASFDSFDS|ASFSDF|ASFSDFDS')
INSERT [dbo].[DoctorAddresses] ([DoctorAddressesId], [DoctorId], [AddressTypeId], [Address], [AddressNumber], [City], [Neighborhood], [StateId], [ZipCode], [Complement], [DoctorAddressesEnabled], [Filter]) VALUES (2, 1, 0, N'ASFDSFDS', N'SAFDSFDS', N'SAFSD', N'SDFSD', 2, N'54445645', N'ASFSDF', 1, N'1|SAFSD|SDFSD|ASFDSFDS')
INSERT [dbo].[DoctorAddresses] ([DoctorAddressesId], [DoctorId], [AddressTypeId], [Address], [AddressNumber], [City], [Neighborhood], [StateId], [ZipCode], [Complement], [DoctorAddressesEnabled], [Filter]) VALUES (3, 1, 0, N'SAFSDF', N'SAFSD', N'SAFDSF', N'SFSD', 2, N'65465456', N'SFDDS', 1, N'1|SAFDSF|SFSD|SAFSDF')
INSERT [dbo].[DoctorAddresses] ([DoctorAddressesId], [DoctorId], [AddressTypeId], [Address], [AddressNumber], [City], [Neighborhood], [StateId], [ZipCode], [Complement], [DoctorAddressesEnabled], [Filter]) VALUES (4, 11, 0, N'LOG 1', N'NUM1 ', N'CIDADE 1', N'BAIRRO 1', 3, N'45645645', N'COP 1', 0, N'11|CIDADE 1|BAIRRO 1|LOG 1')
INSERT [dbo].[DoctorAddresses] ([DoctorAddressesId], [DoctorId], [AddressTypeId], [Address], [AddressNumber], [City], [Neighborhood], [StateId], [ZipCode], [Complement], [DoctorAddressesEnabled], [Filter]) VALUES (5, 14, 0, N'SEGUNDO ENDERE', N'788', N'CIDADE', N'BAIRRO', 4, N'00009999', N'', 0, N'14|CIDADE|BAIRRO|SEGUNDO ENDERE')
SET IDENTITY_INSERT [dbo].[DoctorAddresses] OFF
SET IDENTITY_INSERT [dbo].[Doctors] ON 

INSERT [dbo].[Doctors] ([DoctorID], [DoctorName], [CRM], [UF_CRM], [CPF], [Address], [Number], [Complement], [City], [Neighborhood], [StateID], [ZipCode], [PhoneComercial], [CellPhone], [FocalPoint], [email], [login], [password], [Comments], [Status], [RepresentativeID], [DoctorEnabled], [filter], [ramal]) VALUES (1019, N'MEDICO LAB 1 XTO', N'13456', N'24', N'12111111111', N'LOG MEDICO', N'12', N'', N'CIDADE', N'BAIRRO', 25, N'08888888', N'1255555522', NULL, N'FOCAL', N'EMAIL@MEDICO', N'MEDICO1', N'99a29dc8105fd2fa39d8cdc04733938d', N'SEM OBS XXX', N'1', 1008, 1, N'MEDICO LAB 1 XTO|12111111111', N'1234')
INSERT [dbo].[Doctors] ([DoctorID], [DoctorName], [CRM], [UF_CRM], [CPF], [Address], [Number], [Complement], [City], [Neighborhood], [StateID], [ZipCode], [PhoneComercial], [CellPhone], [FocalPoint], [email], [login], [password], [Comments], [Status], [RepresentativeID], [DoctorEnabled], [filter], [ramal]) VALUES (1020, N'MEDICO 2 LAB 1', N'111', N'5', N'55555555555', N'LOG', N'1', NULL, N'CIDA', N'BAIRRO', 15, N'44444444', N'1255555555', N'44444444444', N'FOCAL', N'EMAIL@MEDICO.COM', N'MEDICO2', N'99a29dc8105fd2fa39d8cdc04733938d', N'SFSDFD', N'1', 1008, 1, N'MEDICO 2 LAB 1|55555555555', N'')
INSERT [dbo].[Doctors] ([DoctorID], [DoctorName], [CRM], [UF_CRM], [CPF], [Address], [Number], [Complement], [City], [Neighborhood], [StateID], [ZipCode], [PhoneComercial], [CellPhone], [FocalPoint], [email], [login], [password], [Comments], [Status], [RepresentativeID], [DoctorEnabled], [filter], [ramal]) VALUES (1021, N'NOVO MEDICO DOMINGO', N'1234', N'3', N'12345645645', N'LOG', N'45', N'AFDSAFSD', N'CIDADE', N'BAIRRO', 24, N'09755555', N'4564564564', N'45645645646', N'FOCAL', N'EMAIL@MEDICO.OM', N'MEDICO3', N'99a29dc8105fd2fa39d8cdc04733938d', NULL, N'1', 1010, 1, N'NOVO MEDICO DOMINGO|12345645645', N'12')
INSERT [dbo].[Doctors] ([DoctorID], [DoctorName], [CRM], [UF_CRM], [CPF], [Address], [Number], [Complement], [City], [Neighborhood], [StateID], [ZipCode], [PhoneComercial], [CellPhone], [FocalPoint], [email], [login], [password], [Comments], [Status], [RepresentativeID], [DoctorEnabled], [filter], [ramal]) VALUES (1022, N'MEDICO DO REP 2 LABORATORIO 2', N'123', N'25', N'23545645645', N'LOG', N'12', NULL, N'CIDADE', N'BAIRRO', 25, N'09855522', N'1245456456', NULL, N'FOCAL', N'EMAIL@EMAIL.COM', N'MEDREP2', N'TESTE', NULL, N'1', 1014, 1, N'MEDICO DO REP 2 LABORATORIO 2|23545645645', N'')
SET IDENTITY_INSERT [dbo].[Doctors] OFF
SET IDENTITY_INSERT [dbo].[Laboratories] ON 

INSERT [dbo].[Laboratories] ([LaboratoryID], [LaboratoryName], [LaboratoryFantasyName], [CNPJ], [Address], [Number], [Neighborhood], [City], [StateID], [Complement], [ZipCode], [PABX], [Responsible], [DeptResponsible], [Email], [ComercialPhone], [CellPhone], [Login], [Password], [Status], [Comments], [LaboratoryEnabled], [filter]) VALUES (1012, N'LABORATORIO 1', N'FANTASIA 1', N'11111111111111', N'LOG 1', N'1', N'BAIRRO', N'CIDADE 1', 24, N'', N'11111111', N'4511111111', N'RESP', N'SFDS', N'EMAILLAB@TESTE.COM', N'11548555555', NULL, N'LAB1', N'99a29dc8105fd2fa39d8cdc04733938d', 1, N'OBSE', 1, N'LABORATORIO 1|FANTASIA 1|11111111111111')
INSERT [dbo].[Laboratories] ([LaboratoryID], [LaboratoryName], [LaboratoryFantasyName], [CNPJ], [Address], [Number], [Neighborhood], [City], [StateID], [Complement], [ZipCode], [PABX], [Responsible], [DeptResponsible], [Email], [ComercialPhone], [CellPhone], [Login], [Password], [Status], [Comments], [LaboratoryEnabled], [filter]) VALUES (1013, N'LABORATORIO 2', N'FANTASIA 2', N'22222222222222', N'LOG 2', N'2', N'BAIRRO 2', N'CIDADE 2', 26, N'', N'08855566', N'4585555555', N'RESP 2', N'FSD', N'EMAILLAB2@TESTE.COM', N'12555555555', NULL, N'LAB2', N'99a29dc8105fd2fa39d8cdc04733938d', 1, N'', 1, N'LABORATORIO 2|FANTASIA 2|22222222222222')
INSERT [dbo].[Laboratories] ([LaboratoryID], [LaboratoryName], [LaboratoryFantasyName], [CNPJ], [Address], [Number], [Neighborhood], [City], [StateID], [Complement], [ZipCode], [PABX], [Responsible], [DeptResponsible], [Email], [ComercialPhone], [CellPhone], [Login], [Password], [Status], [Comments], [LaboratoryEnabled], [filter]) VALUES (1014, N'LAB3 XXXX', N'LAB 3', N'45687911232255', N'LOG 3', N'45', N'BAIRRO', N'CIDADE', 26, N'', N'09855522', N'1125523335', N'REPONSAVE', N'DEPTO', N'EMAIL@RESP.COM', N'11588555552', NULL, N'LAB33', N'553648c8088b83c091b0be46f9ee7498', 1, N'', 1, N'LAB3 XXXX|LAB 3|45687911232255')
SET IDENTITY_INSERT [dbo].[Laboratories] OFF
SET IDENTITY_INSERT [dbo].[Medicines] ON 

INSERT [dbo].[Medicines] ([MedicineId], [EAN], [MedicineName], [LabId], [LabName], [ActivePrincipleId], [ActivePrincipleName], [Presentation], [UnitId], [UnitName], [Dosage], [QtyTotal], [MedicineType], [Comments], [MedicineEnabled], [Filter], [CodeTOTVS], [SystemId]) VALUES (3, N'7894561231234', N'MEDICAMENTO 1', N'1', N'LABORATORIO 1', N'1', N'PRINCIPIO ATIVO 1', N'150 DRÁGEAS', N'1', N'UNIDADE', 500, 50, N'GENÉRICO', NULL, 1, N'7894561231234|MEDICAMENTO 1|PRINCIPIO ATIVO 1', NULL, NULL)
INSERT [dbo].[Medicines] ([MedicineId], [EAN], [MedicineName], [LabId], [LabName], [ActivePrincipleId], [ActivePrincipleName], [Presentation], [UnitId], [UnitName], [Dosage], [QtyTotal], [MedicineType], [Comments], [MedicineEnabled], [Filter], [CodeTOTVS], [SystemId]) VALUES (4, N'7894561231235', N'MEDICAMENTO 2', N'1', N'LABORATORIO 3', N'1', N'PRINCIPIO ATIVO XPTO', N'200 COMPRIMIDOS', N'1', N'UNIDADE', 100, 30, N'REFERÊNCIA', NULL, 1, N'7894561231235|MEDICAMENTO 2|PRINCIPIO ATIVO XPTO', NULL, NULL)
INSERT [dbo].[Medicines] ([MedicineId], [EAN], [MedicineName], [LabId], [LabName], [ActivePrincipleId], [ActivePrincipleName], [Presentation], [UnitId], [UnitName], [Dosage], [QtyTotal], [MedicineType], [Comments], [MedicineEnabled], [Filter], [CodeTOTVS], [SystemId]) VALUES (5, N'7894561231238', N'OUTRO MEDICAMENTO', N'1', N'NOVO LABORATORIO', N'1', N'OUTRO PRINCIPIO', N'7 COMPRIMIDOS', N'1', N'UNIDADE', 100, 7, N'SIMILAR', NULL, 1, N'7894561231238|OUTRO MEDICAMENTO|OUTRO PRINCIPIO', NULL, NULL)
INSERT [dbo].[Medicines] ([MedicineId], [EAN], [MedicineName], [LabId], [LabName], [ActivePrincipleId], [ActivePrincipleName], [Presentation], [UnitId], [UnitName], [Dosage], [QtyTotal], [MedicineType], [Comments], [MedicineEnabled], [Filter], [CodeTOTVS], [SystemId]) VALUES (6, N'7894561234444', N'NEOSALDINA', N'1', N'EMS', N'1', N'DIPIRONA', N'7 COMPRIMIDOS', N'1', N'UNIDADE', 100, 7, N'SIMILAR', NULL, 1, N'7894561234444|NEOSALDINA|DIPIRONA', NULL, NULL)
INSERT [dbo].[Medicines] ([MedicineId], [EAN], [MedicineName], [LabId], [LabName], [ActivePrincipleId], [ActivePrincipleName], [Presentation], [UnitId], [UnitName], [Dosage], [QtyTotal], [MedicineType], [Comments], [MedicineEnabled], [Filter], [CodeTOTVS], [SystemId]) VALUES (7, N'78910111213', N'NOME MEDICAMENTO TESTE', N'1', N'EMS', N'1', N'DIPIRONA SODICA', N'Caixa com 20 comprimidos', N'1', N'UN', 0, 30, N'GENERICO', N'', 1, N'78910111213|NOME MEDICAMENTO TESTE', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Medicines] OFF
SET IDENTITY_INSERT [dbo].[OrderAvaliable] ON 

INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (1, 12, CAST(N'2019-04-22 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 7, CAST(N'2019-04-25 03:00:00.000' AS DateTime), 1, 1, NULL)
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (2, 12, CAST(N'2019-04-22 03:00:00.000' AS DateTime), CAST(N'2019-04-30 03:00:00.000' AS DateTime), 7, CAST(N'2019-04-22 03:00:00.000' AS DateTime), 1, 1, NULL)
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (3, 15, CAST(N'2019-04-22 03:00:00.000' AS DateTime), CAST(N'2019-04-30 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 1, 1, NULL)
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (4, 12, CAST(N'2019-04-22 03:00:00.000' AS DateTime), CAST(N'2019-04-30 03:00:00.000' AS DateTime), 7, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 1, 1, NULL)
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (5, 1016, CAST(N'2019-04-22 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 14, CAST(N'2019-04-22 03:00:00.000' AS DateTime), 1, 1, NULL)
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (6, 1019, CAST(N'2019-04-23 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 60, CAST(N'2019-04-23 03:00:00.000' AS DateTime), 1, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (7, 1020, CAST(N'2019-04-23 03:00:00.000' AS DateTime), CAST(N'2019-08-31 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-24 03:00:00.000' AS DateTime), 1, 1, NULL)
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (8, 1020, CAST(N'2019-04-29 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-29 03:00:00.000' AS DateTime), 1, 1, NULL)
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (9, 1021, CAST(N'2019-04-29 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-29 03:00:00.000' AS DateTime), 1, 1, NULL)
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (10, 1021, CAST(N'2019-04-29 03:00:00.000' AS DateTime), CAST(N'2019-07-30 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (11, 1021, CAST(N'2019-04-29 03:00:00.000' AS DateTime), CAST(N'2019-05-15 03:00:00.000' AS DateTime), 7, CAST(N'2019-04-29 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (12, 1020, CAST(N'2019-04-29 03:00:00.000' AS DateTime), CAST(N'2019-05-29 03:00:00.000' AS DateTime), 1, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (13, 1021, CAST(N'2019-04-29 03:00:00.000' AS DateTime), CAST(N'2019-08-28 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 1012, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (14, 1021, CAST(N'2019-04-29 03:00:00.000' AS DateTime), CAST(N'2019-10-30 03:00:00.000' AS DateTime), 14, CAST(N'2019-04-29 03:00:00.000' AS DateTime), 1012, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (15, 1021, CAST(N'2019-04-29 03:00:00.000' AS DateTime), CAST(N'2019-05-31 03:00:00.000' AS DateTime), 7, CAST(N'2019-04-29 03:00:00.000' AS DateTime), 1012, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (16, 1020, CAST(N'2019-04-30 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (17, 1020, CAST(N'2019-04-30 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (18, 1020, CAST(N'2019-04-30 03:00:00.000' AS DateTime), CAST(N'2019-05-31 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (19, 1020, CAST(N'2019-04-30 03:00:00.000' AS DateTime), CAST(N'2019-06-30 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (20, 1020, CAST(N'2019-04-30 03:00:00.000' AS DateTime), CAST(N'2019-06-30 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (21, 1020, CAST(N'2019-04-30 03:00:00.000' AS DateTime), CAST(N'2019-06-30 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (22, 1019, CAST(N'2019-04-30 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (23, 1019, CAST(N'2019-04-30 03:00:00.000' AS DateTime), CAST(N'2019-06-30 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (24, 1019, CAST(N'2019-04-30 03:00:00.000' AS DateTime), CAST(N'2019-06-30 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (25, 1020, CAST(N'2019-04-30 03:00:00.000' AS DateTime), CAST(N'2019-08-31 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (26, 1019, CAST(N'2019-04-30 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (27, 1019, CAST(N'2019-04-30 03:00:00.000' AS DateTime), CAST(N'2019-08-31 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (28, 1021, CAST(N'2019-04-30 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (29, 1019, CAST(N'2019-04-30 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 30, CAST(N'2019-04-30 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (30, 1021, CAST(N'2019-05-01 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 7, CAST(N'2019-05-01 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (31, 1020, CAST(N'2019-05-08 03:00:00.000' AS DateTime), CAST(N'2019-09-30 03:00:00.000' AS DateTime), 14, CAST(N'2019-05-08 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (32, 1020, CAST(N'2019-05-08 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 14, CAST(N'2019-05-08 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (33, 1020, CAST(N'2019-05-08 03:00:00.000' AS DateTime), CAST(N'2019-08-31 03:00:00.000' AS DateTime), 14, CAST(N'2019-05-08 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (34, 1020, CAST(N'2019-05-08 03:00:00.000' AS DateTime), CAST(N'2019-09-25 03:00:00.000' AS DateTime), 14, CAST(N'2019-05-08 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (35, 1019, CAST(N'2019-05-08 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 14, CAST(N'2019-05-08 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (36, 1022, CAST(N'2019-05-08 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 30, CAST(N'2019-05-08 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliable] ([OrderAvaliableID], [DoctorId], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableEnabled], [status]) VALUES (37, 1020, CAST(N'2019-05-27 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 14, CAST(N'2019-05-27 03:00:00.000' AS DateTime), 0, 1, N'A')
SET IDENTITY_INSERT [dbo].[OrderAvaliable] OFF
SET IDENTITY_INSERT [dbo].[OrderAvaliableRepresentative] ON 

INSERT [dbo].[OrderAvaliableRepresentative] ([OrderAvaliableRepresentativeID], [RepresentativeID], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableRepresentativeEnabled], [status]) VALUES (1, 1009, CAST(N'2019-05-01 03:00:00.000' AS DateTime), CAST(N'2019-09-30 03:00:00.000' AS DateTime), 30, CAST(N'2019-05-01 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliableRepresentative] ([OrderAvaliableRepresentativeID], [RepresentativeID], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableRepresentativeEnabled], [status]) VALUES (2, 1010, CAST(N'2019-05-01 03:00:00.000' AS DateTime), CAST(N'2019-08-31 03:00:00.000' AS DateTime), 30, CAST(N'2019-05-01 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliableRepresentative] ([OrderAvaliableRepresentativeID], [RepresentativeID], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableRepresentativeEnabled], [status]) VALUES (3, 1009, CAST(N'2019-05-01 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 30, CAST(N'2019-05-01 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliableRepresentative] ([OrderAvaliableRepresentativeID], [RepresentativeID], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableRepresentativeEnabled], [status]) VALUES (4, 1012, CAST(N'2019-05-02 03:00:00.000' AS DateTime), CAST(N'2019-06-01 03:00:00.000' AS DateTime), 7, CAST(N'2019-05-02 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliableRepresentative] ([OrderAvaliableRepresentativeID], [RepresentativeID], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableRepresentativeEnabled], [status]) VALUES (5, 1012, CAST(N'2019-05-02 03:00:00.000' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 7, CAST(N'2019-05-02 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliableRepresentative] ([OrderAvaliableRepresentativeID], [RepresentativeID], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableRepresentativeEnabled], [status]) VALUES (6, 1012, CAST(N'2019-05-02 03:00:00.000' AS DateTime), CAST(N'2019-05-31 03:00:00.000' AS DateTime), 7, CAST(N'2019-05-02 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliableRepresentative] ([OrderAvaliableRepresentativeID], [RepresentativeID], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableRepresentativeEnabled], [status]) VALUES (7, 1012, CAST(N'2019-05-02 03:00:00.000' AS DateTime), CAST(N'2019-05-31 03:00:00.000' AS DateTime), 7, CAST(N'2019-05-02 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliableRepresentative] ([OrderAvaliableRepresentativeID], [RepresentativeID], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableRepresentativeEnabled], [status]) VALUES (8, 1011, CAST(N'2019-05-02 03:00:00.000' AS DateTime), CAST(N'2019-05-31 03:00:00.000' AS DateTime), 7, CAST(N'2019-05-02 03:00:00.000' AS DateTime), 0, 1, N'A')
INSERT [dbo].[OrderAvaliableRepresentative] ([OrderAvaliableRepresentativeID], [RepresentativeID], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableRepresentativeEnabled], [status]) VALUES (9, 1008, CAST(N'2019-05-02 03:00:00.000' AS DateTime), CAST(N'2019-05-30 03:00:00.000' AS DateTime), 7, CAST(N'2019-05-02 03:00:00.000' AS DateTime), 1012, 1, N'A')
INSERT [dbo].[OrderAvaliableRepresentative] ([OrderAvaliableRepresentativeID], [RepresentativeID], [startValidity], [endValidity], [frequence], [dateToSend], [laboratoryID], [OrderAvaliableRepresentativeEnabled], [status]) VALUES (10, 1009, CAST(N'2019-05-02 03:00:00.000' AS DateTime), CAST(N'2019-08-29 03:00:00.000' AS DateTime), 30, CAST(N'2019-05-02 03:00:00.000' AS DateTime), 0, 1, N'A')
SET IDENTITY_INSERT [dbo].[OrderAvaliableRepresentative] OFF
SET IDENTITY_INSERT [dbo].[OrderItem] ON 

INSERT [dbo].[OrderItem] ([OrderItemId], [OrderId], [ReleaseOrderItemId], [ReleaseOrderId], [MedicineId], [Dosage], [Quantity], [OrderItemEnabled]) VALUES (1, 1, 4, 4, 5, 10, 15, 1)
SET IDENTITY_INSERT [dbo].[OrderItem] OFF
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([OrderId], [ReleaseOrderId], [OrderDate], [PatientId], [PatientAddressId], [PatientAuthorization], [OrderStatus], [OrderObservation], [OrdersEnabled]) VALUES (1, 4, CAST(N'2019-05-13 04:46:19.497' AS DateTime), 12, 12, N'158555', NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Orders] OFF
SET IDENTITY_INSERT [dbo].[OrdersDoctor] ON 

INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1056, 1020, N'A', CAST(N'2019-05-08 04:21:44.267' AS DateTime), CAST(N'2019-05-08 03:00:00.000' AS DateTime), 1, 32)
INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1057, 1020, N'A', CAST(N'2019-05-08 04:22:45.110' AS DateTime), CAST(N'2019-05-08 03:00:00.000' AS DateTime), 1, 33)
INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1058, 1020, N'A', CAST(N'2019-05-08 04:24:28.910' AS DateTime), CAST(N'2019-05-08 03:00:00.000' AS DateTime), 1, 34)
INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1059, 1019, N'A', CAST(N'2019-05-08 04:29:13.483' AS DateTime), CAST(N'2019-05-08 03:00:00.000' AS DateTime), 1, 35)
INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1060, 1019, N'A', CAST(N'2019-05-08 04:29:18.413' AS DateTime), CAST(N'2019-05-22 03:00:00.000' AS DateTime), 1, 35)
INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1061, 1019, N'A', CAST(N'2019-05-08 04:29:18.460' AS DateTime), CAST(N'2019-06-05 03:00:00.000' AS DateTime), 1, 35)
INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1062, 1019, N'A', CAST(N'2019-05-08 04:29:18.503' AS DateTime), CAST(N'2019-06-19 03:00:00.000' AS DateTime), 1, 35)
INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1063, 1019, N'A', CAST(N'2019-05-08 04:29:18.547' AS DateTime), CAST(N'2019-07-03 03:00:00.000' AS DateTime), 1, 35)
INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1064, 1019, N'A', CAST(N'2019-05-08 04:29:18.587' AS DateTime), CAST(N'2019-07-17 03:00:00.000' AS DateTime), 1, 35)
INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1065, 1019, N'A', CAST(N'2019-05-08 04:29:18.633' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 1, 35)
INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1066, 1022, N'A', CAST(N'2019-05-08 04:35:35.163' AS DateTime), CAST(N'2019-05-08 03:00:00.000' AS DateTime), 1, 36)
INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1067, 1022, N'A', CAST(N'2019-05-08 04:35:37.107' AS DateTime), CAST(N'2019-06-07 03:00:00.000' AS DateTime), 1, 36)
INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1068, 1022, N'A', CAST(N'2019-05-08 04:35:37.137' AS DateTime), CAST(N'2019-07-07 03:00:00.000' AS DateTime), 1, 36)
INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1069, 1020, N'A', CAST(N'2019-05-27 19:57:30.257' AS DateTime), CAST(N'2019-05-27 03:00:00.000' AS DateTime), 1, 37)
INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1070, 1020, N'A', CAST(N'2019-05-27 19:58:00.360' AS DateTime), CAST(N'2019-06-10 03:00:00.000' AS DateTime), 1, 37)
INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1071, 1020, N'A', CAST(N'2019-05-27 19:58:00.417' AS DateTime), CAST(N'2019-06-24 03:00:00.000' AS DateTime), 1, 37)
INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1072, 1020, N'A', CAST(N'2019-05-27 19:58:00.463' AS DateTime), CAST(N'2019-07-08 03:00:00.000' AS DateTime), 1, 37)
INSERT [dbo].[OrdersDoctor] ([OrderDoctorID], [DoctorID], [Status], [CreateDate], [OrderDoctorDateSend], [OrderDoctorEnabled], [OrderAvaliableID]) VALUES (1073, 1020, N'A', CAST(N'2019-05-27 19:58:00.500' AS DateTime), CAST(N'2019-07-22 03:00:00.000' AS DateTime), 1, 37)
SET IDENTITY_INSERT [dbo].[OrdersDoctor] OFF
SET IDENTITY_INSERT [dbo].[OrdersRepresentative] ON 

INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (1, 1012, N'A', CAST(N'2019-05-02 00:26:08.150' AS DateTime), CAST(N'2019-05-02 03:00:00.000' AS DateTime), 1, 5)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (2, 1012, N'A', CAST(N'2019-05-02 00:40:39.777' AS DateTime), CAST(N'2019-05-02 03:00:00.000' AS DateTime), 1, 6)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (3, 1012, N'A', CAST(N'2019-05-02 00:42:44.133' AS DateTime), CAST(N'2019-05-02 03:00:00.000' AS DateTime), 1, 7)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (4, 1012, N'A', CAST(N'2019-05-02 00:43:47.077' AS DateTime), CAST(N'2019-05-09 03:00:00.000' AS DateTime), 1, 7)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (5, 1012, N'A', CAST(N'2019-05-02 00:43:47.207' AS DateTime), CAST(N'2019-05-16 03:00:00.000' AS DateTime), 1, 7)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (6, 1012, N'A', CAST(N'2019-05-02 00:43:47.273' AS DateTime), CAST(N'2019-05-23 03:00:00.000' AS DateTime), 1, 7)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (7, 1012, N'A', CAST(N'2019-05-02 00:43:47.340' AS DateTime), CAST(N'2019-05-30 03:00:00.000' AS DateTime), 1, 7)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (8, 1011, N'A', CAST(N'2019-05-02 00:44:35.013' AS DateTime), CAST(N'2019-05-02 03:00:00.000' AS DateTime), 1, 8)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (9, 1011, N'A', CAST(N'2019-05-02 00:44:35.147' AS DateTime), CAST(N'2019-05-09 03:00:00.000' AS DateTime), 1, 8)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (10, 1011, N'A', CAST(N'2019-05-02 00:44:35.327' AS DateTime), CAST(N'2019-05-16 03:00:00.000' AS DateTime), 1, 8)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (11, 1011, N'A', CAST(N'2019-05-02 00:44:35.457' AS DateTime), CAST(N'2019-05-23 03:00:00.000' AS DateTime), 1, 8)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (12, 1011, N'A', CAST(N'2019-05-02 00:44:35.573' AS DateTime), CAST(N'2019-05-30 03:00:00.000' AS DateTime), 1, 8)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (13, 1008, N'A', CAST(N'2019-05-02 01:45:06.857' AS DateTime), CAST(N'2019-05-02 03:00:00.000' AS DateTime), 1, 9)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (14, 1008, N'A', CAST(N'2019-05-02 01:45:06.940' AS DateTime), CAST(N'2019-05-09 03:00:00.000' AS DateTime), 1, 9)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (15, 1008, N'A', CAST(N'2019-05-02 01:45:07.017' AS DateTime), CAST(N'2019-05-16 03:00:00.000' AS DateTime), 1, 9)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (16, 1008, N'A', CAST(N'2019-05-02 01:45:07.090' AS DateTime), CAST(N'2019-05-23 03:00:00.000' AS DateTime), 1, 9)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (17, 1008, N'A', CAST(N'2019-05-02 01:45:07.167' AS DateTime), CAST(N'2019-05-30 03:00:00.000' AS DateTime), 1, 9)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (18, 1009, N'A', CAST(N'2019-05-02 01:58:24.733' AS DateTime), CAST(N'2019-05-02 03:00:00.000' AS DateTime), 1, 10)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (19, 1009, N'A', CAST(N'2019-05-02 01:58:24.823' AS DateTime), CAST(N'2019-06-01 03:00:00.000' AS DateTime), 1, 10)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (20, 1009, N'A', CAST(N'2019-05-02 01:58:24.900' AS DateTime), CAST(N'2019-07-01 03:00:00.000' AS DateTime), 1, 10)
INSERT [dbo].[OrdersRepresentative] ([OrdersRepresentativeID], [RepresentativeID], [Status], [CreateDate], [OrderRepresentativeDateSend], [OrderRepresentativeEnabled], [OrderRepresentativeAvaliableID]) VALUES (21, 1009, N'A', CAST(N'2019-05-02 01:58:24.977' AS DateTime), CAST(N'2019-07-31 03:00:00.000' AS DateTime), 1, 10)
SET IDENTITY_INSERT [dbo].[OrdersRepresentative] OFF
SET IDENTITY_INSERT [dbo].[Pathologies] ON 

INSERT [dbo].[Pathologies] ([pathologyId], [pathologyDescription], [PathologiesEnabled], [Filter]) VALUES (1, N'Diabetes', 1, NULL)
INSERT [dbo].[Pathologies] ([pathologyId], [pathologyDescription], [PathologiesEnabled], [Filter]) VALUES (2, N'Cancer', 1, NULL)
INSERT [dbo].[Pathologies] ([pathologyId], [pathologyDescription], [PathologiesEnabled], [Filter]) VALUES (4, N'Pressão alta', 1, NULL)
INSERT [dbo].[Pathologies] ([pathologyId], [pathologyDescription], [PathologiesEnabled], [Filter]) VALUES (6, N'Asma', 1, NULL)
SET IDENTITY_INSERT [dbo].[Pathologies] OFF
SET IDENTITY_INSERT [dbo].[PatientAddresses] ON 

INSERT [dbo].[PatientAddresses] ([PatientAddressesId], [PatientId], [AddressTypeId], [Address], [AddressNumber], [City], [Neighborhood], [StateId], [ZipCode], [Complement], [PatientAddressesEnabled], [Filter]) VALUES (1, 3, 1, N'ENDERECO', N'123', N'CIDADED', N'BAIRRO', 3, N'09785555', N'COMPLEMENTO', 0, N'3|CIDADED|BAIRRO|ENDERECO')
INSERT [dbo].[PatientAddresses] ([PatientAddressesId], [PatientId], [AddressTypeId], [Address], [AddressNumber], [City], [Neighborhood], [StateId], [ZipCode], [Complement], [PatientAddressesEnabled], [Filter]) VALUES (2, 4, 1, N'RUA CONTINENTAL', N'910', N'SÃO BERNARDO DO CAMPO', N'SAFDSF', 2, N'09726410', N'', 1, N'4|SÃO BERNARDO DO CAMPO|SAFDSF|RUA CONTINENTAL')
INSERT [dbo].[PatientAddresses] ([PatientAddressesId], [PatientId], [AddressTypeId], [Address], [AddressNumber], [City], [Neighborhood], [StateId], [ZipCode], [Complement], [PatientAddressesEnabled], [Filter]) VALUES (3, 5, 1, N'RUA CONTINENTAL', N'123', N'SÃO BERNARDO DO CAMPO', N'BAIRRO', 1, N'09726410', N'SAFDSFSDAFSD', 1, N'5|SÃO BERNARDO DO CAMPO|BAIRRO|RUA CONTINENTAL')
INSERT [dbo].[PatientAddresses] ([PatientAddressesId], [PatientId], [AddressTypeId], [Address], [AddressNumber], [City], [Neighborhood], [StateId], [ZipCode], [Complement], [PatientAddressesEnabled], [Filter]) VALUES (4, 8, 1, N'SFDSFDSFDS', N'SDFDSFDS', N'SDFDSAFDS', N'SAFDSAFDSADS', 2, N'77888888', N'ASFSD', 1, N'8|SDFDSAFDS|SAFDSAFDSADS|SFDSFDSFDS')
INSERT [dbo].[PatientAddresses] ([PatientAddressesId], [PatientId], [AddressTypeId], [Address], [AddressNumber], [City], [Neighborhood], [StateId], [ZipCode], [Complement], [PatientAddressesEnabled], [Filter]) VALUES (5, 9, 1, N'SFDSFDSFDS', N'SDFDSFDS', N'SDFDSAFDS', N'SAFDSAFDSADS', 2, N'77888888', N'ASFSD', 1, N'9|SDFDSAFDS|SAFDSAFDSADS|SFDSFDSFDS')
INSERT [dbo].[PatientAddresses] ([PatientAddressesId], [PatientId], [AddressTypeId], [Address], [AddressNumber], [City], [Neighborhood], [StateId], [ZipCode], [Complement], [PatientAddressesEnabled], [Filter]) VALUES (6, 10, 1, N'SFDSFDSFDS', N'SDFDSFDS', N'SDFDSAFDS', N'SAFDSAFDSADS', 2, N'77888888', N'ASFSD', 1, N'10|SDFDSAFDS|SAFDSAFDSADS|SFDSFDSFDS')
INSERT [dbo].[PatientAddresses] ([PatientAddressesId], [PatientId], [AddressTypeId], [Address], [AddressNumber], [City], [Neighborhood], [StateId], [ZipCode], [Complement], [PatientAddressesEnabled], [Filter]) VALUES (7, 8, 3, N'NOVO ENDERECO', N'NUMERO', N'CIDADE', N'BAIRRO', 5, N'78896555', N'COMPLEMENTO', 1, N'8|CIDADE|BAIRRO|NOVO ENDERECO')
INSERT [dbo].[PatientAddresses] ([PatientAddressesId], [PatientId], [AddressTypeId], [Address], [AddressNumber], [City], [Neighborhood], [StateId], [ZipCode], [Complement], [PatientAddressesEnabled], [Filter]) VALUES (8, 7, 1, N'SFDSFDSFDS', N'SDFDSFDS', N'SDFDSAFDS', N'SAFDSAFDSADS', 2, N'77888888', N'ASFSD', 1, N'7|SDFDSAFDS|SAFDSAFDSADS|SFDSFDSFDS')
INSERT [dbo].[PatientAddresses] ([PatientAddressesId], [PatientId], [AddressTypeId], [Address], [AddressNumber], [City], [Neighborhood], [StateId], [ZipCode], [Complement], [PatientAddressesEnabled], [Filter]) VALUES (9, 10, 3, N'NOVO ENDERECO', N'NUMERO', N'CIDADE', N'BAIRRO', 5, N'78896555', N'COMPLEMENTO', 1, N'10|CIDADE|BAIRRO|NOVO ENDERECO')
INSERT [dbo].[PatientAddresses] ([PatientAddressesId], [PatientId], [AddressTypeId], [Address], [AddressNumber], [City], [Neighborhood], [StateId], [ZipCode], [Complement], [PatientAddressesEnabled], [Filter]) VALUES (10, 9, 3, N'NOVO ENDERECO', N'NUMERO', N'CIDADE', N'BAIRRO', 5, N'78896555', N'COMPLEMENTO', 1, N'9|CIDADE|BAIRRO|NOVO ENDERECO')
INSERT [dbo].[PatientAddresses] ([PatientAddressesId], [PatientId], [AddressTypeId], [Address], [AddressNumber], [City], [Neighborhood], [StateId], [ZipCode], [Complement], [PatientAddressesEnabled], [Filter]) VALUES (11, 7, 3, N'NOVO ENDERECO', N'NUMERO', N'CIDADE', N'BAIRRO', 5, N'78896555', N'COMPLEMENTO', 1, N'7|CIDADE|BAIRRO|NOVO ENDERECO')
INSERT [dbo].[PatientAddresses] ([PatientAddressesId], [PatientId], [AddressTypeId], [Address], [AddressNumber], [City], [Neighborhood], [StateId], [ZipCode], [Complement], [PatientAddressesEnabled], [Filter]) VALUES (12, 12, 0, N'ENDRECO 2', N'NUMERO 2', N'CIDADE', N'BAIRRO 234', 3, N'55856666', N'COMPLEMENTO 222', 1, N'12|CIDADE|BAIRRO 234|ENDRECO 2')
SET IDENTITY_INSERT [dbo].[PatientAddresses] OFF
SET IDENTITY_INSERT [dbo].[PatientPathologies] ON 

INSERT [dbo].[PatientPathologies] ([PatientPathologiesId], [PatientId], [pathologyId], [PatientPathologiesEnabled], [Filter]) VALUES (1, 5, 6, 1, N'5')
INSERT [dbo].[PatientPathologies] ([PatientPathologiesId], [PatientId], [pathologyId], [PatientPathologiesEnabled], [Filter]) VALUES (2, 6, 2, 1, N'6')
INSERT [dbo].[PatientPathologies] ([PatientPathologiesId], [PatientId], [pathologyId], [PatientPathologiesEnabled], [Filter]) VALUES (3, 8, 4, 1, N'8')
INSERT [dbo].[PatientPathologies] ([PatientPathologiesId], [PatientId], [pathologyId], [PatientPathologiesEnabled], [Filter]) VALUES (4, 9, 4, 1, N'9')
INSERT [dbo].[PatientPathologies] ([PatientPathologiesId], [PatientId], [pathologyId], [PatientPathologiesEnabled], [Filter]) VALUES (5, 10, 4, 1, N'10')
INSERT [dbo].[PatientPathologies] ([PatientPathologiesId], [PatientId], [pathologyId], [PatientPathologiesEnabled], [Filter]) VALUES (6, 7, 4, 1, N'7')
INSERT [dbo].[PatientPathologies] ([PatientPathologiesId], [PatientId], [pathologyId], [PatientPathologiesEnabled], [Filter]) VALUES (7, 11, 1, 1, N'11')
INSERT [dbo].[PatientPathologies] ([PatientPathologiesId], [PatientId], [pathologyId], [PatientPathologiesEnabled], [Filter]) VALUES (8, 11, 4, 1, N'11')
INSERT [dbo].[PatientPathologies] ([PatientPathologiesId], [PatientId], [pathologyId], [PatientPathologiesEnabled], [Filter]) VALUES (9, 11, 6, 1, N'11')
INSERT [dbo].[PatientPathologies] ([PatientPathologiesId], [PatientId], [pathologyId], [PatientPathologiesEnabled], [Filter]) VALUES (10, 12, 1, 1, N'12')
SET IDENTITY_INSERT [dbo].[PatientPathologies] OFF
SET IDENTITY_INSERT [dbo].[PatientPhones] ON 

INSERT [dbo].[PatientPhones] ([PatientPhoneId], [PatientId], [PhoneNumber], [Ramal], [PhoneTypeId], [PatientPhonesEnabled], [Filter]) VALUES (1, 1, N'77777885578', N'RAMAL', 1, 0, N'1|77777885578')
INSERT [dbo].[PatientPhones] ([PatientPhoneId], [PatientId], [PhoneNumber], [Ramal], [PhoneTypeId], [PatientPhonesEnabled], [Filter]) VALUES (2, 1, N'99899995959', N'FDSFSD', 5, 0, N'1|99899995959')
INSERT [dbo].[PatientPhones] ([PatientPhoneId], [PatientId], [PhoneNumber], [Ramal], [PhoneTypeId], [PatientPhonesEnabled], [Filter]) VALUES (3, 2, N'44444444444', N'RAMAL', 1, 0, N'2|44444444444')
INSERT [dbo].[PatientPhones] ([PatientPhoneId], [PatientId], [PhoneNumber], [Ramal], [PhoneTypeId], [PatientPhonesEnabled], [Filter]) VALUES (4, 3, N'78554554545', N'', 5, 0, N'3|78554554545')
INSERT [dbo].[PatientPhones] ([PatientPhoneId], [PatientId], [PhoneNumber], [Ramal], [PhoneTypeId], [PatientPhonesEnabled], [Filter]) VALUES (5, 4, N'48855588555', N'', 5, 1, N'4|48855588555')
INSERT [dbo].[PatientPhones] ([PatientPhoneId], [PatientId], [PhoneNumber], [Ramal], [PhoneTypeId], [PatientPhonesEnabled], [Filter]) VALUES (6, 5, N'78554222555', N'', 5, 1, N'5|78554222555')
INSERT [dbo].[PatientPhones] ([PatientPhoneId], [PatientId], [PhoneNumber], [Ramal], [PhoneTypeId], [PatientPhonesEnabled], [Filter]) VALUES (7, 6, N'11947323029', N'', 2, 1, N'6|11947323029')
INSERT [dbo].[PatientPhones] ([PatientPhoneId], [PatientId], [PhoneNumber], [Ramal], [PhoneTypeId], [PatientPhonesEnabled], [Filter]) VALUES (8, 10, N'64654564564', N'SAFDSF', 2, 1, N'10|64654564564')
INSERT [dbo].[PatientPhones] ([PatientPhoneId], [PatientId], [PhoneNumber], [Ramal], [PhoneTypeId], [PatientPhonesEnabled], [Filter]) VALUES (9, 8, N'64654564564', N'SAFDSF', 2, 1, N'8|64654564564')
INSERT [dbo].[PatientPhones] ([PatientPhoneId], [PatientId], [PhoneNumber], [Ramal], [PhoneTypeId], [PatientPhonesEnabled], [Filter]) VALUES (10, 9, N'64654564564', N'SAFDSF', 2, 1, N'9|64654564564')
INSERT [dbo].[PatientPhones] ([PatientPhoneId], [PatientId], [PhoneNumber], [Ramal], [PhoneTypeId], [PatientPhonesEnabled], [Filter]) VALUES (11, 7, N'64654564564', N'SAFDSF', 2, 1, N'7|64654564564')
INSERT [dbo].[PatientPhones] ([PatientPhoneId], [PatientId], [PhoneNumber], [Ramal], [PhoneTypeId], [PatientPhonesEnabled], [Filter]) VALUES (12, 11, N'11995566666', N'123', 5, 1, N'11|11995566666')
INSERT [dbo].[PatientPhones] ([PatientPhoneId], [PatientId], [PhoneNumber], [Ramal], [PhoneTypeId], [PatientPhonesEnabled], [Filter]) VALUES (13, 12, N'75485555545', N'RAMAL', 5, 1, N'12|75485555545')
SET IDENTITY_INSERT [dbo].[PatientPhones] OFF
SET IDENTITY_INSERT [dbo].[Patients] ON 

INSERT [dbo].[Patients] ([PatientId], [ClientId], [PatientName], [BirthDate], [CPF], [RG], [IdentificationOnClient], [Responsible], [BestContactPeriod], [Comments], [PatientStatus], [CID1], [CID2], [CID3], [PatientEnabled], [Filter]) VALUES (1, 2, N'PACIENTE 1', CAST(N'1992-01-01 02:00:00.000' AS DateTime), N'12345678978', N'4598845465', N'78544552255', N'RESPONSAVEL', N'PELA MANHA', N'', N'1', N'CID 1', N'CID 2', N'CID 3', 1, N'PACIENTE 1|12345678978')
INSERT [dbo].[Patients] ([PatientId], [ClientId], [PatientName], [BirthDate], [CPF], [RG], [IdentificationOnClient], [Responsible], [BestContactPeriod], [Comments], [PatientStatus], [CID1], [CID2], [CID3], [PatientEnabled], [Filter]) VALUES (2, 1, N'PACIENTE 1', CAST(N'1980-01-01 03:00:00.000' AS DateTime), N'12458855588', N'459845465', N'CLIENTE', N'RESPONSAVEL', N'DE MANHA', NULL, N'1', N'CID 1', N'CID 2', N'CID 3', 0, N'PACIENTE 1|12458855588')
INSERT [dbo].[Patients] ([PatientId], [ClientId], [PatientName], [BirthDate], [CPF], [RG], [IdentificationOnClient], [Responsible], [BestContactPeriod], [Comments], [PatientStatus], [CID1], [CID2], [CID3], [PatientEnabled], [Filter]) VALUES (3, 2, N'PACIENTE 5', CAST(N'1979-01-01 03:00:00.000' AS DateTime), N'12346589456', N'546456456456', N'ID DO CLIENTE', N'RESPONSAVEL', N'PELA MANHA', N'', N'1', N'CID 1 ', N'CID 2', N'CID 3', 1, N'PACIENTE 5|12346589456')
INSERT [dbo].[Patients] ([PatientId], [ClientId], [PatientName], [BirthDate], [CPF], [RG], [IdentificationOnClient], [Responsible], [BestContactPeriod], [Comments], [PatientStatus], [CID1], [CID2], [CID3], [PatientEnabled], [Filter]) VALUES (4, 4, N'PACIENTE 1', CAST(N'1979-01-01 03:00:00.000' AS DateTime), N'12432711831', N'465465564', N'CLIENTES', N'RESPOSAVEL', N'PELA MANHA', NULL, N'1', N'CID1', NULL, NULL, 1, N'PACIENTE 1|12432711831')
INSERT [dbo].[Patients] ([PatientId], [ClientId], [PatientName], [BirthDate], [CPF], [RG], [IdentificationOnClient], [Responsible], [BestContactPeriod], [Comments], [PatientStatus], [CID1], [CID2], [CID3], [PatientEnabled], [Filter]) VALUES (5, 2, N'PACIENTE 1', CAST(N'1980-01-01 03:00:00.000' AS DateTime), N'11111111111', N'789546', N'ID NO CLIENTE', N'8855888', N'DE MANHA', NULL, N'1', NULL, NULL, NULL, 0, N'PACIENTE 1|11111111111')
INSERT [dbo].[Patients] ([PatientId], [ClientId], [PatientName], [BirthDate], [CPF], [RG], [IdentificationOnClient], [Responsible], [BestContactPeriod], [Comments], [PatientStatus], [CID1], [CID2], [CID3], [PatientEnabled], [Filter]) VALUES (6, 3, N'PACIENTE TESTE', CAST(N'2019-03-26 03:00:00.000' AS DateTime), N'12432711855', N'455665546', N'CARTAO DE CONVENIO', N'RESPONSAVEL', N'DE MANHA', N'', N'1', N'CID 1', N'CID 2', N'CID 3', 1, N'PACIENTE TESTE|12432711855')
INSERT [dbo].[Patients] ([PatientId], [ClientId], [PatientName], [BirthDate], [CPF], [RG], [IdentificationOnClient], [Responsible], [BestContactPeriod], [Comments], [PatientStatus], [CID1], [CID2], [CID3], [PatientEnabled], [Filter]) VALUES (7, 5, N'NOME DO PACIENT', CAST(N'2019-03-25 03:00:00.000' AS DateTime), N'21659789800', N'RSRSRSRS', N'DFASDFASD', N'RESPOSNAVEL', N'DE MANHA', NULL, N'1', N'CID1', NULL, NULL, 1, N'NOME DO PACIENT|21659789800')
INSERT [dbo].[Patients] ([PatientId], [ClientId], [PatientName], [BirthDate], [CPF], [RG], [IdentificationOnClient], [Responsible], [BestContactPeriod], [Comments], [PatientStatus], [CID1], [CID2], [CID3], [PatientEnabled], [Filter]) VALUES (8, 5, N'NOME DO PACIENT', CAST(N'2019-03-25 03:00:00.000' AS DateTime), N'21659789800', N'RSRSRSRS', N'DFASDFASD', N'RESPOSNAVEL', N'DE MANHA', NULL, N'1', N'CID1', NULL, NULL, 1, N'NOME DO PACIENT|21659789800')
INSERT [dbo].[Patients] ([PatientId], [ClientId], [PatientName], [BirthDate], [CPF], [RG], [IdentificationOnClient], [Responsible], [BestContactPeriod], [Comments], [PatientStatus], [CID1], [CID2], [CID3], [PatientEnabled], [Filter]) VALUES (9, 5, N'NOME DO PACIENT', CAST(N'2019-03-25 03:00:00.000' AS DateTime), N'21659789800', N'RSRSRSRS', N'DFASDFASD', N'RESPOSNAVEL', N'DE MANHA', NULL, N'1', N'CID1', NULL, NULL, 1, N'NOME DO PACIENT|21659789800')
INSERT [dbo].[Patients] ([PatientId], [ClientId], [PatientName], [BirthDate], [CPF], [RG], [IdentificationOnClient], [Responsible], [BestContactPeriod], [Comments], [PatientStatus], [CID1], [CID2], [CID3], [PatientEnabled], [Filter]) VALUES (10, 5, N'NOME DO PACIENT', CAST(N'2019-03-25 03:00:00.000' AS DateTime), N'21659789800', N'RSRSRSRS', N'DFASDFASD', N'RESPOSNAVEL', N'DE MANHA', NULL, N'1', N'CID1', NULL, NULL, 1, N'NOME DO PACIENT|21659789800')
INSERT [dbo].[Patients] ([PatientId], [ClientId], [PatientName], [BirthDate], [CPF], [RG], [IdentificationOnClient], [Responsible], [BestContactPeriod], [Comments], [PatientStatus], [CID1], [CID2], [CID3], [PatientEnabled], [Filter]) VALUES (11, 0, N'NOME PACIENTE 3', CAST(N'2012-01-01 02:00:00.000' AS DateTime), N'12348855522', N'485548', N'1258885666633222', N'RESPONSAVEL', N'MANHA', N'', N'1', N'CID1', N'CID 2.0', N'', 1, N'NOME PACIENTE 3|12348855522')
INSERT [dbo].[Patients] ([PatientId], [ClientId], [PatientName], [BirthDate], [CPF], [RG], [IdentificationOnClient], [Responsible], [BestContactPeriod], [Comments], [PatientStatus], [CID1], [CID2], [CID3], [PatientEnabled], [Filter]) VALUES (12, 3, N'NOME MAIS', CAST(N'2012-01-01 02:00:00.000' AS DateTime), N'15588556666', N'123123', N'IDDSFDSFDS', N'REPSAVAL', N'TARDE', N'OBSER', N'1', N'CID1', N'', N'', 1, N'NOME MAIS|15588556666')
SET IDENTITY_INSERT [dbo].[Patients] OFF
SET IDENTITY_INSERT [dbo].[PhoneType] ON 

INSERT [dbo].[PhoneType] ([PhoneTypeId], [Description], [PhoneTypeEnabled], [Filter]) VALUES (1, N'Celular', 1, NULL)
INSERT [dbo].[PhoneType] ([PhoneTypeId], [Description], [PhoneTypeEnabled], [Filter]) VALUES (2, N'Residencial', 1, NULL)
INSERT [dbo].[PhoneType] ([PhoneTypeId], [Description], [PhoneTypeEnabled], [Filter]) VALUES (5, N'Celular/WhatApp', 1, NULL)
SET IDENTITY_INSERT [dbo].[PhoneType] OFF
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-24 23:57:21.083' AS DateTime), N'sfsd', N'Inclusão', N'Clients', N'ClientId', N'1', N'Inclusão|Clients|ClientId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-24 23:58:40.007' AS DateTime), N'sfsd', N'Inclusão', N'Clients', N'ClientId', N'2', N'Inclusão|Clients|ClientId|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-24 23:59:44.657' AS DateTime), N'sfsd', N'Inclusão', N'ClientsContacts', N'ContactId', N'1', N'Inclusão|ClientsContacts|ContactId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 00:00:13.273' AS DateTime), N'rafaelTeste', N'Inclusão', N'Clients', N'ClientId', N'3', N'Inclusão|Clients|ClientId|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 00:00:17.887' AS DateTime), N'rafaelTeste', N'Inclusão', N'ClientsContacts', N'ContactId', N'2', N'Inclusão|ClientsContacts|ContactId|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 00:13:30.310' AS DateTime), N'sfsd', N'Inclusão', N'Clients', N'ClientId', N'4', N'Inclusão|Clients|ClientId|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 01:03:45.153' AS DateTime), N'joaodokko', N'Inclusão', N'Patients', N'PatientId', N'1', N'Inclusão|Patients|PatientId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 01:03:58.050' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPhones', N'PatientPhoneId', N'2', N'Inclusão|PatientPhones|PatientPhoneId|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 01:19:00.493' AS DateTime), N'joaodokko', N'Inclusão', N'PatientAddresses', N'PatientAddressesId', N'0', N'Inclusão|PatientAddresses|PatientAddressesId|0')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 01:24:21.597' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPhones', N'PatientPhoneId', N'5', N'Inclusão|PatientPhones|PatientPhoneId|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 01:26:17.143' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPhones', N'PatientPhoneId', N'6', N'Inclusão|PatientPhones|PatientPhoneId|6')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 01:31:16.607' AS DateTime), N'joaodokko', N'Inclusão', N'PatientAddresses', N'PatientAddressesId', N'1', N'Inclusão|PatientAddresses|PatientAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-26 07:06:33.167' AS DateTime), N'rafaelTeste', N'Alteração', N'Clients', N'ClientId', N'3', N'Alteração|Clients|ClientId|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-26 07:23:07.490' AS DateTime), N'jao', N'Exclusão', N'Patients', N'PatientId', N'2', N'Exclusão|Patients|PatientId|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-26 07:38:56.240' AS DateTime), N'jao', N'Exclusão', N'Patients', N'PatientId', N'5', N'Exclusão|Patients|PatientId|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-26 07:40:55.837' AS DateTime), N'joaodokko', N'Inclusão', N'Patients', N'PatientId', N'6', N'Inclusão|Patients|PatientId|6')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-26 07:41:01.510' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPathologies', N'PatientPathologiesId', N'2', N'Inclusão|PatientPathologies|PatientPathologiesId|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-26 07:57:49.730' AS DateTime), N'rafaelTeste', N'Alteração', N'Clients', N'ClientId', N'5', N'Alteração|Clients|ClientId|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:15.437' AS DateTime), N'joaodokko', N'Inclusão', N'Patients', N'PatientId', N'9', N'Inclusão|Patients|PatientId|9')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:15.393' AS DateTime), N'joaodokko', N'Inclusão', N'Patients', N'PatientId', N'8', N'Inclusão|Patients|PatientId|8')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:15.393' AS DateTime), N'joaodokko', N'Inclusão', N'Patients', N'PatientId', N'10', N'Inclusão|Patients|PatientId|10')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:15.393' AS DateTime), N'joaodokko', N'Inclusão', N'Patients', N'PatientId', N'7', N'Inclusão|Patients|PatientId|7')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:15.653' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPhones', N'PatientPhoneId', N'9', N'Inclusão|PatientPhones|PatientPhoneId|9')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:15.670' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPhones', N'PatientPhoneId', N'10', N'Inclusão|PatientPhones|PatientPhoneId|10')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:15.827' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPathologies', N'PatientPathologiesId', N'3', N'Inclusão|PatientPathologies|PatientPathologiesId|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:15.910' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPathologies', N'PatientPathologiesId', N'4', N'Inclusão|PatientPathologies|PatientPathologiesId|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:15.957' AS DateTime), N'joaodokko', N'Inclusão', N'PatientAddresses', N'PatientAddressesId', N'1', N'Inclusão|PatientAddresses|PatientAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:16.093' AS DateTime), N'joaodokko', N'Inclusão', N'PatientAddresses', N'PatientAddressesId', N'1', N'Inclusão|PatientAddresses|PatientAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:16.127' AS DateTime), N'joaodokko', N'Inclusão', N'PatientAddresses', N'PatientAddressesId', N'1', N'Inclusão|PatientAddresses|PatientAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:16.360' AS DateTime), N'joaodokko', N'Inclusão', N'PatientAddresses', N'PatientAddressesId', N'1', N'Inclusão|PatientAddresses|PatientAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 14:15:10.060' AS DateTime), N'rafaelTeste', N'Inclusão', N'ClientsContacts', N'ContactId', N'2', N'Inclusão|ClientsContacts|ContactId|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 14:15:10.080' AS DateTime), N'rafaelTeste', N'Inclusão', N'ClientsContacts', N'ContactId', N'3', N'Inclusão|ClientsContacts|ContactId|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 14:37:46.567' AS DateTime), N'jao', N'Exclusão', N'ClientsContacts', N'ContactId', N'2', N'Exclusão|ClientsContacts|ContactId|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 18:35:54.987' AS DateTime), N'jao', N'Inclusão', N'Users', N'UserLogin', N'EMAIL@TESTE.COM', N'Inclusão|Users|UserLogin|EMAIL@TESTE.COM')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 18:40:36.360' AS DateTime), N'jao', N'Inclusão', N'Users', N'UserLogin', N'EMAIL1@TESTE.COM', N'Inclusão|Users|UserLogin|EMAIL1@TESTE.COM')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 19:03:20.373' AS DateTime), N'jao', N'Inclusão', N'Users', N'UserLogin', N'EMAIL2@TESTE.COM', N'Inclusão|Users|UserLogin|EMAIL2@TESTE.COM')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 20:25:18.497' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPhones', N'PatientPhoneId', N'12', N'Inclusão|PatientPhones|PatientPhoneId|12')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 20:25:18.563' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPathologies', N'PatientPathologiesId', N'8', N'Inclusão|PatientPathologies|PatientPathologiesId|8')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-01 03:50:22.263' AS DateTime), N'joaodokko', N'Alteração', N'Patients', N'PatientId', N'11', N'Alteração|Patients|PatientId|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-01 03:50:39.463' AS DateTime), N'joaodokko', N'Alteração', N'Patients', N'PatientId', N'11', N'Alteração|Patients|PatientId|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-01 03:54:14.667' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPhones', N'PatientPhoneId', N'13', N'Inclusão|PatientPhones|PatientPhoneId|13')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-01 03:54:14.737' AS DateTime), N'joaodokko', N'Inclusão', N'PatientAddresses', N'PatientAddressesId', N'1', N'Inclusão|PatientAddresses|PatientAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-09 22:02:04.197' AS DateTime), N'joaodokko', N'Inclusão', N'Representatives', N'RepresentativeID', N'1', N'Inclusão|Representatives|RepresentativeID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-09 22:17:47.360' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1', N'Alteração|Representatives|RepresentativeID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-09 22:31:07.463' AS DateTime), N'jao', N'Exclusão', N'Representatives', N'RepresentativeId', N'3', N'Exclusão|Representatives|RepresentativeId|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-09 22:39:00.230' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1', N'Alteração|Representatives|RepresentativeID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-09 22:40:14.727' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1', N'Alteração|Representatives|RepresentativeID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-09 22:40:35.040' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1', N'Alteração|Representatives|RepresentativeID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-09 22:40:54.530' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1', N'Alteração|Representatives|RepresentativeID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-09 22:41:30.303' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1', N'Alteração|Representatives|RepresentativeID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-09 23:12:08.273' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1', N'Alteração|Representatives|RepresentativeID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-10 01:18:44.690' AS DateTime), N'joaodokko', N'Inclusão', N'Laboratories', N'LaboratoryID', N'2', N'Inclusão|Laboratories|LaboratoryID|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-10 01:19:34.743' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'2', N'Alteração|Laboratories|LaboratoryID|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-10 01:19:58.213' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'2', N'Alteração|Laboratories|LaboratoryID|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-10 01:21:40.070' AS DateTime), N'joaodokko', N'Inclusão', N'Laboratories', N'LaboratoryID', N'3', N'Inclusão|Laboratories|LaboratoryID|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-10 01:28:48.597' AS DateTime), N'jao', N'Exclusão', N'Laboratories', N'LaboratoryId', N'1', N'Exclusão|Laboratories|LaboratoryId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-10 07:23:37.390' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'1', N'Inclusão|Doctors|DoctorID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-10 08:12:12.560' AS DateTime), N'jao', N'Inclusão', N'DoctorAddresses', N'DoctorAddressesId', N'1', N'Inclusão|DoctorAddresses|DoctorAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-10 08:13:47.330' AS DateTime), N'jao', N'Inclusão', N'DoctorAddresses', N'DoctorAddressesId', N'1', N'Inclusão|DoctorAddresses|DoctorAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 12:25:15.037' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'4', N'Inclusão|Doctors|DoctorID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 12:41:53.447' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'4', N'Alteração|Doctors|DoctorsID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 12:43:11.013' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'4', N'Alteração|Doctors|DoctorsID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 15:35:38.210' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'7', N'Inclusão|Doctors|DoctorID|7')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 15:38:09.470' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'8', N'Inclusão|Doctors|DoctorID|8')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 15:43:29.793' AS DateTime), N'x', N'Inclusão', N'Users', N'UserLogin', N'TESTE33', N'Inclusão|Users|UserLogin|TESTE33')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 15:55:36.777' AS DateTime), N'x', N'Inclusão', N'Users', N'UserLogin', N'TESTE331', N'Inclusão|Users|UserLogin|TESTE331')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 15:55:36.797' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'11', N'Inclusão|Doctors|DoctorID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 16:00:06.640' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'9', N'Alteração|Doctors|DoctorsID|9')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 16:42:07.340' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'11', N'Alteração|Doctors|DoctorsID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 16:55:07.583' AS DateTime), N'jao', N'Exclusão', N'DoctorAddresses', N'DoctorAddressesId', N'4', N'Exclusão|DoctorAddresses|DoctorAddressesId|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 17:01:07.650' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'11', N'Alteração|Doctors|DoctorsID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 00:13:30.340' AS DateTime), N'sfsd', N'Inclusão', N'ClientsContacts', N'ContactId', N'3', N'Inclusão|ClientsContacts|ContactId|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-26 07:48:11.593' AS DateTime), N'jao', N'Exclusão', N'Clients', N'ClientId', N'2', N'Exclusão|Clients|ClientId|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-26 07:51:36.367' AS DateTime), N'rafaelTeste', N'Inclusão', N'Clients', N'ClientId', N'5', N'Inclusão|Clients|ClientId|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:15.910' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPathologies', N'PatientPathologiesId', N'5', N'Inclusão|PatientPathologies|PatientPathologiesId|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:15.957' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPathologies', N'PatientPathologiesId', N'6', N'Inclusão|PatientPathologies|PatientPathologiesId|6')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:16.037' AS DateTime), N'joaodokko', N'Inclusão', N'PatientAddresses', N'PatientAddressesId', N'1', N'Inclusão|PatientAddresses|PatientAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:16.037' AS DateTime), N'joaodokko', N'Inclusão', N'PatientAddresses', N'PatientAddressesId', N'1', N'Inclusão|PatientAddresses|PatientAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:16.093' AS DateTime), N'joaodokko', N'Inclusão', N'PatientAddresses', N'PatientAddressesId', N'1', N'Inclusão|PatientAddresses|PatientAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 13:18:10.430' AS DateTime), N'rafaelTeste', N'Inclusão', N'Clients', N'ClientId', N'1', N'Inclusão|Clients|ClientId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 17:31:18.170' AS DateTime), N'jao', N'Exclusão', N'ClientsContacts', N'ContactId', N'4', N'Exclusão|ClientsContacts|ContactId|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 17:34:25.660' AS DateTime), N'jao', N'Inclusão', N'ClientsContacts', N'ContactId', N'5', N'Inclusão|ClientsContacts|ContactId|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 18:41:13.233' AS DateTime), N'jao', N'Inclusão', N'Users', N'UserLogin', N'EMAIL1@TESTE.COM', N'Inclusão|Users|UserLogin|EMAIL1@TESTE.COM')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 19:03:23.637' AS DateTime), N'jao', N'Inclusão', N'ClientsContacts', N'ContactId', N'6', N'Inclusão|ClientsContacts|ContactId|6')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 20:25:18.523' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPathologies', N'PatientPathologiesId', N'7', N'Inclusão|PatientPathologies|PatientPathologiesId|7')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 20:25:18.607' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPathologies', N'PatientPathologiesId', N'9', N'Inclusão|PatientPathologies|PatientPathologiesId|9')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-09 22:38:53.107' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1', N'Alteração|Representatives|RepresentativeID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-10 01:13:34.750' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1', N'Alteração|Laboratories|LaboratoryID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-10 01:23:00.313' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'3', N'Alteração|Laboratories|LaboratoryID|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-10 07:25:57.980' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'3', N'Inclusão|Doctors|DoctorID|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 12:25:44.233' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'4', N'Alteração|Doctors|DoctorsID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 12:26:27.840' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'4', N'Alteração|Doctors|DoctorsID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 16:39:30.030' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'11', N'Alteração|Doctors|DoctorsID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 17:19:47.967' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'11', N'Alteração|Doctors|DoctorsID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 17:24:24.970' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'11', N'Alteração|Doctors|DoctorsID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 17:47:03.460' AS DateTime), N'joaodokko', N'Inclusão', N'Laboratories', N'LaboratoryID', N'4', N'Inclusão|Laboratories|LaboratoryID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 18:01:56.467' AS DateTime), N'joaodokko', N'Inclusão', N'Laboratories', N'LaboratoryID', N'5', N'Inclusão|Laboratories|LaboratoryID|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 18:18:00.143' AS DateTime), N'TESTE332', N'Inclusão', N'Users', N'UserLogin', N'TESTE332', N'Inclusão|Users|UserLogin|TESTE332')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 18:45:30.310' AS DateTime), N'TESTE335', N'Inclusão', N'Users', N'UserLogin', N'TESTE335', N'Inclusão|Users|UserLogin|TESTE335')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 19:48:07.710' AS DateTime), N'joaodokko', N'Inclusão', N'Representatives', N'RepresentativeID', N'4', N'Inclusão|Representatives|RepresentativeID|4')
GO
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-16 19:32:20.543' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-16 19:32:20.543' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'2', N'Inclusão|ProductsAvaliable|ProductAvaliableID|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-16 19:35:07.970' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'4', N'Inclusão|ProductsAvaliable|ProductAvaliableID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 01:54:16.627' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'11', N'Alteração|Doctors|DoctorsID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 19:42:13.903' AS DateTime), N'TESTE331X', N'Inclusão', N'Users', N'UserLogin', N'TESTE331X', N'Inclusão|Users|UserLogin|TESTE331X')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 19:47:14.783' AS DateTime), N'TESTE1234', N'Inclusão', N'Users', N'UserLogin', N'TESTE1234', N'Inclusão|Users|UserLogin|TESTE1234')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 19:58:00.367' AS DateTime), N'TESTE4444', N'Inclusão', N'Users', N'UserLogin', N'TESTE4444', N'Inclusão|Users|UserLogin|TESTE4444')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 19:58:00.420' AS DateTime), N'joaodokko', N'Inclusão', N'DoctorAddresses', N'DoctorAddressesId', N'0', N'Inclusão|DoctorAddresses|DoctorAddressesId|0')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 20:02:49.847' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'14', N'Inclusão|ProductsAvaliable|ProductAvaliableID|14')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 20:06:10.293' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'15', N'Inclusão|Doctors|DoctorID|15')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 20:08:46.520' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'15', N'Alteração|Doctors|DoctorsID|15')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 20:23:23.340' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'18', N'Inclusão|ProductsAvaliable|ProductAvaliableID|18')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-21 19:23:54.657' AS DateTime), N'LOGIN', N'Inclusão', N'Users', N'UserLogin', N'LOGIN', N'Inclusão|Users|UserLogin|LOGIN')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-21 19:25:11.103' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'1015', N'Inclusão|Doctors|DoctorID|1015')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-21 19:38:22.023' AS DateTime), N'joaodokko', N'Inclusão', N'Laboratories', N'LaboratoryID', N'1007', N'Inclusão|Laboratories|LaboratoryID|1007')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-22 05:40:42.930' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'1', N'Inclusão|OrderAvaliable|OrderAvaliableID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-22 05:42:51.840' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'2', N'Inclusão|OrderAvaliable|OrderAvaliableID|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-22 07:09:22.197' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1015', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1015')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-22 07:34:04.327' AS DateTime), N'ABELARDO', N'Inclusão', N'Users', N'UserLogin', N'ABELARDO', N'Inclusão|Users|UserLogin|ABELARDO')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-22 07:34:34.310' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1017', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1017')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 00:29:50.637' AS DateTime), N'joaodokko', N'Inclusão', N'Laboratories', N'LaboratoryID', N'1009', N'Inclusão|Laboratories|LaboratoryID|1009')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 00:40:57.180' AS DateTime), N'LAB1', N'Inclusão', N'Users', N'UserLogin', N'LAB1', N'Inclusão|Users|UserLogin|LAB1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 00:40:58.953' AS DateTime), N'joaodokko', N'Inclusão', N'Laboratories', N'LaboratoryID', N'1011', N'Inclusão|Laboratories|LaboratoryID|1011')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 00:47:43.730' AS DateTime), N'REPRES12', N'Inclusão', N'Users', N'UserLogin', N'REPRES12', N'Inclusão|Users|UserLogin|REPRES12')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 00:51:23.647' AS DateTime), N'REPRES1', N'Inclusão', N'Users', N'UserLogin', N'REPRES1', N'Inclusão|Users|UserLogin|REPRES1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:05:24.513' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'1017', N'Inclusão|Doctors|DoctorID|1017')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:09:51.477' AS DateTime), N'REPRES1', N'Inclusão', N'Users', N'UserLogin', N'REPRES1', N'Inclusão|Users|UserLogin|REPRES1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:12:15.887' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'1018', N'Inclusão|Doctors|DoctorID|1018')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:39:59.570' AS DateTime), N'LAB1', N'Inclusão', N'Users', N'UserLogin', N'LAB1', N'Inclusão|Users|UserLogin|LAB1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:41:12.720' AS DateTime), N'LAB2', N'Inclusão', N'Users', N'UserLogin', N'LAB2', N'Inclusão|Users|UserLogin|LAB2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:42:45.117' AS DateTime), N'REPRES1', N'Inclusão', N'Users', N'UserLogin', N'REPRES1', N'Inclusão|Users|UserLogin|REPRES1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:42:45.140' AS DateTime), N'joaodokko', N'Inclusão', N'Representatives', N'RepresentativeID', N'1008', N'Inclusão|Representatives|RepresentativeID|1008')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:43:05.640' AS DateTime), N'REPRES2', N'Inclusão', N'Users', N'UserLogin', N'REPRES2', N'Inclusão|Users|UserLogin|REPRES2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:48:41.060' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 02:00:28.077' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'1020', N'Inclusão|Doctors|DoctorID|1020')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 02:10:56.647' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1019', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 02:12:06.720' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 02:21:03.587' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 02:46:10.030' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1012', N'Alteração|Laboratories|LaboratoryID|1012')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 02:51:03.427' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1012', N'Alteração|Laboratories|LaboratoryID|1012')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:02:08.320' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1020', N'Alteração|Doctors|DoctorsID|1020')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 01:03:58.030' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPhones', N'PatientPhoneId', N'1', N'Inclusão|PatientPhones|PatientPhoneId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 01:11:28.653' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPhones', N'PatientPhoneId', N'3', N'Inclusão|PatientPhones|PatientPhoneId|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 01:18:24.470' AS DateTime), N'joaodokko', N'Inclusão', N'Patients', N'PatientId', N'3', N'Inclusão|Patients|PatientId|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 01:18:44.480' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPhones', N'PatientPhoneId', N'4', N'Inclusão|PatientPhones|PatientPhoneId|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 01:25:00.407' AS DateTime), N'joaodokko', N'Inclusão', N'PatientAddresses', N'PatientAddressesId', N'1', N'Inclusão|PatientAddresses|PatientAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 01:26:10.230' AS DateTime), N'joaodokko', N'Inclusão', N'Patients', N'PatientId', N'5', N'Inclusão|Patients|PatientId|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 01:31:04.840' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPathologies', N'PatientPathologiesId', N'1', N'Inclusão|PatientPathologies|PatientPathologiesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:15.633' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPhones', N'PatientPhoneId', N'8', N'Inclusão|PatientPhones|PatientPhoneId|8')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-09 22:20:08.050' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'3', N'Alteração|Representatives|RepresentativeID|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 12:26:57.540' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'4', N'Alteração|Doctors|DoctorsID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 15:28:45.110' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'5', N'Inclusão|Doctors|DoctorID|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 18:18:00.197' AS DateTime), N'joaodokko', N'Inclusão', N'Laboratories', N'LaboratoryID', N'6', N'Inclusão|Laboratories|LaboratoryID|6')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-16 19:35:08.037' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'5', N'Inclusão|ProductsAvaliable|ProductAvaliableID|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 01:37:24.900' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'0', N'Alteração|Doctors|DoctorsID|0')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 01:37:54.577' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'0', N'Alteração|Doctors|DoctorsID|0')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 01:40:00.200' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'0', N'Alteração|Doctors|DoctorsID|0')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 01:41:49.877' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'11', N'Alteração|Doctors|DoctorsID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 01:48:55.177' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'11', N'Alteração|Doctors|DoctorsID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 01:53:53.210' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'11', N'Alteração|Doctors|DoctorsID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:17:20.217' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1', N'Inclusão|OrdersDoctor|OrderDoctorID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:31:42.827' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'7', N'Inclusão|ProductsAvaliable|ProductAvaliableID|7')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:40:13.053' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'9', N'Inclusão|ProductsAvaliable|ProductAvaliableID|9')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:45:27.217' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'12', N'Inclusão|ProductsAvaliable|ProductAvaliableID|12')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:45:55.650' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'3', N'Inclusão|OrdersDoctor|OrderDoctorID|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:55:06.283' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'13', N'Inclusão|Doctors|DoctorID|13')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:56:04.603' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'13', N'Alteração|Doctors|DoctorsID|13')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:56:06.610' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'4', N'Inclusão|OrdersDoctor|OrderDoctorID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 19:47:14.807' AS DateTime), N'joaodokko', N'Inclusão', N'Representatives', N'RepresentativeID', N'5', N'Inclusão|Representatives|RepresentativeID|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 20:07:12.917' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'16', N'Inclusão|ProductsAvaliable|ProductAvaliableID|16')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-21 19:40:30.707' AS DateTime), N'LOGINNOVO', N'Inclusão', N'Users', N'UserLogin', N'LOGINNOVO', N'Inclusão|Users|UserLogin|LOGINNOVO')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-22 05:44:07.203' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'3', N'Inclusão|OrderAvaliable|OrderAvaliableID|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-22 07:34:34.207' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'5', N'Inclusão|OrderAvaliable|OrderAvaliableID|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 00:47:43.750' AS DateTime), N'joaodokko', N'Inclusão', N'Representatives', N'RepresentativeID', N'1005', N'Inclusão|Representatives|RepresentativeID|1005')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:48:05.263' AS DateTime), N'MEDICO1', N'Inclusão', N'Users', N'UserLogin', N'MEDICO1', N'Inclusão|Users|UserLogin|MEDICO1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 02:10:56.607' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'6', N'Inclusão|OrderAvaliable|OrderAvaliableID|6')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:03:31.157' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:10:11.123' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'7', N'Inclusão|OrderAvaliable|OrderAvaliableID|7')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:26:13.913' AS DateTime), N'REPRES3', N'Inclusão', N'Users', N'UserLogin', N'REPRES3', N'Inclusão|Users|UserLogin|REPRES3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:26:13.930' AS DateTime), N'joaodokko', N'Inclusão', N'Representatives', N'RepresentativeID', N'1010', N'Inclusão|Representatives|RepresentativeID|1010')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:27:32.520' AS DateTime), N'joaodokko', N'Inclusão', N'Representatives', N'RepresentativeID', N'1011', N'Inclusão|Representatives|RepresentativeID|1011')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:32:44.463' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:33:08.840' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:35:29.513' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:35:52.487' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:37:47.403' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:39:15.617' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:40:00.763' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:41:20.023' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:56:11.733' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1012', N'Alteração|Laboratories|LaboratoryID|1012')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 05:38:08.370' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1006', N'Inclusão|OrdersDoctor|OrderDoctorID|1006')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 05:38:20.207' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'2', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 05:40:39.610' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1007', N'Inclusão|OrdersDoctor|OrderDoctorID|1007')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 05:40:41.513' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'4', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 05:40:43.973' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'5', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 05:40:44.390' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'6', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|6')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 05:46:59.573' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1009', N'Inclusão|OrdersDoctor|OrderDoctorID|1009')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 05:47:01.207' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'8', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|8')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 05:47:01.273' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'10', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|10')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 11:48:23.163' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1020', N'Alteração|Doctors|DoctorsID|1020')
GO
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 12:27:02.337' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1020', N'Alteração|Doctors|DoctorsID|1020')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 12:29:52.363' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1020', N'Alteração|Doctors|DoctorsID|1020')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 13:46:33.797' AS DateTime), N'LOGINREP2', N'Inclusão', N'Users', N'UserLogin', N'LOGINREP2', N'Inclusão|Users|UserLogin|LOGINREP2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 14:40:09.623' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1012', N'Alteração|Representatives|RepresentativeID|1012')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 14:41:13.103' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1012', N'Alteração|Representatives|RepresentativeID|1012')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 14:43:32.593' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1012', N'Alteração|Representatives|RepresentativeID|1012')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 17:24:50.627' AS DateTime), N'MEDICO3', N'Inclusão', N'Users', N'UserLogin', N'MEDICO3', N'Inclusão|Users|UserLogin|MEDICO3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:20:09.573' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'8', N'Inclusão|OrderAvaliable|OrderAvaliableID|8')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:29:20.780' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'9', N'Inclusão|OrderAvaliable|OrderAvaliableID|9')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 01:11:21.730' AS DateTime), N'joaodokko', N'Inclusão', N'Patients', N'PatientId', N'2', N'Inclusão|Patients|PatientId|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-25 01:24:13.310' AS DateTime), N'joaodokko', N'Inclusão', N'Patients', N'PatientId', N'4', N'Inclusão|Patients|PatientId|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-26 07:40:55.860' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPhones', N'PatientPhoneId', N'7', N'Inclusão|PatientPhones|PatientPhoneId|7')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-26 07:56:04.417' AS DateTime), N'jao', N'Exclusão', N'Clients', N'ClientId', N'1', N'Exclusão|Clients|ClientId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 20:25:09.223' AS DateTime), N'joaodokko', N'Inclusão', N'Patients', N'PatientId', N'11', N'Inclusão|Patients|PatientId|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-09 22:40:20.503' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1', N'Alteração|Representatives|RepresentativeID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 17:55:08.167' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'4', N'Alteração|Laboratories|LaboratoryID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 18:45:30.367' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'12', N'Inclusão|Doctors|DoctorID|12')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:55:43.650' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'13', N'Inclusão|ProductsAvaliable|ProductAvaliableID|13')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 19:58:00.397' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'14', N'Inclusão|Doctors|DoctorID|14')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 20:08:58.717' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'5', N'Inclusão|OrdersDoctor|OrderDoctorID|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-22 07:09:22.147' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'4', N'Inclusão|OrderAvaliable|OrderAvaliableID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 05:47:01.230' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1010', N'Inclusão|OrdersDoctor|OrderDoctorID|1010')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 11:48:40.900' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 12:20:56.160' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1020', N'Alteração|Doctors|DoctorsID|1020')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 13:46:33.860' AS DateTime), N'joaodokko', N'Inclusão', N'Representatives', N'RepresentativeID', N'1012', N'Inclusão|Representatives|RepresentativeID|1012')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 17:24:50.950' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'1021', N'Inclusão|Doctors|DoctorID|1021')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:51:08.267' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'11', N'Inclusão|OrderAvaliable|OrderAvaliableID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:52:25.140' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'12', N'Inclusão|OrderAvaliable|OrderAvaliableID|12')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:56:41.550' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1011', N'Inclusão|OrdersDoctor|OrderDoctorID|1011')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:56:41.620' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1012', N'Inclusão|OrdersDoctor|OrderDoctorID|1012')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:56:41.653' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1013', N'Inclusão|OrdersDoctor|OrderDoctorID|1013')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:56:41.673' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'13', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|13')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:56:53.067' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1015', N'Inclusão|OrdersDoctor|OrderDoctorID|1015')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:56:53.100' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1016', N'Inclusão|OrdersDoctor|OrderDoctorID|1016')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:56:53.133' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1017', N'Inclusão|OrdersDoctor|OrderDoctorID|1017')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:56:53.147' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'17', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|17')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 19:55:15.590' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1021', N'Alteração|Doctors|DoctorsID|1021')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 19:56:16.520' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1010', N'Alteração|Representatives|RepresentativeID|1010')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 20:40:42.733' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1013', N'Alteração|Laboratories|LaboratoryID|1013')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 20:58:59.240' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1027', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1027')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:00:08.947' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1028', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1028')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:53.923' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'18', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|18')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.220' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'19', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|19')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.343' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1021', N'Inclusão|OrdersDoctor|OrderDoctorID|1021')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.387' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1022', N'Inclusão|OrdersDoctor|OrderDoctorID|1022')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.420' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1023', N'Inclusão|OrdersDoctor|OrderDoctorID|1023')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.457' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1024', N'Inclusão|OrdersDoctor|OrderDoctorID|1024')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.473' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'24', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|24')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.507' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'25', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|25')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.550' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'26', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|26')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.590' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'27', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|27')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.647' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1029', N'Inclusão|OrdersDoctor|OrderDoctorID|1029')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.683' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1030', N'Inclusão|OrdersDoctor|OrderDoctorID|1030')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.727' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1031', N'Inclusão|OrdersDoctor|OrderDoctorID|1031')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 22:46:56.177' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1029', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1029')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 22:46:56.223' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'32', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|32')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 22:46:56.243' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1033', N'Inclusão|OrdersDoctor|OrderDoctorID|1033')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 22:46:56.333' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1035', N'Inclusão|OrdersDoctor|OrderDoctorID|1035')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 22:46:56.373' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1036', N'Inclusão|OrdersDoctor|OrderDoctorID|1036')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-29 00:44:32.280' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1021', N'Alteração|Doctors|DoctorsID|1021')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-29 03:51:29.363' AS DateTime), N'joaodokko', N'Alteração', N'Users', N'UserId', N'1042', N'Alteração|Users|UserId|1042')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:09:06.867' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'16', N'Inclusão|OrderAvaliable|OrderAvaliableID|16')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:09:06.997' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1037', N'Inclusão|OrdersDoctor|OrderDoctorID|1037')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:11:04.610' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'38', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|38')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:15:26.250' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'18', N'Inclusão|OrderAvaliable|OrderAvaliableID|18')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:15:26.300' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1032', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1032')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:15:26.370' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'39', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|39')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:17:47.447' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'19', N'Inclusão|OrderAvaliable|OrderAvaliableID|19')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:17:47.473' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1033', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1033')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:17:47.530' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'40', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|40')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:20:31.727' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'20', N'Inclusão|OrderAvaliable|OrderAvaliableID|20')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:20:31.790' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1034', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1034')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:20:31.843' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'41', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|41')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-26 07:48:21.827' AS DateTime), N'jao', N'Exclusão', N'Clients', N'ClientId', N'4', N'Exclusão|Clients|ClientId|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:15.763' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPhones', N'PatientPhoneId', N'11', N'Inclusão|PatientPhones|PatientPhoneId|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-01 03:54:14.703' AS DateTime), N'joaodokko', N'Inclusão', N'PatientPathologies', N'PatientPathologiesId', N'10', N'Inclusão|PatientPathologies|PatientPathologiesId|10')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-09 22:19:26.667' AS DateTime), N'joaodokko', N'Inclusão', N'Representatives', N'RepresentativeID', N'3', N'Inclusão|Representatives|RepresentativeID|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-10 01:16:39.793' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1', N'Alteração|Laboratories|LaboratoryID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-10 01:23:12.920' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'3', N'Alteração|Laboratories|LaboratoryID|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 12:42:08.597' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'4', N'Alteração|Doctors|DoctorsID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 15:43:29.810' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'9', N'Inclusão|Doctors|DoctorID|9')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-16 19:34:40.223' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'3', N'Inclusão|ProductsAvaliable|ProductAvaliableID|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:44:50.130' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'11', N'Inclusão|ProductsAvaliable|ProductAvaliableID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:45:51.540' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'11', N'Alteração|Doctors|DoctorsID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 00:40:49.013' AS DateTime), N'joaodokko', N'Inclusão', N'Laboratories', N'LaboratoryID', N'1010', N'Inclusão|Laboratories|LaboratoryID|1010')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 00:44:50.690' AS DateTime), N'REPRES1', N'Inclusão', N'Users', N'UserLogin', N'REPRES1', N'Inclusão|Users|UserLogin|REPRES1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:09:51.500' AS DateTime), N'joaodokko', N'Inclusão', N'Representatives', N'RepresentativeID', N'1007', N'Inclusão|Representatives|RepresentativeID|1007')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:43:05.663' AS DateTime), N'joaodokko', N'Inclusão', N'Representatives', N'RepresentativeID', N'1009', N'Inclusão|Representatives|RepresentativeID|1009')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:59:46.257' AS DateTime), N'MEDICO2', N'Inclusão', N'Users', N'UserLogin', N'MEDICO2', N'Inclusão|Users|UserLogin|MEDICO2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 02:54:48.660' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1012', N'Alteração|Laboratories|LaboratoryID|1012')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:10:11.157' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1021', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1021')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:27:32.503' AS DateTime), N'REPRES4', N'Inclusão', N'Users', N'UserLogin', N'REPRES4', N'Inclusão|Users|UserLogin|REPRES4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:40:27.317' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:41:51.747' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 05:40:42.757' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1008', N'Inclusão|OrdersDoctor|OrderDoctorID|1008')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:33:48.093' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'10', N'Inclusão|OrderAvaliable|OrderAvaliableID|10')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 19:31:11.100' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1020', N'Alteração|Doctors|DoctorsID|1020')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 20:42:05.523' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1012', N'Alteração|Laboratories|LaboratoryID|1012')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.237' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1020', N'Inclusão|OrdersDoctor|OrderDoctorID|1020')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.610' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1028', N'Inclusão|OrdersDoctor|OrderDoctorID|1028')
GO
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 22:46:56.283' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1034', N'Inclusão|OrdersDoctor|OrderDoctorID|1034')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:11:04.480' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1031', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1031')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:24:46.560' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1035', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1035')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:24:46.647' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'42', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|42')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:25:53.403' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1036', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1036')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:25:53.460' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'43', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|43')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:27:12.170' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1037', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1037')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:27:12.220' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'44', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|44')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:27:33.147' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1045', N'Inclusão|OrdersDoctor|OrderDoctorID|1045')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:30:03.237' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1038', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1038')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:30:03.297' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'46', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|46')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:30:42.197' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1047', N'Inclusão|OrdersDoctor|OrderDoctorID|1047')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:30:44.227' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1048', N'Inclusão|OrdersDoctor|OrderDoctorID|1048')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:30:44.250' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'48', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|48')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:44:29.677' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1039', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1039')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:44:34.280' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1049', N'Inclusão|OrdersDoctor|OrderDoctorID|1049')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:48:19.593' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1040', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1040')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:48:31.510' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1050', N'Inclusão|OrdersDoctor|OrderDoctorID|1050')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:49:10.640' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'51', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|51')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:49:49.877' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'52', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|52')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:55:41.970' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1043', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1043')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:55:42.023' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1053', N'Inclusão|OrdersDoctor|OrderDoctorID|1053')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-01 22:48:45.837' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'30', N'Inclusão|OrderAvaliable|OrderAvaliableID|30')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-01 22:48:46.007' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'54', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|54')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-01 23:17:25.330' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:24:54.500' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'4', N'Inclusão|OrderAvaliable|OrderAvaliableID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:25:49.443' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'5', N'Inclusão|OrderAvaliable|OrderAvaliableID|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:25:49.467' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'3', N'Inclusão|ProductsAvaliable|ProductAvaliableID|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:30:28.513' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'1', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:33:09.423' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'4', N'Inclusão|ProductsAvaliable|ProductAvaliableID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:40:55.707' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'2', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:42:32.103' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'5', N'Inclusão|ProductsAvaliable|ProductAvaliableID|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:42:50.807' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'3', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:43:41.560' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'1', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:43:47.123' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'4', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-26 07:51:36.443' AS DateTime), N'rafaelTeste', N'Inclusão', N'ClientsContacts', N'ContactId', N'4', N'Inclusão|ClientsContacts|ContactId|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 14:15:10.030' AS DateTime), N'rafaelTeste', N'Inclusão', N'Clients', N'ClientId', N'3', N'Inclusão|Clients|ClientId|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 15:44:41.630' AS DateTime), N'x', N'Inclusão', N'Users', N'UserLogin', N'TESTE33', N'Inclusão|Users|UserLogin|TESTE33')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 16:40:56.817' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'11', N'Alteração|Doctors|DoctorsID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 16:54:43.580' AS DateTime), N'jao', N'Exclusão', N'DoctorAddresses', N'DoctorAddressesId', N'222', N'Exclusão|DoctorAddresses|DoctorAddressesId|222')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:32:28.917' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'8', N'Inclusão|ProductsAvaliable|ProductAvaliableID|8')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 20:06:10.270' AS DateTime), N'MEDICO11', N'Inclusão', N'Users', N'UserLogin', N'MEDICO11', N'Inclusão|Users|UserLogin|MEDICO11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-21 19:38:21.980' AS DateTime), N'LOGIN3631', N'Inclusão', N'Users', N'UserLogin', N'LOGIN3631', N'Inclusão|Users|UserLogin|LOGIN3631')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 02:55:26.633' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1012', N'Alteração|Laboratories|LaboratoryID|1012')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 05:47:01.187' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'7', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|7')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 13:44:10.847' AS DateTime), N'LOGINREP', N'Inclusão', N'Users', N'UserLogin', N'LOGINREP', N'Inclusão|Users|UserLogin|LOGINREP')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 16:46:16.313' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1012', N'Alteração|Laboratories|LaboratoryID|1012')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 19:04:21.573' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1008', N'Alteração|Representatives|RepresentativeID|1008')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 20:57:48.787' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1021', N'Alteração|Doctors|DoctorsID|1021')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.437' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'23', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|23')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.747' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'31', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|31')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 22:46:56.263' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'33', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|33')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:11:04.583' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1038', N'Inclusão|OrdersDoctor|OrderDoctorID|1038')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:49:10.533' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'27', N'Inclusão|OrderAvaliable|OrderAvaliableID|27')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:49:49.780' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'28', N'Inclusão|OrderAvaliable|OrderAvaliableID|28')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-01 23:13:29.113' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'3', N'Inclusão|OrderAvaliable|OrderAvaliableID|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:43:47.270' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'3', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:43:47.337' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'4', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:43:47.383' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'7', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|7')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:34.970' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'6', N'Inclusão|ProductsAvaliable|ProductAvaliableID|6')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:35.123' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'6', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|6')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:35.277' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'9', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|9')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:35.327' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'9', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|9')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:35.410' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'10', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|10')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:35.453' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'11', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:35.570' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'13', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|13')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:35.657' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'14', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|14')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:19:54.053' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1011', N'Alteração|Representatives|RepresentativeID|1011')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:45:06.800' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'9', N'Inclusão|OrderAvaliable|OrderAvaliableID|9')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:45:06.937' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'16', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|16')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:45:07.013' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'17', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|17')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:45:07.060' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'15', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|15')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:45:07.230' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'20', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|20')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:58:24.727' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'9', N'Inclusão|ProductsAvaliable|ProductAvaliableID|9')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:58:24.787' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'18', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|18')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:58:24.870' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'19', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|19')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:58:24.970' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'23', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|23')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:58:25.023' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'21', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|21')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 11:09:16.200' AS DateTime), N'joaodokko', N'Inclusão', N'PatientAddresses', N'PatientAddressesId', N'1', N'Inclusão|PatientAddresses|PatientAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 13:45:53.257' AS DateTime), N'rafaelTeste', N'Inclusão', N'Clients', N'ClientId', N'2', N'Inclusão|Clients|ClientId|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 13:46:37.450' AS DateTime), N'rafaelTeste', N'Inclusão', N'ClientsContacts', N'ContactId', N'1', N'Inclusão|ClientsContacts|ContactId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 17:04:33.630' AS DateTime), N'jao', N'Exclusão', N'ClientsContacts', N'ContactId', N'3', N'Exclusão|ClientsContacts|ContactId|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 17:31:04.737' AS DateTime), N'jao', N'Inclusão', N'ClientsContacts', N'ContactId', N'4', N'Inclusão|ClientsContacts|ContactId|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-03-31 18:39:46.417' AS DateTime), N'jao', N'Inclusão', N'Users', N'UserLogin', N'EMAIL1@TESTE.COM', N'Inclusão|Users|UserLogin|EMAIL1@TESTE.COM')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-01 03:54:14.620' AS DateTime), N'joaodokko', N'Inclusão', N'Patients', N'PatientId', N'12', N'Inclusão|Patients|PatientId|12')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-09 22:18:26.430' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1', N'Alteração|Representatives|RepresentativeID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-09 22:40:45.850' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1', N'Alteração|Representatives|RepresentativeID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-10 01:19:48.757' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'2', N'Alteração|Laboratories|LaboratoryID|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-10 07:40:48.380' AS DateTime), N'jao', N'Exclusão', N'Doctors', N'DoctorID', N'3', N'Exclusão|Doctors|DoctorID|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-10 07:57:43.367' AS DateTime), N'jao', N'Inclusão', N'DoctorAddresses', N'DoctorAddressesId', N'1', N'Inclusão|DoctorAddresses|DoctorAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:55:59.653' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'13', N'Alteração|Doctors|DoctorsID|13')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 20:02:49.867' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'15', N'Inclusão|ProductsAvaliable|ProductAvaliableID|15')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-22 05:44:22.190' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1014', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1014')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:05:24.487' AS DateTime), N'MEDICO1', N'Inclusão', N'Users', N'UserLogin', N'MEDICO1', N'Inclusão|Users|UserLogin|MEDICO1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:39:52.483' AS DateTime), N'joaodokko', N'Inclusão', N'Laboratories', N'LaboratoryID', N'1012', N'Inclusão|Laboratories|LaboratoryID|1012')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 02:23:12.920' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:10:11.140' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1020', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1020')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 05:33:57.480' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1005', N'Inclusão|OrdersDoctor|OrderDoctorID|1005')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 05:38:14.617' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'1', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 05:40:41.003' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'3', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|3')
GO
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:33:48.170' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1024', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1024')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:56:41.590' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'11', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:56:53.047' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'14', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|14')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.143' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1019', N'Inclusão|OrdersDoctor|OrderDoctorID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.570' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1027', N'Inclusão|OrdersDoctor|OrderDoctorID|1027')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 22:46:56.133' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'15', N'Inclusão|OrderAvaliable|OrderAvaliableID|15')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-29 00:44:24.473' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1020', N'Alteração|Doctors|DoctorsID|1020')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:27:33.170' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'45', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|45')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:30:42.320' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'47', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|47')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:35.527' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'11', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:45:07.137' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'16', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|16')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:58:25.050' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'24', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|24')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-09 23:12:18.650' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1', N'Alteração|Representatives|RepresentativeID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-10 00:59:14.663' AS DateTime), N'joaodokko', N'Inclusão', N'Laboratories', N'LaboratoryID', N'1', N'Inclusão|Laboratories|LaboratoryID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 12:41:15.213' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'4', N'Alteração|Doctors|DoctorsID|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 16:06:09.097' AS DateTime), N'jao', N'Inclusão', N'DoctorAddresses', N'DoctorAddressesId', N'1', N'Inclusão|DoctorAddresses|DoctorAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 16:56:06.517' AS DateTime), N'jao', N'Exclusão', N'Doctors', N'DoctorID', N'7', N'Exclusão|Doctors|DoctorID|7')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 18:01:56.430' AS DateTime), N'TESTE334', N'Inclusão', N'Users', N'UserLogin', N'TESTE334', N'Inclusão|Users|UserLogin|TESTE334')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:15:59.437' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'11', N'Alteração|Doctors|DoctorsID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:22:00.210' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'2', N'Inclusão|OrdersDoctor|OrderDoctorID|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:31:39.737' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'6', N'Inclusão|ProductsAvaliable|ProductAvaliableID|6')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-21 19:40:30.793' AS DateTime), N'joaodokko', N'Inclusão', N'Laboratories', N'LaboratoryID', N'1008', N'Inclusão|Laboratories|LaboratoryID|1008')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 00:51:23.670' AS DateTime), N'joaodokko', N'Inclusão', N'Representatives', N'RepresentativeID', N'1006', N'Inclusão|Representatives|RepresentativeID|1006')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:12:15.863' AS DateTime), N'MEDICO1', N'Inclusão', N'Users', N'UserLogin', N'MEDICO1', N'Inclusão|Users|UserLogin|MEDICO1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:04:00.507' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:42:28.750' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 11:55:41.723' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 12:28:54.807' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1020', N'Alteração|Doctors|DoctorsID|1020')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:20:09.737' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1022', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1022')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:29:20.880' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1023', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1023')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:55:01.387' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1021', N'Alteração|Doctors|DoctorsID|1021')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:56:53.030' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1014', N'Inclusão|OrdersDoctor|OrderDoctorID|1014')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.403' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'22', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|22')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.707' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'30', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|30')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 22:46:56.393' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'36', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|36')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:11:04.453' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'17', N'Inclusão|OrderAvaliable|OrderAvaliableID|17')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:43:47.247' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'5', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:35.300' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'8', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|8')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:35.673' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'15', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|15')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:45:06.910' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'13', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|13')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:58:24.817' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'21', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|21')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-13 15:58:03.233' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'9', N'Alteração|Doctors|DoctorsID|9')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 01:41:13.243' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'11', N'Alteração|Doctors|DoctorsID|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:43:00.840' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'10', N'Inclusão|ProductsAvaliable|ProductAvaliableID|10')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 02:55:06.250' AS DateTime), N'MEDICO1', N'Inclusão', N'Users', N'UserLogin', N'MEDICO1', N'Inclusão|Users|UserLogin|MEDICO1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 19:42:13.927' AS DateTime), N'joaodokko', N'Inclusão', N'Laboratories', N'LaboratoryID', N'7', N'Inclusão|Laboratories|LaboratoryID|7')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-21 19:25:11.073' AS DateTime), N'LOGIN331', N'Inclusão', N'Users', N'UserLogin', N'LOGIN331', N'Inclusão|Users|UserLogin|LOGIN331')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 02:54:04.843' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1012', N'Alteração|Laboratories|LaboratoryID|1012')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:52:58.540' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1008', N'Alteração|Representatives|RepresentativeID|1008')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 14:43:21.800' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1012', N'Alteração|Representatives|RepresentativeID|1012')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:51:08.343' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1025', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1025')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:56:53.080' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'15', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|15')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 20:40:50.497' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1013', N'Alteração|Laboratories|LaboratoryID|1013')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:53.900' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1018', N'Inclusão|OrdersDoctor|OrderDoctorID|1018')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.530' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1026', N'Inclusão|OrdersDoctor|OrderDoctorID|1026')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:09:07.020' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'37', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|37')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:44:34.310' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'49', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|49')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:48:31.537' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'50', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|50')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:55:42.043' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'53', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|53')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-01 23:03:16.560' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'1', N'Inclusão|OrderAvaliable|OrderAvaliableID|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-01 23:04:35.120' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'2', N'Inclusão|OrderAvaliable|OrderAvaliableID|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:33:09.397' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'6', N'Inclusão|OrderAvaliable|OrderAvaliableID|6')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:42:32.080' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'7', N'Inclusão|OrderAvaliable|OrderAvaliableID|7')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:43:47.403' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'5', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|5')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:35.010' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'7', N'Inclusão|ProductsAvaliable|ProductAvaliableID|7')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:45:07.087' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'18', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|18')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-17 20:07:12.937' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'17', N'Inclusão|ProductsAvaliable|ProductAvaliableID|17')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-22 07:34:04.410' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'1016', N'Inclusão|Doctors|DoctorID|1016')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-22 07:34:34.293' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1016', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1016')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-22 08:36:00.140' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1016', N'Alteração|Doctors|DoctorsID|1016')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:41:12.703' AS DateTime), N'joaodokko', N'Inclusão', N'Laboratories', N'LaboratoryID', N'1013', N'Inclusão|Laboratories|LaboratoryID|1013')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 01:48:05.287' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'1019', N'Inclusão|Doctors|DoctorID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 02:10:56.627' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1018', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1018')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:54:21.197' AS DateTime), N'joaodokko', N'Alteração', N'Representatives', N'RepresentativeID', N'1008', N'Alteração|Representatives|RepresentativeID|1008')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:56:37.380' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1012', N'Alteração|Laboratories|LaboratoryID|1012')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 12:28:33.270' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1020', N'Alteração|Doctors|DoctorsID|1020')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:00:08.870' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'14', N'Inclusão|OrderAvaliable|OrderAvaliableID|14')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.370' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'21', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|21')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.663' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'29', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|29')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 22:46:56.353' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'35', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|35')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-01 22:48:45.980' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1054', N'Inclusão|OrdersDoctor|OrderDoctorID|1054')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:24:54.520' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'2', N'Inclusão|ProductsAvaliable|ProductAvaliableID|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:43:47.203' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'2', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:35.637' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'12', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|12')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:45:06.833' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'8', N'Inclusão|ProductsAvaliable|ProductAvaliableID|8')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:33:22.437' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 03:43:08.430' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-23 05:47:01.257' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'9', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|9')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 12:29:12.473' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1020', N'Alteração|Doctors|DoctorsID|1020')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 13:35:19.717' AS DateTime), N'LOGINRPRES', N'Inclusão', N'Users', N'UserLogin', N'LOGINRPRES', N'Inclusão|Users|UserLogin|LOGINRPRES')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:52:25.153' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1026', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1026')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:56:41.637' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'12', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|12')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 18:56:53.120' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'16', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|16')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 20:58:59.213' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'13', N'Inclusão|OrderAvaliable|OrderAvaliableID|13')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.490' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1025', N'Inclusão|OrdersDoctor|OrderDoctorID|1025')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 22:46:56.203' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1032', N'Inclusão|OrdersDoctor|OrderDoctorID|1032')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-29 00:44:16.543' AS DateTime), N'joaodokko', N'Alteração', N'Doctors', N'DoctorsID', N'1019', N'Alteração|Doctors|DoctorsID|1019')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-29 03:34:54.890' AS DateTime), N'joaodokko', N'Inclusão', N'Users', N'UserLogin', N'LAB3', N'Inclusão|Users|UserLogin|LAB3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:09:06.943' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1030', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1030')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:15:26.347' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1039', N'Inclusão|OrdersDoctor|OrderDoctorID|1039')
GO
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:17:47.507' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1040', N'Inclusão|OrdersDoctor|OrderDoctorID|1040')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:20:31.820' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1041', N'Inclusão|OrdersDoctor|OrderDoctorID|1041')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:24:46.603' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1042', N'Inclusão|OrdersDoctor|OrderDoctorID|1042')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:25:53.437' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1043', N'Inclusão|OrdersDoctor|OrderDoctorID|1043')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:27:12.197' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1044', N'Inclusão|OrdersDoctor|OrderDoctorID|1044')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:30:03.270' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1046', N'Inclusão|OrdersDoctor|OrderDoctorID|1046')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:49:10.613' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1051', N'Inclusão|OrdersDoctor|OrderDoctorID|1051')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:49:49.853' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1052', N'Inclusão|OrdersDoctor|OrderDoctorID|1052')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:34.937' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'8', N'Inclusão|OrderAvaliable|OrderAvaliableID|8')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:35.430' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'10', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|10')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:58:24.950' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'20', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|20')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.253' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'20', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|20')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 21:04:54.627' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'28', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|28')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-28 22:46:56.307' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'34', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|34')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-01 22:48:45.863' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1044', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1044')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:35.147' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'7', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|7')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:45:07.207' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'17', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|17')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:58:24.703' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'10', N'Inclusão|OrderAvaliable|OrderAvaliableID|10')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:24:46.490' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'21', N'Inclusão|OrderAvaliable|OrderAvaliableID|21')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:25:53.383' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'22', N'Inclusão|OrderAvaliable|OrderAvaliableID|22')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:27:12.150' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'23', N'Inclusão|OrderAvaliable|OrderAvaliableID|23')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:30:03.217' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'24', N'Inclusão|OrderAvaliable|OrderAvaliableID|24')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:44:29.630' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'25', N'Inclusão|OrderAvaliable|OrderAvaliableID|25')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:48:19.523' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'26', N'Inclusão|OrderAvaliable|OrderAvaliableID|26')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:49:10.563' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1041', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1041')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:49:49.807' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1042', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1042')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-04-30 03:55:41.943' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'29', N'Inclusão|OrderAvaliable|OrderAvaliableID|29')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:43:47.317' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'6', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|6')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:45:06.990' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'14', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|14')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:58:24.897' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'22', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|22')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:35.097' AS DateTime), N'joaodokko', N'Inclusão', N'@OrdersRepresentativeID', N'@OrdersRepresentativeID', N'8', N'Inclusão|@OrdersRepresentativeID|@OrdersRepresentativeID|8')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 00:44:35.550' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'12', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|12')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-02 01:45:07.163' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'19', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|19')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 03:40:05.120' AS DateTime), N'joaodokko', N'Inclusão', N'Laboratories', N'LaboratoryID', N'1014', N'Inclusão|Laboratories|LaboratoryID|1014')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 03:40:05.140' AS DateTime), N'LAB33', N'Inclusão', N'Users', N'UserLogin', N'LAB33', N'Inclusão|Users|UserLogin|LAB33')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 03:40:32.537' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1014', N'Alteração|Laboratories|LaboratoryID|1014')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 03:45:38.177' AS DateTime), N'REP44', N'Inclusão', N'Users', N'UserLogin', N'REP44', N'Inclusão|Users|UserLogin|REP44')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:07:59.687' AS DateTime), N'joaodokko', N'Alteração', N'Users', N'UserId', N'1043', N'Alteração|Users|UserId|1043')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:18:56.383' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'31', N'Inclusão|OrderAvaliable|OrderAvaliableID|31')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:18:56.443' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'55', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|55')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:21:44.300' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'56', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|56')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:22:45.107' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1047', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1047')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:22:45.140' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'57', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|57')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:23:48.667' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'34', N'Inclusão|OrderAvaliable|OrderAvaliableID|34')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:10.673' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1049', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1049')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:13.530' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'59', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|59')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.457' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'62', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|62')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.490' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'63', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|63')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.533' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'65', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|65')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.567' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1063', N'Inclusão|OrdersDoctor|OrderDoctorID|1063')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.610' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1064', N'Inclusão|OrdersDoctor|OrderDoctorID|1064')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.630' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'70', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|70')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.657' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1065', N'Inclusão|OrdersDoctor|OrderDoctorID|1065')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.673' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'72', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|72')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:33:19.730' AS DateTime), N'joaodokko', N'Inclusão', N'Representatives', N'RepresentativeID', N'1014', N'Inclusão|Representatives|RepresentativeID|1014')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:34:23.550' AS DateTime), N'MEDREP2', N'Inclusão', N'Users', N'UserLogin', N'MEDREP2', N'Inclusão|Users|UserLogin|MEDREP2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:35:35.157' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1051', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1051')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:35:35.200' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'73', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|73')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:35:37.130' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'74', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|74')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:35:37.150' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1068', N'Inclusão|OrdersDoctor|OrderDoctorID|1068')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:35:37.163' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'75', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|75')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-11 19:17:26.547' AS DateTime), N'joaodokko', N'Inclusão', N'ReleaseOrder', N'ReleaseOrderId', N'1', N'Inclusão|ReleaseOrder|ReleaseOrderId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-11 19:17:26.567' AS DateTime), N'joaodokko', N'Inclusão', N'ReleaseOrderItem', N'ReleaseOrderItemId', N'1', N'Inclusão|ReleaseOrderItem|ReleaseOrderItemId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-12 12:32:52.140' AS DateTime), N'joaodokko', N'Alteração', N'Patients', N'PatientId', N'11', N'Alteração|Patients|PatientId|11')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-13 02:25:44.523' AS DateTime), N'joaodokko', N'Alteração', N'Patients', N'PatientId', N'1', N'Alteração|Patients|PatientId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-13 02:26:56.357' AS DateTime), N'joaodokko', N'Alteração', N'Patients', N'PatientId', N'12', N'Alteração|Patients|PatientId|12')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-13 02:48:18.397' AS DateTime), N'joaodokko', N'Inclusão', N'ReleaseOrder', N'ReleaseOrderId', N'2', N'Inclusão|ReleaseOrder|ReleaseOrderId|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-13 03:27:30.020' AS DateTime), N'joaodokko', N'Inclusão', N'ReleaseOrderItem', N'ReleaseOrderItemId', N'3', N'Inclusão|ReleaseOrderItem|ReleaseOrderItemId|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-13 04:33:17.680' AS DateTime), N'joaodokko', N'Alteração', N'Patients', N'PatientId', N'3', N'Alteração|Patients|PatientId|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-13 04:41:36.483' AS DateTime), N'joaodokko', N'Inclusão', N'ReleaseOrder', N'ReleaseOrderId', N'4', N'Inclusão|ReleaseOrder|ReleaseOrderId|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-13 04:46:26.177' AS DateTime), N'joaodokko', N'Inclusão', N'Orders', N'OrderId', N'1', N'Inclusão|Orders|OrderId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-14 08:12:36.953' AS DateTime), N'joaodokko', N'Alteração', N'Patients', N'PatientId', N'12', N'Alteração|Patients|PatientId|12')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-14 08:39:47.757' AS DateTime), N'joaodokko', N'Alteração', N'PatientAddresses', N'PatientAddressesId', N'1', N'Alteração|PatientAddresses|PatientAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-21 20:15:51.213' AS DateTime), N'teste', N'Inclusão', N'Medicines', N'MedicineId', N'7', N'Inclusão|Medicines|MedicineId|7')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-27 19:57:21.437' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'37', N'Inclusão|OrderAvaliable|OrderAvaliableID|37')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-27 19:57:53.573' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'76', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|76')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-27 19:58:00.390' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1070', N'Inclusão|OrdersDoctor|OrderDoctorID|1070')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-27 19:58:00.443' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1071', N'Inclusão|OrdersDoctor|OrderDoctorID|1071')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-27 19:58:00.497' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'79', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|79')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-27 19:58:00.517' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1073', N'Inclusão|OrdersDoctor|OrderDoctorID|1073')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 03:40:39.503' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1014', N'Alteração|Laboratories|LaboratoryID|1014')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 03:41:39.793' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1014', N'Alteração|Laboratories|LaboratoryID|1014')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 03:45:38.190' AS DateTime), N'joaodokko', N'Inclusão', N'Representatives', N'RepresentativeID', N'1013', N'Inclusão|Representatives|RepresentativeID|1013')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:18:56.400' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1045', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1045')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:18:56.430' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1055', N'Inclusão|OrdersDoctor|OrderDoctorID|1055')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:21:44.250' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'32', N'Inclusão|OrderAvaliable|OrderAvaliableID|32')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:21:44.290' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1056', N'Inclusão|OrdersDoctor|OrderDoctorID|1056')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:22:45.090' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'33', N'Inclusão|OrderAvaliable|OrderAvaliableID|33')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:23:48.677' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1048', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1048')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:25:12.117' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1058', N'Inclusão|OrdersDoctor|OrderDoctorID|1058')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:10.663' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'35', N'Inclusão|OrderAvaliable|OrderAvaliableID|35')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:10.683' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1050', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1050')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:13.540' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'60', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|60')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.433' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1060', N'Inclusão|OrdersDoctor|OrderDoctorID|1060')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.480' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1061', N'Inclusão|OrdersDoctor|OrderDoctorID|1061')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.500' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'64', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|64')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.540' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'66', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|66')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.573' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'67', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|67')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.620' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'69', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|69')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:30:59.283' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1013', N'Alteração|Laboratories|LaboratoryID|1013')
GO
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:31:49.610' AS DateTime), N'joaodokko', N'Alteração', N'Laboratories', N'LaboratoryID', N'1012', N'Alteração|Laboratories|LaboratoryID|1012')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:33:19.717' AS DateTime), N'REPLAB2', N'Inclusão', N'Users', N'UserLogin', N'REPLAB2', N'Inclusão|Users|UserLogin|REPLAB2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:34:23.570' AS DateTime), N'joaodokko', N'Inclusão', N'Doctors', N'DoctorID', N'1022', N'Inclusão|Doctors|DoctorID|1022')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:35:35.143' AS DateTime), N'joaodokko', N'Inclusão', N'OrderAvaliable', N'OrderAvaliableID', N'36', N'Inclusão|OrderAvaliable|OrderAvaliableID|36')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:35:37.123' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1067', N'Inclusão|OrdersDoctor|OrderDoctorID|1067')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-13 02:26:07.493' AS DateTime), N'joaodokko', N'Alteração', N'Patients', N'PatientId', N'6', N'Alteração|Patients|PatientId|6')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-13 04:01:35.750' AS DateTime), N'joaodokko', N'Alteração', N'PatientAddresses', N'PatientAddressesId', N'1', N'Alteração|PatientAddresses|PatientAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-13 04:41:36.493' AS DateTime), N'joaodokko', N'Inclusão', N'ReleaseOrderItem', N'ReleaseOrderItemId', N'4', N'Inclusão|ReleaseOrderItem|ReleaseOrderItemId|4')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-14 08:24:37.330' AS DateTime), N'joaodokko', N'Inclusão', N'ReleaseOrder', N'ReleaseOrderId', N'1004', N'Inclusão|ReleaseOrder|ReleaseOrderId|1004')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-14 08:24:37.347' AS DateTime), N'joaodokko', N'Inclusão', N'ReleaseOrderItem', N'ReleaseOrderItemId', N'1004', N'Inclusão|ReleaseOrderItem|ReleaseOrderItemId|1004')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-27 19:57:23.290' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1052', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1052')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-27 19:57:53.560' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1069', N'Inclusão|OrdersDoctor|OrderDoctorID|1069')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-27 19:58:00.410' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'77', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|77')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-27 19:58:00.530' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'80', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|80')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:07:11.433' AS DateTime), N'joaodokko', N'Alteração', N'Users', N'UserId', N'1043', N'Alteração|Users|UserId|1043')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:09:33.487' AS DateTime), N'joaodokko', N'Alteração', N'Users', N'UserId', N'1043', N'Alteração|Users|UserId|1043')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:21:44.263' AS DateTime), N'joaodokko', N'Inclusão', N'ProductsAvaliable', N'ProductAvaliableID', N'1046', N'Inclusão|ProductsAvaliable|ProductAvaliableID|1046')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:22:45.130' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1057', N'Inclusão|OrdersDoctor|OrderDoctorID|1057')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:26:00.240' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'58', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|58')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:13.517' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1059', N'Inclusão|OrdersDoctor|OrderDoctorID|1059')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.447' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'61', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|61')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.523' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1062', N'Inclusão|OrdersDoctor|OrderDoctorID|1062')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.583' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'68', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|68')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:29:18.667' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'71', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|71')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-08 04:35:35.187' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1066', N'Inclusão|OrdersDoctor|OrderDoctorID|1066')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-13 04:42:11.343' AS DateTime), N'joaodokko', N'Alteração', N'PatientAddresses', N'PatientAddressesId', N'1', N'Alteração|PatientAddresses|PatientAddressesId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-13 02:48:18.413' AS DateTime), N'joaodokko', N'Inclusão', N'ReleaseOrderItem', N'ReleaseOrderItemId', N'2', N'Inclusão|ReleaseOrderItem|ReleaseOrderItemId|2')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-13 03:27:30.003' AS DateTime), N'joaodokko', N'Inclusão', N'ReleaseOrder', N'ReleaseOrderId', N'3', N'Inclusão|ReleaseOrder|ReleaseOrderId|3')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-13 04:46:26.187' AS DateTime), N'joaodokko', N'Inclusão', N'OrderItem', N'OrderItemId', N'1', N'Inclusão|OrderItem|OrderItemId|1')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-27 19:58:00.480' AS DateTime), N'joaodokko', N'Inclusão', N'OrdersDoctor', N'OrderDoctorID', N'1072', N'Inclusão|OrdersDoctor|OrderDoctorID|1072')
INSERT [dbo].[ProcessLog] ([ProcessLogDate], [ProcessLogUser], [ProcessLogName], [ProcessLogTable], [ProcessLogField], [ProcessLogValue], [Filter]) VALUES (CAST(N'2019-05-27 19:58:00.460' AS DateTime), N'joaodokko', N'Inclusão', N'@ProductsOrderDoctor', N'@ProductsOrderDoctorID', N'78', N'Inclusão|@ProductsOrderDoctor|@ProductsOrderDoctorID|78')
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([productID], [productName], [productCode], [productEnabled], [laboratoryID]) VALUES (1, N'GLIFAGE 500 MG COM 30 COMPRIMIDOS', N'0000062', 1, 1)
INSERT [dbo].[Product] ([productID], [productName], [productCode], [productEnabled], [laboratoryID]) VALUES (2, N'GLIFAGE 850 MG COM 30 COMPRIMIDOS', N'0000065', 1, 1)
INSERT [dbo].[Product] ([productID], [productName], [productCode], [productEnabled], [laboratoryID]) VALUES (3, N'GAZE EMBEBIDA EM ALCOOL 70% SWABS C/100', N'0000069', 1, 1)
INSERT [dbo].[Product] ([productID], [productName], [productCode], [productEnabled], [laboratoryID]) VALUES (4, N'COLETOR PERFURO-CORTANTE 3 LITROS', N'0000070', 1, 1)
SET IDENTITY_INSERT [dbo].[Product] OFF
SET IDENTITY_INSERT [dbo].[ProductsAvaliable] ON 

INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (4, 3, 15, 1, NULL)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (5, 4, 1, 1, NULL)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (6, 4, 10, 1, NULL)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (7, 4, 10, 1, NULL)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (8, 4, 10, 1, NULL)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (9, 1, 20, 1, NULL)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (10, 3, 12, 1, NULL)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (11, 1, 12, 1, NULL)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (12, 1, 4, 1, NULL)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (13, 1, 30, 1, NULL)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (14, 4, 10, 1, NULL)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (15, 1, 30, 1, NULL)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (16, 4, 10, 1, NULL)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (17, 1, 10, 1, NULL)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (18, 1, 10, 1, NULL)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1014, 1, 12, 1, 3)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1015, 3, 10, 1, 4)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1016, 3, 5, 1, 5)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1017, 1, 10, 1, 5)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1018, 1, 10, 1, 6)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1019, 2, 20, 1, 6)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1020, 4, 10, 1, 7)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1021, 2, 12, 1, 7)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1022, 3, 10, 1, 8)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1023, 1, 30, 1, 9)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1024, 3, 20, 1, 10)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1025, 3, 10, 1, 11)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1026, 3, 1, 1, 12)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1027, 1, 10, 1, 13)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1028, 3, 10, 1, 14)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1029, 3, 1, 1, 15)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1030, 3, 10, 1, 16)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1031, 3, 10, 1, 17)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1032, 3, 5, 1, 18)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1033, 1, 10, 1, 19)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1034, 2, 10, 1, 20)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1035, 3, 10, 1, 21)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1036, 3, 10, 1, 22)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1037, 1, 10, 1, 23)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1038, 3, 10, 1, 24)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1039, 1, 10, 1, 25)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1040, 3, 10, 1, 26)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1041, 3, 10, 1, 27)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1042, 3, 2, 1, 28)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1043, 3, 10, 1, 29)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1044, 1, 10, 1, 30)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1045, 2, 10, 1, 31)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1046, 3, 10, 1, 32)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1047, 4, 10, 1, 33)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1048, 1, 10, 1, 34)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1049, 3, 10, 1, 35)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1050, 2, 3, 1, 35)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1051, 3, 12, 1, 36)
INSERT [dbo].[ProductsAvaliable] ([ProductAvaliableID], [ProductID], [Qty], [ProductsAvaliableEnabled], [OrderAvaliableID]) VALUES (1052, 3, 10, 1, 37)
SET IDENTITY_INSERT [dbo].[ProductsAvaliable] OFF
SET IDENTITY_INSERT [dbo].[ProductsAvaliableRepresentative] ON 

INSERT [dbo].[ProductsAvaliableRepresentative] ([ProductsAvaliableRepresentativeID], [ProductID], [Qty], [ProductsAvaliableRepresentativeEnabled], [OrderAvaliableRepresentativeID]) VALUES (1, 1, 20, 1, 3)
INSERT [dbo].[ProductsAvaliableRepresentative] ([ProductsAvaliableRepresentativeID], [ProductID], [Qty], [ProductsAvaliableRepresentativeEnabled], [OrderAvaliableRepresentativeID]) VALUES (2, 2, 10, 1, 4)
INSERT [dbo].[ProductsAvaliableRepresentative] ([ProductsAvaliableRepresentativeID], [ProductID], [Qty], [ProductsAvaliableRepresentativeEnabled], [OrderAvaliableRepresentativeID]) VALUES (3, 3, 10, 1, 5)
INSERT [dbo].[ProductsAvaliableRepresentative] ([ProductsAvaliableRepresentativeID], [ProductID], [Qty], [ProductsAvaliableRepresentativeEnabled], [OrderAvaliableRepresentativeID]) VALUES (4, 3, 10, 1, 6)
INSERT [dbo].[ProductsAvaliableRepresentative] ([ProductsAvaliableRepresentativeID], [ProductID], [Qty], [ProductsAvaliableRepresentativeEnabled], [OrderAvaliableRepresentativeID]) VALUES (5, 1, 10, 1, 7)
INSERT [dbo].[ProductsAvaliableRepresentative] ([ProductsAvaliableRepresentativeID], [ProductID], [Qty], [ProductsAvaliableRepresentativeEnabled], [OrderAvaliableRepresentativeID]) VALUES (6, 3, 3, 1, 8)
INSERT [dbo].[ProductsAvaliableRepresentative] ([ProductsAvaliableRepresentativeID], [ProductID], [Qty], [ProductsAvaliableRepresentativeEnabled], [OrderAvaliableRepresentativeID]) VALUES (7, 2, 10, 1, 8)
INSERT [dbo].[ProductsAvaliableRepresentative] ([ProductsAvaliableRepresentativeID], [ProductID], [Qty], [ProductsAvaliableRepresentativeEnabled], [OrderAvaliableRepresentativeID]) VALUES (8, 3, 10, 1, 9)
INSERT [dbo].[ProductsAvaliableRepresentative] ([ProductsAvaliableRepresentativeID], [ProductID], [Qty], [ProductsAvaliableRepresentativeEnabled], [OrderAvaliableRepresentativeID]) VALUES (9, 3, 5, 1, 10)
SET IDENTITY_INSERT [dbo].[ProductsAvaliableRepresentative] OFF
SET IDENTITY_INSERT [dbo].[ProductsOrderDoctor] ON 

INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (1, 1, 10, 1006)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (2, 2, 20, 1006)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (3, 1, 10, 1007)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (4, 2, 20, 1007)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (5, 1, 10, 1008)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (6, 2, 20, 1008)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (7, 1, 10, 1009)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (8, 2, 20, 1009)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (9, 1, 10, 1010)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (10, 2, 20, 1010)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (11, 3, 10, 1011)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (12, 3, 10, 1012)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (13, 3, 10, 1013)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (14, 3, 20, 1014)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (15, 3, 20, 1015)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (16, 3, 20, 1016)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (17, 3, 20, 1017)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (18, 3, 10, 1018)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (19, 3, 10, 1019)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (20, 3, 10, 1020)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (21, 3, 10, 1021)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (22, 3, 10, 1022)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (23, 3, 10, 1023)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (24, 3, 10, 1024)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (25, 3, 10, 1025)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (26, 3, 10, 1026)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (27, 3, 10, 1027)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (28, 3, 10, 1028)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (29, 3, 10, 1029)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (30, 3, 10, 1030)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (31, 3, 10, 1031)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (32, 3, 1, 1032)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (33, 3, 1, 1033)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (34, 3, 1, 1034)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (35, 3, 1, 1035)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (36, 3, 1, 1036)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (37, 3, 10, 1037)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (38, 3, 10, 1038)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (39, 3, 5, 1039)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (40, 1, 10, 1040)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (41, 2, 10, 1041)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (42, 3, 10, 1042)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (43, 3, 10, 1043)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (44, 1, 10, 1044)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (45, 1, 10, 1045)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (46, 3, 10, 1046)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (47, 3, 10, 1047)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (48, 3, 10, 1048)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (49, 1, 10, 1049)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (50, 3, 10, 1050)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (51, 3, 10, 1051)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (52, 3, 2, 1052)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (53, 3, 10, 1053)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (54, 1, 10, 1054)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (55, 2, 10, 1055)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (56, 3, 10, 1056)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (57, 4, 10, 1057)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (58, 1, 10, 1058)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (59, 3, 10, 1059)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (60, 2, 3, 1059)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (61, 3, 10, 1060)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (62, 2, 3, 1060)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (63, 3, 10, 1061)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (64, 2, 3, 1061)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (65, 3, 10, 1062)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (66, 2, 3, 1062)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (67, 3, 10, 1063)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (68, 2, 3, 1063)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (69, 3, 10, 1064)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (70, 2, 3, 1064)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (71, 3, 10, 1065)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (72, 2, 3, 1065)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (73, 3, 12, 1066)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (74, 3, 12, 1067)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (75, 3, 12, 1068)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (76, 3, 10, 1069)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (77, 3, 10, 1070)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (78, 3, 10, 1071)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (79, 3, 10, 1072)
INSERT [dbo].[ProductsOrderDoctor] ([ProductsOrderDoctorID], [ProductID], [Qty], [OrdersDoctorID]) VALUES (80, 3, 10, 1073)
SET IDENTITY_INSERT [dbo].[ProductsOrderDoctor] OFF
SET IDENTITY_INSERT [dbo].[ProductsOrderRepresentative] ON 

INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (1, 1, 10, 3)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (2, 1, 10, 4)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (3, 1, 10, 5)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (4, 1, 10, 6)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (5, 1, 10, 7)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (6, 3, 3, 8)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (7, 2, 10, 8)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (8, 3, 3, 9)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (9, 2, 10, 9)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (10, 3, 3, 10)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (11, 2, 10, 10)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (12, 3, 3, 11)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (13, 2, 10, 11)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (14, 3, 3, 12)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (15, 2, 10, 12)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (16, 3, 10, 13)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (17, 3, 10, 14)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (18, 3, 10, 15)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (19, 3, 10, 16)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (20, 3, 10, 17)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (21, 3, 5, 18)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (22, 3, 5, 19)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (23, 3, 5, 20)
INSERT [dbo].[ProductsOrderRepresentative] ([ProductsOrderDoctorRepresentativeID], [ProductID], [Qty], [OrdersRepresentativeID]) VALUES (24, 3, 5, 21)
SET IDENTITY_INSERT [dbo].[ProductsOrderRepresentative] OFF
SET IDENTITY_INSERT [dbo].[ReleaseOrder] ON 

INSERT [dbo].[ReleaseOrder] ([ReleaseOrderId], [ReleaseOrderDate], [PatientId], [PatientAuthorization], [DispensationId], [ReleaseOrderStatus], [ReleaseOrderObservation], [ReleaseOrderEnabled]) VALUES (1, CAST(N'2019-05-11 19:17:26.523' AS DateTime), 6, N'XPTO45', 3, N'P', N'OBSERVAÇÃO', 1)
INSERT [dbo].[ReleaseOrder] ([ReleaseOrderId], [ReleaseOrderDate], [PatientId], [PatientAuthorization], [DispensationId], [ReleaseOrderStatus], [ReleaseOrderObservation], [ReleaseOrderEnabled]) VALUES (2, CAST(N'2019-05-13 02:48:18.367' AS DateTime), 6, N'45645', 3, N'P', NULL, 1)
INSERT [dbo].[ReleaseOrder] ([ReleaseOrderId], [ReleaseOrderDate], [PatientId], [PatientAuthorization], [DispensationId], [ReleaseOrderStatus], [ReleaseOrderObservation], [ReleaseOrderEnabled]) VALUES (3, CAST(N'2019-05-13 03:27:29.990' AS DateTime), 3, N'4765456', 3, N'P', NULL, 1)
INSERT [dbo].[ReleaseOrder] ([ReleaseOrderId], [ReleaseOrderDate], [PatientId], [PatientAuthorization], [DispensationId], [ReleaseOrderStatus], [ReleaseOrderObservation], [ReleaseOrderEnabled]) VALUES (4, CAST(N'2019-05-13 04:41:36.470' AS DateTime), 12, N'158555', 1, N'P', NULL, 1)
INSERT [dbo].[ReleaseOrder] ([ReleaseOrderId], [ReleaseOrderDate], [PatientId], [PatientAuthorization], [DispensationId], [ReleaseOrderStatus], [ReleaseOrderObservation], [ReleaseOrderEnabled]) VALUES (1004, CAST(N'2019-05-14 08:24:37.303' AS DateTime), 3, N'3466990', 3, N'P', NULL, 1)
SET IDENTITY_INSERT [dbo].[ReleaseOrder] OFF
SET IDENTITY_INSERT [dbo].[ReleaseOrderItem] ON 

INSERT [dbo].[ReleaseOrderItem] ([ReleaseOrderItemId], [ReleaseOrderId], [MedicineId], [Dosage], [Quantity], [ReleaseOrderItemEnabled]) VALUES (1, 1, 4, 10, 20, 1)
INSERT [dbo].[ReleaseOrderItem] ([ReleaseOrderItemId], [ReleaseOrderId], [MedicineId], [Dosage], [Quantity], [ReleaseOrderItemEnabled]) VALUES (2, 2, 5, 10, 20, 1)
INSERT [dbo].[ReleaseOrderItem] ([ReleaseOrderItemId], [ReleaseOrderId], [MedicineId], [Dosage], [Quantity], [ReleaseOrderItemEnabled]) VALUES (3, 3, 4, 5, 6, 1)
INSERT [dbo].[ReleaseOrderItem] ([ReleaseOrderItemId], [ReleaseOrderId], [MedicineId], [Dosage], [Quantity], [ReleaseOrderItemEnabled]) VALUES (4, 4, 5, 10, 15, 1)
INSERT [dbo].[ReleaseOrderItem] ([ReleaseOrderItemId], [ReleaseOrderId], [MedicineId], [Dosage], [Quantity], [ReleaseOrderItemEnabled]) VALUES (1004, 1004, 4, 34, 12, 1)
SET IDENTITY_INSERT [dbo].[ReleaseOrderItem] OFF
SET IDENTITY_INSERT [dbo].[Representatives] ON 

INSERT [dbo].[Representatives] ([RepresentativeID], [RepresentativeName], [email], [login], [password], [Status], [Comments], [RepresentativeEnabled], [laboratoryID], [cpf], [address], [addressNumber], [complement], [city], [Neighborhood], [stateID], [zipcode], [phoneComercial], [ramal], [cellPhone]) VALUES (1008, N'REPRESENTANTE LAB1', N'EMAILR@TETSE.COM', N'REPRES1', N'99a29dc8105fd2fa39d8cdc04733938d', N'1', NULL, 1, 1012, N'01321231231', N'LOGRADOURO', N'NUMEO', N'4564561', N'5446S5AFD', N'BAIRRO', 4, N'09755522', N'12136545645', NULL, N'11111111111')
INSERT [dbo].[Representatives] ([RepresentativeID], [RepresentativeName], [email], [login], [password], [Status], [Comments], [RepresentativeEnabled], [laboratoryID], [cpf], [address], [addressNumber], [complement], [city], [Neighborhood], [stateID], [zipcode], [phoneComercial], [ramal], [cellPhone]) VALUES (1009, N'REPRESENTANTE LAB2', N'EMAIL@TSTE.COM', N'REPRES2', N'85ee0fe4f155a9af2657d85054ad63ca', N'1', NULL, 1, 1013, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Representatives] ([RepresentativeID], [RepresentativeName], [email], [login], [password], [Status], [Comments], [RepresentativeEnabled], [laboratoryID], [cpf], [address], [addressNumber], [complement], [city], [Neighborhood], [stateID], [zipcode], [phoneComercial], [ramal], [cellPhone]) VALUES (1010, N'REPRESENTANTE 3 LAB 1', N'EMAIL@LAB.COM', N'REPRES3', N'99a29dc8105fd2fa39d8cdc04733938d', N'1', NULL, 1, 1012, N'21321321231', N'LOGRA', N'NUM', N'545632', N'CIDADE', N'BAIRRO', 4, N'46546546', NULL, NULL, NULL)
INSERT [dbo].[Representatives] ([RepresentativeID], [RepresentativeName], [email], [login], [password], [Status], [Comments], [RepresentativeEnabled], [laboratoryID], [cpf], [address], [addressNumber], [complement], [city], [Neighborhood], [stateID], [zipcode], [phoneComercial], [ramal], [cellPhone]) VALUES (1011, N'REPRESENTANTE 4', N'EMAIL@COM.COM', N'REPRES4', N'99a29dc8105fd2fa39d8cdc04733938d', N'1', NULL, 1, 1013, N'12585585555', N'LOGRA', N'1552', NULL, N'CIDADE', N'BAIRRO', 24, N'09985655', N'1255855555', NULL, N'188995555')
INSERT [dbo].[Representatives] ([RepresentativeID], [RepresentativeName], [email], [login], [password], [Status], [Comments], [RepresentativeEnabled], [laboratoryID], [cpf], [address], [addressNumber], [complement], [city], [Neighborhood], [stateID], [zipcode], [phoneComercial], [ramal], [cellPhone]) VALUES (1012, N'NOME REPRESENTANTE XXX', N'EMAILSS@EMAIL.COM', N'LOGINREP2', N'85ee0fe4f155a9af2657d85054ad63ca', N'1', NULL, 1, 1012, N'12356456456', N'LOGRADOURO', N'4522', N'COMP', N'CIDADE', N'BAIRRO', 24, N'09784552', N'44444444444', NULL, NULL)
INSERT [dbo].[Representatives] ([RepresentativeID], [RepresentativeName], [email], [login], [password], [Status], [Comments], [RepresentativeEnabled], [laboratoryID], [cpf], [address], [addressNumber], [complement], [city], [Neighborhood], [stateID], [zipcode], [phoneComercial], [ramal], [cellPhone]) VALUES (1013, N'REP44', N'EMAIL@REP4.COM', N'REP44', N'99a29dc8105fd2fa39d8cdc04733938d', N'1', NULL, 1, 1014, N'13213212313', N'LOG', N'123', NULL, N'CIDDE', N'BAIRRO', 25, N'09785552', NULL, NULL, NULL)
INSERT [dbo].[Representatives] ([RepresentativeID], [RepresentativeName], [email], [login], [password], [Status], [Comments], [RepresentativeEnabled], [laboratoryID], [cpf], [address], [addressNumber], [complement], [city], [Neighborhood], [stateID], [zipcode], [phoneComercial], [ramal], [cellPhone]) VALUES (1014, N'REP LAB 2', N'EMAIL@REP.COM', N'REPLAB2', N'99a29dc8105fd2fa39d8cdc04733938d', N'1', NULL, 1, 1013, N'54645644654', N'LOG', N'234''', NULL, N'CIDADE', N'BAIRRO', 24, N'12348555', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Representatives] OFF
SET IDENTITY_INSERT [dbo].[States] ON 

INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (1, N'ACRE', N'AC', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (2, N'ALAGOAS', N'AL', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (3, N'AMAZONAS', N'AM', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (4, N'AMAPÁ', N'AP', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (5, N'BAHIA', N'BA', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (6, N'CEARÁ', N'CE', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (7, N'DISTRITO FEDERAL', N'DF', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (8, N'ESPÍRITO SANTO', N'ES', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (9, N'GOIÁS', N'GO', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (10, N'MARANHÃO', N'MA', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (11, N'MINAS GERAIS', N'MG', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (12, N'MATO GROSSO DO SUL', N'MS', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (13, N'MATO GROSSO', N'MT', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (14, N'PARÁ', N'PA', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (15, N'PARAÍBA', N'PB', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (16, N'PERNAMBUCO', N'PE', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (17, N'PIAUÍ', N'PI', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (18, N'PARANÁ', N'PR', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (19, N'RIO DE JANEIRO', N'RJ', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (20, N'RIO GRANDE DO NORTE', N'RN', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (21, N'RONDÔNIA', N'RO', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (22, N'RORAIMA', N'RR', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (23, N'RIO GRANDE DO SUL', N'RS', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (24, N'SANTA CATARINA', N'SC', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (25, N'SERGIPE', N'SE', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (26, N'SÃO PAULO', N'SP', 1, NULL)
INSERT [dbo].[States] ([StateId], [StateName], [StateUF], [StateEnabled], [Filter]) VALUES (27, N'TOCANTINS', N'TO', 1, NULL)
SET IDENTITY_INSERT [dbo].[States] OFF
SET IDENTITY_INSERT [dbo].[Systems] ON 

INSERT [dbo].[Systems] ([SystemId], [SystemName]) VALUES (1, N'YouVita-Assistência')
INSERT [dbo].[Systems] ([SystemId], [SystemName]) VALUES (2, N'YouVita-Amostras')
INSERT [dbo].[Systems] ([SystemId], [SystemName]) VALUES (3, N'YouVita-Pesquisa')
SET IDENTITY_INSERT [dbo].[Systems] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1, N'Admin', N'ADMIN@TESTE.COM', N'99a29dc8105fd2fa39d8cdc04733938d', 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1030, N'LABORATORIO 1', N'LAB1', N'99a29dc8105fd2fa39d8cdc04733938d', 1, 3, N'LABORATORIO 1|LAB1', 0, 1012)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1031, N'LABORATORIO 2', N'LAB2', N'99a29dc8105fd2fa39d8cdc04733938d', 1, 3, N'LABORATORIO 2|LAB2', 0, 1013)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1032, N'REPRESENTANTE LAB1', N'REPRES1', N'99a29dc8105fd2fa39d8cdc04733938d', 1, 5, N'REPRESENTANTE LAB1|REPRES1', 0, 1012)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1033, N'REPRESENTANTE LAB2', N'REPRES2', N'85ee0fe4f155a9af2657d85054ad63ca', 1, 5, N'REPRESENTANTE LAB2|REPRES2', 0, 1013)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1034, N'MEDICO LAB 1', N'MEDICO1', N'99a29dc8105fd2fa39d8cdc04733938d', 1, 4, N'MEDICO LAB 1|MEDICO1', 0, 1012)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1035, N'MEDICO 2 LAB 1', N'MEDICO2', N'99a29dc8105fd2fa39d8cdc04733938d', 1, 4, N'MEDICO 2 LAB 1|MEDICO2', 0, 1012)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1036, N'REPRESENTANTE 3 LAB 1', N'REPRES3', N'99a29dc8105fd2fa39d8cdc04733938d', 1, 5, N'REPRESENTANTE 3 LAB 1|REPRES3', 0, 1012)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1037, N'REPRESENTANTE 4', N'REPRES4', N'99a29dc8105fd2fa39d8cdc04733938d', 1, 5, N'REPRESENTANTE 4|REPRES4', 0, 1013)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1038, N'NOME REPSER', N'LOGINRPRES', N'99a29dc8105fd2fa39d8cdc04733938d', 1, 5, N'NOME REPSER|LOGINRPRES', 0, 1012)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1039, N'NOME REPRESENTANTE 3', N'LOGINREP', N'85ee0fe4f155a9af2657d85054ad63ca', 1, 5, N'NOME REPRESENTANTE 3|LOGINREP', 0, 1012)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1040, N'NOME REPRESENTANTE', N'LOGINREP2', N'85ee0fe4f155a9af2657d85054ad63ca', 1, 5, N'NOME REPRESENTANTE|LOGINREP2', 0, 1012)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1041, N'NOVO MEDICO DOMINGO', N'MEDICO3', N'99a29dc8105fd2fa39d8cdc04733938d', 1, 4, N'NOVO MEDICO DOMINGO|MEDICO3', 0, 1012)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1042, N'NOME USUAIRO', N'LAB3', N'85ee0fe4f155a9af2657d85054ad63ca', 1, 3, N'NOME USUAIRO|LAB3', 0, 1013)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1043, N'LAB3', N'LAB33', N'99a29dc8105fd2fa39d8cdc04733938d', 1, 3, N'LAB3|LAB33', 0, 1014)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1044, N'REP44', N'REP44', N'99a29dc8105fd2fa39d8cdc04733938d', 1, 5, N'REP44|REP44', 0, 1014)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1045, N'REP LAB 2', N'REPLAB2', N'99a29dc8105fd2fa39d8cdc04733938d', 1, 5, N'REP LAB 2|REPLAB2', 0, 1013)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1046, N'MEDICO DO REP 2 LABORATORIO 2', N'MEDREP2', N'TESTE', 1, 4, N'MEDICO DO REP 2 LABORATORIO 2|MEDREP2', 0, 1013)
INSERT [dbo].[Users] ([UserId], [UserName], [UserLoginName], [UserPassword], [UserEnabled], [UserType], [Filter], [ClientId], [laboratoryID]) VALUES (1047, N'AdminAssist', N'AdminAssist', N'99a29dc8105fd2fa39d8cdc04733938d', 1, 1, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Users] OFF
SET IDENTITY_INSERT [dbo].[UserType] ON 

INSERT [dbo].[UserType] ([UserTypeID], [UserDescription]) VALUES (1, N'Admin')
INSERT [dbo].[UserType] ([UserTypeID], [UserDescription]) VALUES (2, N'Cliente')
INSERT [dbo].[UserType] ([UserTypeID], [UserDescription]) VALUES (3, N'Laboratório')
INSERT [dbo].[UserType] ([UserTypeID], [UserDescription]) VALUES (4, N'Médico')
INSERT [dbo].[UserType] ([UserTypeID], [UserDescription]) VALUES (5, N'Representante')
INSERT [dbo].[UserType] ([UserTypeID], [UserDescription]) VALUES (6, N'Operador YouVita')
SET IDENTITY_INSERT [dbo].[UserType] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [uq_DoctorLogin]    Script Date: 30/05/2019 20:52:52 ******/
ALTER TABLE [dbo].[Doctors] ADD  CONSTRAINT [uq_DoctorLogin] UNIQUE NONCLUSTERED 
(
	[login] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [uq_RepresentativeEmail]    Script Date: 30/05/2019 20:52:52 ******/
ALTER TABLE [dbo].[Representatives] ADD  CONSTRAINT [uq_RepresentativeEmail] UNIQUE NONCLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [uq_RepresentativeLogin]    Script Date: 30/05/2019 20:52:52 ******/
ALTER TABLE [dbo].[Representatives] ADD  CONSTRAINT [uq_RepresentativeLogin] UNIQUE NONCLUSTERED 
(
	[login] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [uq_UserLoginName]    Script Date: 30/05/2019 20:52:52 ******/
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [uq_UserLoginName] UNIQUE NONCLUSTERED 
(
	[UserLoginName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[spDel_Clients]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spDel_Clients]
(
    @UserLogin varchar(40),
	@ClientId Integer
)
AS
BEGIN

Update Clients set ClientsEnabled = 0 where ClientId = @ClientId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'Clients', 
	@ProcessLogField = 'ClientId', 
	@ProcessLogValue = @ClientId

END

GO
/****** Object:  StoredProcedure [dbo].[spDel_ClientsContacts]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spDel_ClientsContacts]
(
    @UserLogin varchar(40),
	@ContactId Integer
)
AS
BEGIN

Update ClientsContacts set ClientsContactsEnabled = 0 where ContactId = @ContactId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'ClientsContacts', 
	@ProcessLogField = 'ContactId', 
	@ProcessLogValue = @ContactId

END

GO
/****** Object:  StoredProcedure [dbo].[spDel_Configurations]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spDel_Configurations]
(
    @UserLogin varchar(40),
	@ConfigurationId Integer
)
AS
BEGIN

Update Configurations set ConfigurationEnabled = 0 where ConfigurationId = @ConfigurationId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'Configurations', 
	@ProcessLogField = 'ConfigurationId', 
	@ProcessLogValue = @ConfigurationId

END

GO
/****** Object:  StoredProcedure [dbo].[spDel_DoctorAddresses]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spDel_DoctorAddresses]
(
    @UserLogin varchar(40),
	@DoctorAddressesId Integer
)
AS
BEGIN

Update DoctorAddresses set DoctorAddressesEnabled = 0 where DoctorAddressesId = @DoctorAddressesId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'DoctorAddresses', 
	@ProcessLogField = 'DoctorAddressesId', 
	@ProcessLogValue = @DoctorAddressesId

END


GO
/****** Object:  StoredProcedure [dbo].[spDel_Doctors]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spDel_Doctors]
(
    @UserLogin varchar(40),
	@DoctorID Integer
)
AS
BEGIN

Update Doctors set DoctorEnabled = 0 where DoctorID = @DoctorID


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'Doctors', 
	@ProcessLogField = 'DoctorID', 
	@ProcessLogValue = @DoctorID

END

GO
/****** Object:  StoredProcedure [dbo].[spDel_Laboratories]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spDel_Laboratories]
(
    @UserLogin varchar(40),
	@LaboratoryId Integer
)
AS
BEGIN

Update Laboratories set LaboratoryEnabled = 0 where LaboratoryId = @LaboratoryId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'Laboratories', 
	@ProcessLogField = 'LaboratoryId', 
	@ProcessLogValue = @LaboratoryId

END

GO
/****** Object:  StoredProcedure [dbo].[spDel_Medicines]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spDel_Medicines]
(
    @UserLogin varchar(40),
	@MedicineId Integer
)
AS
BEGIN

Update Medicines set MedicineEnabled = 0 where MedicineId = @MedicineId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'Medicines', 
	@ProcessLogField = 'MedicineId', 
	@ProcessLogValue = @MedicineId

END




GO
/****** Object:  StoredProcedure [dbo].[spDel_OrderItem]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spDel_OrderItem]
(
    @UserLogin varchar(40),
	@OrderItemId Integer
)
AS
BEGIN

Update OrderItem set OrderItemEnabled = 0 where OrderItemId = @OrderItemId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'OrderItem', 
	@ProcessLogField = 'OrderItemId', 
	@ProcessLogValue = @OrderItemId

END
GO
/****** Object:  StoredProcedure [dbo].[spDel_Orders]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spDel_Orders]
(
    @UserLogin varchar(40),
	@OrderId Integer
)
AS
BEGIN

Update Orders set OrdersEnabled = 0 where OrderId = @OrderId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'Orders', 
	@ProcessLogField = 'OrderId', 
	@ProcessLogValue = @OrderId

END
GO
/****** Object:  StoredProcedure [dbo].[spDel_OrdersDoctor]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spDel_OrdersDoctor]
(
    @UserLogin varchar(40),
	@OrderDoctorID Integer
)
AS
BEGIN

Update OrdersDoctor set OrderDoctorEnabled = 0 where OrderDoctorID = @OrderDoctorID


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'OrdersDoctor', 
	@ProcessLogField = 'OrderDoctorID', 
	@ProcessLogValue = @OrderDoctorID

END

GO
/****** Object:  StoredProcedure [dbo].[spDel_PatientAddresses]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spDel_PatientAddresses]
(
    @UserLogin varchar(40),
	@PatientAddressId Integer
)
AS
BEGIN

Update PatientAddresses set PatientAddressesEnabled = 0 where PatientAddressesId = @PatientAddressId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'PatientAddresses', 
	@ProcessLogField = 'PatientAddressesId', 
	@ProcessLogValue = @PatientAddressId

END

GO
/****** Object:  StoredProcedure [dbo].[spDel_PatientPathologies]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[spDel_PatientPathologies]
(
    @UserLogin varchar(40),
	@PatientPathologiesId Integer
)
AS
BEGIN

Update PatientPathologies set PatientPathologiesEnabled = 0 where PatientPathologiesId = @PatientPathologiesId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'PatientPathologies', 
	@ProcessLogField = 'PatientPathologiesId', 
	@ProcessLogValue = @PatientPathologiesId

END

GO
/****** Object:  StoredProcedure [dbo].[spDel_PatientPhones]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spDel_PatientPhones]
(
    @UserLogin varchar(40),
	@PatientPhoneId Integer
)
AS
BEGIN


Update PatientPhones set PatientPhonesEnabled = 0 where PatientPhoneId = @PatientPhoneId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'PatientPhones', 
	@ProcessLogField = 'PatientPhoneId', 
	@ProcessLogValue = @PatientPhoneId

END

GO
/****** Object:  StoredProcedure [dbo].[spDel_Patients]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spDel_Patients]
(
    @UserLogin varchar(40),
	@PatientId Integer
)
AS
BEGIN

Update Patients set PatientEnabled = 0 where PatientId = @PatientId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'Patients', 
	@ProcessLogField = 'PatientId', 
	@ProcessLogValue = @PatientId

END

GO
/****** Object:  StoredProcedure [dbo].[spDel_Permissions]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[spDel_Permissions]
(
    @UserLogin varchar(40),
	@PermissionId Integer
)
AS
BEGIN

Update Permissions set PermissionEnabled = 0 where PermissionId = @PermissionId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'Permissions', 
	@ProcessLogField = 'PermissionId', 
	@ProcessLogValue = @PermissionId

END


GO
/****** Object:  StoredProcedure [dbo].[spDel_ReleaseOrder]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spDel_ReleaseOrder]
(
    @UserLogin varchar(40),
	@ReleaseOrderId Integer
)
AS
BEGIN

Update ReleaseOrder set ReleaseOrderEnabled = 0 where ReleaseOrderId = @ReleaseOrderId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'ReleaseOrder', 
	@ProcessLogField = 'ReleaseOrderId', 
	@ProcessLogValue = @ReleaseOrderId

END
GO
/****** Object:  StoredProcedure [dbo].[spDel_ReleaseOrderItem]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spDel_ReleaseOrderItem]
(
    @UserLogin varchar(40),
	@ReleaseOrderItemID Integer
)
AS
BEGIN

Update ReleaseOrderItem set ReleaseOrderItemEnabled = 0 where ReleaseOrderItemID = @ReleaseOrderItemID


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'ReleaseOrderItem', 
	@ProcessLogField = 'ReleaseOrderItemID', 
	@ProcessLogValue = @ReleaseOrderItemID

END
GO
/****** Object:  StoredProcedure [dbo].[spDel_Representatives]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spDel_Representatives]
(
    @UserLogin varchar(40),
	@RepresentativeId Integer
)
AS
BEGIN

Update Representatives set RepresentativeEnabled = 0 where RepresentativeId = @RepresentativeId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'Representatives', 
	@ProcessLogField = 'RepresentativeId', 
	@ProcessLogValue = @RepresentativeId

END

GO
/****** Object:  StoredProcedure [dbo].[spDel_Users]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spDel_Users]
(
    @UserLogin varchar(40),
	@UserId Integer
)
AS
BEGIN

Update Users set UserEnabled = 0 where UserId = @UserId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclusão', 
	@ProcessLogTable = 'Users', 
	@ProcessLogField = 'UserId', 
	@ProcessLogValue = @UserId

END

GO
/****** Object:  StoredProcedure [dbo].[spIns_Clients]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spIns_Clients]
(
	@UserLogin varchar(40),
	@ClientId integer output,
	@ClientName varchar(60),
	@ClientFantasyName varchar(60),
	@CNPJ varchar(14),
	@Address varchar(60),
	@AddressNumber varchar(10),
	@Neighborhood varchar(50),
	@City varchar(50),
    @ZipCode varchar(9),
	@StateId integer, 
	@ResponsibleContact varchar(50),
	@Occupation varchar(50),
	@Email varchar(60),
	@Phone varchar(20),
	@Ramal varchar(10),
    @CellPhone varchar(20),
	@ClientLogin varchar(60),
	@ClientPassword varchar(20),
	@ConfirmPassword varchar(20),	
	@ClientTypeId Integer,
	@ClientsEnabled integer,
	@ClientStatus varchar(1),
	@Comments varchar(200)
)
AS
BEGIN

INSERT INTO [dbo].[Clients]
           ([ClientName]
           ,[ClientFantasyName]
           ,[CNPJ]
           ,[Address]
           ,[AddressNumber]
           ,[Neighborhood]
           ,[City]
           ,[ZipCode]
           ,[StateId]
           ,[ResponsibleContact]
           ,[Occupation]
           ,[Email]
           ,[Phone]
           ,[Ramal]
           ,[CellPhone]
           ,[ClientLogin]
           ,[ClientPassword]
           ,[ConfirmPassword]
           ,[ClientTypeId]
           ,[ClientStatus]
           ,[Comments]
           ,[ClientsEnabled])
     VALUES
    (
	@ClientName,
	@ClientFantasyName,
	@CNPJ,
	@Address,
	@AddressNumber,
	@Neighborhood,
	@City,
    @ZipCode,
	@StateId,
	@ResponsibleContact,
	@Occupation,
	@Email,
	@Phone,
	@Ramal,
    @CellPhone,
	@ClientLogin,
	@ClientPassword,
	@ConfirmPassword,
	@ClientTypeId,
	@ClientStatus,		
	@Comments,
	@ClientsEnabled)



select @ClientId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'Clients', 
	@ProcessLogField = 'ClientId', 
	@ProcessLogValue = @ClientId


END

GO
/****** Object:  StoredProcedure [dbo].[spIns_ClientsContacts]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spIns_ClientsContacts]
(
	@UserLogin varchar(40),
	@ClientId int,
	@ContactId integer output,	
    @ContactName varchar(40),
    @Email varchar(100), 
    @Password varchar(20),
    @Occupation varchar(40),
    @Phone varchar(20),
    @Ramal varchar(8),
    @CellPhone varchar(20),
    @ContactTypeId int,
    @ClientsContactsEnabled int
)
AS
BEGIN

INSERT INTO [dbo].[ClientsContacts]
           ([ClientId]
           ,[ContactName]
           ,[Email]
           ,[Password]
           ,[Occupation]
           ,[Phone]
           ,[Ramal]
           ,[CellPhone]
           ,[ContactTypeId]
           ,[ClientsContactsEnabled])
     VALUES
   (@ClientId,
    @ContactName,
    @Email, 
    @Password,
    @Occupation,
    @Phone,
    @Ramal,
    @CellPhone,
    @ContactTypeId,
    @ClientsContactsEnabled)



Select @ContactId = @@IDENTITY


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'ClientsContacts', 
	@ProcessLogField = 'ContactId', 
	@ProcessLogValue = @ContactId

END

GO
/****** Object:  StoredProcedure [dbo].[spIns_Configurations]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spIns_Configurations]
(
	@UserLogin varchar(40),
	@ConfigurationId Integer output,
	@ConfigurationName varchar(100),
	@ConfigurationValue varchar(400),
	@ConfigurationEnabled Integer
)
AS
BEGIN

Insert Into Configurations (ConfigurationName,ConfigurationValue,ConfigurationEnabled, Filter)  values (@ConfigurationName,@ConfigurationValue,@ConfigurationEnabled, @ConfigurationName)

select @ConfigurationId = @@Identity

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'Configurations', 
	@ProcessLogField = 'ConfigurationId', 
	@ProcessLogValue = @ConfigurationId

END



GO
/****** Object:  StoredProcedure [dbo].[spIns_DoctorAddresses]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[spIns_DoctorAddresses]
(
	@UserLogin varchar(40),
	@DoctorAddressesId int output,
	@DoctorId int,
	@AddressTypeId int,
	@Address varchar(60),
	@AddressNumber varchar(10),
	@City varchar(50),
	@Neighborhood varchar(50),
	@StateId integer, 
	@ZipCode varchar(8),
	@Complement varchar(200),
	@DoctorAddressesEnabled int
)
AS
BEGIN


Insert Into DoctorAddresses
 (DoctorId,	AddressTypeId,	Address,	AddressNumber,	City,	Neighborhood,
	StateId, 	ZipCode,	Complement,	DoctorAddressesEnabled,filter)
values
 (@DoctorId,	@AddressTypeId,	@Address,	@AddressNumber,	@City,	@Neighborhood,
	@StateId, 	@ZipCode,	@Complement,	@DoctorAddressesEnabled,Convert(Varchar(20),@DoctorId) + '|' + @City + '|' + @Neighborhood + '|' + @Address)

select @DoctorAddressesId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'DoctorAddresses', 
	@ProcessLogField = 'DoctorAddressesId', 
	@ProcessLogValue = @DoctorAddressesEnabled

END



GO
/****** Object:  StoredProcedure [dbo].[spIns_Doctors]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[spIns_Doctors]
(
	@UserLogin varchar(40),
	@DoctorID Integer output,
	@DoctorName varchar(60),
	@CRM varchar(10),
	@UF_CRM varchar(2),
	@CPF varchar(14),
	@Address varchar(60),
	@Number varchar(10),
	@Complement varchar(60),
	@City varchar(60),
	@Neighborhood varchar(50),
	@StateID Integer,
	@ZipCode varchar(9),
	@PhoneComercial varchar(20),
	@Ramal varchar(10),
	@CellPhone varchar(20),
	@FocalPoint varchar(20),
	@email varchar(100),
	@login varchar(20),
	@password varchar(40),
	@comments varchar(200),
	@status varchar(10),
	@RepresentativeID Integer,
	@DoctorEnabled Integer

)
AS
BEGIN
INSERT INTO [dbo].[Doctors]
           ([DoctorName]
           ,[CRM]
           ,[UF_CRM]
           ,[CPF]
           ,[Address]
           ,[Number]
           ,[Complement]
           ,[City]
           ,[Neighborhood]
           ,[StateID]
           ,[ZipCode]
           ,[PhoneComercial]
           ,[CellPhone]
           ,[FocalPoint]
           ,[email]
           ,[login]
           ,[password]
           ,[Comments]
           ,[Status]
           ,[RepresentativeID]
           ,[DoctorEnabled]
		   ,filter,
		   ramal)
     VALUES
           (
				@DoctorName,
				@CRM,
				@UF_CRM,
				@CPF,
				@Address,
				@Number,
				@Complement,
				@City,
				@Neighborhood,
				@StateID,
				@ZipCode,
				@PhoneComercial,
				@CellPhone,
				@FocalPoint,
				@email,
				@login,
				@password,
				@comments,
				@status,
				@RepresentativeID,
				@DoctorEnabled,
				@DoctorName + '|' + @CPF,
				@ramal
			)


select @DoctorID = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'Doctors', 
	@ProcessLogField = 'DoctorID', 
	@ProcessLogValue = @DoctorID

END



GO
/****** Object:  StoredProcedure [dbo].[spIns_Laboratories]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spIns_Laboratories]
(
	@UserLogin varchar(40),
	@LaboratoryID integer output,
	@LaboratoryName varchar(60),
	@LaboratoryFantasyName varchar(60),
	@CNPJ varchar(20),
	@Address varchar(60),
	@Number varchar(10),
	@Neighborhood varchar(60),
	@City varchar(60),
	@StateID integer,
	@Complement varchar(60),
	@ZipCode varchar(10),
	@PABX varchar(20),
	@Responsible varchar(30),
	@DeptResponsible varchar(30),
	@Email varchar(100),
	@ComercialPhone varchar(20),
	@CellPhone varchar(20),
	@Login varchar(20),
	@Password varchar(40),
	@Status integer,
	@Comments varchar(200),
	@LaboratoryEnabled integer
)
AS
BEGIN

Insert Into Laboratories (LaboratoryName,LaboratoryFantasyName,CNPJ,Address,Number,Neighborhood,City,StateID,Complement,ZipCode,PABX,Responsible,DeptResponsible,Email,ComercialPhone,CellPhone,Login,Password,Status,Comments,LaboratoryEnabled, filter) 
values (@LaboratoryName,@LaboratoryFantasyName,@CNPJ,@Address,@Number,@Neighborhood,@City,@StateID,@Complement,@ZipCode,@PABX,@Responsible,@DeptResponsible,@Email,@ComercialPhone,@CellPhone,@Login,@Password,@Status,@Comments,@LaboratoryEnabled, @LaboratoryName+'|'+@LaboratoryFantasyName+'|'+@CNPJ)

select @LaboratoryID = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'Laboratories', 
	@ProcessLogField = 'LaboratoryID', 
	@ProcessLogValue = @LaboratoryID

END

GO
/****** Object:  StoredProcedure [dbo].[spIns_Medicines]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spIns_Medicines]
(
	@UserLogin varchar(20),
	@MedicineId int output,
	@EAN varchar(13),
	@MedicineName varchar(100),
	@LabId varchar(10),
	@LabName varchar(100),
	@ActivePrincipleId varchar(10),
	@ActivePrincipleName varchar(100),
	@Presentation varchar(100),
	@UnitId varchar(10),
	@UnitName varchar(100),
	@Dosage int,
	@QtyTotal int,
	@MedicineType varchar(20),
	@Comments varchar(200),
	@MedicineEnabled int,
	@CodeTOTVS varchar(20)=null,
	@SystemId integer = null
)
AS
BEGIN

Insert Into Medicines ( EAN, MedicineName, LabId, LabName, ActivePrincipleId, ActivePrincipleName, Presentation, UnitId, UnitName, Dosage, QtyTotal, MedicineType, Comments, MedicineEnabled, filter, CodeTOTVS, SystemId)
values ( @EAN, @MedicineName, @LabId, @LabName, @ActivePrincipleId, @ActivePrincipleName, @Presentation, @UnitId, @UnitName, @Dosage, @QtyTotal, @MedicineType, @Comments, @MedicineEnabled,@EAN + '|' + @MedicineName, @CodeTOTVS, @SystemId)

select @MedicineId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'Medicines', 
	@ProcessLogField = 'MedicineId', 
	@ProcessLogValue = @MedicineId

END

GO
/****** Object:  StoredProcedure [dbo].[spIns_OrderAvaliable]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  procedure [dbo].[spIns_OrderAvaliable]
(
	@UserLogin varchar(40),
	@DoctorId integer,
	@startValidity datetime,
	@endValidity datetime,
	@frequence integer,
	@dateToSend datetime,
	@laboratoryID integer,
	@OrderAvaliableEnabled int = null,
	@OrderAvaliableID integer output,
	@status varchar(1)
)
AS
BEGIN

Insert Into OrderAvaliable(DoctorId,
							startValidity,
							endValidity,
							frequence,
							dateToSend,
							laboratoryID,
							OrderAvaliableEnabled,
							status)
values (@DoctorId, @startValidity, @endValidity, @frequence, @dateToSend, @laboratoryID, 1, @status)
Select @OrderAvaliableID = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'OrderAvaliable', 
	@ProcessLogField = 'OrderAvaliableID', 
	@ProcessLogValue = @OrderAvaliableID

END



GO
/****** Object:  StoredProcedure [dbo].[spIns_OrderAvaliableRepresentative]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  procedure [dbo].[spIns_OrderAvaliableRepresentative]
(
	@UserLogin varchar(40),
	@RepresentativeID integer,
	@startValidity datetime,
	@endValidity datetime,
	@frequence integer,
	@dateToSend datetime,
	@laboratoryID integer,
	@OrderAvaliableRepresentativeEnabled int = null,
	@OrderAvaliableRepresentativeID integer output,
	@status varchar(1)
)
AS
BEGIN

Insert Into OrderAvaliableRepresentative(RepresentativeID,
							startValidity,
							endValidity,
							frequence,
							dateToSend,
							laboratoryID,
							OrderAvaliableRepresentativeEnabled,
							status)
values (@RepresentativeID, @startValidity, @endValidity, @frequence, @dateToSend, @laboratoryID, 1, @status)
Select @OrderAvaliableRepresentativeID = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'OrderAvaliable', 
	@ProcessLogField = 'OrderAvaliableID', 
	@ProcessLogValue = @OrderAvaliableRepresentativeID

END




GO
/****** Object:  StoredProcedure [dbo].[spIns_OrderItem]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spIns_OrderItem]
(
	@UserLogin varchar(40),
	@OrderItemId integer output,
	@OrderId integer,
	@ReleaseOrderItemId integer,
	@ReleaseOrderId integer,
	@MedicineId integer,
	@Dosage integer,
	@Quantity integer,
	@OrderItemEnabled integer

)
AS
BEGIN
		INSERT INTO [dbo].[OrderItem]
           (
		   [OrderId]
		   ,[ReleaseOrderItemId]
           ,[ReleaseOrderId]
           ,[MedicineId]
           ,[Dosage]
           ,[Quantity]
           ,[OrderItemEnabled]
		   )
     VALUES
           (
		   @OrderId
		   ,@ReleaseOrderItemId
           ,@ReleaseOrderId
           ,@MedicineId
           ,@Dosage
           ,@Quantity
           ,@OrderItemEnabled
		   )

	select @OrderItemId = @@IDENTITY


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'OrderItem', 
	@ProcessLogField = 'OrderItemId', 
	@ProcessLogValue = @OrderItemId

END
GO
/****** Object:  StoredProcedure [dbo].[spIns_Orders]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spIns_Orders]
(
	@UserLogin varchar(40),
	@OrderId integer output,
	@ReleaseOrderId integer,
	@OrderDate datetime,
	@PatientId integer,
	@PatientAddressId integer,
	@PatientAuthorization varchar(20),
	@OrderStatus varchar(1),
	@OrderObservation varchar(200),
	@OrdersEnabled integer

)
AS
BEGIN
		INSERT INTO Orders
				   ([ReleaseOrderId]
				   ,[OrderDate]
				   ,[PatientId]
				   ,[PatientAddressId]
				   ,[PatientAuthorization]
				   ,[OrderStatus]
				   ,[OrderObservation]
				   ,[OrdersEnabled])
			 VALUES
				   (
				   @ReleaseOrderId
				   ,@OrderDate
				   ,@PatientId
				   ,@PatientAddressId
				   ,@PatientAuthorization
				   ,@OrderStatus
				   ,@OrderObservation
				   ,@OrdersEnabled
				   )

	select @OrderId = @@IDENTITY


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'Orders', 
	@ProcessLogField = 'OrderId', 
	@ProcessLogValue = @OrderId

END
GO
/****** Object:  StoredProcedure [dbo].[spIns_OrdersDoctor]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spIns_OrdersDoctor]
(
	@UserLogin varchar(40),
	@OrderDoctorID integer output,
	@DoctorID integer,
	@Status varchar(1),
	@CreateDate datetime,
	@OrderDoctorDateSend datetime,
	@OrderDoctorEnabled integer = null,
	@OrderAvaliableID integer
)
AS
BEGIN

Insert Into OrdersDoctor (DoctorID, Status,CreateDate,OrderDoctorDateSend,OrderDoctorEnabled, OrderAvaliableID) 
values (@DoctorID,@Status,@CreateDate,@OrderDoctorDateSend,@OrderDoctorEnabled, @OrderAvaliableID)

select @OrderDoctorID = @@IDENTITY

update OrderAvaliable set status = 'A' where OrderAvaliableID = @OrderAvaliableID

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'OrdersDoctor', 
	@ProcessLogField = 'OrderDoctorID', 
	@ProcessLogValue = @OrderDoctorID

END

GO
/****** Object:  StoredProcedure [dbo].[spIns_OrdersRepresentative]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spIns_OrdersRepresentative]
(
	@UserLogin varchar(40),
	@OrdersRepresentativeID integer output,
	@RepresentativeID integer,
	@Status varchar(1),
	@CreateDate datetime,
	@OrderRepresentativeDateSend datetime,
	@OrderRepresentativeEnabled integer = null,
	@OrderRepresentativeAvaliableID integer
)
AS
BEGIN

Insert Into OrdersRepresentative (RepresentativeID, Status,CreateDate,OrderRepresentativeDateSend,OrderRepresentativeEnabled, OrderRepresentativeAvaliableID) 
values (@RepresentativeID,@Status,@CreateDate,@OrderRepresentativeDateSend,@OrderRepresentativeEnabled, @OrderRepresentativeAvaliableID)

select @OrdersRepresentativeID = @@IDENTITY

update OrderAvaliableRepresentative set status = 'A' where OrderAvaliableRepresentativeID = @OrderRepresentativeAvaliableID

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = '@OrdersRepresentativeID', 
	@ProcessLogField = '@OrdersRepresentativeID', 
	@ProcessLogValue = @OrdersRepresentativeID

END


GO
/****** Object:  StoredProcedure [dbo].[spIns_PatientAddresses]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  procedure [dbo].[spIns_PatientAddresses]
(
	@UserLogin varchar(40),
	@PatientAddressesId int output,
	@PatientId int,
	@AddressTypeId int,
	@Address varchar(60),
	@AddressNumber varchar(10),
	@City varchar(50),
	@Neighborhood varchar(50),
	@StateId integer, 
	@ZipCode varchar(8),
	@Complement varchar(200),
	@PatientAddressesEnabled int
)
AS
BEGIN


Insert Into PatientAddresses
 (PatientId,	AddressTypeId,	Address,	AddressNumber,	City,	Neighborhood,
	StateId, 	ZipCode,	Complement,	PatientAddressesEnabled,filter)
values
 (@PatientId,	@AddressTypeId,	@Address,	@AddressNumber,	@City,	@Neighborhood,
	@StateId, 	@ZipCode,	@Complement,	@PatientAddressesEnabled,Convert(Varchar(20),@PatientId) + '|' + @City + '|' + @Neighborhood + '|' + @Address)

select @PatientAddressesId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'PatientAddresses', 
	@ProcessLogField = 'PatientAddressesId', 
	@ProcessLogValue = @PatientAddressesEnabled

END



GO
/****** Object:  StoredProcedure [dbo].[spIns_PatientPathologies]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  procedure [dbo].[spIns_PatientPathologies]
(
	@UserLogin varchar(40),
	@PatientPathologiesId integer output,
	@PatientId Integer,
	@PathologyId Integer,
	@PatientPathologiesEnabled integer
)
AS
BEGIN


Insert Into PatientPathologies(PatientId, PathologyId, PatientPathologiesEnabled, Filter)
values ( @PatientId,@PathologyId, @PatientPathologiesEnabled,Convert(Varchar(20),@PatientId))
Select @PatientPathologiesId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'PatientPathologies', 
	@ProcessLogField = 'PatientPathologiesId', 
	@ProcessLogValue = @PatientPathologiesId

END



GO
/****** Object:  StoredProcedure [dbo].[spIns_PatientPhones]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  procedure [dbo].[spIns_PatientPhones]
(
	@UserLogin varchar(40),
	@PatientPhoneId integer output,
	@PatientId Integer,
	@PhoneNumber varchar(12),
	@Ramal varchar(10),
	@PhoneTypeId integer,
	@PatientPhonesEnabled integer
)
AS
BEGIN


Insert Into PatientPhones(PatientId, PhoneNumber, Ramal, PhoneTypeId, PatientPhonesEnabled,filter)
values ( @PatientId,@PhoneNumber, @Ramal, @PhoneTypeId, @PatientPhonesEnabled,Convert(Varchar(20),@PatientId) + '|' + @PhoneNumber)
Select @PatientPhoneId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'PatientPhones', 
	@ProcessLogField = 'PatientPhoneId', 
	@ProcessLogValue = @PatientPhoneId

END



GO
/****** Object:  StoredProcedure [dbo].[spIns_Patients]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[spIns_Patients]
(
	@UserLogin varchar(40),
	@PatientId Integer output,
	@ClientId Integer,
	@PatientName varchar(60),
	@BirthDate DateTime,
	@CPF varchar(11),
	@RG varchar(15),
	@IdentificationOnClient varchar(20),
	@Responsible varchar(60),
	@BestContactPeriod varchar(20),
	@Comments varchar(200),
	@PatientStatus varchar(1),
	@CID1 varchar(20),
	@CID2 varchar(20),
	@CID3 varchar(20),
	@PatientEnabled integer,
	@ClientFantasyName varchar(50)
)
AS
BEGIN


Insert Into Patients (ClientId, PatientName, BirthDate, CPF, RG, IdentificationOnClient, Responsible,
					  BestContactPeriod, Comments, PatientStatus, CID1, CID2, CID3, PatientEnabled,filter)
values (@ClientId, @PatientName, @BirthDate, @CPF, @RG, @IdentificationOnClient, @Responsible,
		@BestContactPeriod, @Comments, @PatientStatus, @CID1, @CID2, @CID3, @PatientEnabled,
		@PatientName + '|' + @CPF)

select @PatientId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'Patients', 
	@ProcessLogField = 'PatientId', 
	@ProcessLogValue = @PatientId

END



GO
/****** Object:  StoredProcedure [dbo].[spIns_PatientsMedicines]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spIns_PatientsMedicines]
(
	@UserLogin varchar(40),
	@PatientId integer,
	@EAN varchar(13),	
    @PatienteAuthorization varchar(20),
    @Qty int,
    @DailyDosage int,
	@Dispensation int)    
AS
BEGIN

INSERT INTO PatientsMedicines(
                               PatientId,
                               EAN,
							   PatienteAuthorization,
							   Qty,
							   DailyDosage,
							   Dispensation)values
   (@PatientId,
	@EAN,	
    @PatienteAuthorization,
    @Qty,
    @DailyDosage,
	@Dispensation)


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'PatientsMedicines', 
	@ProcessLogField = 'PatientsMedicines', 
	@ProcessLogValue = @PatientId

END

GO
/****** Object:  StoredProcedure [dbo].[spIns_Permissions]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create procedure [dbo].[spIns_Permissions]
(
	@UserLogin varchar(40),
	@PermissionId Integer output,
	@PermissionName varchar(150),
	@PermissionEnabled Integer
)
AS
BEGIN


Insert Into Permissions (PermissionName, PermissionEnabled, Filter) values (@PermissionName, @PermissionEnabled, @PermissionName)
select @PermissionId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'Permissions', 
	@ProcessLogField = 'PermissionName', 
	@ProcessLogValue = @PermissionName

END


GO
/****** Object:  StoredProcedure [dbo].[spIns_ProcessLog]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spIns_ProcessLog]
(
	@ProcessLogUser varchar(40),
	@ProcessLogName varchar(100),
	@ProcessLogTable varchar(100),
	@ProcessLogField varchar(100),
	@ProcessLogValue varchar(100)
)
AS

insert into ProcessLog values (getdate(),@ProcessLogUser,@ProcessLogName,@ProcessLogTable,@ProcessLogField,@ProcessLogValue,@ProcessLogName + '|' + @processLogTable + '|' + @ProcessLogField + '|' + @ProcessLogValue);

GO
/****** Object:  StoredProcedure [dbo].[spIns_ProductsAvaliable]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[spIns_ProductsAvaliable]
(
	@UserLogin varchar(40),
	@ProductAvaliableID integer output,
	@ProductID integer ,		
	@Qty integer ,
	@OrderAvaliableID integer,
	@ProductsAvaliableEnabled integer
)
AS
BEGIN

Insert Into ProductsAvaliable (ProductID, Qty, ProductsAvaliableEnabled, OrderAvaliableID  ) 
values (@ProductID, @Qty, @ProductsAvaliableEnabled, @OrderAvaliableID)

select @ProductAvaliableID = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'ProductsAvaliable', 
	@ProcessLogField = 'ProductAvaliableID', 
	@ProcessLogValue = @ProductAvaliableID

END

GO
/****** Object:  StoredProcedure [dbo].[spIns_ProductsAvaliableRepresentative]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  procedure [dbo].[spIns_ProductsAvaliableRepresentative]
(
	@UserLogin varchar(40) =null,
	@ProductsAvaliableRepresentativeID integer output,
	@ProductID integer ,		
	@Qty integer ,
	@OrderAvaliableRepresentativeID integer,
	@ProductsAvaliableRepresentativeEnabled integer
)
AS
BEGIN

Insert Into ProductsAvaliableRepresentative (ProductID, Qty, ProductsAvaliableRepresentativeEnabled, OrderAvaliableRepresentativeID  ) 
values (@ProductID, @Qty, @ProductsAvaliableRepresentativeEnabled, @OrderAvaliableRepresentativeID)

select @ProductsAvaliableRepresentativeID = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'ProductsAvaliable', 
	@ProcessLogField = 'ProductAvaliableID', 
	@ProcessLogValue = @ProductsAvaliableRepresentativeID

END


GO
/****** Object:  StoredProcedure [dbo].[spIns_ProductsOrderDoctor]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spIns_ProductsOrderDoctor]
(
	@UserLogin varchar(40),
	@ProductsOrderDoctorID integer output,
	@ProductID integer,
	@Qty integer,	
	@OrdersDoctorID integer
)
AS
BEGIN

Insert Into ProductsOrderDoctor(ProductID, Qty, OrdersDoctorID)
values(@ProductID, @Qty, @OrdersDoctorID)

select @ProductsOrderDoctorID = @@IDENTITY





-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = '@ProductsOrderDoctor', 
	@ProcessLogField = '@ProductsOrderDoctorID', 
	@ProcessLogValue = @ProductsOrderDoctorID

END

GO
/****** Object:  StoredProcedure [dbo].[spIns_ProductsOrderRepresentative]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spIns_ProductsOrderRepresentative]
(
	@UserLogin varchar(40),
	@ProductsOrderDoctorRepresentativeID integer output,
	@ProductID integer,
	@Qty integer,	
	@OrdersRepresentativeID integer
)
AS
BEGIN

Insert Into ProductsOrderRepresentative(ProductID, Qty, OrdersRepresentativeID)
values(@ProductID, @Qty, @OrdersRepresentativeID)

select @ProductsOrderDoctorRepresentativeID = @@IDENTITY





-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = '@ProductsOrderDoctor', 
	@ProcessLogField = '@ProductsOrderDoctorID', 
	@ProcessLogValue = @ProductsOrderDoctorRepresentativeID

END


GO
/****** Object:  StoredProcedure [dbo].[spIns_ReleaseOrder]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spIns_ReleaseOrder]
(
	@UserLogin varchar(40),
	@ReleaseOrderId integer output,
	@ReleaseOrderDate datetime,
	@PatientId integer,
	@PatientAuthorization varchar(20),
	@DispensationId integer,
	@ReleaseOrderStatus varchar(1),
	@ReleaseOrderObservation varchar(200),
	@ReleaseOrderEnabled integer
)
AS
BEGIN

Insert Into ReleaseOrder (ReleaseOrderDate,PatientId,PatientAuthorization,DispensationId,ReleaseOrderObservation,ReleaseOrderEnabled) values (@ReleaseOrderDate,@PatientId,@PatientAuthorization,@DispensationId,@ReleaseOrderObservation,@ReleaseOrderEnabled)

select @ReleaseOrderId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'ReleaseOrder', 
	@ProcessLogField = 'ReleaseOrderId', 
	@ProcessLogValue = @ReleaseOrderId

END
GO
/****** Object:  StoredProcedure [dbo].[spIns_ReleaseOrderItem]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spIns_ReleaseOrderItem]
(
	@UserLogin varchar(40),
	@ReleaseOrderItemId integer output,
	@ReleaseOrderId integer,
	@MedicineId integer,
	@Dosage integer,
	@Quantity integer,
	@ReleaseOrderItemEnabled integer
)
AS
BEGIN

Insert Into ReleaseOrderItem (ReleaseOrderId,MedicineId,Dosage,Quantity,ReleaseOrderItemEnabled) values (@ReleaseOrderId,@MedicineId,@Dosage,@Quantity,@ReleaseOrderItemEnabled)

select @ReleaseOrderItemId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'ReleaseOrderItem', 
	@ProcessLogField = 'ReleaseOrderItemId', 
	@ProcessLogValue = @ReleaseOrderItemId

END


GO
/****** Object:  StoredProcedure [dbo].[spIns_Representatives]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spIns_Representatives]
(
	@UserLogin varchar(40),
	@RepresentativeID integer output,
	@RepresentativeName varchar(50),
	@Email varchar(100),
	@Login varchar(20),
	@Password varchar(40),
	@Status varchar(10),
	@Comments varchar(200),
	@RepresentativeEnabled integer,
	@laboratoryID integer,
	@cpf varchar(20),
	@address varchar(50),
	@addressNumber varchar(10),
	@complement varchar(100),
	@city varchar(100),
	@Neighborhood varchar(100),
	@stateID int,
	@zipcode varchar(10),
	@phoneComercial varchar(20),
	@ramal varchar(10),
	@cellPhone varchar(20)
)
AS
BEGIN

Insert Into Representatives (RepresentativeName,Email,Login,Password,Status,Comments,RepresentativeEnabled, laboratoryID,
cpf, address, addressnumber, complement, city, neighborhood, stateid, zipcode, phonecomercial, ramal, cellphone)
 values (@RepresentativeName,@Email,@Login,@Password,@Status,@Comments,@RepresentativeEnabled, @laboratoryID,
	@cpf, @address, @addressNumber, @complement, @city, @Neighborhood, @stateID,
    @zipcode, @phoneComercial, @ramal, @cellPhone)

select @RepresentativeID = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'Representatives', 
	@ProcessLogField = 'RepresentativeID', 
	@ProcessLogValue = @RepresentativeID

END

GO
/****** Object:  StoredProcedure [dbo].[spIns_Users]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spIns_Users]
(	
	@UserLogin varchar(40),
	@UserLoginName varchar(40),
	@UserName varchar(40),
	@UserPassword varchar(40),
	@UserEnabled Integer,
	@UserId Integer output,
	@ClientId integer,
	@UserType integer,
	@laboratoryID integer
)
AS
BEGIN

Insert Into Users (UserLoginName,UserName,UserEnabled,UserPassword, filter, ClientId, UserType,laboratoryID) values (@UserLoginName,@UserName,@UserEnabled,@UserPassword,@UserName +'|'+ @UserLoginName, @ClientId, @UserType, @laboratoryID)
select @UserId = @@Identity

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'Users', 
	@ProcessLogField = 'UserLogin', 
	@ProcessLogValue = @UserLoginName

END



GO
/****** Object:  StoredProcedure [dbo].[spUpd_Clients]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spUpd_Clients]
(
	@UserLogin varchar(40),
	@ClientId integer,
	@ClientName varchar(60),
	@ClientFantasyName varchar(60),
	@CNPJ varchar(14),
	@Address varchar(60),
	@AddressNumber varchar(10),
	@City varchar(50),
	@Neighborhood varchar(50),
	@StateId integer, 
	@ZipCode varchar(8),
	@FocalClientContact varchar(40),
	@DispensationId integer,
	@BillingStartDay integer,
	@BillingEndDay integer,
	@Email varchar(60),
	@ClientLogin varchar(60),
	@ClientPassword varchar(20),
	@ConfirmPassword varchar(20),
	@ResponsibleContact varchar(50),
	@Comments varchar(200),
	@ClientStatus varchar(1),
	@ClientTypeId Integer,
	@ClientsEnabled integer
)
AS
BEGIN

update Clients set
	ClientName = @ClientName, 
	ClientFantasyName = @ClientFantasyName, 
	CNPJ = @CNPJ, 
	Address = @Address, 
	AddressNumber = @AddressNumber, 
	City = @City, 
	Neighborhood = @Neighborhood, 
	StateId = @StateId, 
	ZipCode = @ZipCode, 
	FocalClientContact = @FocalClientContact, 
	DispensationId = @DispensationId, 
	BillingStartDay = @BillingStartDay, 
	BillingEndDay = @BillingEndDay, 
	Email = @Email, 
	ClientLogin = @ClientLogin, 
	ClientPassword = @ClientPassword, 
	ConfirmPassword = @ConfirmPassword, 
	ResponsibleContact = @ResponsibleContact, 
	Comments = @Comments, 
	ClientStatus = @ClientStatus, 
	ClientTypeId = @ClientTypeId, 
	ClientsEnabled = @ClientsEnabled,
	Filter = @ClientName + '|' + @ClientFantasyName + '|'+ @CNPJ
where
	ClientId = @ClientId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Alteração', 
	@ProcessLogTable = 'Clients', 
	@ProcessLogField = 'ClientId', 
	@ProcessLogValue = @ClientId

END

GO
/****** Object:  StoredProcedure [dbo].[spUpd_ClientsContacts]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spUpd_ClientsContacts]
(
	@UserLogin varchar(40),
	@ContactId integer,
	@ClientId integer,
	@ContactName varchar(40),
	@Occupation varchar(40),
	@Email varchar(100),
	@Phone varchar(12),
	@Ramal varchar(8),
	@ContactTypeId integer,
	@ClientsContactsEnabled integer
)
AS
BEGIN

update ClientsContacts set
	ClientId = @ClientId,
	ContactName = @ContactName,
	Occupation = @Occupation,
	Email = @Email,
	Phone = @Phone,
	Ramal = @Ramal,
	ContactTypeId = @ContactTypeId,
	ClientsContactsEnabled = @ClientsContactsEnabled ,
	Filter = Convert(Varchar(20),@ClientId) + '|' + @ContactName + '|' + @Email + '|' + @Phone
where
	ContactId = @ContactId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Alteração', 
	@ProcessLogTable = 'ClientsContacts', 
	@ProcessLogField = 'ContactId', 
	@ProcessLogValue = @ContactId

END

GO
/****** Object:  StoredProcedure [dbo].[spUpd_Configurations]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spUpd_Configurations]
(
	@UserLogin varchar(40),
	@ConfigurationId Integer,
	@ConfigurationName varchar(100),
	@ConfigurationValue varchar(400),
	@ConfigurationEnabled Integer
)
AS
BEGIN

update configurations set
	configurationName = @ConfigurationName,
	configurationValue = @ConfigurationValue,
	configurationEnabled = @ConfigurationEnabled,
	Filter = @ConfigurationName
where
	ConfigurationId = @ConfigurationId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Alteração', 
	@ProcessLogTable = 'Configurations', 
	@ProcessLogField = 'ConfigurationId', 
	@ProcessLogValue = @ConfigurationId

END



GO
/****** Object:  StoredProcedure [dbo].[spUpd_DoctorAddresses]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spUpd_DoctorAddresses]
(
	@UserLogin varchar(40),
	@DoctorAddressesId integer,
	@DoctorId integer,
	@AddressTypeId int,
	@Address varchar(60),
	@AddressNumber varchar(10),
	@City varchar(50),
	@Neighborhood varchar(50),
	@StateId integer, 
	@ZipCode varchar(8),
	@Complement varchar(200),
	@DoctorAddressesEnabled integer
)
AS
BEGIN

update DoctorAddresses set
	DoctorId = @DoctorId,
	AddressTypeId = @AddressTypeId,
	Address = @Address,
	AddressNumber = @AddressNumber,
	City = @City,
	Neighborhood = @Neighborhood,
	StateId = @StateId, 
	ZipCode = @ZipCode,
	Complement = @Complement,
	DoctorAddressesEnabled = @DoctorAddressesEnabled,
	Filter = Convert(Varchar(20),@DoctorId) + '|' + @City + '|' + @Neighborhood + '|' + @Address
where
 	DoctorAddressesId = @DoctorAddressesId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Alteração', 
	@ProcessLogTable = 'DoctorAddresses', 
	@ProcessLogField = 'DoctorAddressesId', 
	@ProcessLogValue = @DoctorAddressesEnabled

END



GO
/****** Object:  StoredProcedure [dbo].[spUpd_Doctors]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[spUpd_Doctors]
(
	@UserLogin varchar(40) = null,
	@DoctorID Integer = null,
	@DoctorName varchar(60) = null,
	@CRM varchar(10)= null,
	@UF_CRM varchar(2)= null,
	@CPF varchar(14)= null,
	@Address varchar(60)= null,
	@Number varchar(10)= null,
	@Complement varchar(60) = null,
	@City varchar(60)= null,
	@Neighborhood varchar(50)= null,
	@StateID Integer= null,
	@ZipCode varchar(9)= null,
	@PhoneComercial varchar(20)= null,
	@CellPhone varchar(20)= null,
	@FocalPoint varchar(20)= null,
	@email varchar(100)= null,
	@login varchar(20) = null,
	@password varchar(40) = null,
	@comments varchar(200) =null,
	@status varchar(10) = null,
	@RepresentativeID Integer = null,
	@DoctorEnabled Integer = null,
	@ramal varchar(10) = null
)
AS
BEGIN

UPDATE [dbo].[Doctors]
   SET [DoctorName] = isnull(@DoctorName, DoctorName),
      [CRM] = isnull(@CRM, CRM),
      [UF_CRM] = isnull(@UF_CRM,UF_CRM),
      [CPF] = isnull(@CPF, CPF),
      [Address] = isnull(@Address, Address),
      [Number] = isnull(@Number, Number),
      [Complement] = isnull(@Complement, Complement),
      [City] = isnull(@City, City),
      [Neighborhood] = isnull(@Neighborhood, Neighborhood),
      [StateID] = isnull(@StateID, StateID),
      [ZipCode] = isnull(@ZipCode, ZipCode),
      [PhoneComercial] = isnull(@PhoneComercial, PhoneComercial),
      [CellPhone] = isnull(@CellPhone,CellPhone),
      [FocalPoint] = isnull(@FocalPoint, FocalPoint),
      [email] = isnull(@email, email),
      [password] = isnull(@password, password),
      [Comments] = isnull(@comments, comments), 
      [Status] = isnull(@status, status),
      [RepresentativeID] = isnull(@RepresentativeID, RepresentativeID),
      [DoctorEnabled] = isnull(@DoctorEnabled, DoctorEnabled),
      filter = isnull(@DoctorName, DoctorName )  + '|' + isnull(@CPF, CPF),
	  ramal= isnull(@ramal, ramal)
 WHERE 
	  [DoctorID] = @DoctorID


update Users set UserPassword = isnull(@Password, UserPassword) where UserLoginName = @Login

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Alteração', 
	@ProcessLogTable = 'Doctors', 
	@ProcessLogField = 'DoctorsID', 
	@ProcessLogValue = @DoctorID

END



GO
/****** Object:  StoredProcedure [dbo].[spUpd_Laboratories]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spUpd_Laboratories]
(
	@UserLogin varchar(40) = null,
	@LaboratoryID integer = null,
	@LaboratoryName varchar(60) = null,
	@LaboratoryFantasyName varchar(60) = null,
	@CNPJ varchar(20) = null,
	@Address varchar(60) = null,
	@Number varchar(10) = null,
	@Neighborhood varchar(60) = null,
	@City varchar(60) = null,
	@StateID integer = null,
	@Complement varchar(60) = null,
	@ZipCode varchar(10) = null,
	@PABX varchar(20) = null,
	@Responsible varchar(30) = null,
	@DeptResponsible varchar(30) = null,
	@Email varchar(100) = null,
	@ComercialPhone varchar(20) = null,
	@CellPhone varchar(20) = null,
	@Login varchar(20) = null,
	@Password varchar(40)= null,
	@Status integer = null,
	@Comments varchar(200) = null,
	@LaboratoryEnabled integer
)
AS
BEGIN

update Laboratories set
	LaboratoryName = isnull(@LaboratoryName, LaboratoryName),
	LaboratoryFantasyName = isnull(@LaboratoryFantasyName, LaboratoryFantasyName),
	CNPJ = isnull(@CNPJ,CNPJ),
	Address = isnull(@Address,Address),
	Number = isnull(@Number,Number),
	Neighborhood = isnull(@Neighborhood,Neighborhood),
	City = isnull(@City,City),
	StateID = isnull(@StateID,StateID),
	Complement = isnull(@Complement,Complement),
	ZipCode = isnull(@ZipCode,ZipCode),
	PABX = isnull(@PABX,PABX),
	Responsible = isnull(@Responsible,Responsible),
	DeptResponsible = isnull(@DeptResponsible,DeptResponsible),
	Email = isnull(@Email,Email),
	ComercialPhone = isnull(@ComercialPhone,ComercialPhone),
	CellPhone = isnull(@CellPhone,CellPhone),
	Password = isnull(@Password,Password),
	Status = isnull(@Status,Status),
	Comments = isnull(@Comments,Comments),
	LaboratoryEnabled = @LaboratoryEnabled,
	filter = isnull(@LaboratoryName, LaboratoryName)+'|'+isnull(@LaboratoryFantasyName, LaboratoryFantasyName)+'|'+isnull(@CNPJ, CNPJ)
where
	LaboratoryID = @LaboratoryID


update Users set UserPassword = isnull(@Password, UserPassword) where UserLoginName = @Login

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Alteração', 
	@ProcessLogTable = 'Laboratories', 
	@ProcessLogField = 'LaboratoryID', 
	@ProcessLogValue = @LaboratoryID

END



GO
/****** Object:  StoredProcedure [dbo].[spUpd_Medicines]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spUpd_Medicines]
(
	@UserLogin varchar(20),
	@MedicineId int,
	@EAN varchar(13),
	@MedicineName varchar(100),
	@LabId varchar(10),
	@LabName varchar(100),
	@ActivePrincipleId varchar(10),
	@ActivePrincipleName varchar(100),
	@Presentation varchar(100),
	@UnitId varchar(10),
	@UnitName varchar(100),
	@Dosage int,
	@QtyTotal int,
	@MedicineType varchar(20),
	@Comments varchar(200),
	@MedicineEnabled int
)
AS
BEGIN

update Medicines set
	EAN = @EAN,
	MedicineName = @MedicineName,
	LabId = @LabId,
	LabName = @LabName,
	ActivePrincipleId = @ActivePrincipleId,
	ActivePrincipleName = @ActivePrincipleName,
	Presentation = @Presentation,
	UnitId = @UnitId,
	UnitName = @UnitName,
	Dosage = @Dosage,
	QtyTotal = @QtyTotal,
	MedicineType = @MedicineType,
	Comments = @Comments,
	MedicineEnabled = @MedicineEnabled,
	Filter = @EAN + '|' + @MedicineName
where
	MedicineId = @MedicineId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Alteração', 
	@ProcessLogTable = 'Medicines', 
	@ProcessLogField = 'MedicineId', 
	@ProcessLogValue = @MedicineId

END



GO
/****** Object:  StoredProcedure [dbo].[spUpd_OrderAvaliable]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  procedure [dbo].[spUpd_OrderAvaliable]
(
	@UserLogin varchar(40),
	@DoctorId integer,
	@startValidity datetime,
	@endValidity datetime,
	@frequence integer,
	@dateToSend datetime,
	@laboratoryID integer,
	@OrderAvaliableEnabled int = null,
	@OrderAvaliableID integer output,
	@status varchar(1)
)
AS
BEGIN

Update OrderAvaliable set status = @status where OrderAvaliableID = @OrderAvaliableID

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Alteração', 
	@ProcessLogTable = 'OrderAvaliable', 
	@ProcessLogField = 'OrderAvaliableID', 
	@ProcessLogValue = @OrderAvaliableID

END

GO
/****** Object:  StoredProcedure [dbo].[spUpd_OrdersDoctor]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spUpd_OrdersDoctor]
(
	@UserLogin varchar(40),
	@OrderDoctorID integer,
	@DoctorID integer,
	@AddressDoctorID integer,
	@Status varchar(1),
	@Comments varchar(200) ,
	@CreateDate datetime,
	@OrderDoctorDateSend datetime,
	@OrderDoctorEnabled integer
)
AS
BEGIN

update OrdersDoctor set
	DoctorID = @DoctorID,
	AddressDoctorID = @AddressDoctorID,
	Status = @Status,
	Comments = @Comments,
	CreateDate = @CreateDate,
	OrderDoctorDateSend = @OrderDoctorDateSend,
	OrderDoctorEnabled = @OrderDoctorEnabled

where
	OrderDoctorID = @OrderDoctorID

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Alteração', 
	@ProcessLogTable = 'OrdersDoctor', 
	@ProcessLogField = 'OrderDoctorID', 
	@ProcessLogValue = @OrderDoctorID

END

GO
/****** Object:  StoredProcedure [dbo].[spUpd_PatientAddresses]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spUpd_PatientAddresses]
(
	@UserLogin varchar(40),
	@PatientAddressesId int,
	@PatientId int,
	@AddressTypeId int,
	@Address varchar(60),
	@AddressNumber varchar(10),
	@City varchar(50),
	@Neighborhood varchar(50),
	@StateId integer, 
	@ZipCode varchar(8),
	@Complement varchar(200),
	@PatientAddressesEnabled int
)
AS
BEGIN

update PatientAddresses set
	PatientId = @PatientId,
	AddressTypeId = @AddressTypeId,
	Address = @Address,
	AddressNumber = @AddressNumber,
	City = @City,
	Neighborhood = @Neighborhood,
	StateId = @StateId, 
	ZipCode = @ZipCode,
	Complement = @Complement,
	PatientAddressesEnabled = @PatientAddressesEnabled,
	Filter = Convert(Varchar(20),@PatientId) + '|' + @City + '|' + @Neighborhood + '|' + @Address
where
 	PatientAddressesId = @PatientAddressesId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Alteração', 
	@ProcessLogTable = 'PatientAddresses', 
	@ProcessLogField = 'PatientAddressesId', 
	@ProcessLogValue = @PatientAddressesEnabled

END



GO
/****** Object:  StoredProcedure [dbo].[spUpd_PatientPathologies]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spUpd_PatientPathologies]
(
	@UserLogin varchar(40),
	@PatientPathologiesId Integer,
	@PatientId Integer,
	@PathologyId Integer,
	@PatientPathologiesEnabled integer
)
AS
BEGIN

update PatientPathologies set
			PatientId = @PatientId,
			PathologyId = @PathologyId,
			Filter = Convert(Varchar(20),@PatientId)
where
	PatientPathologiesId = @PatientPathologiesId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Alteração', 
	@ProcessLogTable = 'PatientPathologies', 
	@ProcessLogField = 'PatientPathologiesId', 
	@ProcessLogValue = @PatientPathologiesId

END



GO
/****** Object:  StoredProcedure [dbo].[spUpd_PatientPhones]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spUpd_PatientPhones]
(
	@UserLogin varchar(40),
	@PatientPhoneId Integer,
	@PatientId Integer,
	@PhoneNumber varchar(12),
	@Ramal varchar(10),
	@PhoneTypeId integer,
	@PatientPhonesEnabled integer
)
AS
BEGIN

update PatientPhones set
			PatientId = @PatientId,
			PhoneNumber = @PhoneNumber,
			Ramal = @Ramal,
			PhoneTypeId = @PhoneTypeId,
			Filter = Convert(Varchar(20),@PatientId)
where
	PatientPhoneId = @PatientPhoneId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Alteração', 
	@ProcessLogTable = 'PatientPhones', 
	@ProcessLogField = 'PatientPhoneId', 
	@ProcessLogValue = @PatientPhoneId

END



GO
/****** Object:  StoredProcedure [dbo].[spUpd_Patients]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spUpd_Patients]
(
	@UserLogin varchar(40),
	@PatientId Integer,
	@ClientId Integer,
	@PatientName varchar(60),
	@BirthDate DateTime,
	@CPF varchar(11),
	@RG varchar(15),
	@IdentificationOnClient varchar(20),
	@Responsible varchar(60),
	@BestContactPeriod varchar(20),
	@Comments varchar(200),
	@PatientStatus varchar(1),
	@CID1 varchar(20),
	@CID2 varchar(20),
	@CID3 varchar(20),
	@PatientEnabled integer = 1,
	@ClientFantasyName varchar(50)="vazio"
)
AS
BEGIN

update Patients set
			ClientId = @ClientId,
			PatientName = @PatientName,
			BirthDate = @BirthDate,
			CPF = @CPF,
			RG = @RG,
			IdentificationOnClient = @IdentificationOnClient,
			Responsible =  @Responsible,
			BestContactPeriod = @BestContactPeriod,
			Comments = @Comments,
			PatientStatus = @PatientStatus,
			CID1 = @CID1,
			CID2 = @CID2,
			CID3 = @CID3,
			filter = @PatientName + '|' + @CPF

where
	PatientId = @PatientId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Alteração', 
	@ProcessLogTable = 'Patients', 
	@ProcessLogField = 'PatientId', 
	@ProcessLogValue = @PatientId

END




GO
/****** Object:  StoredProcedure [dbo].[spUpd_Permissions]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spUpd_Permissions]
(
	@UserLogin varchar(40),
	@PermissionId Integer,
	@PermissionName varchar(150),
	@PermissionEnabled Integer
)
AS
BEGIN

update Permissions set
	permissionName = @PermissionName,
	PermissionEnabled = @PermissionEnabled,
	Filter = @PermissionName
where
	PermissionId = @PermissionId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Alteração', 
	@ProcessLogTable = 'Permissions', 
	@ProcessLogField = 'PermissionId', 
	@ProcessLogValue = @PermissionId

END



GO
/****** Object:  StoredProcedure [dbo].[spUpd_ProductsAvaliable]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spUpd_ProductsAvaliable]
(
	@UserLogin varchar(40),
	@ProductAvaliableID integer,
	@ProductID integer ,
	@LaboratoryID integer ,
	@DoctorID integer ,
	@Qty integer ,
	@FrequencyID integer ,
	@DateToSend datetime ,
	@ProductsAvaliableEnabled integer
)
AS
BEGIN

update ProductsAvaliable set
	ProductID = @ProductID,
	LaboratoryID = @LaboratoryID,
	DoctorID = @DoctorID,
	Qty = @Qty,
	FrequencyID = @FrequencyID,
	DateToSend = @DateToSend,
	ProductsAvaliableEnabled = @ProductsAvaliableEnabled

where
	ProductAvaliableID = @ProductAvaliableID

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Alteração', 
	@ProcessLogTable = 'ProductsAvaliable', 
	@ProcessLogField = 'ProductAvaliableID', 
	@ProcessLogValue = @ProductAvaliableID

END

GO
/****** Object:  StoredProcedure [dbo].[spUpd_Representatives]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spUpd_Representatives]
(
	@UserLogin varchar(40) =null,
	@RepresentativeID integer=null,
	@RepresentativeName varchar(50)=null,
	@Email varchar(100) =null,
	@Login varchar(20) = null,
	@Password varchar(40)=null,
	@Status varchar(10) =null,
	@Comments varchar(200) =null,
	@RepresentativeEnabled integer =null,
	@laboratoryID integer =null,
	@cpf varchar(20)=null,
	@address varchar(50)=null,
	@addressNumber varchar(10)=null,
	@complement varchar(100)=null,
	@city varchar(100)=null,
	@Neighborhood varchar(100)=null,
	@stateID int=null,
	@zipcode varchar(10)=null,
	@phoneComercial varchar(20)=null,
	@ramal varchar(10)=null,
	@cellPhone varchar(20)=null
)
AS
BEGIN

update Representatives set
	RepresentativeName = isnull(@RepresentativeName,RepresentativeName),
	Email = isnull(@Email,	Email),
	Password = isnull(@Password,Password),
	Status = isnull(@Status, status),
	Comments = isnull(@Comments,Comments),
	RepresentativeEnabled = isnull(@RepresentativeEnabled,RepresentativeEnabled),
	laboratoryID = isnull(@laboratoryID, laboratoryID),
	cpf = isnull(@cpf, cpf ),
    address = isnull(@address, address ),
    addressNumber = isnull(@addressNumber, addressNumber ),
    complement = isnull(@complement, complement ),
    city = isnull(@city, city ),
    Neighborhood = isnull(@Neighborhood, Neighborhood ),
    stateID = isnull(@stateID, stateID ),
    zipcode = isnull(@zipcode, zipcode ),
    phoneComercial = isnull(@phoneComercial, phoneComercial ),
    ramal = isnull(@ramal, ramal ),
    cellPhone = isnull(@cellPhone, cellPhone )

where
	RepresentativeID = @RepresentativeID

update Users set UserPassword = isnull(@Password, UserPassword) where UserLoginName = @Login

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Alteração', 
	@ProcessLogTable = 'Representatives', 
	@ProcessLogField = 'RepresentativeID', 
	@ProcessLogValue = @RepresentativeID

END

GO
/****** Object:  StoredProcedure [dbo].[spUpd_Users]    Script Date: 30/05/2019 20:52:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spUpd_Users]
(
	@UserLogin varchar(40) =null,
	@UserLoginName varchar(40)=null,
	@UserId Integer=null,
	@UserName varchar(40)=null,
	@UserPassword varchar(40)=null,
	@UserEnabled Integer=null,
	@laboratoryID integer=null,
	@ClientId integer=null,
	@UserType integer=null	
)
AS
BEGIN

update Users set
	UserName = @UserName,
	UserLoginName = @UserLoginName,
	UserEnabled = @UserEnabled,
	UserPassword = @UserPassword,
	Filter = @UserName+'|'+@UserLoginName
where
	UserId = @UserId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Alteração', 
	@ProcessLogTable = 'Users', 
	@ProcessLogField = 'UserId', 
	@ProcessLogValue = @UserId

END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Doctors"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 215
            End
            DisplayFlags = 280
            TopColumn = 20
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'viewDoctors'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'viewDoctors'
GO
USE [master]
GO
ALTER DATABASE [YouVita] SET  READ_WRITE 
GO
