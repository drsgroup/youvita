Alter Table Patients Add Filter varchar(200) null
Alter Table AddressType Add  Filter varchar(200) null
Alter Table PatientPhones Add  Filter varchar(200) null
Alter Table PatientAddresses Add  Filter varchar(200) null
Alter Table MedicinesPMC Add  Filter varchar(200) null
Alter Table ProcessLog Add  Filter varchar(200) null
Alter Table Medicines Add  Filter varchar(200) null
Alter Table Users Add  Filter varchar(200) null
Alter Table PhoneType Add  Filter varchar(200) null
Alter Table States Add  Filter varchar(200) null
Alter Table DispensationPeriod Add  Filter varchar(200) null
Alter Table ContactType Add  Filter varchar(200) null
Alter Table ClientType Add  Filter varchar(200) null
Alter Table Pathologies Add  Filter varchar(200) null
Alter Table Clients Add  Filter varchar(200) null
Alter Table ClientsContacts Add  Filter varchar(200) null
Alter Table PatientPathologies Add  Filter varchar(200) null
Alter Table Permissions Add  Filter varchar(200) null
Alter Table Configurations Add  Filter varchar(200) null