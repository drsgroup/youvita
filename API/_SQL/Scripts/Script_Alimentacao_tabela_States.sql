-- Incluindo dados na tabela States

Use YouVita
GO

INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'AC', 'Acre');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'AL', 'Alagoas');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'AM', 'Amazonas');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'AP', 'Amap�');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'BA', 'Bahia');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'CE', 'Cear�');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'DF', 'Distrito Federal');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'ES', 'Esp�rito Santo');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'GO', 'Goi�s');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'MA', 'Maranh�o');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'MG', 'Minas Gerais');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'MS', 'Mato Grosso do Sul');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'MT', 'Mato Grosso');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'PA', 'Par�');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'PB', 'Para�ba');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'PE', 'Pernambuco');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'PI', 'Piau�');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'PR', 'Paran�');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'RJ', 'Rio de Janeiro');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'RN', 'Rio Grande do Norte');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'RO', 'Rond�nia');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'RR', 'Roraima');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'RS', 'Rio Grande do Sul');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'SC', 'Santa Catarina');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'SE', 'Sergipe');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'SP', 'S�o Paulo');
INSERT INTO States (StateEnabled,StateUF,StateName) VALUES (1,'TO', 'Tocantins');

GO
select * from States