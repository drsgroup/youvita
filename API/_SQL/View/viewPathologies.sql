drop view viewPathologies
go

Create view viewPathologies as 
Select 
	Pathologies.PathologyId,
	Pathologies.PathologyDescription,
	Pathologies.PathologiesEnabled,
	Pathologies.Filter
from Pathologies where Pathologies.PathologiesEnabled = 1