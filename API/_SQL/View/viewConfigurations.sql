drop view viewConfigurations
go

Create view viewConfigurations as 
Select 
	Configurations.ConfigurationId,
	Configurations.ConfigurationName,
	Configurations.ConfigurationValue,
	Configurations.ConfigurationEnabled,
	Configurations.filter
from Configurations where Configurations.ConfigurationEnabled = 1