use YouVita
drop view viewUsers
go

Create view viewUsers as 

Select 
	UserId,
	UserName,
	UserLoginName,
	UserPassword,
	UserEnabled,
	Filter
from Users where UserEnabled = 1;


