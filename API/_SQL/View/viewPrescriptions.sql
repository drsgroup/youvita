use YouVita
go
drop view viewPrescriptions
go

Create view viewPrescriptions as 
Select 
	Prescriptions.PrescriptionId,
	Prescriptions.PatientId,
	Prescriptions.PrescriptionDate,
	Prescriptions.ExpirationDate,
	Prescriptions.CRM,
	Prescriptions.UfCrm,
	Prescriptions.DoctorName,
	Prescriptions.PathImagePrescription,
	Prescriptions.PrescriptionEnabled,
	'' FileContent,
	'' FileType
from Prescriptions 
where Prescriptions.PrescriptionEnabled = 1;

