use YouVita
drop view viewContactType
go

Create view viewContactType as 
Select 
	ContactType.ContactTypeId,
	ContactType.ContactDescription,
	ContactType.ContactTypeEnabled,
	ContactType.Filter
from ContactType where ContactType.ContactTypeEnabled = 1;


