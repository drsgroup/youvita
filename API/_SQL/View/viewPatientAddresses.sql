use youvita
go

drop view viewPatientAddresses
go

create view viewPatientAddresses as
select * from PatientAddresses where PatientAddressesEnabled = 1