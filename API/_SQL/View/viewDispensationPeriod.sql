use YouVita
--drop view viewDispensationPeriod
go

drop view viewDispensationPeriod
go


Create view viewDispensationPeriod as 
Select 
	DispensationPeriod.DispensationId,
	DispensationPeriod.Description,
	DispensationPeriod.QtyDays,
	DispensationPeriod.DispensationPeriodEnabled,
	DispensationPeriod.Filter
from DispensationPeriod where DispensationPeriod.DispensationPeriodEnabled = 1;

--select * from viewStates;