drop view viewClientType
go

Create view viewClientType as 
Select 
	ClientType.ClientTypeId,
	ClientType.ClientTypeDescription,
	ClientType.ClientTypeEnabled,
	ClientType.Filter
from ClientType where ClientType.ClientTypeEnabled = 1