USE [YouVitaHML]

GO

Create view [dbo].[viewClientsContacts] as 
SELECT [ContactId]
      ,[ClientId]
      ,[ContactName]
      ,[Email]
      ,[Password]
      ,[Occupation]
      ,[Phone]
      ,[Ramal]
      ,[CellPhone]
      ,[ClientsContactsEnabled]
  FROM dbo.[ClientsContacts]
where ClientsContacts.ClientsContactsEnabled = 1



