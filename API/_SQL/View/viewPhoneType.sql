drop view viewPhoneType
go

Create view viewPhoneType as 
Select 
	PhoneType.PhoneTypeId,
	PhoneType.Description,
	PhoneType.PhoneTypeEnabled,
	PhoneType.Filter
from PhoneType where PhoneType.PhoneTypeEnabled = 1