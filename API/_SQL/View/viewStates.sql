use YouVita
go

drop view viewStates
go

Create view viewStates as 
Select 
	States.StateId,
	States.StateName,
	States.StateUF,
	States.StateEnabled,
	States.Filter
from States where States.StateEnabled = 1;



