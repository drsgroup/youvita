drop view viewPatientPathologies
go

Create view viewPatientPathologies as 
Select 
	PatientPathologies.PatientPathologiesId,
	PatientPathologies.PatientId,
	PatientPathologies.PathologyId,
	PatientPathologies.PatientPathologiesEnabled,
	PatientPathologies.Filter
from PatientPathologies where PatientPathologies.PatientPathologiesEnabled = 1


