create view viewUserConfigurations as

select 
Us.UserId,
Ut.UserTypeId,
Ut.UserTypeName,
st.SystemId,
st.SystemName,
sr.SystemRouteId,
sr.SystemRoute
from
Users Us
Inner Join UserTypes Ut on (ut.UserTypeId = us.UserType)
Inner Join Systems st on (st.SystemId = ut.SystemId)
Inner Join SystemRoutesUserTypes  sru on (sru.UserTypeId = ut.UserTypeId)
Inner Join SystemRoutes sr on (sr.SystemRouteId = sru.SystemRouteId)