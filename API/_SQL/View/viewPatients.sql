use YouVitaHML
GO
drop view dbo.viewPatients
GO
Create view dbo.viewPatients as 
SELECT PatientId
      ,ClientId
      ,PatientName
      ,BirthDate
      ,CPF
      ,RG
      ,IdentificationOnClient
      ,Responsible
      ,BestContactPeriod
      ,Comments
      ,PatientStatus
      ,CID1
      ,CID2
      ,CID3
      ,PatientEnabled
	  ,Filter
	  ,PatientType
  FROM dbo.Patients
	WHERE
		PatientEnabled = 1

GO


