use YouVita
--drop view viewClients
go

drop view viewClients
go

Create view viewClients as 
SELECT [ClientId]
      ,[ClientName]
      ,[ClientFantasyName]
      ,[CNPJ]
      ,[Address]
      ,[AddressNumber]
      ,[Neighborhood]
      ,[City]
      ,[ZipCode]
      ,[StateId]
      ,[ResponsibleContact]
      ,[Occupation]
      ,[Email]
      ,[Phone]
      ,[Ramal]
      ,[CellPhone]
      ,[ClientLogin]
      ,[ClientPassword]
      ,[ConfirmPassword]
      ,[ClientTypeId]
      ,[ClientStatus]
      ,[Comments]
      ,[ClientsEnabled]
  FROM [Clients] where Clients.ClientsEnabled = 1;

--select * from viewClients;