drop view viewPatientPhones
go

Create view viewPatientPhones as 
Select 
	PatientPhones.PatientPhoneId,
	PatientPhones.PatientId,
	PatientPhones.PhoneNumber,
	PatientPhones.Ramal,
	PatientPhones.PhoneTypeId,
	PatientPhones.PatientPhonesEnabled,
	PatientPhones.filter
from PatientPhones where PatientPhones.PatientPhonesEnabled = 1