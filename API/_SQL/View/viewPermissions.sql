drop view viewPermissions
go

Create view viewPermissions as 
Select 
	Permissions.PermissionId,
	Permissions.PermissionName,
	Permissions.PermissionEnabled,
	Permissions.filter
from Permissions where Permissions.PermissionEnabled = 1