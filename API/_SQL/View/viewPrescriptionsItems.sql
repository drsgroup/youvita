use YouVita
go
drop view viewPrescriptionsItems
go

Create view viewPrescriptionsItems as 
Select 
	PrescriptionsItems.PrescriptionItemId,
	PrescriptionsItems.PrescriptionId,
	PrescriptionsItems.MedicineId,
	PrescriptionsItems.DailyDose,
	PrescriptionsItems.TermDays,
	PrescriptionsItems.PrescriptionItemEnabled
from PrescriptionsItems where PrescriptionsItems.PrescriptionItemEnabled = 1;

--select * from viewPrescriptionsItems;