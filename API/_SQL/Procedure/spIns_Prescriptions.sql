USE [YouVitaHML]
GO

/****** Object:  StoredProcedure [dbo].[spIns_Prescriptions]    Script Date: 06/06/2019 21:42:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[spIns_Prescriptions]
(
	@UserLogin varchar(20),
	@PrescriptionId int output,
    @PatientId int,
    @PrescriptionDate datetime,
    @ExpirationDate datetime,
    @CRM varchar(10),
    @UfCrm varchar(2),
    @DoctorName varchar(60),
	@PathImagePrescription varchar(260),	
	@PrescriptionEnabled int,
	@FileContent varchar(max),
	@FileType varchar(10)
)
AS
BEGIN

Insert Into Prescriptions (PatientId,PrescriptionDate,ExpirationDate,CRM,UfCrm,DoctorName,PathImagePrescription,PrescriptionEnabled)
values (@PatientId,@PrescriptionDate,@ExpirationDate,@CRM,@UfCrm,@DoctorName,@PathImagePrescription,@PrescriptionEnabled)

select @PrescriptionId = @@IDENTITY

-- Guarda o Arquivo
Insert Into PrescriptionFiles values (@PrescriptionId, @FileContent, @FileType)


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclus�o', 
	@ProcessLogTable = 'Prescriptions', 
	@ProcessLogField = 'PrescriptionId', 
	@ProcessLogValue = @PrescriptionId

END
GO


