select *
  from dbo.MedicineInteractionPatient
  where MedicineInteractionPatientID

select * 
  from dbo.MedicineInteractionPatientItem i
where i.InteractionMedicineID = 5

select distinct b.ActivePrincipleName, a.ActivePrincipleName, m.*
from MedicinesInteraction m
join ActivePrinciple a on (m.ActivePrincipleID1 = a.ActivePrincipleID)
join ActivePrinciple b on (m.ActivePrincipleID2 = b.ActivePrincipleID)


select i.ean
  from MedicineInteractionPatient m
  join dbo.MedicineInteractionPatientItem i on (m.MedicineInteractionPatientID = i.InteractionMedicineID)
where m.MedicineInteractionPatientID = 6

select *
  from dbo.MedicineInteractionPatientItem i 
  where i.InteractionMedicineID = 6

select i.ean, j.ean
  from dbo.MedicineInteractionPatientItem i
  cross join dbo.MedicineInteractionPatientItem j
where i.ean <> j.ean
and i.InteractionMedicineID = j.InteractionMedicineID
and j.InteractionMedicineID = 6
order by 1, 2


select m.*
  from dbo.MedicinesInteraction m
  join dbo.ActivePrinciple a on (m.ActivePrincipleID1 = a.ActivePrincipleID)
  join dbo.Medicines ma on (ma.ActivePrincipleId = a.ActivePrincipleID)
  join dbo.ActivePrinciple b on (m.ActivePrincipleID2 = b.ActivePrincipleID)
  join dbo.Medicines mb on (b.ActivePrincipleID = mb.ActivePrincipleId)
where 1 = 1
and ( ma.EAN ='7896094903814'
and mb.ean = '7896112468752')
or (ma.EAN ='7896112468752'
and mb.ean = '7896094903814')

select m.description, m.Severity, ma.ean, mb.ean
  from dbo.MedicinesInteraction m
  join dbo.ActivePrinciple a on (m.ActivePrincipleID1 = a.ActivePrincipleID)
  join dbo.ActivePrinciple b on (m.ActivePrincipleID2 = b.ActivePrincipleID)
  join dbo.Medicines ma on (ma.ActivePrincipleId = a.ActivePrincipleID)
  join dbo.Medicines mb on (mb.ActivePrincipleId = b.ActivePrincipleID)
where (ma.EAN ='7898123906346'
  and mb.EAN = '7896006245322')
  or (ma.EAN ='7896006245322'
  and mb.EAN = '7898123906346')

m.ActivePrincipleID1 = 5512
  and m.ActivePrincipleID2 = 5001



