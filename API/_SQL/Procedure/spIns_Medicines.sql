use YouVita
drop procedure spIns_Medicines
GO

create procedure spIns_Medicines
(
	@UserLogin varchar(20),
	@MedicineId int output,
	@EAN varchar(13),
	@MedicineName varchar(100),
	@LabId varchar(10),
	@LabName varchar(100),
	@ActivePrincipleId varchar(10),
	@ActivePrincipleName varchar(100),
	@Presentation varchar(100),
	@UnitId varchar(10),
	@UnitName varchar(100),
	@Dosage int,
	@QtyTotal int,
	@MedicineType varchar(20),
	@Comments varchar(200),
	@MedicineEnabled int
)
AS
BEGIN

Insert Into Medicines (MedicineId, EAN, MedicineName, LabId, LabName, ActivePrincipleId, ActivePrincipleName, Presentation, UnitId, UnitName, Dosage, QtyTotal, MedicineType, Comments, MedicineEnabled, filter)
values (@MedicineId, @EAN, @MedicineName, @LabId, @LabName, @ActivePrincipleId, @ActivePrincipleName, @Presentation, @UnitId, @UnitName, @Dosage, @QtyTotal, @MedicineType, @Comments, @MedicineEnabled,@EAN + '|' + @MedicineName)

select @MedicineId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclus�o', 
	@ProcessLogTable = 'Medicines', 
	@ProcessLogField = 'MedicineId', 
	@ProcessLogValue = @MedicineId

END