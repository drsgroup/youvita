use YouVita
go

Drop procedure spUpd_Prescriptions
go

create procedure spUpd_Prescriptions
(
	@UserLogin varchar(20),
	@PrescriptionId int,
    @PatientId int,
    @PrescriptionDate datetime,
    @ExpirationDate datetime,
    @CRM varchar(10),
    @UfCrm varchar(2),
    @DoctorName varchar(60),
	@PathImagePrescription varchar(260),	
	@PrescriptionEnabled int
)
AS
BEGIN

update Prescriptions set
	PatientId = @PatientId,
	PrescriptionDate = @PrescriptionDate,
	ExpirationDate = @ExpirationDate,
	CRM = @CRM,
	UfCrm = @UfCrm,
	DoctorName = @DoctorName,
	PathImagePrescription = @PathImagePrescription,	
	PrescriptionEnabled = @PrescriptionEnabled
where
	PrescriptionId = @PrescriptionId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Altera��o', 
	@ProcessLogTable = 'Prescriptions', 
	@ProcessLogField = 'PrescriptionId', 
	@ProcessLogValue = @PrescriptionId

END

