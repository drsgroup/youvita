
drop procedure spIns_Clients
GO

create procedure spIns_Clients
(
	@UserLogin varchar(40),
	@ClientId integer output,
	@ClientName varchar(60),
	@ClientFantasyName varchar(60),
	@CNPJ varchar(14),
	@Address varchar(60),
	@AddressNumber varchar(10),
	@Neighborhood varchar(50),
	@City varchar(50),
    @ZipCode varchar(9),
	@StateId integer, 
	@ResponsibleContact varchar(50),
	@Occupation varchar(50),
	@Email varchar(60),
	@Phone varchar(20),
	@Ramal varchar(10),
    @CellPhone varchar(20),
	@ClientLogin varchar(60),
	@ClientPassword varchar(20),
	@ConfirmPassword varchar(20),	
	@ClientTypeId Integer,
	@ClientsEnabled integer,
	@ClientStatus varchar(1),
	@Comments varchar(200)
)
AS
BEGIN

INSERT INTO [dbo].[Clients]
           ([ClientName]
           ,[ClientFantasyName]
           ,[CNPJ]
           ,[Address]
           ,[AddressNumber]
           ,[Neighborhood]
           ,[City]
           ,[ZipCode]
           ,[StateId]
           ,[ResponsibleContact]
           ,[Occupation]
           ,[Email]
           ,[Phone]
           ,[Ramal]
           ,[CellPhone]
           ,[ClientLogin]
           ,[ClientPassword]
           ,[ConfirmPassword]
           ,[ClientTypeId]
           ,[ClientStatus]
           ,[Comments]
           ,[ClientsEnabled])
     VALUES
    (
	@ClientName,
	@ClientFantasyName,
	@CNPJ,
	@Address,
	@AddressNumber,
	@Neighborhood,
	@City,
    @ZipCode,
	@StateId,
	@ResponsibleContact,
	@Occupation,
	@Email,
	@Phone,
	@Ramal,
    @CellPhone,
	@ClientLogin,
	@ClientPassword,
	@ConfirmPassword,
	@ClientTypeId,
	@ClientStatus,		
	@Comments,
	@ClientsEnabled)



select @ClientId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclus�o', 
	@ProcessLogTable = 'Clients', 
	@ProcessLogField = 'ClientId', 
	@ProcessLogValue = @ClientId


END