USE [YouVita]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

drop procedure [dbo].[spIns_PatientAddresses]
GO

CREATE  procedure [dbo].[spIns_PatientAddresses]
(
	@UserLogin varchar(40),
	@PatientAddressesId int output,
	@PatientId int,
	@AddressTypeId int,
	@Address varchar(60),
	@AddressNumber varchar(10),
	@City varchar(50),
	@Neighborhood varchar(50),
	@StateId integer, 
	@ZipCode varchar(8),
	@Complement varchar(200),
	@PatientAddressesEnabled int
)
AS
BEGIN


Insert Into PatientAddresses
 (PatientId,	AddressTypeId,	Address,	AddressNumber,	City,	Neighborhood,
	StateId, 	ZipCode,	Complement,	PatientAddressesEnabled,filter)
values
 (@PatientId,	@AddressTypeId,	@Address,	@AddressNumber,	@City,	@Neighborhood,
	@StateId, 	@ZipCode,	@Complement,	@PatientAddressesEnabled,Convert(Varchar(20),@PatientId) + '|' + @City + '|' + @Neighborhood + '|' + @Address)

select @PatientAddressesId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclus�o', 
	@ProcessLogTable = 'PatientAddresses', 
	@ProcessLogField = 'PatientAddressesId', 
	@ProcessLogValue = @PatientAddressesEnabled

END

GO
