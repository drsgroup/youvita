drop procedure spIns_PharmaceuticalAttentionsAdverseEffects
GO

Create Procedure spIns_PharmaceuticalAttentionsAdverseEffects
(
	@UserLogin varchar(40),
	@AdverseEffectsId int,
	@PharmaceuticalAttentionId int
)
AS
BEGIN

Insert into PharmaceuticalAttentionsAdverseEffects (AdverseEffectsId, PharmaceuticalAttentionId) values (@AdverseEffectsId, @PharmaceuticalAttentionId)

Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclus�o', 
	@ProcessLogTable = 'PharmaceuticalAttentionsAdverseEffects', 
	@ProcessLogField = 'PharmaceuticalAttentionId', 
	@ProcessLogValue = @PharmaceuticalAttentionId


END