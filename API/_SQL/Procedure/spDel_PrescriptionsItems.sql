use YouVita
drop procedure spDel_PrescriptionsItems
GO

create procedure spDel_PrescriptionsItems
(
    @UserLogin varchar(40),
	@PrescriptionItemId Integer
)
AS
BEGIN

Update PrescriptionsItems set PrescriptionItemEnabled = 0 where PrescriptionItemId = @PrescriptionItemId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclus�o', 
	@ProcessLogTable = 'PrescriptionsItems', 
	@ProcessLogField = 'PrescriptionItemId', 
	@ProcessLogValue = @PrescriptionItemId

END