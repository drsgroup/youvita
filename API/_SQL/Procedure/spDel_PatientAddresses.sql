Use YouVita
GO

drop procedure spDel_PatientAddresses
GO

create procedure spDel_PatientAddresses
(
    @UserLogin varchar(40),
	@PatientAddressId Integer
)
AS
BEGIN

Update PatientAddresses set PatientAddressesEnabled = 0 where PatientAddressesId = @PatientAddressId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclus�o', 
	@ProcessLogTable = 'PatientAddresses', 
	@ProcessLogField = 'PatientAddressesId', 
	@ProcessLogValue = @PatientAddressId

END