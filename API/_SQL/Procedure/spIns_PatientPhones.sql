USE [YouVita]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

drop procedure [dbo].[spIns_PatientPhones]
go


CREATE  procedure [dbo].[spIns_PatientPhones]
(
	@UserLogin varchar(40),
	@PatientPhoneId integer output,
	@PatientId Integer,
	@PhoneNumber varchar(12),
	@Ramal varchar(10),
	@PhoneTypeId integer,
	@PatientPhonesEnabled integer
)
AS
BEGIN


Insert Into PatientPhones(PatientId, PhoneNumber, Ramal, PhoneTypeId, PatientPhonesEnabled,filter)
values ( @PatientId,@PhoneNumber, @Ramal, @PhoneTypeId, @PatientPhonesEnabled,Convert(Varchar(20),@PatientId) + '|' + @PhoneNumber)
Select @PatientPhoneId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclus�o', 
	@ProcessLogTable = 'PatientPhones', 
	@ProcessLogField = 'PatientPhoneId', 
	@ProcessLogValue = @PatientPhoneId

END

GO
