sp_helptext spIns_MedicineInteractionPatient
create table MedicineInteractionPatientItem(MedicineInteractionPatientItemId int not null identity,
                                            InteractionMedicineID int,
											EAN varchar(20))
alter table MedicineInteractionPatientItem add constraint pk_MedicineInteractionPatientItem primary key(MedicineInteractionPatientItemID)
 


alter procedure [dbo].[spIns_MedicineInteractionPatientItem]
(
	@UserLogin varchar(40)= 'teste',
	@MedicineInteractionPatientItemID integer = 0 output,
	@InteractionMedicineID integer,
	@EAN varchar(20)
	
)
AS
BEGIN

INSERT INTO [dbo].MedicineInteractionPatientItem
           (InteractionMedicineID, EAN)
     VALUES
           (@InteractionMedicineID,
            @EAN)

select @MedicineInteractionPatientItemID = @@IDENTITY
-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin,
	@ProcessLogName  = 'Inclus�o', 
	@ProcessLogTable = 'MedicineInteractionPatientItem', 
	@ProcessLogField = 'MedicineInteractionPatientItemID', 
	@ProcessLogValue = @MedicineInteractionPatientItemID

END