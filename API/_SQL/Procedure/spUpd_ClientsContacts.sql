use YouVitaHML
GO
Drop procedure dbo.spUpd_ClientsContacts
GO

create procedure dbo.spUpd_ClientsContacts
(
	@UserLogin varchar(40),
	@ContactId integer,
	@ClientId integer,
	@ContactName varchar(40),
	@Occupation varchar(40),
	@Email varchar(100),
	@Phone varchar(12),
	@Ramal varchar(8),
	@Password varchar(20),
	@CellPhone varchar(20),
	@ClientsContactsEnabled integer
)
AS
BEGIN

update dbo.ClientsContacts set
	ClientId = @ClientId,
	ContactName = @ContactName,
	Occupation = @Occupation,
	Email = @Email,
	Phone = @Phone,
	Ramal = @Ramal,
	Password = @Password,
	CellPhone = @CellPhone,
	ClientsContactsEnabled = @ClientsContactsEnabled
where
	ContactId = @ContactId

-- Obrigatorio, incluir o log do processo.
Exec dbo.spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Altera��o', 
	@ProcessLogTable = 'ClientsContacts', 
	@ProcessLogField = 'ContactId', 
	@ProcessLogValue = @ContactId

END