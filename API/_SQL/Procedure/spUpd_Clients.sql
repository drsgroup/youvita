use YouVita
Drop procedure spUpd_Clients
GO

create procedure spUpd_Clients
(
	@UserLogin varchar(40),
	@ClientId integer,
	@ClientName varchar(60),
	@ClientFantasyName varchar(60),
	@CNPJ varchar(14),
	@Address varchar(60),
	@AddressNumber varchar(10),
	@City varchar(50),
	@Neighborhood varchar(50),
	@StateId integer, 
	@ZipCode varchar(8),
	@FocalClientContact varchar(40),
	@DispensationId integer,
	@BillingStartDay integer,
	@BillingEndDay integer,
	@Email varchar(60),
	@ClientLogin varchar(60),
	@ClientPassword varchar(20),
	@ConfirmPassword varchar(20),
	@ResponsibleContact varchar(50),
	@Comments varchar(200),
	@ClientStatus varchar(1),
	@ClientTypeId Integer,
	@ClientsEnabled integer
)
AS
BEGIN

update Clients set
	ClientName = @ClientName, 
	ClientFantasyName = @ClientFantasyName, 
	CNPJ = @CNPJ, 
	Address = @Address, 
	AddressNumber = @AddressNumber, 
	City = @City, 
	Neighborhood = @Neighborhood, 
	StateId = @StateId, 
	ZipCode = @ZipCode, 
	FocalClientContact = @FocalClientContact, 
	DispensationId = @DispensationId, 
	BillingStartDay = @BillingStartDay, 
	BillingEndDay = @BillingEndDay, 
	Email = @Email, 
	ClientLogin = @ClientLogin, 
	ClientPassword = @ClientPassword, 
	ConfirmPassword = @ConfirmPassword, 
	ResponsibleContact = @ResponsibleContact, 
	Comments = @Comments, 
	ClientStatus = @ClientStatus, 
	ClientTypeId = @ClientTypeId, 
	ClientsEnabled = @ClientsEnabled,
	Filter = @ClientName + '|' + @ClientFantasyName + '|'+ @CNPJ
where
	ClientId = @ClientId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Altera��o', 
	@ProcessLogTable = 'Clients', 
	@ProcessLogField = 'ClientId', 
	@ProcessLogValue = @ClientId

END