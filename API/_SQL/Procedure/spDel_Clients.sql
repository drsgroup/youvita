use YouVita

GO
Drop procedure spDel_Clients
GO

create procedure spDel_Clients
(
    @UserLogin varchar(40),
	@ClientId Integer
)
AS
BEGIN

Update Clients set ClientsEnabled = 0 where ClientId = @ClientId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclus�o', 
	@ProcessLogTable = 'Clients', 
	@ProcessLogField = 'ClientId', 
	@ProcessLogValue = @ClientId

END