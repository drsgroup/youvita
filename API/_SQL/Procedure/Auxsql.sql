exec spExec_InteractionMedicines 6

select *
  from MedicineInteractionPatientResults

  delete MedicineInteractionPatientResults

  select m.*, md1.MedicineName, md2.MedicineName
    from MedicineInteractionPatientResults m
	join Medicines md1 on (m.ean1 = md1.EAN)
	join Medicines md2 on (m.ean2 = md2.EAN)
where m.MedicineInteractionPatientItemId = 7

  exec spExec_InteractionMedicines 7

  select * from MedicineInteractionPatientResults
  select * from MedicineInteractionPatient
  
  
select *
 from MedicineInteractionPatient

select i.*, m.MedicineName,  m.ActivePrincipleName
 from MedicineInteractionPatientItem i
 join Medicines m on (i.ean = m.EAN)
where i.InteractionMedicineId = 10

select * from Patients
select p.*, c.ClientFantasyName from viewPatients p join viewClients c on (p.ClientId = c.ClientId) where 1 = 1


  alter table MedicineInteractionPatientResults add ean1 varchar(20)
  alter table MedicineInteractionPatientResults add ean2 varchar(20);

  select m.*, p.PatientName
    from MedicineInteractionPatient m
	join viewPatients p on (m.PatientID = p.PatientId)

	 select m.*, p.PatientName from MedicineInteractionPatient m  join viewPatients p on (m.PatientID = p.PatientId)  where m.patientId = 10


 select m.*, p.PatientName from MedicineInteractionPatient m 
            join viewPatients p on (m.PatientID = p.PatientId) 
			join viewClients c on (p.ClientId = c.ClientId)
            where 1 = 1
			and p.PatientName

			select * from MedicineInteractionPatient

select m.*, p.PatientName,  c.ClientName
from MedicineInteractionPatient m    
join viewPatients p on(m.PatientID = p.PatientId) 
join viewClients c on(p.ClientId = c.ClientId)   where 1 = 1 

select m.*, p.PatientName,  c.clientName 
from MedicineInteractionPatient m    
join viewPatients p on(m.PatientID = p.PatientId) 
join viewClients c on(p.ClientId = c.ClientId)   
where 1 = 1 
and m.DateInteraction >= '2019-01-01'


select m.*, p.PatientName,  c.clientName 
from MedicineInteractionPatient m    
join viewPatients p on(m.PatientID = p.PatientId) join viewClients c on(p.ClientId = c.ClientId)   where 1 = 1  and m.DateInteraction >= '2019-05-08' and m.DateInteraction <= '2019-06-26 23:59:59'


sp_help prescriptionfiles

alter procedure dbo.spIns_Prescriptions
(
	@UserLogin varchar(20),
	@PrescriptionId int output,
    @PatientId int,
    @PrescriptionDate datetime,
    @ExpirationDate datetime,
    @CRM varchar(10),
    @UfCrm varchar(2),
    @DoctorName varchar(60),
	@PathImagePrescription varchar(260),	
	@PrescriptionEnabled int,
	@FileContent varchar(max),
	@FileType varchar(10)
)
AS
BEGIN

Insert Into Prescriptions (PatientId,PrescriptionDate,ExpirationDate,CRM,UfCrm,DoctorName,PathImagePrescription,PrescriptionEnabled)
values (@PatientId,@PrescriptionDate,@ExpirationDate,@CRM,@UfCrm,@DoctorName,@PathImagePrescription,@PrescriptionEnabled)

select @PrescriptionId = @@IDENTITY

-- Guarda o Arquivo
Insert Into PrescriptionFiles values (@PrescriptionId, @FileContent, @FileType)


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclus�o', 
	@ProcessLogTable = 'Prescriptions', 
	@ProcessLogField = 'PrescriptionId', 
	@ProcessLogValue = @PrescriptionId

END