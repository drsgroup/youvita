USE [YouVitaHML]
GO
DROP PROCEDURE [dbo].[spIns_ClientsContacts]
GO

/****** Object:  StoredProcedure [dbo].[spIns_ClientsContacts]    Script Date: 13/06/2019 21:24:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spIns_ClientsContacts]
(
	@UserLogin varchar(40),
	@ClientId int,
	@ContactId integer output,	
    @ContactName varchar(40),
    @Email varchar(100), 
    @Password varchar(20),
    @Occupation varchar(40),
    @Phone varchar(20),
    @Ramal varchar(8),
    @CellPhone varchar(20),
    @ClientsContactsEnabled int
)
AS
BEGIN

INSERT INTO [dbo].[ClientsContacts]
           ([ClientId]
           ,[ContactName]
           ,[Email]
           ,[Password]
           ,[Occupation]
           ,[Phone]
           ,[Ramal]
           ,[CellPhone]
           ,[ClientsContactsEnabled])
     VALUES
   (@ClientId,
    @ContactName,
    @Email, 
    @Password,
    @Occupation,
    @Phone,
    @Ramal,
    @CellPhone,
    @ClientsContactsEnabled)



Select @ContactId = @@IDENTITY


-- Obrigatorio, incluir o log do processo.
Exec dbo.spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclus�o', 
	@ProcessLogTable = 'ClientsContacts', 
	@ProcessLogField = 'ContactId', 
	@ProcessLogValue = @ContactId

END


GO


