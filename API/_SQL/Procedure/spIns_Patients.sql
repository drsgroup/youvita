use YouVitaHML
GO
drop procedure dbo.spIns_Patients

GO

CREATE  procedure dbo.spIns_Patients
(
	@UserLogin varchar(40),
	@PatientId Integer output,
	@ClientId Integer,
	@PatientName varchar(60),
	@BirthDate DateTime,
	@CPF varchar(11),
	@RG varchar(15),
	@IdentificationOnClient varchar(20),
	@Responsible varchar(60),
	@BestContactPeriod varchar(20),
	@Comments varchar(200),
	@PatientStatus varchar(1),
	@CID1 varchar(20),
	@CID2 varchar(20),
	@CID3 varchar(20),
	@PatientEnabled integer,
	@ClientFantasyName varchar(50),
	@PatientType varchar(1)
)
AS
BEGIN


Insert Into dbo.Patients (ClientId, PatientName, BirthDate, CPF, RG, IdentificationOnClient, Responsible,
					  BestContactPeriod, Comments, PatientStatus, CID1, CID2, CID3, PatientEnabled,filter,PatientType)
values (@ClientId, @PatientName, @BirthDate, @CPF, @RG, @IdentificationOnClient, @Responsible,
		@BestContactPeriod, @Comments, @PatientStatus, @CID1, @CID2, @CID3, @PatientEnabled,
		@PatientName + '|' + @CPF,@PatientType)

select @PatientId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec dbo.spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclus�o', 
	@ProcessLogTable = 'Patients', 
	@ProcessLogField = 'PatientId', 
	@ProcessLogValue = @PatientId

END



GO


