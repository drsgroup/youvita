use YouVita
drop procedure spIns_ReleaseOrder
GO

create procedure spIns_ReleaseOrder
(
	@UserLogin varchar(40),
	@ReleaseOrderId integer output,
	@ReleaseOrderDate datetime,
	@PatientId integer,
	@PatientAuthorization varchar(20),
	@DispensationId integer,
	@ReleaseOrderStatus varchar(1),
	@Crm integer,
	@StateId integer,
	@ReleaseOrderObservation varchar(200),
	@ReleaseOrderEnabled integer
)
AS
BEGIN

Insert Into ReleaseOrder (ReleaseOrderDate,PatientId,PatientAuthorization,DispensationId,Crm, StateId, ReleaseOrderObservation,
ReleaseOrderEnabled) values (@ReleaseOrderDate,@PatientId,@PatientAuthorization,@DispensationId,@Crm, @StateId, @ReleaseOrderObservation,@ReleaseOrderEnabled)

select @ReleaseOrderId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclus�o', 
	@ProcessLogTable = 'ReleaseOrder', 
	@ProcessLogField = 'ReleaseOrderId', 
	@ProcessLogValue = @ReleaseOrderId

END