use YouVitaHML
GO

drop procedure dbo.spUpd_Patients
GO

CREATE procedure dbo.spUpd_Patients
(
	@UserLogin varchar(40),
	@PatientId Integer,
	@ClientId Integer,
	@PatientName varchar(60),
	@BirthDate DateTime,
	@CPF varchar(11),
	@RG varchar(15),
	@IdentificationOnClient varchar(20),
	@Responsible varchar(60),
	@BestContactPeriod varchar(20),
	@Comments varchar(200),
	@PatientStatus varchar(1),
	@CID1 varchar(20),
	@CID2 varchar(20),
	@CID3 varchar(20),
	@PatientEnabled integer = 1,
	@ClientFantasyName varchar(50)="vazio",
	@PatientType varchar(1)
)
AS
BEGIN

update dbo.Patients set
			ClientId = @ClientId,
			PatientName = @PatientName,
			BirthDate = @BirthDate,
			CPF = @CPF,
			RG = @RG,
			IdentificationOnClient = @IdentificationOnClient,
			Responsible =  @Responsible,
			BestContactPeriod = @BestContactPeriod,
			Comments = @Comments,
			PatientStatus = @PatientStatus,
			CID1 = @CID1,
			CID2 = @CID2,
			CID3 = @CID3,
			filter = @PatientName + '|' + @CPF,
			PatientType = @PatientType

where
	PatientId = @PatientId

-- Obrigatorio, incluir o log do processo.
Exec dbo.spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Altera��o', 
	@ProcessLogTable = 'Patients', 
	@ProcessLogField = 'PatientId', 
	@ProcessLogValue = @PatientId

END




GO


