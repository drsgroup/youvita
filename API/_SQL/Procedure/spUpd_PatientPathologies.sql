USE [YouVita]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

drop procedure [dbo].[spUpd_PatientPathologies]
GO


CREATE procedure [dbo].[spUpd_PatientPathologies]
(
	@UserLogin varchar(40),
	@PatientPathologiesId Integer,
	@PatientId Integer,
	@PathologyId Integer,
	@PatientPathologiesEnabled integer
)
AS
BEGIN

update PatientPathologies set
			PatientId = @PatientId,
			PathologyId = @PathologyId,
			Filter = Convert(Varchar(20),@PatientId)
where
	PatientPathologiesId = @PatientPathologiesId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Altera��o', 
	@ProcessLogTable = 'PatientPathologies', 
	@ProcessLogField = 'PatientPathologiesId', 
	@ProcessLogValue = @PatientPathologiesId

END

GO

