use YouVita
drop procedure spDel_Prescriptions
GO

create procedure spDel_Prescriptions
(
    @UserLogin varchar(40),
	@PrescriptionId Integer
)
AS
BEGIN

Update Prescriptions set PrescriptionEnabled = 0 where PrescriptionId = @PrescriptionId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclus�o', 
	@ProcessLogTable = 'Prescriptions', 
	@ProcessLogField = 'PrescriptionId', 
	@ProcessLogValue = @PrescriptionId

END