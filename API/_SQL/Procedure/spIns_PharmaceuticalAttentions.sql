drop procedure spIns_PharmaceuticalAttentions 
GO

Create Procedure spIns_PharmaceuticalAttentions
(
	@UserLogin varchar(40),
	@PharmaceuticalAttentionId Integer output,
	@PatientId Integer,
	@DateAttention datetime,
	@Observation varchar
)
AS
BEGIN


Insert into PharmaceuticalAttentions
(PatientId,DateAttention,Observation,Filter)
values (@PatientId,@DateAttention,@Observation, Convert(Varchar(10),@PatientId) + '|' + Convert(Varchar(10),@DateAttention,103))

select @PharmaceuticalAttentionId = @@IDENTITY

Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclus�o', 
	@ProcessLogTable = 'PharmaceuticalAttentions', 
	@ProcessLogField = 'PharmaceuticalAttentionId', 
	@ProcessLogValue = @PharmaceuticalAttentionId

END