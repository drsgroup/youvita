USE [YouVita]
GO

drop procedure [dbo].[spIns_Permissions]
go


Create procedure [dbo].[spIns_Permissions]
(
	@UserLogin varchar(40),
	@PermissionId Integer output,
	@PermissionName varchar(150),
	@PermissionEnabled Integer
)
AS
BEGIN


Insert Into Permissions (PermissionName, PermissionEnabled, Filter) values (@PermissionName, @PermissionEnabled, @PermissionName)
select @PermissionId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclusão', 
	@ProcessLogTable = 'Permissions', 
	@ProcessLogField = 'PermissionName', 
	@ProcessLogValue = @PermissionName

END
