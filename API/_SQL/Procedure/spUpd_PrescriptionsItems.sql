use YouVita
go
Drop procedure spUpd_PrescriptionsItems
go

create procedure spUpd_PrescriptionsItems
(
	@UserLogin varchar(20),
	@PrescriptionItemId int,
	@PrescriptionId int,
	@MedicineId int,
	@DailyDose int,
	@TermDays int,
	@PrescriptionItemEnabled int
)
AS
BEGIN

update PrescriptionsItems set
	PrescriptionId = @PrescriptionId,
	MedicineId = @MedicineId,
	DailyDose = @DailyDose,
	TermDays = @TermDays,
	PrescriptionItemEnabled = @PrescriptionItemEnabled
where
	PrescriptionItemId = @PrescriptionItemId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Altera��o', 
	@ProcessLogTable = 'PrescriptionsItems', 
	@ProcessLogField = 'PrescriptionItemId', 
	@ProcessLogValue = @PrescriptionItemId

END

