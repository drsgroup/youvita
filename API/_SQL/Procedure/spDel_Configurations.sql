Drop procedure spDel_Configurations
GO

create procedure spDel_Configurations
(
    @UserLogin varchar(40),
	@ConfigurationId Integer
)
AS
BEGIN

Update Configurations set ConfigurationEnabled = 0 where ConfigurationId = @ConfigurationId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclus�o', 
	@ProcessLogTable = 'Configurations', 
	@ProcessLogField = 'ConfigurationId', 
	@ProcessLogValue = @ConfigurationId

END