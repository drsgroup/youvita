use YouVita
drop procedure spIns_PrescriptionsItems
GO

create procedure spIns_PrescriptionsItems
(
	@UserLogin varchar(20),
	@PrescriptionItemId int output,
	@PrescriptionId int,
	@MedicineId int,
	@DailyDose int,
	@TermDays int,
	@PrescriptionItemEnabled int
)
AS
BEGIN

Insert Into PrescriptionsItems (PrescriptionId,MedicineId,DailyDose,TermDays,PrescriptionItemEnabled)
values (@PrescriptionId,@MedicineId,@DailyDose,@TermDays,@PrescriptionItemEnabled)

select @PrescriptionItemId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclus�o', 
	@ProcessLogTable = 'PrescriptionsItems', 
	@ProcessLogField = 'PrescriptionItemId', 
	@ProcessLogValue = @PrescriptionItemId

END