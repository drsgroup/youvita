use YouVita

Drop procedure spDel_Users
GO

create procedure spDel_Users
(
    @UserLogin varchar(40),
	@UserId Integer
)
AS
BEGIN

Update Users set UserEnabled = 0 where UserId = @UserId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclus�o', 
	@ProcessLogTable = 'Users', 
	@ProcessLogField = 'UserId', 
	@ProcessLogValue = @UserId

END