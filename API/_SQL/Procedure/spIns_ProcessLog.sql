Drop procedure spIns_ProcessLog
GO

create procedure spIns_ProcessLog
(
	@ProcessLogUser varchar(40),
	@ProcessLogName varchar(100),
	@ProcessLogTable varchar(100),
	@ProcessLogField varchar(100),
	@ProcessLogValue varchar(100)
)
AS

insert into ProcessLog values (getdate(),@ProcessLogUser,@ProcessLogName,@ProcessLogTable,@ProcessLogField,@ProcessLogValue,@ProcessLogName + '|' + @processLogTable + '|' + @ProcessLogField + '|' + @ProcessLogValue);