drop procedure spIns_Configurations
GO

create procedure spIns_Configurations
(
	@UserLogin varchar(40),
	@ConfigurationId Integer output,
	@ConfigurationName varchar(100),
	@ConfigurationValue varchar(400),
	@ConfigurationEnabled Integer
)
AS
BEGIN

Insert Into Configurations (ConfigurationName,ConfigurationValue,ConfigurationEnabled, Filter)  values (@ConfigurationName,@ConfigurationValue,@ConfigurationEnabled, @ConfigurationName)

select @ConfigurationId = @@Identity

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclus�o', 
	@ProcessLogTable = 'Configurations', 
	@ProcessLogField = 'ConfigurationId', 
	@ProcessLogValue = @ConfigurationId

END

