USE [YouVita]
GO

drop procedure [dbo].[spUpd_Permissions]
GO


create procedure [dbo].[spUpd_Permissions]
(
	@UserLogin varchar(40),
	@PermissionId Integer,
	@PermissionName varchar(150),
	@PermissionEnabled Integer
)
AS
BEGIN

update Permissions set
	permissionName = @PermissionName,
	PermissionEnabled = @PermissionEnabled,
	Filter = @PermissionName
where
	PermissionId = @PermissionId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Altera��o', 
	@ProcessLogTable = 'Permissions', 
	@ProcessLogField = 'PermissionId', 
	@ProcessLogValue = @PermissionId

END

GO


