USE YouVita
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP PROCEDURE dbo.spUpd_PatientAddresses
GO

CREATE procedure dbo.spUpd_PatientAddresses
(
	@UserLogin varchar(40),
	@PatientAddressesId int,
	@PatientId int,
	@AddressTypeId int,
	@Address varchar(60),
	@AddressNumber varchar(10),
	@City varchar(50),
	@Neighborhood varchar(50),
	@StateId integer, 
	@ZipCode varchar(8),
	@Complement varchar(200),
	@PatientAddressesEnabled int
)
AS
BEGIN

update PatientAddresses set
	PatientId = @PatientId,
	AddressTypeId = @AddressTypeId,
	Address = @Address,
	AddressNumber = @AddressNumber,
	City = @City,
	Neighborhood = @Neighborhood,
	StateId = @StateId, 
	ZipCode = @ZipCode,
	Complement = @Complement,
	PatientAddressesEnabled = @PatientAddressesEnabled,
	Filter = Convert(Varchar(20),@PatientId) + '|' + @City + '|' + @Neighborhood + '|' + @Address
where
 	PatientAddressesId = @PatientAddressesId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Altera��o', 
	@ProcessLogTable = 'PatientAddresses', 
	@ProcessLogField = 'PatientAddressesId', 
	@ProcessLogValue = @PatientAddressesEnabled

END

GO


