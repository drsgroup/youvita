use YouVita

GO
Drop procedure spDel_ClientsContacts
GO

create procedure spDel_ClientsContacts
(
    @UserLogin varchar(40),
	@ContactId Integer
)
AS
BEGIN

Update ClientsContacts set ClientsContactsEnabled = 0 where ContactId = @ContactId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclus�o', 
	@ProcessLogTable = 'ClientsContacts', 
	@ProcessLogField = 'ContactId', 
	@ProcessLogValue = @ContactId

END