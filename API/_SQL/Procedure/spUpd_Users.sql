use YouVita
Drop procedure spUpd_Users
GO

create procedure spUpd_Users
(
	@UserLogin varchar(40),
	@UserLoginName varchar(40),
	@UserId Integer,
	@UserName varchar(40),
	@UserPassword varchar(40),
	@UserEnabled Integer
)
AS
BEGIN

update Users set
	UserName = @UserName,
	UserLoginName = @UserLoginName,
	UserEnabled = @UserEnabled,
	UserPassword = @UserPassword,
	Filter = @UserName+'|'+@UserLoginName
where
	UserId = @UserId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Altera��o', 
	@ProcessLogTable = 'Users', 
	@ProcessLogField = 'UserId', 
	@ProcessLogValue = @UserId

END