USE [YouVita]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

drop procedure [dbo].[spIns_PatientPathologies]
GO

CREATE  procedure [dbo].[spIns_PatientPathologies]
(
	@UserLogin varchar(40),
	@PatientPathologiesId integer output,
	@PatientId Integer,
	@PathologyId Integer,
	@PatientPathologiesEnabled integer
)
AS
BEGIN


Insert Into PatientPathologies(PatientId, PathologyId, PatientPathologiesEnabled, Filter)
values ( @PatientId,@PathologyId, @PatientPathologiesEnabled,Convert(Varchar(20),@PatientId))
Select @PatientPathologiesId = @@IDENTITY

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclus�o', 
	@ProcessLogTable = 'PatientPathologies', 
	@ProcessLogField = 'PatientPathologiesId', 
	@ProcessLogValue = @PatientPathologiesId

END

GO
