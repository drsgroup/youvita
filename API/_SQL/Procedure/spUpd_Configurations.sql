Drop procedure spUpd_Configurations
GO

create procedure spUpd_Configurations
(
	@UserLogin varchar(40),
	@ConfigurationId Integer,
	@ConfigurationName varchar(100),
	@ConfigurationValue varchar(400),
	@ConfigurationEnabled Integer
)
AS
BEGIN

update configurations set
	configurationName = @ConfigurationName,
	configurationValue = @ConfigurationValue,
	configurationEnabled = @ConfigurationEnabled,
	Filter = @ConfigurationName
where
	ConfigurationId = @ConfigurationId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Altera��o', 
	@ProcessLogTable = 'Configurations', 
	@ProcessLogField = 'ConfigurationId', 
	@ProcessLogValue = @ConfigurationId

END

