drop procedure spDel_Patients
GO

create procedure spDel_Patients
(
    @UserLogin varchar(40),
	@PatientId Integer
)
AS
BEGIN

Update Patients set PatientEnabled = 0 where PatientId = @PatientId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclus�o', 
	@ProcessLogTable = 'Patients', 
	@ProcessLogField = 'PatientId', 
	@ProcessLogValue = @PatientId

END