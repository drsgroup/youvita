USE [YouVita]
GO

/****** Object:  StoredProcedure [dbo].[spUpd_Patients]    Script Date: 12/03/2019 23:27:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

drop procedure [dbo].[spUpd_PatientPhones]
GO

CREATE procedure [dbo].[spUpd_PatientPhones]
(
	@UserLogin varchar(40),
	@PatientPhoneId Integer,
	@PatientId Integer,
	@PhoneNumber varchar(12),
	@Ramal varchar(10),
	@PhoneTypeId integer,
	@PatientPhonesEnabled integer
)
AS
BEGIN

update PatientPhones set
			PatientId = @PatientId,
			PhoneNumber = @PhoneNumber,
			Ramal = @Ramal,
			PhoneTypeId = @PhoneTypeId,
			Filter = Convert(Varchar(20),@PatientId)
where
	PatientPhoneId = @PatientPhoneId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Altera��o', 
	@ProcessLogTable = 'PatientPhones', 
	@ProcessLogField = 'PatientPhoneId', 
	@ProcessLogValue = @PatientPhoneId

END

GO

