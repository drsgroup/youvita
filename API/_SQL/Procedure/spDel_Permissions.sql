USE [YouVita]
GO

drop procedure [dbo].[spDel_Permissions]
GO


create procedure [dbo].[spDel_Permissions]
(
    @UserLogin varchar(40),
	@PermissionId Integer
)
AS
BEGIN

Update Permissions set PermissionEnabled = 0 where PermissionId = @PermissionId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclus�o', 
	@ProcessLogTable = 'Permissions', 
	@ProcessLogField = 'PermissionId', 
	@ProcessLogValue = @PermissionId

END
GO