use YouVita

GO
Drop procedure spDel_Medicines
GO

create procedure spDel_Medicines
(
    @UserLogin varchar(40),
	@MedicineId Integer
)
AS
BEGIN

Update Medicines set MedicineEnabled = 0 where MedicineId = @MedicineId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclus�o', 
	@ProcessLogTable = 'Medicines', 
	@ProcessLogField = 'MedicineId', 
	@ProcessLogValue = @MedicineId

END


