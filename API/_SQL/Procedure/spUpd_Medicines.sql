use YouVita
go

Drop procedure spUpd_Medicines
go

create procedure spUpd_Medicines
(
	@UserLogin varchar(20),
	@MedicineId int,
	@EAN varchar(13),
	@MedicineName varchar(100),
	@LabId varchar(10),
	@LabName varchar(100),
	@ActivePrincipleId varchar(10),
	@ActivePrincipleName varchar(100),
	@Presentation varchar(100),
	@UnitId varchar(10),
	@UnitName varchar(100),
	@Dosage int,
	@QtyTotal int,
	@MedicineType varchar(20),
	@Comments varchar(200),
	@MedicineEnabled int
)
AS
BEGIN

update Medicines set
	EAN = @EAN,
	MedicineName = @MedicineName,
	LabId = @LabId,
	LabName = @LabName,
	ActivePrincipleId = @ActivePrincipleId,
	ActivePrincipleName = @ActivePrincipleName,
	Presentation = @Presentation,
	UnitId = @UnitId,
	UnitName = @UnitName,
	Dosage = @Dosage,
	QtyTotal = @QtyTotal,
	MedicineType = @MedicineType,
	Comments = @Comments,
	MedicineEnabled = @MedicineEnabled,
	Filter = @EAN + '|' + @MedicineName
where
	MedicineId = @MedicineId

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Altera��o', 
	@ProcessLogTable = 'Medicines', 
	@ProcessLogField = 'MedicineId', 
	@ProcessLogValue = @MedicineId

END

