alter procedure spExec_InteractionMedicines(@InteractionMedicineID integer)
as
begin
	declare 
	@EAN1 VARCHAR(20),
	@EAN2 VARCHAR(20)
	  
	  
	DECLARE CR CURSOR FOR   
		select i.ean, j.ean
		  from dbo.MedicineInteractionPatientItem i
		  cross join dbo.MedicineInteractionPatientItem j
		where i.ean <> j.ean
		and i.InteractionMedicineID = j.InteractionMedicineID
		and j.InteractionMedicineID = @InteractionMedicineID
		order by 1, 2;

	begin
	  
		OPEN CR;  
		FETCH NEXT FROM CR   
		INTO @ean1, @ean2
		  
		WHILE @@FETCH_STATUS = 0  
		BEGIN  
			insert into MedicineInteractionPatientResults(MedicineInteractionPatientItemId,
														   DateInteraction,
														   ActivePrinciple1,
														   ActivePrinciple2,
														   Result,
														   Severity, ean1, ean2)
			select @InteractionMedicineID, getdate(), m.ActivePrincipleID1, m.ActivePrincipleID2, m.description, m.Severity, ma.EAN, mb.EAN
			  from dbo.MedicinesInteraction m
			  join dbo.ActivePrinciple a on (m.ActivePrincipleID1 = a.ActivePrincipleID)
			  join dbo.ActivePrinciple b on (m.ActivePrincipleID2 = b.ActivePrincipleID)
			  join dbo.Medicines ma on (ma.ActivePrincipleId = a.ActivePrincipleID)
			  join dbo.Medicines mb on (mb.ActivePrincipleId = b.ActivePrincipleID)
			where (ma.EAN = @ean1
			  and mb.EAN = @ean2)
			  

			  

			FETCH NEXT FROM CR   
			INTO @ean1, @ean2
		END   
		CLOSE cr;  
		DEALLOCATE cr;  
	end
end