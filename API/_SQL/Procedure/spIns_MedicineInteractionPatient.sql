
create procedure [dbo].[spIns_MedicineInteractionPatient]
(
	@UserLogin varchar(40),
	@InteractionMedicineID integer output,
	@PatientID integer,
	@DateInteraction datetime,
	@Severity integer,
	@Comments varchar(200),
	@MedicineInteractionEnabled integer

)
AS
BEGIN

INSERT INTO [dbo].[MedicineInteractionPatient]
           ([PatientID]
           ,[DateInteraction]
           ,[Severity]
           ,[MedicineInteractionEnabled]
           ,[Comments])
     VALUES
           (@PatientID,
            @DateInteraction,
            @Severity,
            @MedicineInteractionEnabled,
            @Comments)

select @InteractionMedicineID = @@IDENTITY
-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclus�o', 
	@ProcessLogTable = 'MedicineInteraction', 
	@ProcessLogField = 'MedicineInteractionID', 
	@ProcessLogValue = @InteractionMedicineID

END