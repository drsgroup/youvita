use YouVita
drop procedure spIns_Users
GO

create procedure spIns_Users
(
	@UserLogin varchar(40),
	@UserLoginName varchar(40),
	@UserName varchar(40),
	@UserPassword varchar(40),
	@UserEnabled Integer,
	@UserId Integer output
)
AS
BEGIN

Insert Into Users (UserLoginName,UserName,UserEnabled,UserPassword, filter) values (@UserLoginName,@UserName,@UserEnabled,@UserPassword,@UserName +'|'+ @UserLoginName)
select @UserId = @@Identity

-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Inclus�o', 
	@ProcessLogTable = 'Users', 
	@ProcessLogField = 'UserLogin', 
	@ProcessLogValue = @UserLoginName

END

