drop procedure spDel_PatientPathologies
go


create procedure spDel_PatientPathologies
(
    @UserLogin varchar(40),
	@PatientPathologiesId Integer
)
AS
BEGIN

Update PatientPathologies set PatientPathologiesEnabled = 0 where PatientPathologiesId = @PatientPathologiesId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclus�o', 
	@ProcessLogTable = 'PatientPathologies', 
	@ProcessLogField = 'PatientPathologiesId', 
	@ProcessLogValue = @PatientPathologiesId

END