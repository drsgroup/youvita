DECLARE 
@EAN1 VARCHAR(20),
@EAN2 VARCHAR(20)
  
  
DECLARE CR CURSOR FOR   
select i.ean, j.ean
  from dbo.MedicineInteractionPatientItem i
  cross join dbo.MedicineInteractionPatientItem j
where i.ean <> j.ean
and i.InteractionMedicineID = j.InteractionMedicineID
and j.InteractionMedicineID = 6
order by 1, 2;

  
OPEN CR;	  
  
FETCH NEXT FROM CR   
INTO @ean1, @ean2
  
WHILE @@FETCH_STATUS = 0  
BEGIN  
select m.description, m.Severity, ma.ean, mb.ean
  from dbo.MedicinesInteraction m
  join dbo.ActivePrinciple a on (m.ActivePrincipleID1 = a.ActivePrincipleID)
  join dbo.ActivePrinciple b on (m.ActivePrincipleID2 = b.ActivePrincipleID)
  join dbo.Medicines ma on (ma.ActivePrincipleId = a.ActivePrincipleID)
  join dbo.Medicines mb on (mb.ActivePrincipleId = b.ActivePrincipleID)
where (ma.EAN ='7898123906346'
  and mb.EAN = '7896006245322')
  or (ma.EAN ='7896006245322'
  and mb.EAN = '7898123906346')    
	FETCH NEXT FROM CR   
INTO @ean1, @ean2

END   
CLOSE cr;  
DEALLOCATE cr;  