drop procedure spDel_PharmaceuticalAttentionsAdverseEffects
GO

Create Procedure spDel_PharmaceuticalAttentionsAdverseEffects
(
	@AdverseEffectsId int,
	@PharmaceuticalAttentionId int
)
AS
BEGIN

 Delete from PharmaceuticalAttentionsAdverseEffects where AdverseEffectsId = @AdverseEffectsId and  PharmaceuticalAttentionId = @PharmaceuticalAttentionId

END