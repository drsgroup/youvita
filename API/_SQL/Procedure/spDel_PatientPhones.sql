drop procedure spDel_PatientPhones
go

create procedure spDel_PatientPhones
(
    @UserLogin varchar(40),
	@PatientPhoneId Integer
)
AS
BEGIN


Update PatientPhones set PatientPhonesEnabled = 0 where PatientPhoneId = @PatientPhoneId


-- Obrigatorio, incluir o log do processo.
Exec spIns_ProcessLog 
	@ProcessLogUser  = @UserLogin, 
	@ProcessLogName  = 'Exclus�o', 
	@ProcessLogTable = 'PatientPhones', 
	@ProcessLogField = 'PatientPhoneId', 
	@ProcessLogValue = @PatientPhoneId

END