create table MedicineInteractionPatientResults(MedicineInteractionPatientResultsID integer not null identity,
                                               MedicineInteractionPatientItemId integer,
											   DateInteraction datetime,
											   ActivePrinciple1 integer,
											   ActivePrinciple2 integer,
											   Result varchar(4000),
											   Severity integer)
alter table MedicineInteractionPatientResults add constraint pk_MedicineInteractionPatientResults primary key(MedicineInteractionPatientResultsID)
