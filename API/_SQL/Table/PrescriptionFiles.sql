create table PrescriptionFiles
(
   PrescriptionId int not null,
   FileContent varchar(max) not null,
   FileType varchar(10) not null
)