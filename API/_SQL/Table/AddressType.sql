create table AddressType
(
	AddressTypeId integer identity not null, 
	AddressTypeDescription varchar(40), 
	AddressTypeEnabled integer not null
)
go
ALTER TABLE AddressType ADD CONSTRAINT pk_AddressType PRIMARY KEY (AddressTypeId)