use youvita
go

create table PrescriptionsItems (
PrescriptionItemId integer identity not null,
PrescriptionId integer not null,
MedicineId integer not null,
DailyDose integer not null,
TermDays integer not null,
PrescriptionItemEnabled integer not null
)
go
ALTER TABLE PrescriptionsItems ADD CONSTRAINT pk_PrescriptionItemId PRIMARY KEY (PrescriptionItemId)




