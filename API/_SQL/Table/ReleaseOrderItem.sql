use YouVita

go
drop table ReleaseOrderItem
go
create table ReleaseOrderItem(
ReleaseOrderItemId integer identity not null,
ReleaseOrderId integer not null, 
MedicineId integer not null, 
Dosage integer not null, 
Quantity integer not null, 
ReleaseOrderItemEnabled integer not null
)
alter table ReleaseOrderItem add constraint pk_ReleaseOrderItem primary key(ReleaseOrderItemId)
