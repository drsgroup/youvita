CREATE TABLE Permissions(
	PermissionId integer IDENTITY(1,1) NOT NULL,
	PermissionName varchar(150) NOT NULL,
	PermissionEnabled integer NOT NULL
)
GO
ALTER TABLE Permissions ADD CONSTRAINT pk_Permissions PRIMARY KEY (PermissionId)