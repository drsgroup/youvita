create table States(StateId integer identity not null, StateName varchar(50), StateUF varchar(2), StateEnabled integer not null) 
go
ALTER TABLE States ADD CONSTRAINT pk_States PRIMARY KEY (StateId)