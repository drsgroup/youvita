create table DispensationPeriod(DispensationId integer identity not null, Description varchar(20) not null, QtyDays integer not null, DispensationPeriodEnabled integer not null) 
go
ALTER TABLE DispensationPeriod ADD CONSTRAINT pk_DispensationPeriod PRIMARY KEY (DispensationId)
