use youvita
go

create table MedicinesPMC (
   MedicienPMCId integer identity not null,
   MedicineId integer not null,
   Value numeric(10,2),
   Aliquot numeric(5,2),
   MedicinesPMCEnabled integer,
   StartDate datetime,
   EndDate datetime
)
go
ALTER TABLE MedicinesPMC ADD CONSTRAINT pk_MedicienPMCId PRIMARY KEY (MedicienPMCId)