use youvita
go

create table PatientAddresses(
	PatientAddressesId int identity not null,
	PatientId int not null,
	AddressTypeId int not null,
	Address varchar(60) not null,
	AddressNumber varchar(10) not null,
	City varchar(50) not null,
	Neighborhood varchar(50) not null,
	StateId integer not null, 
	ZipCode varchar(8) not null,
	Complement varchar(200) null,
	PatientAddressesEnabled int not null
)
go
ALTER TABLE PatientAddresses ADD CONSTRAINT pk_PatientAddresses PRIMARY KEY (PatientAddressesId)


