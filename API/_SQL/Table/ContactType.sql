create table ContactType(ContactTypeId integer identity not null, ContactDescription varchar(40) not null, ContactTypeEnabled integer not null)
go
ALTER TABLE ContactType ADD CONSTRAINT pk_ContactType PRIMARY KEY (ContactTypeId)
