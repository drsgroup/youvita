use YouVita

go
drop table ReleaseOrder
go
create table ReleaseOrder(
ReleaseOrderId integer identity not null,
ReleaseOrderDate datetime not null,
PatientId integer not null, 
PatientAuthorization varchar(20) not null,
DispensationId integer not null, 
ReleaseOrderStatus  varchar(1) Default 'P',
ReleaseOrderObservation varchar(200) null,
Crm integer not null,
StateId integer not null,
ReleaseOrderEnabled integer not null
)
alter table ReleaseOrder add constraint pk_ReleaseOrder primary key(ReleaseOrderId)