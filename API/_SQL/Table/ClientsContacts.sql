create table ClientsContacts(
ContactId integer identity not null,
ClientId integer not null,
ContactName varchar(40) not null,
Email varchar(100) not null,
Password varchar(20) null,
Occupation varchar(20) null,
Phone varchar(20) not null,
Ramal varchar(10) null,
CellPhone varchar(20) null,
ContactTypeId integer not null,
ClientsContactsEnabled integer not null)
go
ALTER TABLE ClientsContacts ADD CONSTRAINT pk_ClientsContacts PRIMARY KEY (ContactId)

