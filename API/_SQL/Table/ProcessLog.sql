create table ProcessLog
(
	ProcessLogDate datetime not null,
	ProcessLogUser varchar(40) not null,
	ProcessLogName varchar(100) not null,
	ProcessLogTable varchar(100) not null,
	ProcessLogField varchar(100) not null,
	ProcessLogValue varchar(100) not null
)

