use youvita
go

create table Prescriptions (
PrescriptionId integer identity not null,
PatientId integer not null,
PrescriptionDate datetime not null,
ExpirationDate datetime not null,
CRM varchar(10) not null,
UfCrm varchar(2) not null,
DoctorName varchar(60) not null,
PathImagePrescription varchar(260) not null,	
PrescriptionEnabled integer not null
)
go
ALTER TABLE Prescriptions ADD CONSTRAINT pk_PrescriptionId PRIMARY KEY (PrescriptionId)




