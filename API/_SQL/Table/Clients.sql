create table Clients(
ClientId integer identity not null,
ClientName varchar(60) not null,
ClientFantasyName varchar(60) not null,
CNPJ varchar(14) not null,
Address varchar(60) not null,
AddressNumber varchar(10) not null,
Neighborhood varchar(50) not null,
City varchar(50) not null,
ZipCode varchar(8) not null, --cep
StateId integer not null, --vem da tabela State
ResponsibleContact varchar(50) null,
Occupation varchar(50) null,
Email varchar(60) not null,
Phone varchar(20) null,
Ramal varchar(10) null,
CellPhone varchar(20) null,
ClientLogin varchar(60) not null,
ClientPassword varchar(20) not null,
ConfirmPassword varchar(20) not null,
ClientTypeId Integer not null,
ClientStatus varchar(1) not null,
Comments varchar(200) null,
ClientsEnabled integer not null
)
go
ALTER TABLE Clients ADD CONSTRAINT pk_Clients PRIMARY KEY (ClientId)