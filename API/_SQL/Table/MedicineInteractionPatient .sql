create table MedicineInteractionPatient ( MedicineInteractionPatientID int identity not null,
                                            PatientID integer,
                                            DateInteraction date,
                                            Severity integer,
                                            MedicineInteractionEnabled integer,
                                            Comments varchar(200))
alter table MedicineInteractionPatient add constraint pk_MedicineInteractionPatient primary key (MedicineInteractionPatientID)