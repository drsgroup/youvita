create table Users(
	UserId Integer IDENTITY Not Null,
	UserName varchar(40) Not Null,
	UserLoginName varchar(40) Not Null,
	UserPassword varchar(40) Not Null,
	UserEnabled Integer Not Null)
go
ALTER TABLE Users ADD CONSTRAINT pk_Users PRIMARY KEY (UserId)
ALTER TABLE Users ADD CONSTRAINT uq_UserLoginName UNIQUE (UserLoginName)


