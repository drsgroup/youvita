use YouVitaHML
go
drop table [dbo].[Patients]
go

--Patients
CREATE TABLE [dbo].[Patients](
	[PatientId] [int] IDENTITY(1,1) NOT NULL,
	[ClientId] [int] NOT NULL,
	[PatientName] [varchar](60) NOT NULL,
	[BirthDate] [datetime] NULL,
	[CPF] [varchar](11) NOT NULL,
	[RG] [varchar](15) NOT NULL,
	[IdentificationOnClient] [varchar](20) NOT NULL,
	[Responsible] [varchar](60) NULL,
	[BestContactPeriod] [varchar](20) NOT NULL,
	[Comments] [varchar](200) NULL,
	[PatientStatus] [varchar](1) NOT NULL,
	[CID1] [varchar](20) NULL,
	[CID2] [varchar](20) NULL,
	[CID3] [varchar](20) NULL,
	[PatientEnabled] [int] NOT NULL,
	[Filter] [varchar](200) NULL,
	[PatientType] [varchar](1) NOT NULL
)
go
ALTER TABLE dbo.Patients ADD CONSTRAINT pk_Patients PRIMARY KEY (PatientId)
