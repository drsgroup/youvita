create table PatientPhones(
PatientPhoneId integer identity not null,
PatientId integer not null,
PhoneNumber varchar(12) not null,
Ramal varchar(10) null,
PhoneTypeId integer,
PatientPhonesEnabled integer not null)
go
ALTER TABLE PatientPhones ADD CONSTRAINT pk_PatientPhones PRIMARY KEY (PatientPhoneId)


