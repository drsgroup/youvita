use youvita
go

create table Medicines (
    MedicineId integer identity not null,
    EAN varchar(13) not null,
    MedicineName varchar(100),
    LabId varchar(10),
    LabName varchar(100),
    ActivePrincipleId varchar(10),
    ActivePrincipleName varchar(100),
    Presentation varchar(100),
    UnitId varchar(10),
    UnitName varchar(100),
    Dosage integer,
    QtyTotal integer,
    MedicineType varchar(20),
    Comments varchar(200),
	MedicineEnabled integer not null
)
go
ALTER TABLE Medicines ADD CONSTRAINT pk_MedicineId PRIMARY KEY (MedicineId)




