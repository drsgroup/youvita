create table PatientPathologies(
PatientPathologiesId integer identity not null,
PatientId integer not null,
pathologyId integer,
PatientPathologiesEnabled integer not null)
go
ALTER TABLE PatientPathologies ADD CONSTRAINT pk_PatientPathologies PRIMARY KEY (PatientPathologiesId)

