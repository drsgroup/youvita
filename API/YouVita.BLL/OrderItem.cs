﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class OrderItem
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.OrderItem().List(filter);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.OrderItem OrderItem)
        {
            return new DAL.OrderItem().Insert(OrderItem);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.OrderItem OrderItem)
        {
            return new DAL.OrderItem().Delete(OrderItem);
        }
    }
}
