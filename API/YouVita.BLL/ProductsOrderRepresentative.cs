﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class ProductsOrderRepresentative
    {
        public Framework.DTO.DTOWebContract List(string OrdersDoctorID)
        {
            return new DAL.ProductsOrderRepresentative().ListByID(OrdersDoctorID);
        }

        public string GetProductCode(string productID)
        {
            return new DAL.ProductsOrderRepresentative().GetProductCode(productID);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.ProductsOrderRepresentative ProductsOrderDoctor)
        {
            return new DAL.ProductsOrderRepresentative().Insert(ProductsOrderDoctor);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.ProductsOrderRepresentative ProductsOrderDoctor)
        {
            return new DAL.ProductsOrderRepresentative().Delete(ProductsOrderDoctor);
        }
    }
}