﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class Orders
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            DTO.ListOrders listOrders = new DTO.ListOrders();

            var ret = new DAL.Orders().List(filter);

            // Subcadastro de Item do Pedido
            foreach (DTO.Orders orders in (DTO.ListOrders)ret.ObjectDTO)
            {
                orders.ListOrderItems = (DTO.ListOrderItem)new BLL.OrderItem().List(orders.orderId.ToString()).ObjectDTO;
                listOrders.Add(orders);
            }

            ret.ObjectDTO = listOrders;

            return ret;
        }

        public Framework.DTO.DTOWebContract ListByPatientId(string patientId)
        {
            DTO.ListOrders listOrders = new DTO.ListOrders();
            var ret = new DAL.Orders().ListByPatientId(patientId);         
            return ret;
        }
        

        public Framework.DTO.DTOWebContract Insert(DTO.Orders Orders)
        {
            // Insere os Dados
            var ret = new DAL.Orders().Insert(Orders);

            // Subcadastro de Item de Pedido
            foreach (var orderItem in Orders.ListOrderItems)
            {
                orderItem.orderId = ((DTO.Orders)ret.ObjectDTO).orderId;
                orderItem.userLogin = ((DTO.Orders)ret.ObjectDTO).userLogin;
                orderItem.orderItemEnabled = Framework.Application.DTODefaultDataEnabledValue;
                new BLL.OrderItem().Insert(orderItem);
            }
            return ret;
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Orders Orders)
        {
            return new DAL.Orders().Delete(Orders);
        }

    }
}
