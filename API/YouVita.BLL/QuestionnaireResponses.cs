﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class QuestionnaireResponses
    {
        public Framework.DTO.DTOWebContract GetByIdResearches(string filter)
        {
            return new DAL.QuestionnaireResponses().GetByIdResearches(filter);
        }


        public Framework.DTO.DTOWebContract Insert(DTO.QuestionnaireResponses questionnaireResponses)
        {
            //aqui verificar se possui mais de uma resposta
            if (questionnaireResponses.Respostas.Contains(',')){
                var resps = questionnaireResponses.Respostas.Split(',');
                var ret = new Framework.DTO.DTOWebContract();
                for (int i = 0; i < resps.Length; i++)
                {
                    var resp = questionnaireResponses;                    
                    resp.AnswersId = Convert.ToInt32(resps[i]);
                    ret = new DAL.QuestionnaireResponses().Insert(resp);
                }
                return ret;
            }
            else {
                return new DAL.QuestionnaireResponses().Insert(questionnaireResponses);
            }
        }
    }
}
