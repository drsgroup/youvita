﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class PharmaceuticalAttentionsAdverseEffects
    {

        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.PharmaceuticalAttentionsAdverseEffects().List(filter);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.PharmaceuticalAttentionsAdverseEffects entity)
        {
            return new DAL.PharmaceuticalAttentionsAdverseEffects().Insert(entity);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.PharmaceuticalAttentionsAdverseEffects entity)
        {
            return new DAL.PharmaceuticalAttentionsAdverseEffects().Delete(entity);
        }

    }
}
