﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class PatientAddresses
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.PatientAddresses().List(filter);
        }

        public Framework.DTO.DTOWebContract GetByIdPatient(string filter)
        {
            return new DAL.PatientAddresses().GetByIdPatient(filter);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.PatientAddresses PatientAddresses)
        {
            return new DAL.PatientAddresses().Insert(PatientAddresses);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.PatientAddresses PatientAddresses)
        {
            return new DAL.PatientAddresses().Delete(PatientAddresses);
        }

        public Framework.DTO.DTOWebContract Update(DTO.PatientAddresses PatientAddresses)
        {
            return new DAL.PatientAddresses().Update(PatientAddresses);
        }
    }
}
