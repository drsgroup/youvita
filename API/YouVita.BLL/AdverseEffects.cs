﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class AdverseEffects
    {
        public Framework.DTO.DTOWebContract List(string filter)
        { 
            return new DAL.AdverseEffects().List(filter);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.AdverseEffects AdverseEffects)
        {
            return new DAL.AdverseEffects().Insert(AdverseEffects);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.AdverseEffects AdverseEffects)
        {
            return new DAL.AdverseEffects().Delete(AdverseEffects);
        }

        public Framework.DTO.DTOWebContract Update(DTO.AdverseEffects AdverseEffects)
        {
            return new DAL.AdverseEffects().Update(AdverseEffects);
        }
    }
}
