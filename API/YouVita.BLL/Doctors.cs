﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class Doctors
    {
        public Framework.DTO.DTOWebContract List(string doctorName, string CPF, string CRM, string UF_CRM, string representativeID, string laboratoryID)
        {
            DTO.ListDoctors listDoctors = new DTO.ListDoctors();

            var ret = new DAL.Doctors().List(doctorName, CPF, CRM, UF_CRM, representativeID, laboratoryID);

            // Subcadastro de Endereco
            //foreach (DTO.Doctors Doctor in (DTO.ListDoctors)ret.ObjectDTO)
            //{
            //    Doctor.ListDoctorAddresses = (DTO.ListDoctorAddresses)new BLL.DoctorAddresses().GetByDoctorID(Doctor.doctorID.ToString()).ObjectDTO; 
            //    listDoctors.Add(Doctor);
            //}
            //ret.ObjectDTO = ret;
            return ret;

        }

        public Framework.DTO.DTOWebContract ListLabID(string laboratoryID, string doctor )
        {
            DTO.ListDoctors listDoctors = new DTO.ListDoctors();
            return new DAL.Doctors().ListLabID(laboratoryID, doctor);
        }
        

        public Framework.DTO.DTOWebContract Insert(DTO.Doctors Doctor)
        {
            DTO.Users user = new DTO.Users();            
            user.userName = Doctor.doctorName;
            user.userPassword = Doctor.password;
            user.userLoginName = Doctor.login;
            user.ClientId = 0;
            user.userLogin = Doctor.login;            
            user.userEnabled = Framework.Application.DTODefaultDataEnabledValue;
            user.userType = 4; //Médico
            user.laboratoryID = Doctor.laboratoryID;
            BLL.Users bll = new Users();
            bll.Insert(user);
            
            var ret = new DAL.Doctors().Insert(Doctor); 
            // Subcadastro de Endereco
            if (Doctor.ListDoctorAddresses != null)
            {
                foreach (var DoctorAddress in Doctor.ListDoctorAddresses)
                {
                    DoctorAddress.doctorId = ((DTO.Doctors)ret.ObjectDTO).doctorID;
                    DoctorAddress.userLogin = ((DTO.Doctors)ret.ObjectDTO).userLogin;
                    DoctorAddress.doctorAddressesEnabled = Framework.Application.DTODefaultDataEnabledValue;
                    new BLL.DoctorAddresses().Insert(DoctorAddress);
                }
            }
            return ret;
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Doctors Doctor)
        {
            return new DAL.Doctors().Delete(Doctor);
        }

        public Framework.DTO.DTOWebContract GetDoctorByID(string doctorID)
        {
            return new DAL.Doctors().GetDoctorByID(doctorID);
        }
        

        public Framework.DTO.DTOWebContract GetDoctorByLogin(string login)
        {
            return new DAL.Doctors().GetDoctorByLogin(login);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Doctors Doctor)
        {
            return new DAL.Doctors().Update(Doctor);
        }
    }
}
