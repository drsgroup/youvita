﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class ProjectProduct
    {
        public Framework.DTO.DTOWebContract Insert(DTO.ProjectProduct projectProduct)
        {
            // Insere os Dados
            return new DAL.ProjectProduct().Insert(projectProduct);

        }

        public Framework.DTO.DTOWebContract ListAll(int projectId, int laboratoryId)
        {
            // Insere os Dados
            return new DAL.ProjectProduct().ListAll(projectId, laboratoryId);

        }

        public Framework.DTO.DTOWebContract ListLabId(string projectId)
        {
            // Insere os Dados
            return new DAL.ProjectProduct().ListLabId(projectId);

        }

        

        public void Delete(int projectId)
        {
            // Insere os Dados
            new DAL.ProjectProduct().Delete(projectId);

        }
    }
}
