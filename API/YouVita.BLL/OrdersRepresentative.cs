﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.BLL
{
    public class OrdersRepresentative
    {
        public Framework.DTO.DTOWebContract List(string representativeName, string cpf, string email, string laboratoryID)
        {
            return new DAL.OrdersRepresentative().List(representativeName, cpf, email, laboratoryID);
        }

        public Framework.DTO.DTOWebContract Resend(Int32 ordersRepresentativeID)
        {
            new DAL.ProductsOrderRepresentative().Resend(ordersRepresentativeID);
            return new DTOWebContract() { Status = "OK", Message = "", ObjectDTO = null };
        }

        public Framework.DTO.DTOWebContract ListByParams(string userLogin, string laboratoryID, string representativeLogin, string representativeName)
        {
            DTO.ListOrdersRepresentative listOrders = new DTO.ListOrdersRepresentative();
            var ret = new DAL.OrdersRepresentative().ListByParams(userLogin, laboratoryID, representativeLogin, representativeName);

            // Subcadastro de Endereco

            if (ret.ObjectDTO != null)
            {
                foreach (DTO.OrdersRepresentative order in (DTO.ListOrdersRepresentative)ret.ObjectDTO)
                {
                    order.Products = (DTO.ListProductsOrderRepresentative)new BLL.ProductsOrderRepresentative().List(order.ordersRepresentativeID.ToString()).ObjectDTO;
                    listOrders.Add(order);
                }
            }

            ret.ObjectDTO = listOrders;
            return ret;
        }

        public Framework.DTO.DTOWebContract Insert(DTO.OrdersRepresentative order)
        {
            var ret = new DAL.OrdersRepresentative().Insert(order);
            if (order.Products != null)
            {
                foreach (var produto in order.Products)
                {
                    produto.OrdersRepresentativeID = ((DTO.OrdersRepresentative)ret.ObjectDTO).ordersRepresentativeID;
                    produto.userLogin = ((DTO.OrdersRepresentative)ret.ObjectDTO).userLogin;
                    new BLL.ProductsOrderRepresentative().Insert(produto);
                }
            }
            return ret;
        }

        public Framework.DTO.DTOWebContract Delete(DTO.OrdersRepresentative OrdersRepresentative)
        {
            return new DAL.OrdersRepresentative().Delete(OrdersRepresentative);
        }

        public Framework.DTO.DTOWebContract Update(DTO.OrdersRepresentative OrdersRepresentative)
        {
            return new DAL.OrdersRepresentative().Update(OrdersRepresentative);
        }
    }
}
