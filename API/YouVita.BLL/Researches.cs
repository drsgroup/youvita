﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class Researches
    {
        public Framework.DTO.DTOWebContract List(string filter, Int16 ClientId)
        {
            DTO.ListResearches listResearches = new DTO.ListResearches();

            var ret = new DAL.Researches().List(filter, ClientId);

            // Subcadastro de questões
            if (ret.ObjectDTO != null)
            {
                foreach (DTO.Researches Researches in (DTO.ListResearches)ret.ObjectDTO)
                {
                    Researches.ListQuestionnaireResponses = (DTO.ListQuestionnaireResponses)new BLL.QuestionnaireResponses().GetByIdResearches(Researches.ResearchesId.ToString()).ObjectDTO;
                    listResearches.Add(Researches);
                }
            }

            ret.ObjectDTO = listResearches;
            return ret;

        }

        public Framework.DTO.DTOWebContract List(string ResearchId)
        {
            DTO.ListResearches listResearches = new DTO.ListResearches();

            var ret = new DAL.Researches().List(ResearchId);
            return ret;
        }

        public Framework.DTO.DTOWebContract GetByPatient(string patientId)
        {
            DTO.ListResearches listResearches = new DTO.ListResearches();

            var ret = new DAL.Researches().GetByPatient(patientId);
            return ret;
        }

        public Framework.DTO.DTOWebContract List(string patientName, string CPF, string identificationOnClient)
        {
            DTO.ListResearches listResearches = new DTO.ListResearches();

            var ret = new DAL.Researches().List(patientName, CPF, identificationOnClient);

            // Subcadastro de questões
            //if (ret.ObjectDTO != null)
            //{
            //    foreach (DTO.Researches Researches in (DTO.ListResearches)ret.ObjectDTO)
            //    {
            //        Researches.ListQuestionnaireResponses = (DTO.ListQuestionnaireResponses)new BLL.QuestionnaireResponses().GetByIdResearches(Researches.ResearchesId.ToString()).ObjectDTO;
            //        listResearches.Add(Researches);
            //    }
            //}

            //ret.ObjectDTO = listResearches;
            return ret;

        }

        public Framework.DTO.DTOWebContract Insert(DTO.Researches Researches)
        {
            var ret = new DAL.Researches().Insert(Researches);

            // Subcadastro de questões
            if (Researches.ListQuestionnaireResponses != null)
            {
                foreach (var questionnaireResponses in Researches.ListQuestionnaireResponses)
                {
                    questionnaireResponses.ResearchesId = ((DTO.Researches)ret.ObjectDTO).ResearchesId;
                    questionnaireResponses.userLogin = ((DTO.Researches)ret.ObjectDTO).userLogin;
                    questionnaireResponses.QuestionnaireResponsesEnabled = Framework.Application.DTODefaultDataEnabledValue;
                    new BLL.QuestionnaireResponses().Insert(questionnaireResponses);
                }
            }

            return ret;
        }
    }
}
