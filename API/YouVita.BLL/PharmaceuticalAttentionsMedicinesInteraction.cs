﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class PharmaceuticalAttentionsMedicinesInteraction
    {

        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.PharmaceuticalAttentionsMedicinesInteraction().List(filter);
        }

        public Framework.DTO.DTOWebContract ListMedicinePatient(string patientId)
        {
            return new DAL.PharmaceuticalAttentionsMedicinesInteraction().ListMedicinePatient(patientId);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.PharmaceuticalAttentionsMedicinesInteraction entity)
        {
            return new DAL.PharmaceuticalAttentionsMedicinesInteraction().Insert(entity);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.PharmaceuticalAttentionsMedicinesInteraction entity)
        {
            return new DAL.PharmaceuticalAttentionsMedicinesInteraction().Delete(entity);
        }

    }
}
