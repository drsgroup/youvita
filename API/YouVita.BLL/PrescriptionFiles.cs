﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class PrescriptionFiles
    {

        public Framework.DTO.DTOWebContract List(string PrescriptionId)
        {
            return new DAL.PrescriptionFiles().List(PrescriptionId);
        }

    }
}
