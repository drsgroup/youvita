﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class InteractionMedicineBLL
    {
        public Framework.DTO.DTOWebContract Insert(DTO.MedicineInteractionPatient interaction)
        {
            // Insere os Dados
            var ret = new DAL.InteractionMedicineDAL().Insert(interaction);
            var id = ((DTO.MedicineInteractionPatient)ret.ObjectDTO).MedicineInteractionPatientID;
            // Subcadastro de Item de Pedido
            foreach (var item in interaction.Items)
            {
                item.InteractionMedicineID = ((DTO.MedicineInteractionPatient)ret.ObjectDTO).MedicineInteractionPatientID;
                new DAL.InteractionMedicineDAL().InsertItem(item); 
            }
            //fazer a interação medicamento
            new DAL.InteractionMedicineDAL().ExecInteractionMedicines(id);
            return ret;
        }

        public Framework.DTO.DTOWebContract Update(DTO.MedicineInteractionPatient interaction)
        {
            // Insere os Dados
            var ret = new DAL.InteractionMedicineDAL().Update(interaction);            
            return ret;
        }


        public Framework.DTO.DTOWebContract ListByPatientId(Int32 patientId)
        {
            // Insere os Dados
            var ret = new DAL.InteractionMedicineDAL().ListByPatientId(patientId);            
            return ret;
        }

        public Framework.DTO.DTOWebContract List(string clientName, string patientName, DateTime? dtIni, DateTime? dtFim)
        {
            // Insere os Dados
            var ret = new DAL.InteractionMedicineDAL().List(clientName, patientName, dtIni, dtFim);
            return ret;
        }

        

        public Framework.DTO.DTOWebContract ListById(Int32 MedicineInteractionPatientItemId)
        {
            // Insere os Dados
            var ret = new DAL.InteractionMedicineDAL().ListByPatientId(MedicineInteractionPatientItemId);
            return ret;
        }

        public Framework.DTO.DTOWebContract ResultById(Int32 MedicineInteractionPatientItemId)
        {
            // Insere os Dados
            var ret = new DAL.InteractionMedicineDAL().ListResultById(MedicineInteractionPatientItemId);
            return ret;
        }


        public Framework.DTO.DTOWebContract GetMedicinesById(Int32 InteractionMedicineID)
        {
            // Insere os Dados
            var ret = new DAL.InteractionMedicineDAL().GetMedicinesById(InteractionMedicineID);
            return ret;
        }



    }
}
