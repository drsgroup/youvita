﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class ReportDetailReleaseOrderByClient
    {
        public Framework.DTO.DTOWebContract List(string clientId, DateTime startDate, DateTime endDate)
        {
            return new DAL.ReportDetailReleaseOrderByClient().List(clientId,startDate,endDate);
        }

    }
}
