﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.DAL;
using YouVita.DTO;
using YouVita.Framework.DTO;

namespace YouVita.BLL
{
    public class Prescriptions
    {
        public Framework.DTO.DTOWebContract ListByFilter(DateTime? prescriptionDate, DateTime? expirationDate, string patientName, string CRM, string UfCRM, string DoctorName)
        {
            DTO.ListPrescriptions listPrescriptions = new DTO.ListPrescriptions();

            var ret = new DAL.Prescriptions().ListByFilter( prescriptionDate,  expirationDate,  patientName,  CRM,  UfCRM,  DoctorName);

            // Subcadastro de Item do Pedido da Receita
            foreach (DTO.Prescriptions Prescriptions in (DTO.ListPrescriptions)ret.ObjectDTO)
            {
                Prescriptions.ListPrescriptionItem = (DTO.ListPrescriptionItems)new BLL.PrescriptionsItems().ListByPrescription(Prescriptions.prescriptionId.ToString()).ObjectDTO;
                listPrescriptions.Add(Prescriptions);
            }
            ret.ObjectDTO = listPrescriptions;
            return ret;
        }

        public Framework.DTO.DTOWebContract ListByPrescription(string prescriptionId)
        {
            DTO.ListPrescriptions listPrescriptions = new DTO.ListPrescriptions();

            var ret = new DAL.Prescriptions().ListByPrescription(prescriptionId);

            // Subcadastro de Item do Pedido da Receita
            foreach (DTO.Prescriptions Prescriptions in (DTO.ListPrescriptions)ret.ObjectDTO)
            {
                Prescriptions.ListPrescriptionItem = (DTO.ListPrescriptionItems)new BLL.PrescriptionsItems().ListByPrescription(Prescriptions.prescriptionId.ToString()).ObjectDTO;
                listPrescriptions.Add(Prescriptions);
            }
            ret.ObjectDTO = listPrescriptions;
            return ret;
        }

        public Framework.DTO.DTOWebContract ListByPrescriptionPatientId(string patientId)
        {
            DTO.ListPrescriptions listPrescriptions = new DTO.ListPrescriptions();

            var ret = new DAL.Prescriptions().ListByPrescriptionPatientId(patientId);

            // Subcadastro de Item do Pedido da Receita
            foreach (DTO.Prescriptions Prescriptions in (DTO.ListPrescriptions)ret.ObjectDTO)
            {
                Prescriptions.ListPrescriptionItem = (DTO.ListPrescriptionItems)new BLL.PrescriptionsItems().ListByPrescription(Prescriptions.prescriptionId.ToString()).ObjectDTO;
                listPrescriptions.Add(Prescriptions);
            }
            ret.ObjectDTO = listPrescriptions;
            return ret;
        }
        

        public Framework.DTO.DTOWebContract Insert(DTO.Prescriptions Prescriptions)
        {

            // Insere os Dados
            var ret = new DAL.Prescriptions().Insert(Prescriptions);

            // Subcadastro de Item de Pedido da Receita
            foreach (var PrescriptionItem in Prescriptions.ListPrescriptionItem)
            {
                PrescriptionItem.prescriptionId = ((DTO.Prescriptions)ret.ObjectDTO).prescriptionId;
                PrescriptionItem.userLogin = ((DTO.Prescriptions)ret.ObjectDTO).userLogin;
                PrescriptionItem.prescriptionItemEnabled = Framework.Application.DTODefaultDataEnabledValue;
                new BLL.PrescriptionsItems().Insert(PrescriptionItem);
            }
            return ret;
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Prescriptions Prescription)
        {
            return new DAL.Prescriptions().Delete(Prescription);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Prescriptions Prescription)
        {
            return new DAL.Prescriptions().Update(Prescription);
        }
    }
}
