﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.DAL;
using YouVita.DTO;
using YouVita.Framework.DTO;

namespace YouVita.BLL
{
    public class PrescriptionsItems
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.PrescriptionsItems().List(filter);
        }

        public Framework.DTO.DTOWebContract ListByPrescription(string prescriptionId)
        {
            return new DAL.PrescriptionsItems().ListByPrescription(prescriptionId);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.PrescriptionsItems prescriptionItem)
        {
            return new DAL.PrescriptionsItems().Insert(prescriptionItem);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.PrescriptionsItems prescriptionItem)
        {
            return new DAL.PrescriptionsItems().Delete(prescriptionItem);
        }

        public Framework.DTO.DTOWebContract Update(DTO.PrescriptionsItems prescriptionItem)
        {
            return new DAL.PrescriptionsItems().Update(prescriptionItem);
        }
    }
}
