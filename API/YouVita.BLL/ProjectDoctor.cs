﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class ProjectDoctor
    {

        public Framework.DTO.DTOWebContract Insert(DTO.ProjectDoctor projectDoctor)
        {
            // Insere os Dados
            return new DAL.ProjectDoctor().Insert(projectDoctor);

        }

        public Framework.DTO.DTOWebContract ListAll(int projectId, int laboratoryId)
        {
            // Insere os Dados
            return new DAL.ProjectDoctor().ListAll(projectId, laboratoryId);

        }

        public Framework.DTO.DTOWebContract ListByProjectId(int projectId)
        {
            // Insere os Dados
            return new DAL.ProjectDoctor().ListByProjectId(projectId);

        }

        public Framework.DTO.DTOWebContract Update(DTO.ProjectDoctor projectDoctor)
        {
            // Insere os Dados
            return new DAL.ProjectDoctor().Update(projectDoctor);
        }

        public void Delete(int projectId)
        {
            // Insere os Dados
            new DAL.ProjectDoctor().Delete(projectId);           
        }
    }
}
