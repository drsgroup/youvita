﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class ProductsAvaliableRepresentative
    {
        public Framework.DTO.DTOWebContract List(string login)
        {
            return new DAL.ProductsAvaliableRepresentative().GetProductsAvaliable(login);
        }

        public Framework.DTO.DTOWebContract GetListById(string OrderAvaliableID)
        {
            return new DAL.ProductsAvaliableRepresentative().GetProductsAvaliableByID(OrderAvaliableID);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.ProductsAvaliableRepresentative ProductsAvaliableRepresentative)
        {
            return new DAL.ProductsAvaliableRepresentative().Insert(ProductsAvaliableRepresentative);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.ProductsAvaliableRepresentative ProductsAvaliableRepresentative)
        {
            return new DAL.ProductsAvaliableRepresentative().Delete(ProductsAvaliableRepresentative);
        }

        public void DeleteByOrderAvaliableRepresentativeID(int OrderAvaliableId)
        {
            new DAL.ProductsAvaliableRepresentative().DeleteByOrderAvaliableRepresentativeID(OrderAvaliableId);
        }

        public Framework.DTO.DTOWebContract Update(DTO.ProductsAvaliableRepresentative ProductsAvaliableRepresentative)
        {
            return new DAL.ProductsAvaliableRepresentative().Update(ProductsAvaliableRepresentative);
        }
    }
}
