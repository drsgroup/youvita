﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class UserConfigurations
    {

        public Framework.DTO.DTOWebContract List(string userId)
        {
            return new DAL.UserConfigurations().List(userId);
        }

    }
}
