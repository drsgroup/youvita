﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace YouVita.BLL
{
    public class Patients
    {
        public Framework.DTO.DTOWebContract List(string patientName, string CPF, string identificationOnClient, int clientId)
        {
            DTO.ListPatients listPatients = new DTO.ListPatients();

            var ret = new DAL.Patients().List(patientName, CPF, identificationOnClient, clientId);

            if (ret.ObjectDTO != null)
            {
                // Subcadastro de Endereco
                foreach (DTO.Patients Patient in (DTO.ListPatients)ret.ObjectDTO)
                {
                    Patient.ListPatientAddresses = (DTO.ListPatientAddresses)new BLL.PatientAddresses().GetByIdPatient(Patient.PatientId.ToString()).ObjectDTO;
                    Patient.ListPatientPhones = (DTO.ListPatientPhones)new BLL.PatientPhones().GetByIdPatient(Patient.PatientId.ToString()).ObjectDTO;
                    //Patient.ListReleaseOrder = (DTO.ListReleaseOrder)new BLL.ReleaseOrder().ListByPatient(Patient.PatientId.ToString()).ObjectDTO;
                    listPatients.Add(Patient);
                }
            }

            ret.ObjectDTO = listPatients;
            return ret;

        }

        public Framework.DTO.DTOWebContract Insert(DTO.Patients Patient)
        {
            var ret = new DAL.Patients().Insert(Patient);

            // Subcadastro de telefone
            if (Patient.ListPatientPhones != null)
            {
                foreach (var PatientPhone in Patient.ListPatientPhones)
                {
                    PatientPhone.patientId = ((DTO.Patients)ret.ObjectDTO).PatientId;
                    PatientPhone.userLogin = ((DTO.Patients)ret.ObjectDTO).userLogin;
                    PatientPhone.patientPhonesEnabled = Framework.Application.DTODefaultDataEnabledValue;
                    new BLL.PatientPhones().Insert(PatientPhone);
                }
            }

            // Subcadastro de Endereco
            if (Patient.ListPatientAddresses != null)
            {
                foreach (var PatientAddress in Patient.ListPatientAddresses)
                {
                    PatientAddress.PatientId = ((DTO.Patients)ret.ObjectDTO).PatientId;
                    PatientAddress.userLogin = ((DTO.Patients)ret.ObjectDTO).userLogin;
                    PatientAddress.PatientAddressesEnabled = Framework.Application.DTODefaultDataEnabledValue;
                    new BLL.PatientAddresses().Insert(PatientAddress);
                }
            }

            if (Patient.ReleaseOrder != null)
            {
                Patient.ReleaseOrder.patientId = ((DTO.Patients)ret.ObjectDTO).PatientId;
                Patient.ReleaseOrder.userLogin = ((DTO.Patients)ret.ObjectDTO).userLogin;
                Patient.ReleaseOrder.releaseOrderEnabled = Framework.Application.DTODefaultDataEnabledValue;
                new BLL.ReleaseOrder().Insert(Patient.ReleaseOrder);
            }         
            return ret;
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Patients Patient)
        {
            return new DAL.Patients().Delete(Patient);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Patients Patient)
        {
            if (Patient.ReleaseOrder != null)
            {
                Patient.ReleaseOrder.userLogin = "xxx";
                new BLL.ReleaseOrder().Insert(Patient.ReleaseOrder);
            }

            return new DAL.Patients().Update(Patient);
        }
    }
}
