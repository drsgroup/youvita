﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.DAL;
using System.Configuration;
using System.IO;

namespace YouVita.BLL
{
    public class GenerateOrderDoctorFile
    {

        public string removerAcentos(string texto)
        {
            string comAcentos = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç";
            string semAcentos = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc";

            for (int i = 0; i < comAcentos.Length; i++)
            {
                texto = texto.Replace(comAcentos[i].ToString(), semAcentos[i].ToString());
            }
            return texto;
        }

        public void GenerateFile()
        {
            try
            {
                DAL.OrdersDoctor dal = new DAL.OrdersDoctor();
                var dados = dal.GetListOrderDoctor();

                string separator = ";";

                StringBuilder sb = new StringBuilder();
                string idPedido = string.Empty;
                string caminho = ConfigurationManager.AppSettings["OrderFolder"];
                string tipo = String.Empty;

                foreach (DataRow row in dados.Rows)
                {
                    string linha = String.Empty;
                    if (idPedido != row["OrderDoctorId"].ToString())
                    {
                        if (!String.IsNullOrEmpty(idPedido))
                        {
                            //salvar o arquivo e limpar o stringbuilder
                            using (System.IO.StreamWriter file = new System.IO.StreamWriter(caminho + @"\WEB" + row["tipo"].ToString()  + idPedido + ".txt"))
                            {
                                file.WriteLine(sb.ToString()); // "sb" is the StringBuilder
                            }
                            sb.Clear();

                            this.SaveLogDataBase("Arquivo de pedido gerado", caminho + @"\WEB" + row["tipo"].ToString() + idPedido + ".txt");

                            //fazer update do pedido
                            dal.UpdateStatus(Convert.ToInt32(idPedido), "E", row["tipo"].ToString());

                            this.SaveLogDataBase("Pedido marcado como enviado", idPedido);
                        }

                        linha = "1"; //tipo do registro
                        linha += separator + row["CNPJ"].ToString().PadLeft(15, '0');
                        linha += separator + row["CPF"].ToString();
                        linha += separator + row["DoctorName"].ToString();
                        linha += separator + row["ZipCode"].ToString();
                        linha += separator + row["Address"].ToString();
                        linha += separator + row["Number"].ToString();
                        linha += separator + row["Neighborhood"].ToString();
                        linha += separator + row["City"].ToString();
                        linha += separator + row["tipo"].ToString() + row["OrderAvaliableID"].ToString() + "-" + row["tipo"].ToString() + row["OrderDoctorID"].ToString();
                        linha += separator + ((DateTime)row["OrderDoctorDateSend"]).ToString("dd/MM/yyyy");
                        linha += separator + ((DateTime)row["OrderDoctorDateSend"]).ToString("dd/MM/yyyy");
                        sb.AppendLine(linha);
                    }

                    linha = "2";
                    linha += separator + row["productCode"].ToString();
                    linha += separator + row["Qty"].ToString();
                    linha += separator + row["ProjectStockType"].ToString(); //tipo de estoque
                    linha += separator + new BLL.Configurations().GetValue("RestricaoProdutoDefault");  //restricao ao produto
                    sb.AppendLine(linha);
                    idPedido = row["OrderDoctorID"].ToString();
                    tipo = row["tipo"].ToString();
                }

                if (sb.Length > 0)
                {
                    //salvar o arquivo e limpar o stringbuilder
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(caminho + @"\WEB" + tipo+ idPedido + ".txt"))
                    {
                        file.WriteLine(removerAcentos(sb.ToString())); // "sb" is the StringBuilder
                    }
                    this.SaveLogDataBase("Arquivo de pedido gerado", caminho + @"\WEB" + tipo + idPedido + ".txt");

                    //fazer update do pedido
                    dal.UpdateStatus(Convert.ToInt32(idPedido), "E", tipo);

                    this.SaveLogDataBase("Pedido marcado como enviado", idPedido);
                }
            }catch(Exception ex)
            {
                this.SaveLogDataBase("Erro na geração do arquivos de Pedidos", ex.Message);
            }
        }

        public void checkReturn()
        {
            this.SaveLogDataBase("teste:" + "iniciando", "");
            var path = ConfigurationManager.AppSettings["OrderFolder"];
            var pathstart = path;
            path = path + @"ERRO\\";
            this.SaveLogDataBase("path:"+path, "");
            var files = Directory.GetFiles(path);
            this.SaveLogDataBase("arquivos obtidos:" + path, "");

            foreach (var f in files.Where(x => x.Contains("WEBPD")))
            {
                this.SaveLogDataBase("abrir arquivo:" + Path.GetFileName(f), "");
                var nomearquivo = Path.GetFileName(f);
                var partsNomeArquivo = nomearquivo.Split('_');
                var nmarq = nomearquivo;
                if (partsNomeArquivo.Length > 0)
                {
                    nomearquivo = partsNomeArquivo[0];
                }
                try
                {
                    var idPedido = nomearquivo.Replace("WEBPD", "").Replace(".txt", ""); //obtendo id do pedido
                                                                                         //incluir status no pedido
                    DAL.OrdersDoctor dal = new DAL.OrdersDoctor();
                    dal.UpdateStatus(Convert.ToInt32(idPedido), "X", "PD"); // x é erro retornado no TOTVS

                    //mover arquivo para backup
                    this.SaveLogDataBase("movendo:"+ path + nmarq +" para "+ pathstart + @"bkp\\" + nmarq, "");
                    File.Move(path + nmarq, pathstart + @"bkp\\" + nmarq);
                    
                }
                catch (Exception ex){
                    this.SaveLogDataBase("Erro: " + ex.StackTrace, "");

                }

            }

            foreach (var f in files.Where(x => x.Contains("WEBPR")))
            {
                var nomearquivo = Path.GetFileName(f);
                var partsNomeArquivo = nomearquivo.Split('_');
                var nmarq = nomearquivo;
                if (partsNomeArquivo.Length > 0)
                {
                    nomearquivo = partsNomeArquivo[0];
                }
                var idPedido = nomearquivo.Replace("WEBPR", "").Replace(".txt", ""); //obtendo id do pedido
                //incluir status no pedido
                DAL.OrdersDoctor dal = new DAL.OrdersDoctor();
                dal.UpdateStatus(Convert.ToInt32(idPedido), "X", "PR"); // x é erro retornado no TOTVS

                //mover arquivo para backup
                this.SaveLogDataBase("movendo:" + path + nmarq + " para " + pathstart + @"bkp\\" + nmarq, "");
                File.Move(path + nmarq, pathstart + @"bkp\\" + nmarq);
                
            }
        }

        private void SaveLogDataBase(string LogServiceDescription, string LogServiceComplement)
        {
            new YouVita.BLL.LogServices().Insert(new DTO.LogServices() { LogServiceId = 0, LogServiceDate = DateTime.Now, LogServiceName = "YouVitaService", LogServiceDescription = LogServiceDescription, LogServiceComplement = LogServiceComplement, userLogin = "YouVitaService" });
        }
    }
}
