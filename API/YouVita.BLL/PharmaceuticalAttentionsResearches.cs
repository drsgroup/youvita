﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class PharmaceuticalAttentionsResearches
    {

        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.PharmaceuticalAttentionsResearches().List(filter);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.PharmaceuticalAttentionsResearches entity)
        {
            return new DAL.PharmaceuticalAttentionsResearches().Insert(entity);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.PharmaceuticalAttentionsResearches entity)
        {
            return new DAL.PharmaceuticalAttentionsResearches().Delete(entity);
        }

    }
}
