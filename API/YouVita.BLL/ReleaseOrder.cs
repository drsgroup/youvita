﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.DAL;
using YouVita.DTO;
using YouVita.Framework.DTO;

namespace YouVita.BLL
{
    public class ReleaseOrder
    {
        public Framework.DTO.DTOWebContract ListByFilter(DateTime? ReleaseOrderDate, String PatientName)
        {
            DTO.ListReleaseOrder listReleaseOrder = new DTO.ListReleaseOrder();

            var ret = new DAL.ReleaseOrder().ListByFilter(ReleaseOrderDate, PatientName);

            if(ret != null)
            {
                // Subcadastro de Item do Pedido
                foreach (DTO.ReleaseOrder ReleaseOrder in (DTO.ListReleaseOrder)ret.ObjectDTO)
                {
                    ReleaseOrder.ListReleaseOrderItem = (DTO.ListReleaseOrderItem)new BLL.ReleaseOrderItem().List(ReleaseOrder.releaseOrderId.ToString()).ObjectDTO;
                    listReleaseOrder.Add(ReleaseOrder);

                }
            }

            ret.ObjectDTO = listReleaseOrder;
            return ret;
        }

        public Framework.DTO.DTOWebContract ListByOrder(string releaseOrderId)
        {
            DTO.ListReleaseOrder listReleaseOrder = new DTO.ListReleaseOrder();

            var ret = new DAL.ReleaseOrder().ListByOrder(releaseOrderId);

            // Subcadastro de Item do Pedido
            foreach (DTO.ReleaseOrder ReleaseOrder in (DTO.ListReleaseOrder)ret.ObjectDTO)
            {
                ReleaseOrder.ListReleaseOrderItem = (DTO.ListReleaseOrderItem)new BLL.ReleaseOrderItem().List(ReleaseOrder.releaseOrderId.ToString()).ObjectDTO;
                listReleaseOrder.Add(ReleaseOrder);

            }
            ret.ObjectDTO = listReleaseOrder;
            return ret;
        }


        public Framework.DTO.DTOWebContract ListByOrderPatient(string patientId)
        {
            DTO.ListReleaseOrder listReleaseOrder = new DTO.ListReleaseOrder();

            var ret = new DAL.ReleaseOrder().ListByOrderPatient(patientId);

            // Subcadastro de Item do Pedido
            foreach (DTO.ReleaseOrder ReleaseOrder in (DTO.ListReleaseOrder)ret.ObjectDTO)
            {
                ReleaseOrder.ListReleaseOrderItem = (DTO.ListReleaseOrderItem)new BLL.ReleaseOrderItem().List(ReleaseOrder.releaseOrderId.ToString()).ObjectDTO;
                listReleaseOrder.Add(ReleaseOrder);

            }
            ret.ObjectDTO = listReleaseOrder;
            return ret;
        }

        public Framework.DTO.DTOWebContract Insert(DTO.ReleaseOrder ReleaseOrder)
        {
            Int32 qtdEnvios = 0;
            if (ReleaseOrder.QtdEnvios == 0)
            {
                qtdEnvios = 1;
            }
            else
            {
                qtdEnvios = ReleaseOrder.QtdEnvios+1;
            }
            DTOWebContract ret = new DTOWebContract();
            ReleaseOrder.releaseOrderEnabled = Framework.Application.DTODefaultDataEnabledValue;
            if (qtdEnvios > 1) {
                var dtInicial = ReleaseOrder.releaseOrderDate;
                for (var i = 1; i <= qtdEnvios; i++)
                {
                    // Insere os Dados                    
                    ReleaseOrder.userLogin = "xxx";
                    ret = new DAL.ReleaseOrder().Insert(ReleaseOrder);
                    // Subcadastro de Item de Pedido
                    foreach (var ReleaseOrderItem in ReleaseOrder.ListReleaseOrderItem)
                    {
                        ReleaseOrderItem.releaseOrderId = ((DTO.ReleaseOrder)ret.ObjectDTO).releaseOrderId;
                        ReleaseOrderItem.userLogin = ((DTO.ReleaseOrder)ret.ObjectDTO).userLogin;
                        ReleaseOrderItem.releaseOrderItemEnabled = Framework.Application.DTODefaultDataEnabledValue;
                        var ok = new BLL.ReleaseOrderItem().Insert(ReleaseOrderItem);
                    }
                    ReleaseOrder.releaseOrderDate = dtInicial.AddDays(i * Convert.ToInt32(ReleaseOrder.dispensation));
                }
                return ret;
            }
            else
            {                
                ret = new DAL.ReleaseOrder().Insert(ReleaseOrder);
                // Subcadastro de Item de Pedido
                foreach (var ReleaseOrderItem in ReleaseOrder.ListReleaseOrderItem)
                {
                    ReleaseOrderItem.releaseOrderId = ((DTO.ReleaseOrder)ret.ObjectDTO).releaseOrderId;
                    ReleaseOrderItem.userLogin = ((DTO.ReleaseOrder)ret.ObjectDTO).userLogin;
                    ReleaseOrderItem.releaseOrderItemEnabled = Framework.Application.DTODefaultDataEnabledValue;
                    var ok = new BLL.ReleaseOrderItem().Insert(ReleaseOrderItem);
                }
                return ret;
            }            
        }

        public Framework.DTO.DTOWebContract Delete(DTO.ReleaseOrder ReleaseOrder)
        {
            var releaseOrderItem = new DTO.ReleaseOrderItem();
            releaseOrderItem.userLogin = ReleaseOrder.userLogin;
            releaseOrderItem.releaseOrderId = ReleaseOrder.releaseOrderId;

            var ret = new DAL.ReleaseOrderItem().Delete(releaseOrderItem);

            return new DAL.ReleaseOrder().Delete(ReleaseOrder);
        }

        public Framework.DTO.DTOWebContract ListByPatient(string patientId)
        {
            DTO.ListReleaseOrder listReleaseOrder = new DTO.ListReleaseOrder();

            var ret = new DAL.ReleaseOrder().ListByPatient(patientId);

            // Subcadastro de Item do Pedido
            foreach (DTO.ReleaseOrder ReleaseOrder in (DTO.ListReleaseOrder)ret.ObjectDTO)
            {
                ReleaseOrder.ListReleaseOrderItem = (DTO.ListReleaseOrderItem)new BLL.ReleaseOrderItem().List(ReleaseOrder.releaseOrderId.ToString()).ObjectDTO;
                listReleaseOrder.Add(ReleaseOrder);

            }
            ret.ObjectDTO = listReleaseOrder;
            return ret;
        }

    }
}
