﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.DAL;
using YouVita.DTO;
using YouVita.Framework.DTO;

namespace YouVita.BLL
{
    public class ReleaseOrderItem
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.ReleaseOrderItem().List(filter);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.ReleaseOrderItem ReleaseOrderItem)
        {
            return new DAL.ReleaseOrderItem().Insert(ReleaseOrderItem);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.ReleaseOrderItem ReleaseOrderItem)
        {
            return new DAL.ReleaseOrderItem().Delete(ReleaseOrderItem);
        }

        
        public Framework.DTO.DTOWebContract ListItemsByPatientId(string patientId)
        {
            return new DAL.ReleaseOrderItem().ListItemsByPatientId(patientId);
        }

    }
}
