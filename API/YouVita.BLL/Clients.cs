﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.DAL;
using YouVita.DTO;
using YouVita.Framework.DTO;

namespace YouVita.BLL
{
    public class Clients 
    {
        public Framework.DTO.DTOWebContract List(string clientName, string clientFantasyName, string clientCNPJ)
        {
            DTO.ListClients listClients = new DTO.ListClients();

            var ret = new DAL.Clients().List(clientName, clientFantasyName, clientCNPJ);

            // Subcadastro de Endereco
            if (ret.ObjectDTO != null)
            {
                foreach (DTO.Clients Client in (DTO.ListClients)ret.ObjectDTO)
                {
                    Client.ListClientsContacts = (DTO.ListClientsContacts)new BLL.ClientsContacts().GetByIdClient(Client.ClientId.ToString()).ObjectDTO;
                    listClients.Add(Client);

                }
            }

            ret.ObjectDTO = listClients;
            return ret;
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Clients Client)
        {            
            // Insere os Dados
            var ret = new DAL.Clients().Insert(Client);

            // Subcadastro de Contatos
            foreach (var ClientContacts in Client.ListClientsContacts)
            {
                ClientContacts.ClientId = ((DTO.Clients)ret.ObjectDTO).ClientId;
                ClientContacts.userLogin = ((DTO.Clients)ret.ObjectDTO).userLogin;
                ClientContacts.ClientsContactsEnabled = Framework.Application.DTODefaultDataEnabledValue;
                new BLL.ClientsContacts().Insert(ClientContacts);
            }
            
            //inserir o login
            DTO.Users user = new DTO.Users();
            user.userName = Client.ClientName;
            user.userPassword = Client.ClientPassword;
            user.userLoginName = Client.ClientLogin;
            user.userLogin = Client.ClientLogin;
            user.userEnabled = Framework.Application.DTODefaultDataEnabledValue;
            user.ClientId = ((DTO.Clients)ret.ObjectDTO).ClientId;
            user.userType = 2; //Cliente
            user.laboratoryID = 0;
            BLL.Users bll = new Users();
            bll.Insert(user);

            return ret;
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Clients Client)
        {
            return new DAL.Clients().Delete(Client);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Clients Client)
        {
            // Atualiza os Dados
            return new DAL.Clients().Update(Client);
        }
    }
}
