﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class Permissions
    {

        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.Permissions().List(filter);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Permissions Permission)
        {
            return new DAL.Permissions().Insert(Permission);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Permissions Permission)
        {
            return new DAL.Permissions().Delete(Permission);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Permissions Permission)
        {
            return new DAL.Permissions().Update(Permission);
        }
    }
}
