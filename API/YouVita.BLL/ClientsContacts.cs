﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.DAL;
using YouVita.DTO;
using YouVita.Framework.DTO;

namespace YouVita.BLL
{
    public class ClientsContacts
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.ClientsContacts().List(filter);
        }

        public Framework.DTO.DTOWebContract GetByIdClient(string filter)
        {
            return new DAL.ClientsContacts().GetByIdClient(filter);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.ClientsContacts ClientContact)
        {
            //criar o login
            DTO.Users user = new DTO.Users();
            user.userLogin = ClientContact.userLogin;
            user.userName = ClientContact.ContactName;
            user.userPassword = ClientContact.Password;
            user.ClientId = ClientContact.ClientId;
            user.userLoginName = ClientContact.Email;
            user.userEnabled = Framework.Application.DTODefaultDataEnabledValue;
            user.userType = 2;
            BLL.Users bll = new Users();
            bll.Insert(user);

            return new DAL.ClientsContacts().Insert(ClientContact);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.ClientsContacts ClientContact)
        {
            return new DAL.ClientsContacts().Delete(ClientContact);
        }

        public Framework.DTO.DTOWebContract Update(DTO.ClientsContacts ClientContact)
        {
            return new DAL.ClientsContacts().Update(ClientContact);
        }
    }
}
