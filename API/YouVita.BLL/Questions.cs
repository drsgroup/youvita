﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class Questions
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {

            DTO.ListQuestions listQuestions = new DTO.ListQuestions();

            var ret = new DAL.Questions().List(filter);

            if (ret.ObjectDTO != null)
            {
                // Subcadastro de questões
                foreach (DTO.Questions questions in (DTO.ListQuestions)ret.ObjectDTO)
                {
                    questions.ListAnswers = (DTO.ListAnswers)new BLL.Answers().GetByIdAnswers(questions.QuestionsId.ToString()).ObjectDTO;
                    listQuestions.Add(questions);
                }
            }

            ret.ObjectDTO = listQuestions;
            return ret;
        }
    }
}
