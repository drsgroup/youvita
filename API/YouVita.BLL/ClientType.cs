﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class ClientType
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.ClientType().List(filter);
        }
    }
}
