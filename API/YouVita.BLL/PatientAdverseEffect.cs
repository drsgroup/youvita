﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class PatientAdverseEffect
    {
        public Framework.DTO.DTOWebContract Insert(DTO.PatientAdverseEffect effect)
        {
            return new DAL.PatientAdverseEffect().Insert(effect);
        }

        public Framework.DTO.DTOWebContract List(string patientId)
        {
            return new DAL.PatientAdverseEffect().List(patientId);
        }
    }
}
