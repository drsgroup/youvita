﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.DAL;
using YouVita.DTO;
using YouVita.Framework.DTO;


namespace YouVita.BLL
{
    public class Laboratories
    {

        public Framework.DTO.DTOWebContract List(string laboratoryName, string laboratoryFantasyName, string CNPJ)
        {
            return new DAL.Laboratories().List(laboratoryName, laboratoryFantasyName, CNPJ);
        }

        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.Laboratories().List(filter);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Laboratories Laboratory)
        {
            var ret = new DAL.Laboratories().Insert(Laboratory);
            DTO.Users user = new DTO.Users();
            user.userName = Laboratory.laboratoryName;
            user.userPassword = Laboratory.password;
            user.userLoginName = Laboratory.login;
            user.ClientId = 0;
            user.userLogin = Laboratory.login;            
            user.userEnabled = Framework.Application.DTODefaultDataEnabledValue;
            user.userType = 3; //laboratorio
            user.laboratoryID = (ret.ObjectDTO as DTO.Laboratories).laboratoryID;
            BLL.Users bll = new Users();
            bll.Insert(user);
            return ret;
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Laboratories Laboratory)
        {
            return new DAL.Laboratories().Delete(Laboratory);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Laboratories Laboratory)
        {
            return new DAL.Laboratories().Update(Laboratory);
        }
    }
}
