﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class Answers
    {
        public Framework.DTO.DTOWebContract GetByIdAnswers(string filter)
        {
            return new DAL.Answers().GetByIdAnswers(filter);
        }

        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.Answers().List(filter);
        }
    }
}
