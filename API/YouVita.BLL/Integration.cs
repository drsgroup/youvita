﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using YouVita.Framework.DTO;

namespace YouVita.BLL
{
    public class Integration
    {
        public Framework.DTO.DTOWebContract getStock(string productCode, string stockType)
        {
            Framework.DTO.DTOWebContract ret = new DTOWebContract();
            
            ret.Message = "";
            ret.ObjectDTO = getStockTOTVS(productCode, stockType); 
            ItemStock x = new ItemStock();
            //x.qtd_produto = "10";
            //ret.ObjectDTO = x;
            ret.Status = (ret.ObjectDTO == null) ? "NOK" : "OK";
            return ret;
        }        

        public WebClient getClient()
        {
            WebClient client = new WebClient();
            string url = ConfigurationManager.AppSettings["urlToken"];
            var body = new reqTokenBody();
            body.client_id = ConfigurationManager.AppSettings["client_id"];
            body.client_secret = ConfigurationManager.AppSettings["client_secret"];
            body.grant_type = ConfigurationManager.AppSettings["grant_type"];
            client.Encoding = System.Text.Encoding.UTF8;
            client.Headers.Add("Content-Type", "application/json");            
            string strBody = new JavaScriptSerializer().Serialize(body);
            string response = client.UploadString(url, "POST", strBody);

            var resBody = new JavaScriptSerializer().Deserialize<resToken>(response);

            WebClient ret = new WebClient();
            ret.Headers.Add("Content-Type", "application/json");
            ret.Headers.Add("Authorization", "Bearer " + resBody.access_token);

            return ret;
        }

        public ItemStock getStockTOTVS(string productCode, string stockType)
        {
            string url = ConfigurationManager.AppSettings["url_stock"];
            var response = getClient().DownloadString(url + productCode+ @"/"+ (String.IsNullOrEmpty(stockType)?"TE0664":stockType));
            try
            {
                var itemStock = new JavaScriptSerializer().Deserialize<ItemStock>(response);
                if (itemStock == null)
                {
                    throw new System.ArgumentException("Erro ao obter o estoque", "original");
                }
                else
                {
                    return itemStock;
                }
            }catch (Exception e)
            {
                //var x = new ItemStock();
                //x.qtd_produto = "0";
                //return x;
                return null;
            }            
        }



        public void getProductsFromTOTVS()
        {
            BLL.Product bll = new Product();
            //obter todos os depositantes
            var deps = (DTO.ListLaboratories)new BLL.Laboratories().List("", "", "").ObjectDTO;

            foreach (var dep in deps)
            {
                string url = ConfigurationManager.AppSettings["url_products"];
                url += "?depositante=" + dep.cnpj.PadLeft(15, '0');
                var response = getClient().DownloadString(url);
                var products = new JavaScriptSerializer().Deserialize<List<TOTVSProduct>>(response);

                foreach (TOTVSProduct p in products)
                {
                    DTO.Product prd = new DTO.Product
                    {
                        productCode = p.cod_item,
                        productEnabled = 1,
                        productName = p.descricao_item,
                        userLogin = "IMPORTACAO",
                        productID = 0,
                        CNPJ = p.depositante.Substring(1, 14)
                    };
                    bll.Insert(prd);
                }
            }
        }

        public string getOrderTrack(string pedido, string depositante)
        {
            try
            {
                var url = ConfigurationManager.AppSettings["url_orderStatus"];
                url += @"?pedido=" + pedido + "&depositante=" + depositante.PadLeft(15, '0');
                var response = getClient().DownloadString(url);

                var itemTrack = new JavaScriptSerializer().Deserialize<dataTrack>(response);
                return itemTrack.data.status_sc;
            }catch(Exception ex)
            {
                return String.Empty;
            }
        }      
    }

    public class reqTokenBody
    {
        public string client_id { get; set; }
        public string client_secret { get; set; }
        public string grant_type { get; set; }
    }

    public class resToken
    {
        public string token_type { get; set; }
        public Int64 expires_in { get; set; }
        public string access_token { get; set; }
    }

    public class TOTVSProduct
    {
        public string depositante { get; set; }
        public string cod_item { get; set; }
        public string descricao_item { get; set; }
    }


    public class dataStock
    {
        public List<ItemStock> data { get; set; }
    }

    public class ItemStock
    { 
        //public string id { get; set; }
        //public string chave_logix { get; set; }
        //public string data_geracao { get; set; }
        //public string depositante { get; set; }
        //public string cnpj_origem { get; set; }
        //public string data_atual { get; set; }
        //public string hora_atual { get; set; }
        //public string tipo_estoque { get; set; }
        //public string desc_tipo_estoque { get; set; }
        //public string codigo_produto { get; set; }
        //public string desc_produto { get; set; }
        //public string unidade_medida { get; set; }
        //public string lote { get; set; }
        //public string data_validade { get; set; }
        //public string desc_restricao { get; set; }
        //public string qtd_regul_reser { get; set; }
        public string qtd_produto { get; set; }
        //public string qtd_fiscal { get; set; }
        //public string qtd_avariada { get; set; }
        //public string avaria { get; set; }
        //public string peca { get; set; }
        //public string serie { get; set; }
        //public string created_at { get; set; }
        //public string updated_at { get; set; }
    }

    public class ItemTrack
    {
        public string sc { get; set;  }
        public string pedido { get; set; }
        public string cod_status { get; set; }
        public string status_sc { get; set; }
        public string depositante { get; set; }
        public string tp_estoque { get; set; }
    }

    public class dataTrack
    {
        public ItemTrack data { get; set; }
    }
}

    


