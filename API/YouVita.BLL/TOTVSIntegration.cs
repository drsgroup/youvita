﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class TOTVSIntegration
    {
        private ClientService.WSCFM_SA1SOAP client;
        private string _token  ="Ku34DB/cAh1Mt54BpEORTfl3Cul8FlIf1LCmxIEAep7CyrsedvU+CU8z4CihoRv6ABoReug/CwB8ou5TbfVfZiXfYVwKFFX9rx48wdLaDRzOmg===";
        public TOTVSIntegration()
        {
            client = new ClientService.WSCFM_SA1SOAPClient();
        }

        public string SendDoctor(DTO.Doctors doctor)
        {            
            var cl = new ClientService.TCLIENTE();
            cl.CA1BAIRRO = doctor.neighborhood;
            cl.CA1CEP = doctor.zipCode;
            cl.CA1CNPJCPF = doctor.cpf;
            cl.CA1DDD = doctor.phoneComercial.Substring(1, 2);
            cl.CA1FONE = doctor.phoneComercial.Substring(3, doctor.phoneComercial.Length - 3);
            cl.CA1NOME = doctor.doctorName;
            cl.CA1NOMEMUNICIPIO = doctor.city;
            cl.CA1PESSOA = "F";
            cl.CA1ENDERECO = doctor.address + "," + doctor.number;
            cl.CA1UF = "SP";
            cl.CA1IDPORTAL = doctor.doctorID.ToString();
            cl.CA1INSCRIEST = " ";
            cl.CA1INSCRIMUN = " ";
            cl.CA1LOJA = " ";           
            cl.CA1COD = " ";
            cl.CA1NREDUZ = " ";
            cl.CA1TIPO = " ";


            var clientService = new ClientService.WSCFM_SA1SOAPClient();
            var ret = clientService.INTEGRACLIENTE(_token, "01", "0200", "I", cl);
            return ret.CCODIGO;
        }

        public string SendOrder(DTO.OrdersDoctor order, string doctorIDTOTVS)
        {
            var header = new OrderService.STRUCTSC5();
            header.CC5CLIENTE = doctorIDTOTVS;
            header.CC5CONDPAG ="001";
            header.CC5TIPO ="N";
            header.CC5TIPOCLIENTE = "F";
            header.CC5TIPODRS = "A";
            header.CC5EMISSAO = DateTime.Now.Date.ToString("yyyyMMdd");
            header.CC5IDPORTAL = " ";
            header.CC5LOJA = "01";
            header.CC5MENNOTA = " ";           

            var itens = new OrderService.STRUCTSC6();
            itens.AITENSSC6 = new OrderService.ARRAYOFASTRUCTSC6();           
            foreach (DTO.ProductsOrderDoctor product in order.Products)
            {
                itens.AITENSSC6.Add(new OrderService.ASTRUCTSC6 { CC6PRODUTO = product.productCodeTOTVS, NC6QTDE = product.Qty.ToString()  });
            }
            
            
            var orderService = new OrderService.WSCFM_SC5SOAPClient();
            var ret = orderService.INSERTPEDIDO(_token, "01", "0200", header, itens);

            return ret.CNUMPV; //numero pedido

        }



    }
}

