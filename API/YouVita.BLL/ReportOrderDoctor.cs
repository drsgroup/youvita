﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class ReportOrderDoctor
    {

        public Framework.DTO.DTOWebContract List(string LaboratoryId, string RepresentativeId, string DoctorId, string dtStart, string dtEnd, string type)
        {
            return new DAL.ReportOrderDoctor().List(LaboratoryId, RepresentativeId, DoctorId, dtStart, dtEnd, type);
        }
    }
}
