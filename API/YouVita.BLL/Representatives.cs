﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.DAL;
using YouVita.DTO;
using YouVita.Framework.DTO;


namespace YouVita.BLL
{
    public class Representatives
    {
        public Framework.DTO.DTOWebContract List(string representativeName, string CPF, string email, string laboratoryId)
        {
            return new DAL.Representatives().List(representativeName, CPF, email, laboratoryId);
        }

        public Framework.DTO.DTOWebContract ListLabID(string laboratoryID)
        {
            return new DAL.Representatives().ListLabID(laboratoryID);
        }
        

        public Framework.DTO.DTOWebContract Insert(DTO.Representatives Representative)
        {
            DTO.Users user = new DTO.Users();
            user.userName = Representative.representativeName;
            user.userPassword = Representative.password;
            user.userLoginName = Representative.login;
            user.ClientId = 0;
            user.userLogin = Representative.login;
            user.userEnabled = Framework.Application.DTODefaultDataEnabledValue;
            user.laboratoryID = Representative.laboratoryID;
            user.userType = 5; //Médico
            BLL.Users bll = new Users();
            bll.Insert(user);
            return new DAL.Representatives().Insert(Representative);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Representatives Representative)
        {
            return new DAL.Representatives().Delete(Representative);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Representatives Representative)
        {
            return new DAL.Representatives().Update(Representative);
        }
    }
}
