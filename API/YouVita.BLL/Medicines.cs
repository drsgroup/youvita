﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class Medicines
    {
        public Framework.DTO.DTOWebContract List(string EAN, string MedicineName, string LabName, string ActivePrincipleName, string Presentation)
        {
            return new DAL.Medicines().List(EAN, MedicineName, LabName, ActivePrincipleName, Presentation);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Medicines Medicines)
        {
            return new DAL.Medicines().Insert(Medicines);
        }

        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.Medicines().List(filter);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Medicines Medicines)
        {
            return new DAL.Medicines().Delete(Medicines);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Medicines Medicines)
        {
            return new DAL.Medicines().Update(Medicines);
        }
    }
}
