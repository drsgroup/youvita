﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.DAL;
using YouVita.DTO;
using YouVita.Framework.DTO;

namespace YouVita.BLL
{
    public class OrdersDoctor
    {
        public Framework.DTO.DTOWebContract List(string doctorName, string CPF, string CRM, string UF_CRM, string laboratoryID, string representativeID)
        {
            return new DAL.OrdersDoctor().List(doctorName, CPF, CRM, UF_CRM, laboratoryID, representativeID);
        }

        public Framework.DTO.DTOWebContract ListByParams(string userLogin, string laboratoryID, string representativeLogin, string doctorName, string doctorLogin)
        {
            DTO.ListOrdersDoctor listOrders = new DTO.ListOrdersDoctor();
            var ret = new DAL.OrdersDoctor().ListByParams(userLogin, laboratoryID, representativeLogin, doctorName, doctorLogin);          

            // Subcadastro de Endereco
            
            foreach (DTO.OrdersDoctor order in (DTO.ListOrdersDoctor)ret.ObjectDTO)
            {
                order.Products = (DTO.ListProductsOrderDoctor)new BLL.ProductsOrderDoctor().List(order.orderDoctorID.ToString()).ObjectDTO;
                listOrders.Add(order);
            }

            ret.ObjectDTO = listOrders;
            return ret;
        }

        public Framework.DTO.DTOWebContract Insert(DTO.OrdersDoctor order)
        {
            var ret = new DAL.OrdersDoctor().Insert(order);
            if (order.Products != null)
            {
                foreach (var produto in order.Products)
                {
                    produto.OrdersDoctorID = ((DTO.OrdersDoctor)ret.ObjectDTO).orderDoctorID;
                    produto.userLogin = ((DTO.OrdersDoctor)ret.ObjectDTO).userLogin;
                    new BLL.ProductsOrderDoctor().Insert(produto);
                }
            }
            return ret;
        }

        public Framework.DTO.DTOWebContract Delete(DTO.OrdersDoctor OrderDoctor)
        {
            return new DAL.OrdersDoctor().Delete(OrderDoctor);
        }

        public Framework.DTO.DTOWebContract Update(DTO.OrdersDoctor OrderDoctor)
        {
            return new DAL.OrdersDoctor().Update(OrderDoctor);
        }

        public Framework.DTO.DTOWebContract Resend(Int32 orderDoctorId)
        {
            new DAL.OrdersDoctor().Resend(orderDoctorId);            
            return new DTOWebContract(){Status = "OK", Message ="", ObjectDTO = null };          
        }

        public string GetCNPJ(Int32 orderDoctorId)
        {
            return new DAL.OrdersDoctor().GetCNPJ(orderDoctorId);            
        }
    }
}
