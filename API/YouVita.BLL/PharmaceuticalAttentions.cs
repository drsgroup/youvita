﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class PharmaceuticalAttentions
    {
        public Framework.DTO.DTOWebContract List(string patientName, string CPF, string identificationOnClient)
        {
            return new DAL.PharmaceuticalAttentions().List(patientName, CPF, identificationOnClient);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.PharmaceuticalAttentions entity)
        {
            return new DAL.PharmaceuticalAttentions().Insert(entity);
        }


    }
}
