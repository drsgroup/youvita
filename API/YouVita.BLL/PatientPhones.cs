﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class PatientPhones
    {

        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.PatientPhones().List(filter);
        }

        public Framework.DTO.DTOWebContract GetByIdPatient(string filter)
        {
            return new DAL.PatientPhones().GetByIdPatient(filter);
        }
        

        public Framework.DTO.DTOWebContract Insert(DTO.PatientPhones PatientPhone)
        {
            return new DAL.PatientPhones().Insert(PatientPhone);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.PatientPhones PatientPhone)
        {
            return new DAL.PatientPhones().Delete(PatientPhone);
        }

        public Framework.DTO.DTOWebContract Update(DTO.PatientPhones PatientPhone)
        {
            return new DAL.PatientPhones().Update(PatientPhone);
        }
    }
}
