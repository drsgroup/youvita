﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.DAL;
using YouVita.DTO;
using YouVita.Framework.DTO;

namespace YouVita.BLL
{
    public class DispensationPeriod
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.DispensationPeriod().List(filter);
        }
    }
}
