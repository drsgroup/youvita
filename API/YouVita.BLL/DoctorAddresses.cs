﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class DoctorAddresses
    {
        public Framework.DTO.DTOWebContract List(string doctorID)
        {
            return new DAL.DoctorAddresses().List(doctorID);
        }

        public Framework.DTO.DTOWebContract GetByDoctorID(string id)
        {
            return new DAL.DoctorAddresses().GetByDoctorID(id);
        }

        

        public Framework.DTO.DTOWebContract Insert(DTO.DoctorAddresses DoctorAddresses)
        {
            return new DAL.DoctorAddresses().Insert(DoctorAddresses);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.DoctorAddresses DoctorAddresses)
        {
            return new DAL.DoctorAddresses().Delete(DoctorAddresses);
        }

        public Framework.DTO.DTOWebContract Update(DTO.DoctorAddresses DoctorAddresses)
        {
            return new DAL.DoctorAddresses().Update(DoctorAddresses);
        }
    }
}
