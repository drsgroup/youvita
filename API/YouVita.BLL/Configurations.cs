﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.DAL;
using YouVita.DTO;
using YouVita.Framework.DTO;

namespace YouVita.BLL
{
    public class Configurations
    {

        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.Configurations().List(filter);
        }

        public string GetValue(string filter)
        {
            Framework.DTO.DTOWebContract dtoWebContract = new DAL.Configurations().List(filter);
            DTO.ListConfigurations listConfig = (DTO.ListConfigurations)dtoWebContract.ObjectDTO;
            if (listConfig.Count > 0)
            {
                return listConfig[0].configurationValue;
            }
            else
            {
                return string.Empty;
            }
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Configurations Configuration)
        {

            return new DAL.Configurations().Insert(Configuration);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Configurations Configuration)
        {
            return new DAL.Configurations().Delete(Configuration);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Configurations Configuration)
        {
            return new DAL.Configurations().Update(Configuration);
        }

    }
}
