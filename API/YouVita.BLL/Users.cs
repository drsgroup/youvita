﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.DAL;
using YouVita.DTO;
using YouVita.Framework.DTO;

namespace YouVita.BLL
{
    public class Users
    {
        public Framework.DTO.DTOWebContract GetUserByLogin(DTO.Users Users)
        {
            return new DAL.Users().GetUserByLogin(Users);
        }
        public Framework.DTO.DTOWebContract GetUserByLogin(string login)
        {
            return new DAL.Users().GetUserByLogin(login);
        }

        public Framework.DTO.DTOWebContract GetUsersLabs(string userName, string userLogin, string LaboratoryID)
        {
            return new DAL.Users().GetUsersLabs(userName, userLogin, LaboratoryID);
        }        

        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.Users().List(filter);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Users User)
        {
            return new DAL.Users().Insert(User);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Users User)
        {
            return new DAL.Users().Delete(User);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Users User)
        {
            return new DAL.Users().Update(User);
        }
    }
}
