﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{

    public class OrderAvaliableRepresentative
    {
        public Framework.DTO.DTOWebContract Insert(DTO.OrderAvaliableRepresentative OrderAvaliableRepresentative)
        {
            OrderAvaliableRepresentative.OrderAvaliableRepresentativeEnabled = Framework.Application.DTODefaultDataEnabledValue;
            var ret = new DAL.OrderAvaliableRepresentativeDAL().Insert(OrderAvaliableRepresentative);
            foreach (var product in OrderAvaliableRepresentative.Items)
            {
                product.OrderAvaliableRepresentativeID = ((DTO.OrderAvaliableRepresentative)ret.ObjectDTO).OrderAvaliableRepresentativeID;
                product.userLogin = ((DTO.OrderAvaliableRepresentative)ret.ObjectDTO).userLogin;
                product.ProductsAvaliableRepresentativeEnabled = Framework.Application.DTODefaultDataEnabledValue;
                new BLL.ProductsAvaliableRepresentative().Insert(product);
            }

            if (OrderAvaliableRepresentative.status == "A")//ja entrou aprovado
            {
                new BLL.OrderAvaliableRepresentative().ApproveOrder(OrderAvaliableRepresentative);
            }
            return ret;
        }

        public Framework.DTO.DTOWebContract Update(DTO.OrderAvaliableRepresentative OrderAvaliableRepresentative)
        {
            OrderAvaliableRepresentative.OrderAvaliableRepresentativeEnabled = Framework.Application.DTODefaultDataEnabledValue;
            var ret = new DAL.OrderAvaliableRepresentativeDAL().Update(OrderAvaliableRepresentative);
            
            // Desativa todos os itens
            new BLL.ProductsAvaliableRepresentative().DeleteByOrderAvaliableRepresentativeID(OrderAvaliableRepresentative.OrderAvaliableRepresentativeID);

            // Inclui novamente
            foreach (var product in OrderAvaliableRepresentative.Items)
            {
                product.OrderAvaliableRepresentativeID = ((DTO.OrderAvaliableRepresentative)ret.ObjectDTO).OrderAvaliableRepresentativeID;
                product.userLogin = ((DTO.OrderAvaliableRepresentative)ret.ObjectDTO).userLogin;
                product.ProductsAvaliableRepresentativeEnabled = Framework.Application.DTODefaultDataEnabledValue;
                new BLL.ProductsAvaliableRepresentative().Insert(product);
            }

            if (OrderAvaliableRepresentative.status == "A")//ja entrou aprovado
            {
                new BLL.OrderAvaliableRepresentative().ApproveOrder(OrderAvaliableRepresentative);
            }
            return ret;
        }

        public Framework.DTO.DTOWebContract Delete(DTO.OrderAvaliableRepresentative OrderAvaliableRepresentative)
        {

            // Desativa todos os itens
            //new BLL.ProductsAvaliableRepresentative().DeleteByOrderAvaliableRepresentativeID(OrderAvaliableRepresentative.OrderAvaliableRepresentativeID);
            
            // Desativa o Pedido
            OrderAvaliableRepresentative.OrderAvaliableRepresentativeEnabled = Framework.Application.DTODefaultDataDisabledValue;
            var ret = new DAL.OrderAvaliableRepresentativeDAL().Delete(OrderAvaliableRepresentative);

            return ret;
        }


        public Framework.DTO.DTOWebContract List(string filter)
        {
            DTO.ListOrderAvaliableRepresentative listOrders = new DTO.ListOrderAvaliableRepresentative();

            var ret = new DAL.OrderAvaliableRepresentativeDAL().List();

            // Subcadastro de Endereco
            foreach (DTO.OrderAvaliableRepresentative order in (DTO.ListOrderAvaliableRepresentative)ret.ObjectDTO)
            {
                order.Items = (DTO.ListProductsAvaliableRepresentative)new BLL.ProductsAvaliableRepresentative().GetListById(order.OrderAvaliableRepresentativeID.ToString()).ObjectDTO;
                listOrders.Add(order);
            }
            ret.ObjectDTO = listOrders;
            return ret;

        }

        public Framework.DTO.DTOWebContract ListByLogin(string login, string status)
        {
            DTO.ListOrderAvaliableRepresentative listOrders = new DTO.ListOrderAvaliableRepresentative();

            var ret = new DAL.OrderAvaliableRepresentativeDAL().ListByLogin(login, status);

            // Subcadastro de Endereco
            if (ret.ObjectDTO != null)
            {
                foreach (DTO.OrderAvaliableRepresentative order in (DTO.ListOrderAvaliableRepresentative)ret.ObjectDTO)
                {
                    order.Items = (DTO.ListProductsAvaliableRepresentative)new BLL.ProductsAvaliableRepresentative().GetListById(order.OrderAvaliableRepresentativeID.ToString()).ObjectDTO;
                    listOrders.Add(order);
                }
            }
            ret.ObjectDTO = listOrders;
            return ret;

        }

        public Framework.DTO.DTOWebContract ListByRepresentativeName(string doctor)
        {
            DTO.ListOrderAvaliableRepresentative listOrders = new DTO.ListOrderAvaliableRepresentative();

            var ret = new DAL.OrderAvaliableRepresentativeDAL().ListByRepresentativeName(doctor);

            // Subcadastro de Endereco
            if (ret.ObjectDTO != null)
            {
                foreach (DTO.OrderAvaliableRepresentative order in (DTO.ListOrderAvaliableRepresentative)ret.ObjectDTO)
                {
                    order.Items = (DTO.ListProductsAvaliableRepresentative)new BLL.ProductsAvaliableRepresentative().GetListById(order.OrderAvaliableRepresentativeID.ToString()).ObjectDTO;
                    listOrders.Add(order);
                }
            }
            ret.ObjectDTO = listOrders;
            return ret;

        }

        public Framework.DTO.DTOWebContract ListByRepresLogin(string represLogin, string representativeName, string laboratoryID)
        {
            DTO.ListOrderAvaliableRepresentative listOrders = new DTO.ListOrderAvaliableRepresentative();

            var ret = new DAL.OrderAvaliableRepresentativeDAL().ListByRepresLogin(represLogin, representativeName, laboratoryID);

            // Subcadastro de Endereco
            if (ret.ObjectDTO != null)
            {
                foreach (DTO.OrderAvaliableRepresentative order in (DTO.ListOrderAvaliableRepresentative)ret.ObjectDTO)
                {
                    order.Items = (DTO.ListProductsAvaliableRepresentative)new BLL.ProductsAvaliableRepresentative().GetListById(order.OrderAvaliableRepresentativeID.ToString()).ObjectDTO;
                    listOrders.Add(order);
                }
            }
            ret.ObjectDTO = listOrders;
            return ret;
        }

        public Framework.DTO.DTOWebContract ApproveOrder(DTO.OrderAvaliableRepresentative order)
        {
            var ret = new Framework.DTO.DTOWebContract();
            try
            {
                //aqui fazer a aprovar o pedido e gerar as "reposicoes"
                var startDate = DateTime.Now;
                var ctrlDate = order.dateToSend;

                var bll = new BLL.OrdersRepresentative();
                while (ctrlDate <= order.endValidity)
                {
                    var orderRepresentative = new DTO.OrdersRepresentative();
                    orderRepresentative.createDate = DateTime.Now;
                    orderRepresentative.orderRepresentativeAvaliableID = order.OrderAvaliableRepresentativeID;
                    orderRepresentative.status = "A";
                    orderRepresentative.userLogin = order.userLogin;
                    orderRepresentative.representativeID = order.RepresentativeID;
                    orderRepresentative.orderRepresentativeDateSend = ctrlDate;
                    orderRepresentative.Products = new DTO.ListProductsOrderRepresentative();
                    orderRepresentative.orderRepresentativeEnabled = YouVita.Framework.Application.DTODefaultDataEnabledValue;

                    foreach (var item in order.Items)
                    {
                        var produto = new DTO.ProductsOrderRepresentative();
                        produto.ProductID = item.productID;
                        produto.Qty = item.qty;
                        produto.userLogin = item.userLogin;

                        var bllProduct = new BLL.ProductsOrderRepresentative();
                        produto.productCodeTOTVS = bllProduct.GetProductCode(produto.ProductID.ToString());
                        orderRepresentative.Products.Add(produto);
                    }
                    bll.Insert(orderRepresentative);

                    //se a data for igual hoje, enviar para o TOTVS
                    //if (orderRepresentative.orderRepresentativeDateSend.Date == DateTime.Now.Date)
                    //{
                    //    var totvs = new TOTVSIntegration();
                    //    var repres = (new BLL.Representatives().ListLabID(orderRepresentative.representativeID.ToString()).ObjectDTO as DTO.ListRepresentatives);
                    //    var represREt = totvs.SendDoctor(repres[0]);

                    //    //medico enviado
                    //    //enviar pedido
                    //    var x = totvs.SendOrder(orderDoctor, doctorRet);
                    //}
                    ctrlDate = ctrlDate.AddDays(order.frequence); //incremento em dias
                }

                //aqui atualizar o pedido como aprovado
                ret.Status = "OK";

            }
            catch (Exception ex)
            {
                ret.Status = "NOK";
                ret.Message = "Erro ao aprovar o pedido: " + ex.Message;
            }
            return ret;
        }
    }
}
