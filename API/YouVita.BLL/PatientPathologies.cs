﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class PatientPathologies
    {

        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.PatientPathologies().List(filter);
        }


        public Framework.DTO.DTOWebContract GetByIdPatient(string filter)
        {
            return new DAL.PatientPathologies().GetByIdPatient(filter);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.PatientPathologies PatientPathologie)
        {
            return new DAL.PatientPathologies().Insert(PatientPathologie);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.PatientPathologies PatientPathologie)
        {
            return new DAL.PatientPathologies().Delete(PatientPathologie);
        }

        public Framework.DTO.DTOWebContract Update(DTO.PatientPathologies PatientPathologie)
        {
            return new DAL.PatientPathologies().Update(PatientPathologie);
        }
    }
}
