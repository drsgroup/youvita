﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.DAL;
using YouVita.DTO;
using YouVita.Framework.DTO;

namespace YouVita.BLL
{
    public class Product
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.Product().List(filter);
        }

        public Framework.DTO.DTOWebContract List(string filter, Int64 laboratoryID)
        {
            return new DAL.Product().List(filter, laboratoryID);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Product product)
        {
            return new DAL.Product().Insert(product);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Product product)
        {
            return new DAL.Product().Delete(product);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Product product)
        {
            return new DAL.Product().Update(product);
        }

        public Framework.DTO.DTOWebContract GetStock(Int64 laboratoryID, Int64 productID, string stockType)
        {
            Framework.DTO.DTOWebContract ret = new DTOWebContract();            
            //buscar o codigo do produto
            var produtos = new DAL.Product().GetProductCode(1, productID);
            string productCode = "";
            if ( (produtos.ObjectDTO != null) &&
                 ((produtos.ObjectDTO as ListProducts).Count > 0) )
            {
                productCode = ( (produtos.ObjectDTO as DTO.ListProducts)[0] as DTO.Product).productCode;
            }

            try
            {                
                var bll = new Integration();

                var r = bll.getStock(productCode, stockType);
                return r;
            }
            catch (Exception e)
            {
                ret.Status = "NOK";
                ret.Message = "Erro ao tentar obter o estoque :"+ e.Message;
                ret.ObjectDTO = null;
            }
            return ret;           
        }


        public void ImportFromTOTVS()
        {
            BLL.Integration bll = new Integration();
            bll.getProductsFromTOTVS();
            
        }
    }
}
