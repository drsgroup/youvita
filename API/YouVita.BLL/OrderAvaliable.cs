﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class OrderAvaliable
    {
        public Framework.DTO.DTOWebContract Insert(DTO.OrderAvaliable OrderAvaliable)
        {
            OrderAvaliable.OrderAvaliableEnabled = Framework.Application.DTODefaultDataEnabledValue;
            var ret = new DAL.OrderAvaliableDAL().Insert(OrderAvaliable);
            if (OrderAvaliable.Items != null)
            {
                foreach (var product in OrderAvaliable.Items)
                {
                    product.OrderAvaliableID = ((DTO.OrderAvaliable)ret.ObjectDTO).OrderAvaliableID;
                    product.userLogin = ((DTO.OrderAvaliable)ret.ObjectDTO).userLogin;
                    product.productsAvaliableEnabled = Framework.Application.DTODefaultDataEnabledValue;
                    new BLL.ProductsAvaliable().Insert(product);
                }
            }

            if (OrderAvaliable.status == "A")//ja entrou aprovado
            {
                OrderAvaliable.doctorAddessesId = 0; //deixar para a proc decidir
                new BLL.OrderAvaliable().ApproveOrder(OrderAvaliable);
            }          
            return ret;
        }


        public Framework.DTO.DTOWebContract Update(DTO.OrderAvaliable OrderAvaliable)
        {
            
            OrderAvaliable.OrderAvaliableEnabled = Framework.Application.DTODefaultDataEnabledValue;
            var ret = new DAL.OrderAvaliableDAL().Update(OrderAvaliable);
            
            // Desativa todos os itens
            new BLL.ProductsAvaliable().DeleteByOrderAvaliableId(OrderAvaliable.OrderAvaliableID);

            // Inclui novamente todos os itens
            if (OrderAvaliable.Items != null)
            {
                foreach (var product in OrderAvaliable.Items)
                {
                    product.OrderAvaliableID = ((DTO.OrderAvaliable)ret.ObjectDTO).OrderAvaliableID;
                    product.userLogin = ((DTO.OrderAvaliable)ret.ObjectDTO).userLogin;
                    product.productsAvaliableEnabled = Framework.Application.DTODefaultDataEnabledValue;
                    new BLL.ProductsAvaliable().Insert(product);
                }
            }

            if (OrderAvaliable.status == "A")//ja entrou aprovado
            {
                OrderAvaliable.doctorAddessesId = 0; //deixar para a proc decidir
                new BLL.OrderAvaliable().ApproveOrder(OrderAvaliable);
            }
            return ret;
        }


        public Framework.DTO.DTOWebContract Delete(DTO.OrderAvaliable OrderAvaliable)
        {

            // Desativa todos os itens
            new BLL.ProductsAvaliable().DeleteByOrderAvaliableId(OrderAvaliable.OrderAvaliableID);

            // Desativa o Pedido
            OrderAvaliable.OrderAvaliableEnabled = Framework.Application.DTODefaultDataDisabledValue;
            //var ret = new DAL.OrderAvaliableDAL().Update(OrderAvaliable);
            var ret = new DAL.OrderAvaliableDAL().Delete(new DTO.OrderAvaliable() { OrderAvaliableID = OrderAvaliable.OrderAvaliableID, userLogin ="ok"});

            return ret;
        }


        public Framework.DTO.DTOWebContract List(string filter)
        {
            DTO.ListOrderAvaliable listOrders = new DTO.ListOrderAvaliable();

            var ret = new DAL.OrderAvaliableDAL().List();

            // Subcadastro de Endereco
            foreach (DTO.OrderAvaliable order in (DTO.ListOrderAvaliable)ret.ObjectDTO)
            {
                order.Items = (DTO.ListProductsAvaliable)new BLL.ProductsAvaliable().GetListById(order.OrderAvaliableID.ToString()).ObjectDTO;


                listOrders.Add(order);
            }
            ret.ObjectDTO = listOrders;
            return ret;

        }

        public Framework.DTO.DTOWebContract ListByLogin(string login, string status)
        {
            DTO.ListOrderAvaliable listOrders = new DTO.ListOrderAvaliable();

            var ret = new DAL.OrderAvaliableDAL().ListByLogin(login, status);

            // Subcadastro de Endereco
            if (ret.ObjectDTO != null)
            {
                foreach (DTO.OrderAvaliable order in (DTO.ListOrderAvaliable)ret.ObjectDTO)
                {
                    order.Items = (DTO.ListProductsAvaliable)new BLL.ProductsAvaliable().GetListById(order.OrderAvaliableID.ToString()).ObjectDTO;
                    listOrders.Add(order);
                }
            }
            ret.ObjectDTO = listOrders;
            return ret;

        }

        public Framework.DTO.DTOWebContract ListByDoctorName(string userlogin, string doctor)
        {
            DTO.ListOrderAvaliable listOrders = new DTO.ListOrderAvaliable();

            var ret = new DAL.OrderAvaliableDAL().ListByDoctorName(userlogin, doctor);

            // Subcadastro de Endereco
            if (ret.ObjectDTO != null)
            {
                foreach (DTO.OrderAvaliable order in (DTO.ListOrderAvaliable)ret.ObjectDTO)
                {
                    order.Items = (DTO.ListProductsAvaliable)new BLL.ProductsAvaliable().GetListById(order.OrderAvaliableID.ToString()).ObjectDTO;
                    listOrders.Add(order);
                }
            }
            ret.ObjectDTO = listOrders;
            return ret;

        }

        public Framework.DTO.DTOWebContract ListByRepresLogin(string represLogin, string doctor)
        {
            DTO.ListOrderAvaliable listOrders = new DTO.ListOrderAvaliable();

            var ret = new DAL.OrderAvaliableDAL().ListByRepresLogin(represLogin, doctor);

            // Subcadastro de Endereco
            if (ret.ObjectDTO != null)
            {
                foreach (DTO.OrderAvaliable order in (DTO.ListOrderAvaliable)ret.ObjectDTO)
                {
                    order.Items = (DTO.ListProductsAvaliable)new BLL.ProductsAvaliable().GetListById(order.OrderAvaliableID.ToString()).ObjectDTO;
                    listOrders.Add(order);
                }
            }
            ret.ObjectDTO = listOrders;
            return ret;
        }        

        public Framework.DTO.DTOWebContract ApproveOrder(DTO.OrderAvaliable order)
        {
            var ret = new Framework.DTO.DTOWebContract();
            try
            {
                //aqui fazer a aprovar o pedido e gerar as "reposicoes"
                var startDate = DateTime.Now;
                var ctrlDate = order.dateToSend;

                var bll = new BLL.OrdersDoctor();                
                while (ctrlDate <= order.endValidity)
                {
                    var orderDoctor = new DTO.OrdersDoctor();
                    orderDoctor.createDate = DateTime.Now;
                    orderDoctor.OrderAvaliableID = order.OrderAvaliableID;                    
                    orderDoctor.status = "A";
                    orderDoctor.userLogin = order.userLogin;
                    orderDoctor.doctorID = order.DoctorId;
                    orderDoctor.orderDoctorDateSend = ctrlDate;
                    orderDoctor.doctorAddessesId = order.doctorAddessesId;
                    orderDoctor.Products = new DTO.ListProductsOrderDoctor();
                    orderDoctor.orderDoctorEnabled = Framework.Application.DTODefaultDataEnabledValue;

                    foreach (var item in order.Items)
                    {
                        var produto = new DTO.ProductsOrderDoctor();
                        produto.ProductID = item.productID;
                        produto.Qty = item.qty;
                        produto.userLogin = item.userLogin;

                        var bllProduct = new BLL.ProductsOrderDoctor();
                        produto.productCodeTOTVS = bllProduct.GetProductCode(produto.ProductID.ToString());
                        orderDoctor.Products.Add(produto);                       
                    }
                    bll.Insert(orderDoctor);

                    //se a data for igual hoje, enviar para o TOTVS
                    //if (orderDoctor.orderDoctorDateSend.Date == DateTime.Now.Date)
                    //{
                    //    var totvs = new TOTVSIntegration();
                    //    var doctor = (new BLL.Doctors().GetDoctorByID(orderDoctor.doctorID.ToString()).ObjectDTO as DTO.ListDoctors);
                    //    var doctorRet = totvs.SendDoctor(doctor[0]);

                    //    //medico enviado
                    //    //enviar pedido
                    //    //try
                    //    //{
                    //    //    var x = totvs.SendOrder(orderDoctor, doctorRet);
                    //    //}
                    //    //catch (Exception e){ 
                    //    //    //todo: incluir tratamento de erro ao enviar totvs
                    //    //}
                    //}
                    ctrlDate = ctrlDate.AddDays(order.frequence); //incremento em dias
                }       
         
                //aqui atualizar o pedido como aprovado
                ret.Status = "OK";

            }
            catch (Exception ex)
            {
                ret.Status = "NOK";
                ret.Message = "Erro ao aprovar o pedido: " + ex.Message;
            }
            return ret;
        }

        public Framework.DTO.DTOWebContract CheckEditCancel(string OrderAvaliableId, string login)        
        {
            var user = (DTO.ListUsers)new DAL.Users().GetUserByLogin(login).ObjectDTO;

            if (user[0].userType == 1) //usuario admin
            { //admin
                return new Framework.DTO.DTOWebContract() { Status = "OK", Message = "" };
            }
            else
            {                
                var editable = new DAL.OrderAvaliableDAL().CanEditCancel(OrderAvaliableId);
                if (!editable)
                {
                    return new Framework.DTO.DTOWebContract() { Status = "NOK", Message = "" };
                }
                else
                {
                    return new Framework.DTO.DTOWebContract() { Status = "OK", Message = "" };
                }               
            }
        }
    }
}
