﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class Pathologies
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            return new DAL.Pathologies().List(filter);
        }
    }
}
