﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class ReportReleaseOrderByClient
    {

        public Framework.DTO.DTOWebContract List(string clientId)
        {
            return new DAL.ReportReleaseOrderByClient().List(clientId);
        }

    }
}
