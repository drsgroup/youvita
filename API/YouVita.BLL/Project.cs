﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.BLL
{
    public class Project
    {

        public Framework.DTO.DTOWebContract Insert(DTO.Project project)
        {
            // Insere os Dados
            var ret = new DAL.Project().Insert(project);

            // Subcadastro de Contatos
            if (project.listProjectProduct != null)
            {
                foreach (var product in project.listProjectProduct)
                {
                    product.ProjectId = ((DTO.Project)ret.ObjectDTO).projectId;
                    product.userLogin = ((DTO.Project)ret.ObjectDTO).userLogin;
                    product.DateIncluded = DateTime.Now;
                    product.DisabledDate = DateTime.Now;
                    product.ProjectProductEnabled = Framework.Application.DTODefaultDataEnabledValue;
                    new BLL.ProjectProduct().Insert(product);
                }
            }

            if (project.listProjectDoctor != null)
            {
                foreach (var d in project.listProjectDoctor)
                {
                    d.projectId = ((DTO.Project)ret.ObjectDTO).projectId;
                    d.userLogin = ((DTO.Project)ret.ObjectDTO).userLogin;
                    d.DateIncluded = DateTime.Now;
                    d.DisabledDate = DateTime.Now;
                    d.projectDoctorEnabled = Framework.Application.DTODefaultDataEnabledValue;
                    new BLL.ProjectDoctor().Insert(d);
                }
            }
            return ret;
        }

        public Int32 GetExistsStock(string stock, int projectId)
        {
            return new DAL.Project().GetExistsStock(stock, projectId);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Project project)
        {         
            // Atualiza os Dados
            var ret = new DAL.Project().Update(project);

            new BLL.ProjectDoctor().Delete(project.projectId);

            new BLL.ProjectProduct().Delete(project.projectId);



            if (project.listProjectProduct != null)
            {
                foreach (var product in project.listProjectProduct)
                {
                    product.ProjectId = ((DTO.Project)ret.ObjectDTO).projectId;
                    product.userLogin = ((DTO.Project)ret.ObjectDTO).userLogin;
                    product.DateIncluded = DateTime.Now;
                    product.DisabledDate = DateTime.Now;
                    product.ProjectProductEnabled = Framework.Application.DTODefaultDataEnabledValue;
                    new BLL.ProjectProduct().Insert(product);
                }
            }

            if (project.listProjectDoctor != null)
            {
                foreach (var d in project.listProjectDoctor)
                {
                    d.projectId = ((DTO.Project)ret.ObjectDTO).projectId;
                    d.userLogin = ((DTO.Project)ret.ObjectDTO).userLogin;
                    d.DateIncluded = DateTime.Now;
                    d.DisabledDate = DateTime.Now;
                    d.projectDoctorEnabled = Framework.Application.DTODefaultDataEnabledValue;
                    new BLL.ProjectDoctor().Insert(d);
                }
            }
            return ret;


        }

        public Framework.DTO.DTOWebContract List(string filter)
        {
            // Atualiza os Dados
            return new DAL.Project().ListFilter(filter);
        }

        public Framework.DTO.DTOWebContract ListLabId(string laboratoryId)
        {
            // Atualiza os Dados
            return new DAL.Project().ListLabId(laboratoryId);
        }        


        public Framework.DTO.DTOWebContract Delete(DTO.Project project)
        {
            return new DAL.Project().Delete(project);
        }



    }
}
