﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.DAL;
using YouVita.DTO;
using YouVita.Framework.DTO;

namespace YouVita.BLL
{
    public class ProductsAvaliable
    {
        public Framework.DTO.DTOWebContract List(string login)
        {
            return new DAL.ProductsAvaliable().GetProductsAvaliable(login);
        }

        public Framework.DTO.DTOWebContract GetListById(string OrderAvaliableID)
        {
            return new DAL.ProductsAvaliable().GetProductsAvaliableByID(OrderAvaliableID);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.ProductsAvaliable ProductAvaliable)
        {
            return new DAL.ProductsAvaliable().Insert(ProductAvaliable);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.ProductsAvaliable ProductAvaliable)
        {
            return new DAL.ProductsAvaliable().Delete(ProductAvaliable);
        }


        public void DeleteByOrderAvaliableId(int OrderAvaliableId)
        {
            new DAL.ProductsAvaliable().DeleteByOrderAvaliableId(OrderAvaliableId);
        }


        public Framework.DTO.DTOWebContract Update(DTO.ProductsAvaliable ProductAvaliable)
        {
            return new DAL.ProductsAvaliable().Update(ProductAvaliable);
        }
    }
}
