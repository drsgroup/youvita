﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.DAL;
using YouVita.DTO;
using YouVita.Framework.DTO;

namespace YouVita.BLL
{
    public class ProductsOrderDoctor
    {
        public Framework.DTO.DTOWebContract List(string OrdersDoctorID)
        {
            return new DAL.ProductsOrderDoctor().ListByID(OrdersDoctorID);
        }

        public string GetProductCode(string productID)
        {
            return new DAL.ProductsOrderDoctor().GetProductCode(productID);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.ProductsOrderDoctor ProductsOrderDoctor)
        {
            return new DAL.ProductsOrderDoctor().Insert(ProductsOrderDoctor);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.ProductsOrderDoctor ProductsOrderDoctor)
        {
            return new DAL.ProductsOrderDoctor().Delete(ProductsOrderDoctor);
        }



    }
}
