﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.BLL;

namespace YouVita.WinService
{
    public class YouVitaServiceProcess
    {

        public void Execute()
        {
            try
            {
                this.SaveLogDataBase("Inicio do Processamento", "");
                //----------------------------------------
                this.SaveLogDataBase("Gerando Arquivos", "");
                BLL.GenerateOrderDoctorFile bll = new GenerateOrderDoctorFile();
                bll.GenerateFile();
                this.SaveLogDataBase("Fim da geração dos arquivos", "");
                //----------------------------------------


                //----------------------------------------
                this.SaveLogDataBase("Inicio da checagem dos arquivos de retorno", "");
                bll.checkReturn();
                this.SaveLogDataBase("Fim da checagem dos arquivos de retorno", "");
                //----------------------------------------


                //----------------------------------------
                this.SaveLogDataBase("Inicio da importação dos produtos", "");
                BLL.Product product = new Product();
                product.ImportFromTOTVS();
                this.SaveLogDataBase("Fim da importação dos produtos", "");
                //----------------------------------------


                //----------------------------------------
                var bllOrder = new BLL.OrdersDoctor();




                //----------------------------------------





                this.SaveLogDataBase("Fim do Processamento", "");
            }
            catch(Exception ex)
            {

                this.SaveLogDataBase("Erro no Processamento", ex.Message);
            }

        }




        public void SaveLogDataBase(string LogServiceDescription, string LogServiceComplement)
        {
            //new YouVita.BLL.LogServices().Insert(new DTO.LogServices() { LogServiceId = 0, LogServiceDate = DateTime.Now, LogServiceName = "YouVitaService", LogServiceDescription = LogServiceDescription, LogServiceComplement = LogServiceComplement, userLogin = "YouVitaService" });

        }



    }
}
