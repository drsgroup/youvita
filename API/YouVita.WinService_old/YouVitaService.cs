﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;

namespace YouVita.WinService
{
    public partial class YouVitaService : ServiceBase
    {

        private int eventId = 1;

        public YouVitaService()
        {
            InitializeComponent();
            eventLogService = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("YouVitaService"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "YouVitaService", "YouVitaServiceLog");
            }
            eventLogService.Source = "YouVitaService";
            eventLogService.Log = "YouVitaServiceLog";
        }

        protected override void OnStart(string[] args)  
        {
            eventLogService.WriteEntry("Start Service");
            new YouVitaServiceProcess().SaveLogDataBase("Start Service", "");
            Timer timer = new Timer();
            timer.Interval = 60000*5; // 5 minutos
            timer.Elapsed += new ElapsedEventHandler(this.Execute);
            timer.Start();
        }

        protected override void OnStop()
        {
            eventLogService.WriteEntry("Finish Service");
            new YouVitaServiceProcess().SaveLogDataBase("Finish Service", "");
        }

        public void Execute(object sender, ElapsedEventArgs args)
        {
            eventLogService.WriteEntry("Monitoring the System", EventLogEntryType.Information, eventId++);
            new YouVitaServiceProcess().Execute();
        }
    }
}
