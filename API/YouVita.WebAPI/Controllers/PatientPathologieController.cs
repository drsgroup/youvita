﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class PatientPathologieController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Get(string userLogin, string filter)
        {
            return new BLL.PatientPathologies().List(filter);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin)
        {
            return new BLL.PatientPathologies().List("");
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, DTO.PatientPathologies patientPathologies)
        {
            patientPathologies.userLogin = userLogin;
            patientPathologies.patientPathologiesEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.PatientPathologies().Insert(patientPathologies);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int patientPathologiesId)
        {
            return new BLL.PatientPathologies().Delete(new DTO.PatientPathologies()
            {
                userLogin = userLogin,
                PatientPathologiesId = patientPathologiesId
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, DTO.PatientPathologies patientPathologies)
        {
            patientPathologies.userLogin = userLogin;
            patientPathologies.patientPathologiesEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.PatientPathologies().Update(patientPathologies);
        }

    }
}
