﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;


namespace YouVita.WebAPI.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class PatientAdverseEffectController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Post(string userLogin, DTO.PatientAdverseEffect effect)
        {
            effect.userLogin = userLogin;            
            return new BLL.PatientAdverseEffect().Insert(effect);
        }

        public Framework.DTO.DTOWebContract Get(string patientId)
        {            
            return new BLL.PatientAdverseEffect().List(patientId);
        }

    }
}
