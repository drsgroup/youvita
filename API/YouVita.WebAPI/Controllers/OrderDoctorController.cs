﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = false)]
    public class OrderDoctorController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(string userLogin, string doctorName = "", string CPF = "", string CRM = "", string UF_CRM = "", string laboratoryID = "", string representativeID = "")
        {
            return new BLL.OrdersDoctor().List(doctorName, CPF, CRM, UF_CRM, laboratoryID, representativeID);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.OrdersDoctor OrderDoctor)
        {
            OrderDoctor.userLogin = userLogin;
            OrderDoctor.orderDoctorEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.OrdersDoctor().Insert(OrderDoctor);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int OrderDoctorID)
        {
            return new BLL.OrdersDoctor().Delete(new DTO.OrdersDoctor()
            {
                userLogin = userLogin,
                orderDoctorID = OrderDoctorID
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, [FromBody] DTO.OrdersDoctor OrderDoctor)
        {
            OrderDoctor.userLogin = userLogin;
            OrderDoctor.orderDoctorEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.OrdersDoctor().Update(OrderDoctor);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string laboratoryID, string representativeLogin, string doctorName, string doctorLogin)
        {
            return new BLL.OrdersDoctor().ListByParams(userLogin, laboratoryID, representativeLogin, doctorName, doctorLogin);
        }
    }
}
