﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace YouVita.WebAPI.Controllers
{
    public class OrderRepresentativeResendController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.OrdersRepresentative order)
        {
            return new BLL.OrdersRepresentative().Resend(order.ordersRepresentativeID);
        }
    }
}
