﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{
    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ResearchesController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Get(string patientName, string CPF, string identificationOnClient)
        {
            return new BLL.Researches().List(patientName, CPF, identificationOnClient);
        }

        public Framework.DTO.DTOWebContract Get(string ResearchId)
        {
            return new BLL.Researches().List(ResearchId);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string patientId)
        {
            return new BLL.Researches().GetByPatient(patientId);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.Researches researches)
        {
            researches.userLogin = userLogin;
            researches.CreateDate = DateTime.Now;
            researches.ResearchesEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Researches().Insert(researches);
        }
    }
}
