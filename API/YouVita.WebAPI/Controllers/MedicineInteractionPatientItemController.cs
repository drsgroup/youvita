﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class MedicineInteractionPatientItemController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Get(Int32 InteractionMedicineID)
        {
            return new BLL.InteractionMedicineBLL().GetMedicinesById(InteractionMedicineID);
        }
    }
}
