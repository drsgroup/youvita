﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace YouVita.WebAPI.Controllers
{
    public class OrderDoctorResendController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.OrdersDoctor OrderDoctor)
        {
            return new BLL.OrdersDoctor().Resend(OrderDoctor.orderDoctorID);
        }
    }
}
