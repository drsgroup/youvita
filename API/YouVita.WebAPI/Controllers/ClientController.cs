﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    //[ApiExplorerSettings(IgnoreApi = true)]
    public class ClientController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(string clientName, string clientFantasyName, string clientCNPJ)
        {
            return new BLL.Clients().List(clientName, clientFantasyName, clientCNPJ);
        }


        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.Clients client)
        {
            client.userLogin = userLogin;
            client.ClientsEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Clients().Insert(client);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int clientId)
        {
            return new BLL.Clients().Delete(new DTO.Clients()
            {
                userLogin = userLogin,
                ClientId = clientId
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, [FromBody] DTO.Clients client)
        {
            client.userLogin = userLogin;
            client.ClientsEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Clients().Update(client);
        }
    }
}
