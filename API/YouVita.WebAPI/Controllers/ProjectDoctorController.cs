﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{
    public class ProjectDoctorController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Get(string userLogin, int laboratoryID, int projectId)
        {
            return new BLL.ProjectDoctor().ListAll(projectId, laboratoryID);

        }

        public Framework.DTO.DTOWebContract Get(string userLogin, int projectId)
        {
            return new BLL.ProjectDoctor().ListByProjectId(projectId);

        }
    }
}
