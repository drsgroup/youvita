﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    //[Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class LoginController : YouVitaApiController
    {
        
        public Framework.DTO.DTOWebContract Post([FromBody] DTO.Users user)
        {
            return new BLL.Users().GetUserByLogin(user);
        }

    }

}
