﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class PatientAddressController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(string userLogin, string filter)
        {
            return new BLL.PatientAddresses().List(filter);
        }

        public Framework.DTO.DTOWebContract Get(string patientId)
        {
            return new BLL.PatientAddresses().GetByIdPatient(patientId);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, DTO.PatientAddresses patientAddresses)
        {
            patientAddresses.userLogin = userLogin;
            patientAddresses.PatientAddressesEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.PatientAddresses().Insert(patientAddresses);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int patientPathologiesId)
        {
            return new BLL.PatientAddresses().Delete(new DTO.PatientAddresses()
            {
                userLogin = userLogin,
                PatientAddressesId = patientPathologiesId
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, DTO.PatientAddresses patientAddresses)
        {
            patientAddresses.userLogin = userLogin;
            patientAddresses.PatientAddressesEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.PatientAddresses().Update(patientAddresses);
        }
    }
}
