﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ConfigurationController : YouVitaApiController
    {
        
        public Framework.DTO.DTOWebContract  Get(string userLogin)
        {
            return new BLL.Configurations().List("");
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string filter)
        {
            return new BLL.Configurations().List(filter);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.Configurations configurations)
        {
            configurations.userLogin = userLogin;
            configurations.configurationEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Configurations().Insert(configurations);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int ConfigurationId)
        {
            return new BLL.Configurations().Delete(new DTO.Configurations()
            {
                userLogin = userLogin,
                configurationId = ConfigurationId
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, int configurationId, string configurationName, string configurationValue) 
        {
            return new BLL.Configurations().Update(new DTO.Configurations()
            {
                userLogin = userLogin,
                configurationId = configurationId,
                configurationName = configurationName,
                configurationValue = configurationValue,
                configurationEnabled = YouVita.Framework.Application.DTODefaultDataEnabledValue //*Obrigatorio Preenchimento no Update
            });
        }

    }
}
