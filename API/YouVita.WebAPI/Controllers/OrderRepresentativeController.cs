﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = false)]
    public class OrderRepresentativeController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(string userLogin, string representativeName = "", string cpf = "", string email = "", string laboratoryID = "")
        {
            return new BLL.OrdersRepresentative().List(representativeName, cpf, email, laboratoryID);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.OrdersRepresentative OrderDoctor)
        {
            OrderDoctor.userLogin = userLogin;
            OrderDoctor.orderRepresentativeEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.OrdersRepresentative().Insert(OrderDoctor);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int ordersRepresentativeID)
        {
            return new BLL.OrdersRepresentative().Delete(new DTO.OrdersRepresentative()
            {
                userLogin = userLogin,
                ordersRepresentativeID = ordersRepresentativeID
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, [FromBody] DTO.OrdersRepresentative OrderDoctor)
        {
            OrderDoctor.userLogin = userLogin;
            OrderDoctor.orderRepresentativeEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.OrdersRepresentative().Update(OrderDoctor);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string laboratoryID, string representativeLogin, string representativeName)
        {
            return new BLL.OrdersRepresentative().ListByParams(userLogin, laboratoryID, representativeLogin, representativeName);
        }
    }
}
