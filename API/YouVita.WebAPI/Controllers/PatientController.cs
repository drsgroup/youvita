﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class PatientController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(string patientName, string CPF, string identificationOnClient, int clientId)
        {
            return new BLL.Patients().List(patientName, CPF, identificationOnClient, clientId);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.Patients patients)
        {
            patients.userLogin = userLogin;
            patients.PatientEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Patients().Insert(patients);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int patientId)
        {
            return new BLL.Patients().Delete(new DTO.Patients()
            {
                userLogin = userLogin,
                PatientId = patientId
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, [FromBody] DTO.Patients patients)
        {
            patients.userLogin = userLogin;
            patients.PatientEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Patients().Update(patients);
        }
    }
}
