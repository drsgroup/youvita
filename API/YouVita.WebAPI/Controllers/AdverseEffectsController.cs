﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class AdverseEffectsController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(string filter)
        {
            return new BLL.AdverseEffects().List(filter);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.AdverseEffects AdverseEffects)
        {
            AdverseEffects.userLogin = userLogin;
            AdverseEffects.AdverseEffectsEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.AdverseEffects().Insert(AdverseEffects);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int AdverseEffectsId)
        {
            return new BLL.AdverseEffects().Delete(new DTO.AdverseEffects()
            {
                userLogin = userLogin,
                AdverseEffectsId = AdverseEffectsId
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, [FromBody] DTO.AdverseEffects AdverseEffects)
        {
            AdverseEffects.userLogin = userLogin;
            AdverseEffects.AdverseEffectsEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.AdverseEffects().Update(AdverseEffects);
        }
    }
}
