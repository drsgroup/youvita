﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = false)]    
    public class OrderAvaliableRepresentativeController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.OrderAvaliableRepresentative order)
        {
            order.userLogin = userLogin;
            order.OrderAvaliableRepresentativeEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.OrderAvaliableRepresentative().Insert(order);
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, [FromBody] DTO.OrderAvaliableRepresentative order)
        {
            order.userLogin = userLogin;
            order.OrderAvaliableRepresentativeEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.OrderAvaliableRepresentative().Insert(order);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int OrderAvaliableRepresentativeId)
        {
            return new BLL.OrderAvaliableRepresentative().Delete(
                new DTO.OrderAvaliableRepresentative()
                {
                    OrderAvaliableRepresentativeID = OrderAvaliableRepresentativeId,
                    userLogin = userLogin
                });
                                                                                                          
        }

        public Framework.DTO.DTOWebContract Get(string userLogin )
        {
            return new BLL.OrderAvaliableRepresentative().List(userLogin);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string representativeLogin, string status)
        {
            return new BLL.OrderAvaliableRepresentative().ListByLogin(representativeLogin, status);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string representativeName)
        {
            return new BLL.OrderAvaliableRepresentative().ListByRepresentativeName(representativeName);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string representativeLogin, string status, string representativeName, string laboratoryID)
        {
            return new BLL.OrderAvaliableRepresentative().ListByRepresLogin(representativeLogin, representativeName, laboratoryID);
        }
    }
}
