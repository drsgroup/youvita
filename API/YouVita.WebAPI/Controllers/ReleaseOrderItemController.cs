﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ReleaseOrderItemController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(string userLogin, string filter)
        {
            return new BLL.ReleaseOrderItem().List(filter);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin)
        {
            return new BLL.ReleaseOrderItem().List("");
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string filter, string patientId)
        {
            return new BLL.ReleaseOrderItem().ListItemsByPatientId(patientId);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.ReleaseOrderItem releaseOrderItem)
        {
            releaseOrderItem.userLogin = userLogin;
            releaseOrderItem.releaseOrderItemEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.ReleaseOrderItem().Insert(releaseOrderItem);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int releaseOrderItemId)
        {
            return new BLL.ReleaseOrderItem().Delete(new DTO.ReleaseOrderItem()
            {
                userLogin = userLogin,
                releaseOrderItemId = releaseOrderItemId
            });
        }
    }
}
