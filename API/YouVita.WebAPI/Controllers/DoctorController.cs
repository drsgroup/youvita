﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = false)]
    public class DoctorController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(string userLogin, string doctorName = "", string CPF = "", string CRM = "", string UF_CRM = "", string representativeID = "", string laboratoryID = "")
        {
            return new BLL.Doctors().List(doctorName, CPF, CRM, UF_CRM, representativeID, laboratoryID);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string laboratoryID, string doctor, string nok)
        {
            return new BLL.Doctors().ListLabID(laboratoryID, doctor);            
        }

        public Framework.DTO.DTOWebContract Get(string doctorID, string login, string doctorName, string obs, string crm)
        {
            return new BLL.Doctors().GetDoctorByID(doctorID);
        }

        public Framework.DTO.DTOWebContract Get(string login, string login_2, string login_3)
        {
            return new BLL.Doctors().GetDoctorByLogin(login);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.Doctors doctor)
        {
            doctor.userLogin = userLogin;
            doctor.doctorEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Doctors().Insert(doctor);
        }


        public Framework.DTO.DTOWebContract Delete(string userLogin, int doctorID)
        {
            return new BLL.Doctors().Delete(new DTO.Doctors()
            {
                userLogin = userLogin,
                doctorID = doctorID
            });
        }


        public Framework.DTO.DTOWebContract Put(string userLogin, [FromBody] DTO.Doctors doctor)
        {
            doctor.userLogin = userLogin;
            doctor.doctorEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Doctors().Update(doctor);
        }
    }
}
