﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DoctorAddressesController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(string userLogin, string doctorID)
        {
            return new BLL.DoctorAddresses().List(doctorID);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin)
        {
            return new BLL.DoctorAddresses().List("");
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, DTO.DoctorAddresses doctorAddresses)
        {
            doctorAddresses.userLogin = userLogin;
            doctorAddresses.doctorAddressesEnabled = Framework.Application.DTODefaultDataEnabledValue;
            doctorAddresses.defaultAddress = (doctorAddresses.defaultAddress == "true") ? "S" : "N";            
            return new BLL.DoctorAddresses().Insert(doctorAddresses);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int doctorAddressesId)
        {
            return new BLL.DoctorAddresses().Delete(new DTO.DoctorAddresses()
            {
                userLogin = userLogin,
                doctorAddressesId = doctorAddressesId
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, DTO.DoctorAddresses doctorAddresses)
        {
            doctorAddresses.userLogin = userLogin;
            doctorAddresses.doctorAddressesEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.DoctorAddresses().Update(doctorAddresses);
        }
    }
}
