﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class OrdersController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(string userLogin, string filter)
        {
            return new BLL.Orders().List(filter); 
        }

        public Framework.DTO.DTOWebContract Get(string userLogin)
        {
            return new BLL.Orders().List("");
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string patientId, string ok)
        {
            return new BLL.Orders().ListByPatientId(patientId);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, DTO.Orders orders)
        {
            orders.userLogin = userLogin;
            orders.ordersEnabled = Framework.Application.DTODefaultDataEnabledValue;
            orders.orderDate = DateTime.Now;
            return new BLL.Orders().Insert(orders);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int orderId)
        {
            return new BLL.Orders().Delete(new DTO.Orders()
            {
                userLogin = userLogin,
                orderId = orderId
            });
        }
    }
}
