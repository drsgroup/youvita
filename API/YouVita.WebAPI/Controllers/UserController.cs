﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = false)]
    public class UserController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Get(string userLogin = "", string laboratoryID = "", string userName = "")
        {
            return new BLL.Users().GetUsersLabs(userName, userLogin, laboratoryID);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string filter)
        {
            return new BLL.Users().GetUserByLogin(filter);
        }

        public Framework.DTO.DTOWebContract Get(string login)
        {
            return new BLL.Users().GetUserByLogin(login);
        }        

        public Framework.DTO.DTOWebContract Post(string userLogin, DTO.Users users)
        {
            users.userLogin = userLogin;
            users.userEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Users().Insert(users);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int userId)
        {
            return new BLL.Users().Delete(new DTO.Users()
            {
                userLogin = userLogin,
                userId = userId
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, DTO.Users users)
        {
            users.userLogin = userLogin;
            users.userEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Users().Update(users);
        }
    }
}
