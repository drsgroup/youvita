﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{
    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = false)]
    public class ProjectProductController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Get(string userLogin, int laboratoryID, int projectId)
        {
            return new BLL.ProjectProduct().ListAll(projectId, laboratoryID);

        }

        public Framework.DTO.DTOWebContract Get(string projectId)
        {
            return new BLL.ProjectProduct().ListLabId(projectId);

        }


    }
}
