﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class PatientPhoneController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Get(string userLogin, string filter)
        {
            return new BLL.PatientPhones().List(filter);
        }
        

        public Framework.DTO.DTOWebContract Get(string userLogin, string patientId, string ok)
        {
            return new BLL.PatientPhones().GetByIdPatient(patientId);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, DTO.PatientPhones patientPhones)
        {
            patientPhones.userLogin = userLogin;
            patientPhones.patientPhonesEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.PatientPhones().Insert(patientPhones);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int patientPhoneId)
        {
            return new BLL.PatientPhones().Delete(new DTO.PatientPhones()
            {
                userLogin = userLogin,
                patientPhoneId = patientPhoneId
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, DTO.PatientPhones patientPhones)
        {
            patientPhones.userLogin = userLogin;
            patientPhones.patientPhonesEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.PatientPhones().Update(patientPhones);
        }

    }
}
