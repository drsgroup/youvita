﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{
    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    //[ApiExplorerSettings(IgnoreApi = true)]
    public class InterationMedicineController : YouVitaApiController
    {        
        public Framework.DTO.DTOWebContract Post([FromBody] DTO.MedicineInteractionPatient interaction)
        {
            interaction.DateInteraction = DateTime.Now;
            interaction.userLogin = "userLogin";
            interaction.MedicineInteractionEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.InteractionMedicineBLL().Insert(interaction);                
        }


        public Framework.DTO.DTOWebContract Get(Int32 patientid)
        {
            return new BLL.InteractionMedicineBLL().ListByPatientId(patientid);
        }

        public Framework.DTO.DTOWebContract Get(string clientName, string patientName, DateTime? dtIni, DateTime? dtFim )
        {
            return new BLL.InteractionMedicineBLL().List(clientName, patientName, dtIni, dtFim );
            //return null;
        }

        public Framework.DTO.DTOWebContract Put([FromBody] DTO.MedicineInteractionPatient interaction)
        {
            interaction.userLogin = "xxx";
            interaction.MedicineInteractionEnabled = Framework.Application.DTODefaultDataEnabledValue;
            interaction.DateInteraction = DateTime.Now;
            return new BLL.InteractionMedicineBLL().Update(interaction);
        }
    }

    
}
