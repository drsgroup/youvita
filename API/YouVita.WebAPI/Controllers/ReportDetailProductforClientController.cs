﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{
    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ReportDetailProductforClientController : ApiController
    {

        public Framework.DTO.DTOWebContract Get(string clientId, DateTime startDate, DateTime endDate)
        {
            return new BLL.ReportDetailProductforClient().List(clientId, startDate, endDate);
        }

    }

}
