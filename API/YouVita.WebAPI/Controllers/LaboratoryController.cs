﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = false)]
    public class LaboratoryController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Get(string userLogin, string laboratoryName = "", string laboratoryFantasyName = "", string CNPJ = "")
        {
            return new BLL.Laboratories().List(laboratoryName, laboratoryFantasyName, CNPJ);
        }


        public Framework.DTO.DTOWebContract Get(string userLogin, string filter)
        {
            //return new BLL.Laboratories().List(laboratoryName, laboratoryFantasyName, CNPJ);
            return new BLL.Laboratories().List(filter);
        }


        //public Framework.DTO.DTOWebContract Get(string userLogin, string filter)
        //{
        //    return new BLL.Laboratories().List(filter);
        //}


        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.Laboratories Laboratory)
        {
            Laboratory.userLogin = userLogin;
            Laboratory.laboratoryEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Laboratories().Insert(Laboratory);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int laboratoryID)
        {
            return new BLL.Laboratories().Delete(new DTO.Laboratories()
            {
                userLogin = userLogin,
                laboratoryID = laboratoryID
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, [FromBody] DTO.Laboratories Laboratory)
        {
            Laboratory.userLogin = userLogin;
            Laboratory.laboratoryEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Laboratories().Update(Laboratory);
        }
    }
}
