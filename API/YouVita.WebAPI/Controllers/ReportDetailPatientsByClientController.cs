﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{
    [Interceptor]    
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ReportDetailPatientsByClientController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Get(string clientId)
        {
            return new BLL.ReportDetailPatientsByClient().List(clientId);
        }

    }
}
