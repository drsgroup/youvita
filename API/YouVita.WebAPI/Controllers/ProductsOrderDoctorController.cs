﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace YouVita.WebAPI.Controllers
{
    public class ProductsOrderDoctorController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Delete(string userLogin, int ProductsOrderDoctorID)
        {
            return new BLL.ProductsOrderDoctor().Delete(new DTO.ProductsOrderDoctor() { ProductsOrderDoctorID = ProductsOrderDoctorID, userLogin = userLogin });
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.ProductsOrderDoctor prod)
        {
            BLL.ProductsOrderDoctor bll = new BLL.ProductsOrderDoctor();
            Framework.DTO.DTOWebContract ret = new Framework.DTO.DTOWebContract();
            
            prod.userLogin = userLogin;            
            ret = bll.Insert(prod);

            return ret;
            
        }
    }
}
