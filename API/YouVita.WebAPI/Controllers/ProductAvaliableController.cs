﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ProductAvaliableController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(string userLogin, string filter)
        {
            return new BLL.ProductsAvaliable().List(filter);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin)
        {
            return new BLL.ProductsAvaliable().List(userLogin);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.ListProductsAvaliable ProductAvaliable)
        {
            BLL.ProductsAvaliable bll = new BLL.ProductsAvaliable();
            Framework.DTO.DTOWebContract ret = new Framework.DTO.DTOWebContract();            

            for (var i = 0; i < ProductAvaliable.Count; i++)
            {
                ProductAvaliable[i].userLogin = userLogin;
                ProductAvaliable[i].productsAvaliableEnabled = Framework.Application.DTODefaultDataEnabledValue;
                ret = bll.Insert(ProductAvaliable[i]);
            }
            return ret;           
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int productAvaliableID)
        {
            return new BLL.ProductsAvaliable().Delete(new DTO.ProductsAvaliable()
            {
                userLogin = userLogin,
                productAvaliableID = productAvaliableID
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, [FromBody] DTO.ProductsAvaliable ProductAvaliable)
        {
            ProductAvaliable.userLogin = userLogin;
            ProductAvaliable.productsAvaliableEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.ProductsAvaliable().Update(ProductAvaliable);
        }
    }
}
