﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ProductAvaliableRepresentativeController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(string userLogin, string filter)
        {
            return new BLL.ProductsAvaliableRepresentative().List(filter);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin)
        {
            return new BLL.ProductsAvaliableRepresentative().List(userLogin);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.ListProductsAvaliableRepresentative ProductAvaliable)
        {
            BLL.ProductsAvaliableRepresentative bll = new BLL.ProductsAvaliableRepresentative();
            Framework.DTO.DTOWebContract ret = new Framework.DTO.DTOWebContract();

            for (var i = 0; i < ProductAvaliable.Count; i++)
            {
                ProductAvaliable[i].userLogin = userLogin;
                ProductAvaliable[i].ProductsAvaliableRepresentativeEnabled = Framework.Application.DTODefaultDataEnabledValue;
                ret = bll.Insert(ProductAvaliable[i]);
            }
            return ret;
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int ProductsAvaliableRepresentativeID)
        {
            return new BLL.ProductsAvaliableRepresentative().Delete(new DTO.ProductsAvaliableRepresentative()
            {
                userLogin = userLogin,
                ProductsAvaliableRepresentativeID = ProductsAvaliableRepresentativeID
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, [FromBody] DTO.ProductsAvaliableRepresentative ProductAvaliable)
        {
            ProductAvaliable.userLogin = userLogin;
            ProductAvaliable.ProductsAvaliableRepresentativeEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.ProductsAvaliableRepresentative().Update(ProductAvaliable);
        }
    }
}
