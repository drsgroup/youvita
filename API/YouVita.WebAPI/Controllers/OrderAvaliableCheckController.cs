﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{
    //[Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = false)]

    public class OrderAvaliableCheckController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(string OrderAvaliableId, string userLogin)
        {
            return new BLL.OrderAvaliable().CheckEditCancel(OrderAvaliableId, userLogin);
        }
    }
}
