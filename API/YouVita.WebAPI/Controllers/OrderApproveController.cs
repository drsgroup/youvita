﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class OrderApproveController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.OrderAvaliable order)
        {
            order.userLogin = userLogin;
            return new BLL.OrderAvaliable().ApproveOrder(order);
        }
    }
}
