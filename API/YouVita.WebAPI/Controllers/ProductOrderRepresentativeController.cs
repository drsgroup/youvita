﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{
    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ProductOrderRepresentativeController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.ListProductsOrderRepresentative productsOrderRepresentative)
        {
            BLL.ProductsAvaliableRepresentative bll = new BLL.ProductsAvaliableRepresentative();
            Framework.DTO.DTOWebContract ret = new Framework.DTO.DTOWebContract();

            foreach (var item in productsOrderRepresentative)
            {
                var produto = new DTO.ProductsOrderRepresentative();
                produto.ProductID = item.ProductID;
                produto.Qty = item.Qty;
                produto.userLogin = item.userLogin;
                produto.OrdersRepresentativeID = item.OrdersRepresentativeID;

                var bllProduct = new BLL.ProductsOrderRepresentative();
                produto.productCodeTOTVS = bllProduct.GetProductCode(produto.ProductID.ToString());
                ret = new BLL.ProductsOrderRepresentative().Insert(produto);
                //gravar
            }           
            return ret;
        }


        public Framework.DTO.DTOWebContract Delete(string userLogin, int productsOrderDoctorRepresentativeID)
        {
            return new BLL.ProductsOrderRepresentative().Delete(new DTO.ProductsOrderRepresentative()
            {
                userLogin = userLogin,
                ProductsOrderDoctorRepresentativeID = productsOrderDoctorRepresentativeID
            });
        }
    }
}
