﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = false)]
    public class OrderAvaliableController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.OrderAvaliable order)
        {
            order.userLogin = userLogin;
            order.OrderAvaliableEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.OrderAvaliable().Insert(order);
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, [FromBody] DTO.OrderAvaliable order)
        {
            order.userLogin = userLogin;
            order.OrderAvaliableEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.OrderAvaliable().Update(order);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int OrderAvaliableId)
        {
            return new BLL.OrderAvaliable().Delete(new DTO.OrderAvaliable() { OrderAvaliableID = OrderAvaliableId, userLogin = userLogin });
        }


        public Framework.DTO.DTOWebContract Get(string userLogin)
        {
            return new BLL.OrderAvaliable().List(userLogin);
        }
        public Framework.DTO.DTOWebContract Get(string userLogin, string loginDoctor, string status)
        {
            return new BLL.OrderAvaliable().ListByLogin(loginDoctor, status);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string doctorName)
        {
            return new BLL.OrderAvaliable().ListByDoctorName(userLogin, doctorName);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string representativeLogin, string status, string doctor)
        {
            return new BLL.OrderAvaliable().ListByRepresLogin(representativeLogin, doctor);
        }      
    }
}
