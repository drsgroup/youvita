﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class PrescriptionItemController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(string userLogin, string filter)
        {
            return new BLL.PrescriptionsItems().List(filter);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin)
        {
            return new BLL.PrescriptionsItems().List("");
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.PrescriptionsItems prescriptionItem)
        {
            prescriptionItem.userLogin = userLogin;
            prescriptionItem.prescriptionItemEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.PrescriptionsItems().Insert(prescriptionItem);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int prescriptionItemId)
        {
            return new BLL.PrescriptionsItems().Delete(new DTO.PrescriptionsItems()
            {
                userLogin = userLogin,
                prescriptionItemId = prescriptionItemId
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, [FromBody] DTO.PrescriptionsItems prescriptionItem)
        {
            prescriptionItem.userLogin = userLogin;
            prescriptionItem.prescriptionItemEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.PrescriptionsItems().Update(prescriptionItem);
        }
    }
}
