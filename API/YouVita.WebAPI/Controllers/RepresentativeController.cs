﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = false)]
    public class RepresentativeController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(string userLogin, string representativeName = "", string CPF = "", string email = "", string laboratoryId = "")
        {
            return new BLL.Representatives().List(representativeName, CPF, email, laboratoryId);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string laboratoryID, string ok)
        {
            return new BLL.Representatives().ListLabID(laboratoryID);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.Representatives representatives)
        {
            representatives.userLogin = userLogin;
            representatives.representativeEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Representatives().Insert(representatives);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int representativeID)
        {
            return new BLL.Representatives().Delete(new DTO.Representatives()
            {
                userLogin = userLogin,
                representativeID = representativeID
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, [FromBody] DTO.Representatives representatives)
        {
            representatives.userLogin = userLogin;
            representatives.representativeEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Representatives().Update(representatives);
        }
    }
}
