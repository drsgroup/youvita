﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace YouVita.WebAPI.Controllers
{
    public class OrderDoctorTrackingController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Get(string OrderDoctorId, string pedido)
        {
            var cnpj = new BLL.OrdersDoctor().GetCNPJ(Convert.ToInt32(OrderDoctorId));

            var track = new BLL.Integration().getOrderTrack(pedido, cnpj);
            return new Framework.DTO.DTOWebContract() { Status = "OK", Message = track, ObjectDTO = null };
            
        }
    }
}
