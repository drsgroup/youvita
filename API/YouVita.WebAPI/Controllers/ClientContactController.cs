﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ClientContactController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(string userLogin, string filter)
        {
            return new BLL.ClientsContacts().List(filter);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin)
        {
            return new BLL.ClientsContacts().List("");
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.ClientsContacts clientContacts)
        {
            clientContacts.userLogin = userLogin;
            clientContacts.ClientsContactsEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.ClientsContacts().Insert(clientContacts);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int contactId)
        {
            return new BLL.ClientsContacts().Delete(new DTO.ClientsContacts()
            {
                userLogin = userLogin,
                ContactId = contactId
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, [FromBody] DTO.ClientsContacts clientContacts)
        {
            clientContacts.userLogin = userLogin;
            clientContacts.ClientsContactsEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.ClientsContacts().Update(clientContacts);
        }
    }
}
