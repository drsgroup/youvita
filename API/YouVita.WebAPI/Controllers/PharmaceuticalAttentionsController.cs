﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Interceptor]
    public class PharmaceuticalAttentionsController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Get(string patientName, string CPF, string identificationOnClient)
        {
            return new BLL.PharmaceuticalAttentions().List(patientName, CPF, identificationOnClient);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, DTO.PharmaceuticalAttentions entity)
        {
            entity.userLogin = userLogin;
            return new BLL.PharmaceuticalAttentions().Insert(entity);
        }

    }
}
