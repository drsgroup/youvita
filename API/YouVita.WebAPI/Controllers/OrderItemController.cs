﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class OrderItemController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(string userLogin, string filter)
        {
            return new BLL.OrderItem().List(filter);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin)
        {
            return new BLL.OrderItem().List("");
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, DTO.OrderItem orderItem)
        {
            orderItem.userLogin = userLogin;
            orderItem.orderItemEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.OrderItem().Insert(orderItem);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int orderItemId)
        {
            return new BLL.OrderItem().Delete(new DTO.OrderItem()
            {
                userLogin = userLogin,
                orderItemId = orderItemId
            });
        }
    }
}
