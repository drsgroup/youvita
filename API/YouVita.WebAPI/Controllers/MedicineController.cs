﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class MedicineController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Get(string EAN, string MedicineName, string LabName, string ActivePrincipleName, string Presentation)
        {
            return new BLL.Medicines().List(EAN, MedicineName, LabName, ActivePrincipleName, Presentation);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string filter)
        {
            return new BLL.Medicines().List(filter);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, DTO.Medicines medicines)
        {
            medicines.userLogin = userLogin;
            medicines.MedicineEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Medicines().Insert(medicines);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int medicineId)
        {
            return new BLL.Medicines().Delete(new DTO.Medicines()
            {
                userLogin = userLogin,
                MedicineId = medicineId
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, DTO.Medicines medicines)
        {
            medicines.userLogin = userLogin;
            medicines.MedicineEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Medicines().Update(medicines);
        }

    }
}
