﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class PermissionController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Get(string userLogin, string filter)
        {
            return new BLL.Permissions().List(filter);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin)
        {
            return new BLL.Permissions().List("");
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, DTO.Permissions permissions)
        {
            permissions.userLogin = userLogin;
            permissions.permissionEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Permissions().Insert(permissions);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int permissionId)
        {
            return new BLL.Permissions().Delete(new DTO.Permissions()
            {
                userLogin = userLogin,
                permissionId = permissionId
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, int permissionId, DTO.Permissions permissions)
        {
            permissions.userLogin = userLogin;
            permissions.permissionEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Permissions().Update(permissions);
        }

    }
}
