﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{
    [Interceptor]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ProjectController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.Project project)
        {
            project.userLogin = userLogin;
            project.ProjectEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Project().Insert(project);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string filter)
        {
            return new BLL.Project().List(filter);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string stock, string projectId)
        {
            var projId = (String.IsNullOrEmpty(projectId) || projectId =="undefined") ? 0 : Convert.ToInt32(projectId);
            var qtd = new BLL.Project().GetExistsStock(stock, projId);
            var ret = new Framework.DTO.DTOWebContract();
            if (qtd == 0)
            {
                ret.Status = "OK";
            }
            else
            {
                ret.Status = "NOK";
                ret.Message = "Estoque já utilizado em outro projeto";
            }

            return ret;
        }

        public Framework.DTO.DTOWebContract Get(string laboratoryId)
        {
            return new BLL.Project().ListLabId(laboratoryId);
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, [FromBody] DTO.Project project)
        {
            project.userLogin = userLogin;
            project.ProjectEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Project().Update(project);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int projectId)
        {            
            
            return new BLL.Project().Delete(new DTO.Project()
            {
                userLogin = userLogin,
                projectId = projectId
            });
        }

    }
}

    




