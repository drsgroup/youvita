﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ReleaseOrderController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(DateTime? ReleaseOrderDate, String PatientName)
        {
            return new BLL.ReleaseOrder().ListByFilter(ReleaseOrderDate, PatientName);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin,string releaseOrderId, string ok)
        {
            return new BLL.ReleaseOrder().ListByOrder(releaseOrderId);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string releaseOrderId, string patientId, string ok)
        {
            return new BLL.ReleaseOrder().ListByOrderPatient(patientId);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.ReleaseOrder releaseOrder)
        {
            releaseOrder.userLogin = userLogin;
            releaseOrder.releaseOrderEnabled = Framework.Application.DTODefaultDataEnabledValue;
            releaseOrder.releaseOrderDate = DateTime.Now;
            return new BLL.ReleaseOrder().Insert(releaseOrder);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int releaseOrderId)
        {
            return new BLL.ReleaseOrder().Delete(new DTO.ReleaseOrder()
            {
                userLogin = userLogin,
                releaseOrderId = releaseOrderId
            });
        }
    }
}
