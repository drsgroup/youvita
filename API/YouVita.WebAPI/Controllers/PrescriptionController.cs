﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = true)]
    public class PrescriptionController : YouVitaApiController
    {
        public Framework.DTO.DTOWebContract Get(DateTime? prescriptionDate, DateTime? expirationDate, string patientName, string CRM, string UfCRM, string DoctorName)
        {
            return new BLL.Prescriptions().ListByFilter( prescriptionDate,  expirationDate,  patientName,  CRM,  UfCRM,  DoctorName);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string prescriptionId, string ok)
        {
            return new BLL.Prescriptions().ListByPrescription(prescriptionId);
        }

        public Framework.DTO.DTOWebContract Get(string userLogin, string prescriptionId, string patientId, string ok)
        {
            return new BLL.Prescriptions().ListByPrescriptionPatientId(patientId);
        }

        public Framework.DTO.DTOWebContract Post(string userLogin, [FromBody] DTO.Prescriptions prescription)
        {
            prescription.userLogin = userLogin;
            prescription.prescriptionEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Prescriptions().Insert(prescription);
        }

        public Framework.DTO.DTOWebContract Delete(string userLogin, int prescriptionId)
        {
            return new BLL.Prescriptions().Delete(new DTO.Prescriptions()
            {
                userLogin = userLogin,
                prescriptionId = prescriptionId
            });
        }

        public Framework.DTO.DTOWebContract Put(string userLogin, DTO.Prescriptions prescription)
        {
            prescription.userLogin = userLogin;
            prescription.prescriptionEnabled = Framework.Application.DTODefaultDataEnabledValue;
            return new BLL.Prescriptions().Update(prescription);
        }
    }
}
