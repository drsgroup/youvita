﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace YouVita.WebAPI.Controllers
{

    [Interceptor]
    // Define se a API será demonstrada na Documentação Swagger
    [ApiExplorerSettings(IgnoreApi = false)]
    public class ProductController : YouVitaApiController
    {

        public Framework.DTO.DTOWebContract Get(string userLogin, int laboratoryID)
        {
            var x = new BLL.Product().List("", laboratoryID);
            return x;
        }

        public Framework.DTO.DTOWebContract Get(Int64 laboratoryID, Int64 productID, string stockType)
        {
            var x = new BLL.Product().GetStock(laboratoryID, productID, stockType);
            return x;
        } 


    }
}
