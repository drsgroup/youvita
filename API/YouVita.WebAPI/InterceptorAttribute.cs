﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;


namespace YouVita.WebAPI
{
    public class InterceptorAttribute : ActionFilterAttribute
    {


        /// <summary>
        /// Intercepta a chamado e valida o token
        /// </summary>
        /// <param name="actionContext"></param>
        public override async void OnActionExecuting(HttpActionContext actionContext)
        {

            string Token = "";
            var request = actionContext.Request;
            bool TokenValid = true;
            string msg = "Token inválido ou não informado.";

            try
            {

                if (new BLL.Configurations().GetValue(Framework.Application.TokenValidate).ToUpper() == "TRUE")
                {
                    //Preenche DTO para validação do token
                    Token = request.Headers.GetValues("token").ToList()[0];

                    // Valida os Token API
                    if (Token == new BLL.Configurations().GetValue(Framework.Application.TokenAPINameConfig))
                    {
                        TokenValid = true;
                    }

                    // Valida os Token Servico
                    if (Token == new BLL.Configurations().GetValue(Framework.Application.TokenServiceNameConfig))
                    {
                        TokenValid = true;
                    }

                    if (!string.IsNullOrEmpty(Token))
                    {
                        TokenValid = false;
                    }


                    if (!TokenValid)
                    {
                        //Se token for inválido, retorna 401 - Unauthorized
                        actionContext.Response = new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.Unauthorized,
                            Content = new StringContent(msg)
                        };

                    }
                }
            }
            catch (Exception ex)
            {
                //Se token for inválido, retorna 401 - Unauthorized
                actionContext.Response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new StringContent(msg)
                };
            }


        }


        /// <summary>
        /// Atualiza se necessario o Token e devolve para o Hader a Resposta com o novo Token
        /// </summary>
        /// <param name="actionExecutedContext"></param>
        //public override async void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        //{
        //    //LogDTO logDTO = new LogDTO();
        //    //LogBLL _logBLL = new LogBLL();
        //    //TokenBLL _tokenBLL = new TokenBLL();
        //    //UserDTO userDTO = new UserDTO();

        //    var request = actionExecutedContext.Request;
        //    //     var response = actionExecutedContext.Response;

        //    //Gera Novo Token
        //    var Token = request.Headers.GetValues("token").ToList()[0];
        //    //var newToken = _tokenBLL.RegenerateToken(Token);

        //    //userDTO = _tokenBLL.DecodeToken(Token);

        //    //Preenche Header da Resposta com Token
        //    //actionExecutedContext.Response.Headers.Add("token", newToken);

        //    ////Grava Log de Reposta
        //    //logDTO.ClientID = Convert.ToInt32(userDTO.ClientID);
        //    //logDTO.StatusCode = ((int)actionExecutedContext.Response.StatusCode).ToString();
        //    //logDTO.Url = request.RequestUri.AbsolutePath;
        //    //logDTO.Response = await actionExecutedContext.Response.Content.ReadAsStringAsync();
        //    //_logBLL.AddLog(logDTO);

        //}




    }
}