﻿namespace YouVita.WinService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceYouVitaInstallerHML = new System.ServiceProcess.ServiceInstaller();
            this.serviceProcessInstaller1HML = new System.ServiceProcess.ServiceProcessInstaller();
            // 
            // serviceYouVitaInstallerHML
            // 
            this.serviceYouVitaInstallerHML.Description = "Servico Complementar do projeto Youvita";
            this.serviceYouVitaInstallerHML.DisplayName = "YouVitaService-HML";
            this.serviceYouVitaInstallerHML.ServiceName = "YouVitaService -HML";
            // 
            // serviceProcessInstaller1HML
            // 
            this.serviceProcessInstaller1HML.Password = null;
            this.serviceProcessInstaller1HML.Username = null;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.serviceYouVitaInstallerHML,
            this.serviceProcessInstaller1HML});

        }

        #endregion
        private System.ServiceProcess.ServiceInstaller serviceYouVitaInstallerHML;
        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller1HML;
    }
}