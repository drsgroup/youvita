﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Timers;
using System.Configuration;

namespace YouVita.WinService
{
    public partial class YouVitaService : ServiceBase
    {

        private int eventId = 1;

        public YouVitaService()
        {
            InitializeComponent();
            //eventLogService = new System.Diagnostics.EventLog();
            //if (!System.Diagnostics.EventLog.SourceExists("YouVitaService"))
            //{
            //    System.Diagnostics.EventLog.CreateEventSource(
            //        "YouVitaService", "YouVitaServiceLog");
            //}
            //eventLogService.Source = "YouVitaService";
            //eventLogService.Log = "YouVitaServiceLog";
        }

        protected override void OnStart(string[] args)  
        {
            //eventLogService.WriteEntry("Start Service");
            new YouVitaServiceProcess().SaveLogDataBase("Start Service", "");

            try
            {
                new YouVitaServiceProcess().SaveLogDataBase("Get Configurations", "");

                Timer timerProcessGenerateFile = new Timer();
                
                timerProcessGenerateFile.Interval = 60000 * Convert.ToInt16(new BLL.Configurations().GetValue("IntervalMinuteProcessGenerateFile"));
                timerProcessGenerateFile.Elapsed += new ElapsedEventHandler(this.ExecuteProcessGenerateFile);
                timerProcessGenerateFile.Start();

                Timer timerProcessGetReturn = new Timer();
                timerProcessGetReturn.Interval = 60000 * Convert.ToInt16(new BLL.Configurations().GetValue("IntervalMinuteProcessGetReturn"));
                timerProcessGetReturn.Elapsed += new ElapsedEventHandler(this.ExecuteProcessGetReturn);
                timerProcessGetReturn.Start();

                Timer timerProcessImportFile = new Timer();
                timerProcessImportFile.Interval = 60000 * Convert.ToInt16(new BLL.Configurations().GetValue("IntervalMinuteProcessImportFile"));
                timerProcessImportFile.Elapsed += new ElapsedEventHandler(this.ExecuteProcessImportFile);
                timerProcessImportFile.Start();
            }

            catch (Exception ex)
            {
                new YouVitaServiceProcess().SaveLogDataBase("Erro Get Configurations",ex.StackTrace);
            }
        }

        protected override void OnStop()
        {
            //eventLogService.WriteEntry("Finish Service");
            new YouVitaServiceProcess().SaveLogDataBase("Finish Service", "");
        }

        public void ExecuteProcessGenerateFile(object sender, ElapsedEventArgs args)
        {
            //eventLogService.WriteEntry("Monitoring the System", EventLogEntryType.Information, eventId++);
            // Gera os pedidos
            if (new BLL.Configurations().GetValue("ExecuteProcessGenerateFile") == "Y")
                new YouVitaServiceProcess().GenerateFile();
        }

        public void ExecuteProcessGetReturn(object sender, ElapsedEventArgs args)
        {
            //eventLogService.WriteEntry("Monitoring the System", EventLogEntryType.Information, eventId++);
            // Valida o retorno da criacao dos pedidos
            if (new BLL.Configurations().GetValue("ExecuteProcessGetReturn") == "Y")
                new YouVitaServiceProcess().GetReturn();
        }

        public void ExecuteProcessImportFile(object sender, ElapsedEventArgs args)
        {
            //eventLogService.WriteEntry("Monitoring the System", EventLogEntryType.Information, eventId++);
            // Leitura do Retorno do Arquivo do Legado
            if (new BLL.Configurations().GetValue("ExecuteProcessImportFile") == "Y")
                new YouVitaServiceProcess().ImportFile();
        }


    }
}
