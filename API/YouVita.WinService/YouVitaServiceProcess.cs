﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.BLL;

namespace YouVita.WinService
{
    public class YouVitaServiceProcess
    {

        public void GenerateFile()
        {
            try
            {                
                //----------------------------------------
                BLL.GenerateOrderDoctorFile bll = new GenerateOrderDoctorFile();
                this.SaveLogDataBase("Gerando Arquivos", "");
                bll.GenerateFile();
                this.SaveLogDataBase("Fim da geração dos arquivos", "");
                //----------------------------------------
                
            }
            catch(Exception ex)
            {
                this.SaveLogDataBase("Erro no Processamento: GenerateFile", ex.Message);
            }

        }


        public void GetReturn()
        {
            try
            {               
                //----------------------------------------
                BLL.GenerateOrderDoctorFile bll = new GenerateOrderDoctorFile();
                this.SaveLogDataBase("Inicio da checagem dos arquivos de retorno", "");
                bll.checkReturn();
                this.SaveLogDataBase("Fim da checagem dos arquivos de retorno", "");
                //----------------------------------------                
            }
            catch (Exception ex)
            {
                this.SaveLogDataBase("Erro no Processamento: GetReturn", ex.InnerException.StackTrace);
            }

        }



        public void ImportFile()
        {
            try
            {
                
                //----------------------------------------
                BLL.GenerateOrderDoctorFile bll = new GenerateOrderDoctorFile();
                this.SaveLogDataBase("Inicio da importação dos produtos", "");
                BLL.Product product = new Product();
                product.ImportFromTOTVS();
                this.SaveLogDataBase("Fim da importação dos produtos", "");
                //----------------------------------------                
            }
            catch (Exception ex)
            {
                this.SaveLogDataBase("Erro no Processamento: ImportFile", ex.Message);
            }

        }



        public void SaveLogDataBase(string LogServiceDescription, string LogServiceComplement)
        {
            new YouVita.BLL.LogServices().Insert(new DTO.LogServices() { LogServiceId = 0, LogServiceDate = DateTime.Now, LogServiceName = "YouVitaService", LogServiceDescription = LogServiceDescription, LogServiceComplement = LogServiceComplement, userLogin = "YouVitaService" });
        }

    }
}
