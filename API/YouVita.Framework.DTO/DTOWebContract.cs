﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace YouVita.Framework.DTO
{
    public class DTOWebContract
    {

        public string Status { get; set; }

        public string Message { get; set; }

        public DateTime StartProcess { get; set; }

        public DateTime FinishProcess { get; set; }

        public Object ObjectDTO { get; set; }
        


    }
}
