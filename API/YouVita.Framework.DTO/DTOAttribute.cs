﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace YouVita.Framework.DTO
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DTOAttribute : Attribute
    {
        private int _lengthField;
        private string _nameField;
        private int _parameterProcedure;
        private int _primaryKey;
        private bool _defaultField;
        private int _isNotNull;
        private string _nameScreen;

        public DTOAttribute(string DTONameField, string DTONameScreen,  int DTOPrimaryKey, int DTOLengthField, int DTONotNullField, int DTOParameterProcedure, bool DTODefaultField=false)
        {
            _nameField = DTONameField.ToUpper();
            _lengthField = DTOLengthField;
            _parameterProcedure = DTOParameterProcedure;
            _primaryKey = DTOPrimaryKey;
            _defaultField = DTODefaultField;
            _isNotNull = DTONotNullField;
            _nameScreen = DTONameScreen;
        }

        public int lenghtField
        {
            get { return _lengthField; }
        }

        public string nameField
        {
            get { return _nameField;  }
        }

        public int parameterProcedure
        {
            get { return _parameterProcedure;  }
        }

        public int primaryKey
        {
            get { return _primaryKey;  }
        }

        public bool defaultField
        {
            get { return _defaultField; }
        }

        public int isNotNull
        {
            get { return _isNotNull;  }
        }

        public string nameScreen
        {
            get { return _nameScreen;  }
        }

    }
}
