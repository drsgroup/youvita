﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using YouVita.Framework.DTO;
using YouVita.Framework.Data;

namespace YouVita.Framework.DTO
{
    public class DTOHelper    {

        private static string messageValidate;

        #region Method Private
        public static DTOWebContract completeDTOWebContract(DTOWebContract webContract, string Status, string Message)
        {
            // Validacao para regras de negocio externa antes da camada DAL.
            if (webContract.StartProcess == DateTime.MinValue)
            {
                webContract.StartProcess = DateTime.Now;
            }

            webContract.Status = Status;
            webContract.Message = Message;
            webContract.FinishProcess = DateTime.Now;

            return webContract;
        }
        #endregion

        public static DataTable ExecQuery(string queryString){
            DBProvider provider = new DBProvider();
            DataTable dtbDados = provider.getDatatable(queryString);
            return dtbDados;
        }


        public static void ExecNonQuery(string queryString)
        {
            DBProvider provider = new DBProvider();
            provider.executeQuery(queryString);
        }

        #region ValidateDTO
        public static bool validateDTODataBase(object entity)
        {

            string message = "";

            try
            {
                Type typeU = entity.GetType();
                PropertyInfo[] properties = typeU.GetProperties();

                foreach (PropertyInfo property in properties)
                {

                    // Obtem o atributo do item para identificar se e um campo valor ou id
                    DTOAttribute fieldAttribute = (DTOAttribute)Attribute.GetCustomAttribute(property, typeof(DTOAttribute));

                    if (fieldAttribute.parameterProcedure == DTOParameterProcedure.Yes)
                    {
                        if (fieldAttribute.isNotNull == DTONotNull.Yes)
                        {
                            if (property.GetValue(entity) == null)
                            {
                                message += "|| " + fieldAttribute.nameScreen + " obrigatório.";
                            }
                        }

                        if (property.GetValue(entity) != null)
                        {
                            if (property.GetValue(entity).ToString().Length > fieldAttribute.lenghtField)
                            {
                                message += "|| " + fieldAttribute.nameScreen + " está maior que o permitido.";
                            }
                        }
                    }

                }

                if (string.IsNullOrEmpty(message))
                {
                    messageValidate = string.Empty;
                    return false;
                }
                else
                {
                    messageValidate = message;
                    return true;
                }
            }

            catch (Exception e)
            {
                throw e;
            }

        }
        #endregion

        #region List
        public static DTOWebContract listDTO<T, U>(string queryString)
            where T : class
            where U : class
        {
            DTOWebContract objDTOWebContract = new DTOWebContract();

            try
            {
                objDTOWebContract.StartProcess = DateTime.Now;
                
                Type type = typeof(T);
                Type typeU = typeof(U);

                DBProvider provider = new DBProvider();
                DataTable dtbDados = provider.getDatatable(queryString);


                T list = Activator.CreateInstance<T>();
                MethodInfo methods = type.GetMethod("Add");


                PropertyInfo[] properties = typeU.GetProperties();
                DataColumnCollection columns = dtbDados.Columns;  

                for (int i = 0; i < dtbDados.Rows.Count; i++)
                {

                    U item = Activator.CreateInstance<U>();
                    foreach (PropertyInfo property in properties)
                    {

                        // Obtem o atributo do item para identificar se e um campo valor ou id
                        DTOAttribute fieldAttribute = (DTOAttribute)Attribute.GetCustomAttribute(property, typeof(DTOAttribute));

                        //if (fieldAttribute.parameterProcedure == DTOParameterProcedure.Yes)
                        {
                            if (fieldAttribute.defaultField == false)
                            {
                                if (columns.Contains(property.Name))
                                {
                                    if (!System.DBNull.Value.Equals(dtbDados.Rows[i][property.Name]))
                                    {
                                        property.SetValue(item, dtbDados.Rows[i][property.Name]);
                                    }
                                }
                            }
                        }

                    }
                    methods.Invoke(list, new object[] { item });
                }

                objDTOWebContract.ObjectDTO = list;
                return Framework.DTO.DTOHelper.completeDTOWebContract(objDTOWebContract, YouVita.Framework.Application.DTOWebContractStatusOK, messageValidate);
            }

            catch (Exception e)
            {
                return Framework.DTO.DTOHelper.completeDTOWebContract(objDTOWebContract, YouVita.Framework.Application.DTOWebContractStatusNOK, e.Message);
            }
        }
        #endregion


        #region Insert
        public static DTOWebContract insertDTODataBase(object entity)
        {
            string fieldNameKey = "";

            DTOWebContract objDTOWebContract = new DTOWebContract();
            try
            {

                objDTOWebContract.StartProcess = DateTime.Now;
                

                // Validar entidade
                if (validateDTODataBase(entity))
                {
                    return Framework.DTO.DTOHelper.completeDTOWebContract(objDTOWebContract, YouVita.Framework.Application.DTOWebContractStatusNOK, messageValidate);
                }

                Type typeU = entity.GetType();
                PropertyInfo[] properties = typeU.GetProperties();

                SqlCommand sqlComm = new SqlCommand();

                sqlComm.CommandType = CommandType.StoredProcedure;
                sqlComm.CommandText = YouVita.Framework.Application.DTOStartNameProcedureInsert + typeU.Name.ToString();

                foreach (PropertyInfo property in properties)
                {

                    DTOAttribute fieldAttribute = (DTOAttribute)Attribute.GetCustomAttribute(property, typeof(DTOAttribute));

                    if (fieldAttribute.parameterProcedure == DTOParameterProcedure.Yes)
                    {
                        if (fieldAttribute.primaryKey == DTOPrimaryKey.Yes)
                        {
                            fieldNameKey = fieldAttribute.nameField;
                            sqlComm.Parameters.AddWithValue(fieldAttribute.nameField, ((object)property.GetValue(entity)) ?? DBNull.Value).Direction = ParameterDirection.Output;
                        }
                        else
                        {
                            sqlComm.Parameters.AddWithValue(fieldAttribute.nameField, ((object)property.GetValue(entity)) ?? DBNull.Value);
                        }
                    }
                }

                new Data.DBProvider().executeCommand(ref sqlComm);
                
                foreach (PropertyInfo property in properties)
                {
                    DTOAttribute fieldAttribute = (DTOAttribute)Attribute.GetCustomAttribute(property, typeof(DTOAttribute));
                    if (fieldAttribute.primaryKey == DTOPrimaryKey.Yes)
                    {
                        property.SetValue(entity, sqlComm.Parameters[fieldNameKey].Value);
                    }
                }
                objDTOWebContract.ObjectDTO = entity;
                return Framework.DTO.DTOHelper.completeDTOWebContract(objDTOWebContract, YouVita.Framework.Application.DTOWebContractStatusOK, string.Empty);
            }

            catch (Exception e)
            {
                return Framework.DTO.DTOHelper.completeDTOWebContract(objDTOWebContract, YouVita.Framework.Application.DTOWebContractStatusNOK, e.Message);
            }
        }
        #endregion

        #region Update
        public static DTOWebContract updateDTODataBase(object entity)
        {
            DTOWebContract objDTOWebContract = new DTOWebContract();
            try
            {
                objDTOWebContract.StartProcess = DateTime.Now;
                objDTOWebContract.ObjectDTO = entity;

                // Validar entidade
                if (validateDTODataBase(entity))
                {
                    return Framework.DTO.DTOHelper.completeDTOWebContract(objDTOWebContract, YouVita.Framework.Application.DTOWebContractStatusNOK, messageValidate);
                }

                Type typeU = entity.GetType();
                PropertyInfo[] properties = typeU.GetProperties();

                SqlCommand sqlComm = new SqlCommand();

                sqlComm.CommandType = CommandType.StoredProcedure;
                sqlComm.CommandText = YouVita.Framework.Application.DTOStartNameProcedureUpdate + typeU.Name.ToString();

                foreach (PropertyInfo property in properties)
                {

                    // Obtem o atributo do item para identificar se e um campo valor ou id
                    DTOAttribute fieldAttribute = (DTOAttribute)Attribute.GetCustomAttribute(property, typeof(DTOAttribute));

                    if (fieldAttribute.parameterProcedure == DTOParameterProcedure.Yes)
                    {
                        sqlComm.Parameters.AddWithValue(fieldAttribute.nameField, property.GetValue(entity));
                    }

                }

                new Data.DBProvider().executeCommand(sqlComm);
                return Framework.DTO.DTOHelper.completeDTOWebContract(objDTOWebContract, YouVita.Framework.Application.DTOWebContractStatusOK, string.Empty);
            }

            catch (Exception e)
            {
                return Framework.DTO.DTOHelper.completeDTOWebContract(objDTOWebContract, YouVita.Framework.Application.DTOWebContractStatusNOK, e.Message);
            }

            }
        #endregion

        #region Delete
        public static DTOWebContract deleteDTODataBase(object entity)
        {
            DTOWebContract objDTOWebContract = new DTOWebContract();
            try
            {
                objDTOWebContract.StartProcess = DateTime.Now;
                objDTOWebContract.ObjectDTO = entity;

                Type typeU = entity.GetType();
                PropertyInfo[] properties = typeU.GetProperties();

                SqlCommand sqlComm = new SqlCommand();

                sqlComm.CommandType = CommandType.StoredProcedure;
                sqlComm.CommandText = YouVita.Framework.Application.DTOStartNameProcedureDelete + typeU.Name.ToString();

                foreach (PropertyInfo property in properties)
                {

                    // Obtem o atributo do item para identificar se e um campo valor ou id
                    DTOAttribute fieldAttribute = (DTOAttribute)Attribute.GetCustomAttribute(property, typeof(DTOAttribute));

                    if (fieldAttribute.parameterProcedure == DTOParameterProcedure.Yes)
                    {
                        if (fieldAttribute.primaryKey == DTOPrimaryKey.Yes || fieldAttribute.defaultField == true)
                        {
                           sqlComm.Parameters.AddWithValue(fieldAttribute.nameField, property.GetValue(entity));
                        }
                    }

                }

                new Data.DBProvider().executeCommand(sqlComm);
                return Framework.DTO.DTOHelper.completeDTOWebContract(objDTOWebContract, YouVita.Framework.Application.DTOWebContractStatusOK, string.Empty);
            }

            catch (Exception e)
            {
                return Framework.DTO.DTOHelper.completeDTOWebContract(objDTOWebContract, YouVita.Framework.Application.DTOWebContractStatusNOK, e.Message);
            }
        }
        #endregion

    }
}
