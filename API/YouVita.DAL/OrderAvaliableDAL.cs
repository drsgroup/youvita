﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class OrderAvaliableDAL
    {
        public Framework.DTO.DTOWebContract ListByDoctor(string DoctorID)
        {
            string queryString = "select * from viewOrderAvaliable ";
            queryString += " Where doctorID = " + DoctorID;

            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrderAvaliable, DTO.OrderAvaliable>(queryString);            
        }

        
        public Framework.DTO.DTOWebContract ListByDoctorName(string userlogin, string doctor)
        {
            string queryString = "SELECT O.*, d.DoctorName, ";
            queryString += " (select max(od.OrderDoctorDateSend) from viewOrdersDoctor od where od.status = 'Pedido enviado' ";
            queryString += "  and od.OrderAvaliableID = o.OrderAvaliableID) as lastSend ";
            queryString += " FROM viewOrderAvaliable o join viewDoctors d on(o.doctorID = d.DoctorID) ";
            queryString += " join viewUsers v on(d.login = v.UserLoginName) where (select count(*) from viewUsers vu where vu.userLoginName = "+ 
                YouVita.Framework.Data.DBFormat.toFormat(userlogin, YouVita.Framework.Data.DBFormat.typeValueString) + " and isnull(vu.laboratoryId, 0) = 0) > 0";

            //queryString += " and o.Status = " + YouVita.Framework.Data.DBFormat.toFormat("P", YouVita.Framework.Data.DBFormat.typeValueString);
            if (!string.IsNullOrEmpty(doctor))
            {
                queryString += " and d.doctorname like '%" + doctor + "%'";
            }
            queryString += " union ";
            queryString += " select o.*, d.DoctorName, ";
            queryString += " (select max(od.OrderDoctorDateSend) from viewOrdersDoctor od where od.status = 'Pedido enviado' ";
            queryString += "  and od.OrderAvaliableID = o.OrderAvaliableID) as lastSend ";
            queryString += "  from viewUsers v join viewLaboratories l on(l.LaboratoryID = v.laboratoryID) ";
            queryString += " join viewRepresentatives r on(r.laboratoryID = l.LaboratoryID) join viewDoctors d on(r.RepresentativeID = d.RepresentativeID) ";
            queryString += " join viewOrderAvaliable o on(o.DoctorId = d.DoctorID) where v.UserLoginName = " + YouVita.Framework.Data.DBFormat.toFormat(userlogin, YouVita.Framework.Data.DBFormat.typeValueString);
            //queryString += " and o.Status = " + YouVita.Framework.Data.DBFormat.toFormat("P", YouVita.Framework.Data.DBFormat.typeValueString);
            if (!string.IsNullOrEmpty(doctor))
            {
                queryString += " and d.doctorname like '%" + doctor + "%'";
            }
            queryString += " union ";
            queryString += " select o.*, d.DoctorName, ";
            queryString += " (select max(od.OrderDoctorDateSend) from viewOrdersDoctor od where od.status = 'Pedido enviado' ";
            queryString += "  and od.OrderAvaliableID = o.OrderAvaliableID) as lastSend ";
            queryString +=" from viewUsers v join viewRepresentatives r on(r.login = v.UserLoginName) ";
            queryString += " join viewDoctors d on(r.RepresentativeID = d.RepresentativeID) join viewOrderAvaliable o on(o.DoctorId = d.DoctorID) ";
            queryString += " where v.UserLoginName = "+ YouVita.Framework.Data.DBFormat.toFormat(userlogin, YouVita.Framework.Data.DBFormat.typeValueString);
            //queryString += " and o.Status = " + YouVita.Framework.Data.DBFormat.toFormat("P", YouVita.Framework.Data.DBFormat.typeValueString);
            if (!string.IsNullOrEmpty(doctor))
            {
                queryString += " and d.doctorname like '%" + doctor + "%'";
            }
            
            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrderAvaliable, DTO.OrderAvaliable>(queryString);
        }

        public Framework.DTO.DTOWebContract ListByLogin(string login, String status)
        {
            string queryString = "SELECT O.*, d.DoctorName, ";
            queryString += " (select max(od.OrderDoctorDateSend) from viewOrdersDoctor od where od.status = 'Pedido enviado' ";
            queryString += "  and od.OrderAvaliableID = o.OrderAvaliableID) as lastSend ";
            queryString += " FROM viewOrderAvaliable o ";
            queryString +=  " join viewDoctors d on (o.doctorID = d.DoctorID) ";
            queryString +=  " join viewUsers v on (d.login = v.UserLoginName) where 1 = 1 ";

            if (!string.IsNullOrEmpty(login))
            {
                queryString += " and v.UserLoginName = " + YouVita.Framework.Data.DBFormat.toFormat(login, YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!String.IsNullOrEmpty(status)){
                queryString +=  " and o.status = "+ YouVita.Framework.Data.DBFormat.toFormat(status, YouVita.Framework.Data.DBFormat.typeValueString); 
            }
            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrderAvaliable, DTO.OrderAvaliable>(queryString);
        }

        public Framework.DTO.DTOWebContract ListByRepresLogin(string represLogin, string doctor)
        {
            string queryString = "SELECT O.*, d.DoctorName, ";
            queryString += " (select max(od.OrderDoctorDateSend) from viewOrdersDoctor od where od.status = 'Pedido enviado' ";
            queryString += "  and od.OrderAvaliableID = o.OrderAvaliableID) as lastSend ";
            queryString += " FROM viewOrderAvaliable o ";
            queryString +=  " join viewDoctors d on (o.doctorID = d.DoctorID) ";
            queryString += " join viewUsers v on (d.login = v.UserLoginName) join viewRepresentatives r on (d.RepresentativeID = r.RepresentativeID) ";
            queryString += " where r.login = " + YouVita.Framework.Data.DBFormat.toFormat(represLogin, YouVita.Framework.Data.DBFormat.typeValueString);

            if (doctor != "undefined" && doctor != "null" && !String.IsNullOrEmpty(doctor))
            {
                queryString += " and d.doctorName like '%" + doctor + "%'";
            }
            
            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrderAvaliable, DTO.OrderAvaliable>(queryString);
        }

        public bool CanEditCancel(string OrderAvaliableId)
        {
            string queryString = @"select count(*) as qtd
               from viewOrdersDoctor v
              where v.OrderAvaliableID = " + OrderAvaliableId +
              @"  and ((v.Status = 'Pedido Enviado' or v.Status = 'Erro ao enviar') 
                or(v.OrderDoctorDateSend <= getdate()+5)) ";

            var i = Framework.DTO.DTOHelper.ExecQuery(queryString);
            if (Convert.ToInt32(i.Rows[0]["qtd"].ToString()) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }




        public Framework.DTO.DTOWebContract ListByRepresentative(string RepresentativeID)
        {
            string queryString = "select o.* from viewDoctors d ";
            queryString += "  join viewOrderAvaliable o on (d.DoctorID = o.doctorid) ";
            queryString += " where d.RepresentativeID = " + RepresentativeID;            

            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrderAvaliable, DTO.OrderAvaliable>(queryString);
        }

        public Framework.DTO.DTOWebContract ListByLab(string LabID)
        {
            string queryString = "select o.* from viewOrderAvaliable o ";            
            queryString += " where o.laboratoryID = " + LabID;

            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrderAvaliable, DTO.OrderAvaliable>(queryString);
        }

        public Framework.DTO.DTOWebContract List()
        {
            string queryString = "select o.*, d.DoctorName, ";
            queryString += " (select max(od.OrderDoctorDateSend) from viewOrdersDoctor od where od.status = 'Pedido enviado' ";
            queryString += "  and od.OrderAvaliableID = o.OrderAvaliableID) as lastSend ";
            queryString += " from viewOrderAvaliable o ";

            queryString += " join viewDoctors d on (o.doctorid = d.DoctorID) ";
            //queryString += " where o.laboratoryID = " + LabID;

            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrderAvaliable, DTO.OrderAvaliable>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.OrderAvaliable OrderAvaliable)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(OrderAvaliable);
        }

        public Framework.DTO.DTOWebContract Update(DTO.OrderAvaliable OrderAvaliable)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(OrderAvaliable);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.OrderAvaliable OrderAvaliable)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(OrderAvaliable);
        }


    }
}
