﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class QuestionnaireResponses
    {
        public Framework.DTO.DTOWebContract GetByIdResearches(string filter)
        {
            string queryString = "select * from viewQuestionnaireResponses";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where ResearchesId = " + filter;
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListQuestionnaireResponses, DTO.QuestionnaireResponses>(queryString);
        }


        public Framework.DTO.DTOWebContract Insert(DTO.QuestionnaireResponses questionnaireResponses)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(questionnaireResponses);
        }
    }
}
