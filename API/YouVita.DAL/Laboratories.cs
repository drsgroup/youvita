﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class Laboratories
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewLaboratories";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where Filter like " + YouVita.Framework.Data.DBFormat.toFormat("%" + filter + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListLaboratories, DTO.Laboratories>(queryString);
        }

        public Framework.DTO.DTOWebContract List(string laboratoryName, string laboratoryFantasyName, string CNPJ)
        {
            string queryString = "select * from viewLaboratories where 1 = 1";

            if (!string.IsNullOrEmpty(laboratoryName))
            {
                queryString += " and LaboratoryName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + laboratoryName + "%", YouVita.Framework.Data.DBFormat.typeValueString);                
            }


            if (!string.IsNullOrEmpty(laboratoryFantasyName))
            {
                queryString += " and laboratoryFantasyName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + laboratoryFantasyName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }


            if (!string.IsNullOrEmpty(CNPJ))
            {
                queryString += " and CNPJ like " + YouVita.Framework.Data.DBFormat.toFormat("%" + CNPJ + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }
            queryString += " order by LaboratoryName ";

            return Framework.DTO.DTOHelper.listDTO<DTO.ListLaboratories, DTO.Laboratories>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Laboratories Laboratory)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(Laboratory);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Laboratories Laboratory)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(Laboratory);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Laboratories Laboratory)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(Laboratory);
        }

    }
}
