﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class ProductsOrderRepresentative
    {
        public Framework.DTO.DTOWebContract ListByID(string OrdersRepresentativeID)
        {
            string queryString = "select p.*, pr.productName from viewProductsOrderRepresentative p ";
            queryString += " join viewProduct pr on (p.ProductID = pr.productID) ";
            queryString += " Where p.OrdersRepresentativeID = " + OrdersRepresentativeID;

            return Framework.DTO.DTOHelper.listDTO<DTO.ListProductsOrderRepresentative, DTO.ProductsOrderRepresentative>(queryString);
        }

        public void Resend(Int32 ordersRepresentativeID)
        {
            string sql = " update ordersRepresentative set status = " + YouVita.Framework.Data.DBFormat.toFormat("A", YouVita.Framework.Data.DBFormat.typeValueString);
            sql += " where OrdersRepresentativeID = " + ordersRepresentativeID.ToString();
            Framework.DTO.DTOHelper.ExecNonQuery(sql);
        }

        public string GetProductCode(string productID)
        {
            string queryString = "select productCode from viewProduct p ";
            queryString += " Where p.productID = " + productID;

            var ret = Framework.DTO.DTOHelper.ExecQuery(queryString);
            if (ret.Rows.Count > 0)
            {
                return ret.Rows[0]["productCode"].ToString();
            }
            else
            {
                return "0000";
            }
        }


        public Framework.DTO.DTOWebContract Insert(DTO.ProductsOrderRepresentative ProductsOrderRepresentative)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(ProductsOrderRepresentative);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.ProductsOrderRepresentative OrderDoctor)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(OrderDoctor);
        }

        public Framework.DTO.DTOWebContract Update(DTO.ProductsOrderRepresentative OrderDoctor)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(OrderDoctor);
        }
    }
}
