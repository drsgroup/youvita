﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class ReleaseOrder
    {


        public Framework.DTO.DTOWebContract ListByFilter(DateTime? ReleaseOrderDate, String PatientName)
        {
            //string queryString = "select * from viewReleaseOrder";
            string queryString = "select r.*, p.PatientName, c.ClientName, c.ClientFantasyName, p.CPF from viewReleaseOrder r ";
            queryString += " join viewPatients p on (r.PatientId = p.PatientId) ";
            queryString += " join viewClients c on (c.ClientId = p.ClientId) Where 1=1 ";

            if (ReleaseOrderDate != DateTime.MinValue && ReleaseOrderDate!= null)
            {
                queryString += " And r.ReleaseOrderDate = " + YouVita.Framework.Data.DBFormat.toFormat(ReleaseOrderDate.ToString(), YouVita.Framework.Data.DBFormat.typeValueDatetime);
            }

            if (!string.IsNullOrEmpty(PatientName))
            {
                queryString += " And p.PatientName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + PatientName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListReleaseOrder, DTO.ReleaseOrder>(queryString);
        }

        public Framework.DTO.DTOWebContract ListByOrder(string releaseOrderId)
        {
            //string queryString = "select * from viewReleaseOrder";
            string queryString = "select r.*, p.PatientName from viewReleaseOrder r ";
            queryString += " join viewPatients p on (r.PatientId = p.PatientId) ";
            queryString += " Where ReleaseOrderId = " + YouVita.Framework.Data.DBFormat.toFormat(releaseOrderId, YouVita.Framework.Data.DBFormat.typeValueString);
           

            return Framework.DTO.DTOHelper.listDTO<DTO.ListReleaseOrder, DTO.ReleaseOrder>(queryString);
        }
        
        public Framework.DTO.DTOWebContract ListByOrderPatient(string patientId)
        {
            //string queryString = "select * from viewReleaseOrder";
            string queryString = "select r.*, p.PatientName, c.clientName, p.CPF from viewReleaseOrder r ";
            queryString += " join viewPatients p on (r.PatientId = p.PatientId) ";
            queryString += " join viewClients c on (p.ClientId = c.ClientId) ";
            queryString += " Where p.patientID = " + patientId;

            return Framework.DTO.DTOHelper.listDTO<DTO.ListReleaseOrder, DTO.ReleaseOrder>(queryString);
        }

        public Framework.DTO.DTOWebContract ListByPatient(string patientId)
        {
            //string queryString = "select * from viewReleaseOrder";
            string queryString = "select r.*, p.PatientName from viewReleaseOrder r ";
            queryString += " join viewPatients p on (r.PatientId = p.PatientId) ";
            queryString += " Where r.PatientId = " + YouVita.Framework.Data.DBFormat.toFormat(patientId, YouVita.Framework.Data.DBFormat.typeValueString);


            return Framework.DTO.DTOHelper.listDTO<DTO.ListReleaseOrder, DTO.ReleaseOrder>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.ReleaseOrder ReleaseOrder)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(ReleaseOrder);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.ReleaseOrder ReleaseOrder)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(ReleaseOrder);
        }
    }
}
