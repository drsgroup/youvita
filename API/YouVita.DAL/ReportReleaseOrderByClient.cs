﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class ReportReleaseOrderByClient
    {

        public Framework.DTO.DTOWebContract List(string clientId)
        {
            string queryString = "select * from viewReportReleaseOrderByClient";

            if (!string.IsNullOrEmpty(clientId))
            {
                queryString += " where ClientId = " + clientId;
            }


            return Framework.DTO.DTOHelper.listDTO<DTO.ListReportReleaseOrderByClient, DTO.ReportReleaseOrderByClient>(queryString);
        }

    }
}
