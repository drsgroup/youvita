﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class PatientPhones
    {

        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select top 3 * from viewPatientPhones";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where Filter like " + YouVita.Framework.Data.DBFormat.toFormat("%" + filter + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListPatientPhones, DTO.PatientPhones>(queryString);
        }

        public Framework.DTO.DTOWebContract GetByIdPatient(string filter)
        {
            string queryString = "select top 3 * from viewPatientPhones";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where PatientId = "+filter;
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListPatientPhones, DTO.PatientPhones>(queryString);
        }

        

        public Framework.DTO.DTOWebContract Insert(DTO.PatientPhones PatientPhone)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(PatientPhone);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.PatientPhones PatientPhone)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(PatientPhone);
        }

        public Framework.DTO.DTOWebContract Update(DTO.PatientPhones PatientPhone)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(PatientPhone);
        }

    }
}
