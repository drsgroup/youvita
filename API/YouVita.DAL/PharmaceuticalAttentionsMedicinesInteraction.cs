﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class PharmaceuticalAttentionsMedicinesInteraction 
    {

        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewPharmaceuticalAttentionsMedicinesInteraction";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where PharmaceuticalAttentionId = " + filter;
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListPharmaceuticalAttentionsMedicinesInteraction, DTO.PharmaceuticalAttentionsMedicinesInteraction>(queryString);
        }

        public Framework.DTO.DTOWebContract ListMedicinePatient(string patientId)
        {           
            string queryString = "select distinct m.* ";
            queryString = queryString + " from releaseOrder o join releaseOrderItem i on (o.releaseorderid = i.releaseorderid) ";
            queryString = queryString + " join medicines m on (m.medicineid = i.medicineid) where o.patientid = " + patientId;
            Framework.DTO.DTOHelper.ExecNonQuery(queryString);

            return Framework.DTO.DTOHelper.listDTO<DTO.ListMedicines, DTO.Medicines>(queryString);
        }        

        public Framework.DTO.DTOWebContract Insert(DTO.PharmaceuticalAttentionsMedicinesInteraction entity)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(entity);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.PharmaceuticalAttentionsMedicinesInteraction entity)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(entity);
        }

    }
}
