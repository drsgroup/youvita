﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class AdverseEffects
    {

        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewAdverseEffects";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where AdverseEffectsDescription like " + YouVita.Framework.Data.DBFormat.toFormat("%" + filter + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListAdverseEffects, DTO.AdverseEffects>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.AdverseEffects AdverseEffects)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(AdverseEffects);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.AdverseEffects AdverseEffects)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(AdverseEffects);
        }

        public Framework.DTO.DTOWebContract Update(DTO.AdverseEffects AdverseEffects)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(AdverseEffects);
        }
    }
}
