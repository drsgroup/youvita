﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class ClientsContacts
    {

        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewClientsContacts";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where Filter like " + YouVita.Framework.Data.DBFormat.toFormat("%" + filter + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListClientsContacts, DTO.ClientsContacts>(queryString);
        }

        public Framework.DTO.DTOWebContract GetByIdClient(string filter)
        {
            string queryString = "select * from viewClientsContacts";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where ClientId = " + filter;
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListClientsContacts, DTO.ClientsContacts>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.ClientsContacts ClientsContacts)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(ClientsContacts);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.ClientsContacts ClientsContacts)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(ClientsContacts);
        }

        public Framework.DTO.DTOWebContract Update(DTO.ClientsContacts ClientsContacts)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(ClientsContacts);
        }

    }
}
