﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class Researches
    {

        public Framework.DTO.DTOWebContract List(string filter, Int16 ClientId)
        {
            string queryString = "select r.*, p.PatientName, q.Quiz from viewResearches r join viewPatients p on (r.PatientId = p.PatientId) join viewQuestionnaires q " +
                                    "on (q.QuestionnairesId = r.QuestionnairesId) where 1 = 1";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " and p.Filter like " + YouVita.Framework.Data.DBFormat.toFormat("%" + filter + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (ClientId > 1)
            {
                queryString += " and p.ClientId = " + ClientId.ToString();
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListResearches, DTO.Researches>(queryString);
        }

        public Framework.DTO.DTOWebContract List(string ResearchId)
        {
            string queryString = "select r.*, qu.Question, qu.QuestionsId, isnull(a.Answer, q.Answer) as  complemento ";
            queryString = queryString + "  from QuestionnaireResponses q left join Answers a on (q.AnswersId = a.AnswersId) ";
            queryString = queryString + " join Questions qu on (q.QuestionsId = qu.QuestionsId) ";
            queryString = queryString + " join Researches r on (q.ResearchesId = r.ResearchesId) ";
            queryString = queryString + " where q.ResearchesId = " + ResearchId;
            queryString = queryString + " order by q.questionsid ";
            
            var ret = Framework.DTO.DTOHelper.listDTO<YouVita.DTO.ListResearchResponses, YouVita.DTO.ResearchResponses>(queryString);
            return ret;           
        }

        public Framework.DTO.DTOWebContract List(string patientName, string CPF, string identificationOnClient)
        {
            string queryString = "select r.ResearchesId, r.CreateDate, p.CPF, p.PatientName, p.ClientId, p.IdentificationOnClient ";
            queryString = queryString + " from viewResearches r join viewPatients p on (r.PatientId = p.PatientId) ";
            queryString = queryString + " where 1 = 1 ";
            if (!string.IsNullOrEmpty(patientName))
            {
                queryString = queryString + " and p.patientName like "+
                    YouVita.Framework.Data.DBFormat.toFormat("%" + patientName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(CPF))
            {
                queryString = queryString + " and p.CPF = " +
                    YouVita.Framework.Data.DBFormat.toFormat(CPF, YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(identificationOnClient))
            {
                queryString = queryString + " and p.IdentificationOnClient like " +
                    YouVita.Framework.Data.DBFormat.toFormat("%" + identificationOnClient+"%", YouVita.Framework.Data.DBFormat.typeValueString);
            }
            var ret = Framework.DTO.DTOHelper.listDTO<YouVita.DTO.ListResearchReturnDTO, YouVita.DTO.ResearchReturnDTO>(queryString);
            return ret;
        }

        public Framework.DTO.DTOWebContract GetByPatient(string patientId)
        {
            string queryString = "select r.ResearchesId, r.CreateDate, p.CPF, p.PatientName, p.ClientId, p.IdentificationOnClient ";
            queryString = queryString + " from viewResearches r join viewPatients p on (r.PatientId = p.PatientId) ";
            queryString = queryString + " where p.patientId ="+ patientId;
            queryString = queryString + " order by r.CreateDate desc ";



            var ret = Framework.DTO.DTOHelper.listDTO<YouVita.DTO.ListResearchReturnDTO, YouVita.DTO.ResearchReturnDTO>(queryString);
            return ret;
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Researches researches)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(researches);
        }
    }
}
