﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class Doctors
    {

        public Framework.DTO.DTOWebContract List(string doctorName, string CPF, string CRM, string UF_CRM, string representativeID, string laboratoryID)
        {
            string queryString = "select vd.* from viewDoctors vd  ";
            queryString += " join viewRepresentatives r on(vd.representativeId = r.representativeId) where 1 = 1";          

            if (!string.IsNullOrEmpty(doctorName))
            {
                queryString += " and doctorName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + doctorName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(CPF))
            {
                queryString += " and CPF like " + YouVita.Framework.Data.DBFormat.toFormat("%" + CPF + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(CRM))
            {
                queryString += " and CRM like " + YouVita.Framework.Data.DBFormat.toFormat("%" + CRM + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(UF_CRM))
            {
                queryString += " and UF_CRM like " + YouVita.Framework.Data.DBFormat.toFormat("%" + UF_CRM + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (representativeID != "undefined" && representativeID != "0" && representativeID != "")
            {
                queryString += " and r.representativeID = " + representativeID;
            }

            if (laboratoryID != "undefined" && laboratoryID != "0" && laboratoryID != "")
            {
                queryString += " and r.laboratoryID = " + laboratoryID;
            }

            queryString += " order by DoctorName ";

            return Framework.DTO.DTOHelper.listDTO<DTO.ListDoctors, DTO.Doctors>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Doctors Doctor)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(Doctor);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Doctors Doctor)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(Doctor);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Doctors Doctor)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(Doctor);
        }

        public Framework.DTO.DTOWebContract GetDoctorByID(string doctorID)
        {           
            string sql = " select * from viewDoctors v ";
            sql += " where v.DoctorID = " + doctorID.ToString();
            return Framework.DTO.DTOHelper.listDTO<DTO.ListDoctors, DTO.Doctors>(sql);
        }

        public Framework.DTO.DTOWebContract ListLabID(string laboratoryID, string doctor)
        {
            string sql = " select v.* , l.LaboratoryFantasyName, r.RepresentativeName  "; //, s.stateUF ";
            sql += " from viewDoctors v  join viewRepresentatives r on (v.RepresentativeID = r.RepresentativeID) ";
            sql += " join viewLaboratories l on (r.laboratoryID = l.LaboratoryID) "; // join viewStates s on (v.StateID = s.StateId)
            sql += " where 1 = 1 ";            
            if (laboratoryID != "undfined"&& laboratoryID != null && laboratoryID !="0")
            {
                sql += " and r.laboratoryID = " + laboratoryID;
            }

            if (!String.IsNullOrEmpty(doctor))
            {
                sql += " and v.doctorName like '%" + doctor + "%'";
            }


            return Framework.DTO.DTOHelper.listDTO<DTO.ListDoctors, DTO.Doctors>(sql);
        }       


        public Framework.DTO.DTOWebContract GetDoctorByLogin(string login)
        {           
            string sql = " select * from viewDoctors v ";
            sql += " where v.login = " + YouVita.Framework.Data.DBFormat.toFormat(login, YouVita.Framework.Data.DBFormat.typeValueString);
            return Framework.DTO.DTOHelper.listDTO<DTO.ListDoctors, DTO.Doctors>(sql);
        }
    }
}
