﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class Clients
    {

        public Framework.DTO.DTOWebContract List(string clientName, string clientFantasyName, string clientCNPJ)
        {
            string queryString = "select * from viewClients where 1 = 1 ";

            if (!string.IsNullOrEmpty(clientName))
            {
                queryString += " And clientName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + clientName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(clientFantasyName))
            {
                queryString += " And clientFantasyName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + clientFantasyName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(clientCNPJ))
            {
                queryString += " And CNPJ like " + YouVita.Framework.Data.DBFormat.toFormat("%" + clientCNPJ + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListClients, DTO.Clients>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Clients Clients)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(Clients);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Clients Clients)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(Clients);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Clients Clients)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(Clients);
        }

    }
}
