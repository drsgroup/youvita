﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class PatientAddresses
    {

        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewPatientAddresses";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where Filter like " + YouVita.Framework.Data.DBFormat.toFormat("%" + filter + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListPatientAddresses, DTO.PatientAddresses>(queryString);
        }

        public Framework.DTO.DTOWebContract GetByIdPatient(string filter)
        {
            string queryString = "select * from viewPatientAddresses";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where PatientId = "+ filter;
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListPatientAddresses, DTO.PatientAddresses>(queryString);
        }

        

        public Framework.DTO.DTOWebContract Insert(DTO.PatientAddresses PatientAddresses)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(PatientAddresses);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.PatientAddresses PatientAddresses)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(PatientAddresses); 
        }

        public Framework.DTO.DTOWebContract Update(DTO.PatientAddresses PatientAddresses)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(PatientAddresses);
        }

    }
}
