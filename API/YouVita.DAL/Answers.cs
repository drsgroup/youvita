﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class Answers
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewAnswers";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where Filter like " + YouVita.Framework.Data.DBFormat.toFormat("%" + filter + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListAnswers, DTO.Answers>(queryString);
        }


        public Framework.DTO.DTOWebContract GetByIdAnswers(string filter)
        {
            string queryString = "select * from viewAnswers";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where QuestionsId = " + filter;
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListAnswers, DTO.Answers>(queryString);
        }
    }
}
