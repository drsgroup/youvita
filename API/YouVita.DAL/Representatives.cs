﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class Representatives
    {
        public Framework.DTO.DTOWebContract List(string representativeName, string CPF, string email, string laboratoryId)
        {
            string queryString = "select r.*, l.laboratoryName, l.laboratoryFantasyName from viewRepresentatives r ";
            queryString += " join viewLaboratories l on(r.laboratoryId = l.laboratoryId) ";

            if (!string.IsNullOrEmpty(representativeName))
            {
                queryString += " and RepresentativeName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + representativeName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(CPF))
            {
                queryString += " and CPF like " + YouVita.Framework.Data.DBFormat.toFormat("%" + CPF + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(email))
            {
                queryString += " and email like " + YouVita.Framework.Data.DBFormat.toFormat("%" + email + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (laboratoryId != "undefined" && laboratoryId != "0" && laboratoryId != "")
            {
                queryString += " and l.laboratoryID = " + laboratoryId;
            }
            queryString += " order by RepresentativeName ";

            return Framework.DTO.DTOHelper.listDTO<DTO.ListRepresentatives, DTO.Representatives>(queryString);
        }
        

        public Framework.DTO.DTOWebContract ListLabID(string laboratoryID)
        {
            string queryString = "select * from viewRepresentatives";
            if (laboratoryID != "undefined" && laboratoryID != "0")
            {
                queryString += " Where laboratoryID = " + laboratoryID;
            }
            queryString += " order by RepresentativeName ";


            return Framework.DTO.DTOHelper.listDTO<DTO.ListRepresentatives, DTO.Representatives>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Representatives Representatives)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(Representatives);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Representatives Representatives)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(Representatives);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Representatives Representatives)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(Representatives);
        }
    }
}
