﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class OrdersRepresentative
    {
        public Framework.DTO.DTOWebContract List(string representativeName, string cpf, string email, string laboratoryID)
        {
            string queryString = "  select o.*, d.RepresentativeName, d.cpf, d.email, l.LaboratoryID ";
            queryString += " from viewOrdersRepresentative o  join viewRepresentatives d on (o.RepresentativeID = d.RepresentativeID) ";
            queryString += "  join viewRepresentatives r on (d.RepresentativeID = r.RepresentativeID) ";
            queryString += "  join viewLaboratories l on (r.laboratoryID = l.LaboratoryID) ";
            queryString += " where 1 = 1 ";

            if (!string.IsNullOrEmpty(representativeName))
            {
                queryString += " and d.representativeName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + representativeName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(cpf))
            {
                queryString += " and d.cpf like " + YouVita.Framework.Data.DBFormat.toFormat("%" + cpf + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(email))
            {
                queryString += " and d.email like " + YouVita.Framework.Data.DBFormat.toFormat("%" + email + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (laboratoryID != "undefined" && laboratoryID != "0" && laboratoryID != "")
            {
                queryString += " and l.laboratoryID = " + laboratoryID;
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrdersRepresentative, DTO.OrdersRepresentative>(queryString);
        }

        public Framework.DTO.DTOWebContract ListByParams(string userLogin, string laboratoryID, string representativeLogin, string representativeName)
        {
            string queryString = "  select o.*, d.RepresentativeName, l.LaboratoryFantasyName ";
            queryString += " from viewOrdersRepresentative o  join viewRepresentatives d on (o.RepresentativeID = d.RepresentativeID) ";
            queryString += "  join viewRepresentatives r on (d.RepresentativeID = r.RepresentativeID) ";
            queryString += "  join viewLaboratories l on (r.laboratoryID = l.LaboratoryID) ";
            queryString += " where 1 = 1 ";

            if (!String.IsNullOrEmpty(laboratoryID) && laboratoryID != "0")
            {
                queryString += " and l.laboratoryID = " + laboratoryID;
            }

            if (!String.IsNullOrEmpty(representativeLogin))
            {
                queryString += " and r.login = " + YouVita.Framework.Data.DBFormat.toFormat(representativeLogin, YouVita.Framework.Data.DBFormat.typeValueString);           }

            if (!String.IsNullOrEmpty(representativeName))
            {
                queryString += " and d.RepresentativeName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + representativeName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }
            
            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrdersRepresentative, DTO.OrdersRepresentative>(queryString);
        }



        public Framework.DTO.DTOWebContract Insert(DTO.OrdersRepresentative OrdersRepresentative)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(OrdersRepresentative);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.OrdersRepresentative OrdersRepresentative)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(OrdersRepresentative);
        }

        public Framework.DTO.DTOWebContract Update(DTO.OrdersRepresentative OrdersRepresentative)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(OrdersRepresentative);
        }
    }
}
