﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class PharmaceuticalAttentions
    {

        public Framework.DTO.DTOWebContract List(string patientName, string CPF, string identificationOnClient)
        {
            string queryString = "select v.*, p.PatientName, p.CPF, p.IdentificationOnClient from viewPharmaceuticalAttentions v ";
            queryString = queryString + " join viewPatients p on (p.PatientId = p.PatientId) ";
            queryString = queryString + " where 1 = 1 ";

            if (!string.IsNullOrEmpty(patientName)){
                queryString = queryString + " and p.PatientName like " + YouVita.Framework.Data.DBFormat.toFormat(" % " + patientName + " % ", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(CPF))
            {
                queryString = queryString + " and p.CPF = " + YouVita.Framework.Data.DBFormat.toFormat(CPF, YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(identificationOnClient))
            {
                queryString = queryString + " and p.IdentificationOnClient = " + YouVita.Framework.Data.DBFormat.toFormat(identificationOnClient, YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListPharmaceuticalAttentions, DTO.PharmaceuticalAttentions>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.PharmaceuticalAttentions entity)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(entity);
        }

    }
}
