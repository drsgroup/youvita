﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class UserConfigurations
    {

        public Framework.DTO.DTOWebContract List(string userId)
        {
            string queryString = "select * from viewUserConfigurations";

            if (!string.IsNullOrEmpty(userId))
            {
                queryString += " Where UserId = " + userId;
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListUserConfigurations, DTO.UserConfigurations>(queryString);
        }


    }
}
