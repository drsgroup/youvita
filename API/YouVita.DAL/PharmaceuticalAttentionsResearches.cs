﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class PharmaceuticalAttentionsResearches
    {

        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewPharmaceuticalAttentionsResearches";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where PharmaceuticalAttentionId = " + filter;
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListPharmaceuticalAttentionsResearches, DTO.PharmaceuticalAttentionsResearches>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.PharmaceuticalAttentionsResearches entity)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(entity);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.PharmaceuticalAttentionsResearches entity)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(entity);
        }

    }
}
