﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class DoctorAddresses
    {
        public Framework.DTO.DTOWebContract List(string doctorID)
        {
            string queryString = "select v.*, s.StateName from viewDoctorAddresses v ";
            queryString += " join viewStates s on(v.StateId = s.StateId ) ";
            

            if (!string.IsNullOrEmpty(doctorID))
            {
                queryString += " Where DoctorId = " + doctorID;
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListDoctorAddresses, DTO.DoctorAddresses>(queryString);
        }

         public Framework.DTO.DTOWebContract GetByDoctorID(string filter)
        {
            string queryString = "select * from viewDoctorAddresses";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where DoctorID = " + filter;
            }          
            return Framework.DTO.DTOHelper.listDTO<DTO.ListDoctorAddresses, DTO.DoctorAddresses>(queryString);    
        }

        public Framework.DTO.DTOWebContract Insert(DTO.DoctorAddresses DoctorAddresses)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(DoctorAddresses);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.DoctorAddresses DoctorAddresses)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(DoctorAddresses);
        }

        public Framework.DTO.DTOWebContract Update(DTO.DoctorAddresses DoctorAddresses)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(DoctorAddresses);
        }
    }
}
