﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class InteractionMedicineDAL
    {
        public Framework.DTO.DTOWebContract Insert(DTO.MedicineInteractionPatient interaction)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(interaction);
        }

        public Framework.DTO.DTOWebContract Update(DTO.MedicineInteractionPatient interaction)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(interaction);
        }

        public Framework.DTO.DTOWebContract InsertItem(DTO.MedicineInteractionPatientItem item)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(item);
        }

        public void ExecInteractionMedicines(Int32 MedicineInteractionPatientItemID)
        {
            Framework.DTO.DTOHelper.ExecNonQuery("exec spExec_InteractionMedicines " + MedicineInteractionPatientItemID.ToString());
        }        

        public Framework.DTO.DTOWebContract ListByPatientId(Int32 patientId)
        {
            string queryString = " select m.*, p.PatientName from MedicineInteractionPatient m ";
            queryString = queryString + " join viewPatients p on (m.PatientID = p.PatientId) ";
            queryString = queryString + " where m.patientId = " + patientId.ToString();

            return Framework.DTO.DTOHelper.listDTO<DTO.ListMedicineInteractionPatient, DTO.MedicineInteractionPatient>(queryString);
        }

        public Framework.DTO.DTOWebContract List(Int32 patientId)
        {
            string queryString = " select m.*, p.PatientName from MedicineInteractionPatient m ";
            queryString = queryString + " join viewPatients p on (m.PatientID = p.PatientId) ";
            queryString = queryString + " where m.patientId = " + patientId.ToString();

            return Framework.DTO.DTOHelper.listDTO<DTO.ListMedicineInteractionPatient, DTO.MedicineInteractionPatient>(queryString);
        }

        public Framework.DTO.DTOWebContract List(string clientName, string patientName, DateTime? dtIni, DateTime? dtFim)
        {
            string queryString = "select m.*, p.PatientName,  c.clientName from MedicineInteractionPatient m  ";
            queryString += "  join viewPatients p on(m.PatientID = p.PatientId) join viewClients c on(p.ClientId = c.ClientId) ";
            queryString += "  where 1 = 1 ";
            if (!String.IsNullOrEmpty(clientName)) {
                queryString += " and c.ClientFantasyName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + clientName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }
            if (!String.IsNullOrEmpty(patientName)) {
                queryString = queryString + " and p.PatientName like  "+ YouVita.Framework.Data.DBFormat.toFormat("%" + patientName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }  
            
            if (dtIni != null)
            {
                DateTime dtIniok = dtIni ?? DateTime.Now;
                queryString = queryString + " and m.DateInteraction >= " +YouVita.Framework.Data.DBFormat.toFormat(dtIniok.ToString("yyyy-MM-dd"), YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (dtFim!= null)
            {
                DateTime dtFimOk = dtFim ?? DateTime.Now;
                queryString = queryString + " and m.DateInteraction <= " + YouVita.Framework.Data.DBFormat.toFormat(dtFimOk.ToString("yyyy-MM-dd 23:59:59"), YouVita.Framework.Data.DBFormat.typeValueString);
            }


            return Framework.DTO.DTOHelper.listDTO<DTO.ListMedicineInteractionPatient, DTO.MedicineInteractionPatient>(queryString);
        }


        public Framework.DTO.DTOWebContract ListResultById(Int32 InteractionMedicineID)
        {
            string queryString = "select m.*, md1.MedicineName as Medicine1, md2.MedicineName as Medicine2";
            queryString = queryString + " from MedicineInteractionPatientResults m ";
            queryString = queryString + " join Medicines md1 on (m.ean1 = md1.EAN) ";
            queryString = queryString + " join Medicines md2 on (m.ean2 = md2.EAN) ";
            queryString = queryString + " where m.MedicineInteractionPatientItemId =  " + InteractionMedicineID.ToString();
            return Framework.DTO.DTOHelper.listDTO<DTO.ListMedicineInteractionPatientResults, DTO.MedicineInteractionPatientResults>(queryString);
        }

        
         public Framework.DTO.DTOWebContract GetMedicinesById(Int32 InteractionMedicineID)
        {
            string queryString = "select i.*, m.MedicineName,  m.ActivePrincipleName ";
            queryString = queryString + " from MedicineInteractionPatientItem i  join Medicines m on (i.ean = m.EAN) ";
            queryString = queryString + " where i.InteractionMedicineId = " + InteractionMedicineID.ToString();
            return Framework.DTO.DTOHelper.listDTO<DTO.ListMedicineInteractionPatientItems, DTO.MedicineInteractionPatientItem>(queryString);
        }

    }
}
