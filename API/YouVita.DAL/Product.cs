﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class Product
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewProduct";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where Filter like " + YouVita.Framework.Data.DBFormat.toFormat("%" + filter + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrdersDoctor, DTO.OrdersDoctor>(queryString);
        }

        public Framework.DTO.DTOWebContract List(string filter, Int64 laboratoryID)
        {
            string queryString = "select * from viewProduct";
            //queryString += " where laboratoryID = " + laboratoryID.ToString();
            queryString += " order by productName ";

            return Framework.DTO.DTOHelper.listDTO<DTO.ListProducts, DTO.Product>(queryString);
        }

        public Framework.DTO.DTOWebContract GetProductCode(Int64 laboratoryID, Int64 productID)
        {
            string queryString = "select * from viewProduct";
            queryString += " where 1 = 1 ";
            //laboratoryID = " + laboratoryID.ToString();
            queryString += " and productID = " + productID.ToString();
            queryString += " order by productName ";
            return Framework.DTO.DTOHelper.listDTO<DTO.ListProducts, DTO.Product>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Product product)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(product);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Product product)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(product);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Product product)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(product);
        }
    }
}
