﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class PrescriptionsItems
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select r.*,m.medicineName,m.LabId,m.LabName,m.EAN from viewPrescriptionsItems r ";
            queryString += " join viewMedicines m on (m.MedicineId = r.MedicineId) ";

            //retorna todos os itens que forem do Id da Receita
            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where PrescriptionItemId = " + YouVita.Framework.Data.DBFormat.toFormat(filter, YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListPrescriptionItems, DTO.PrescriptionsItems>(queryString);
        }

        public Framework.DTO.DTOWebContract ListByPrescription(string filter)
        {
            string queryString = "select r.*,m.medicineName,m.LabId,m.LabName,m.EAN, m.Presentation from viewPrescriptionsItems r ";
            queryString += " join viewMedicines m on (m.MedicineId = r.MedicineId) ";

            //retorna todos os itens que forem do Id da Receita
            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where PrescriptionId = " + YouVita.Framework.Data.DBFormat.toFormat(filter, YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListPrescriptionItems, DTO.PrescriptionsItems>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.PrescriptionsItems prescriptionItem)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(prescriptionItem);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.PrescriptionsItems prescriptionItem)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(prescriptionItem);
        }

        public Framework.DTO.DTOWebContract Update(DTO.PrescriptionsItems prescriptionItem)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(prescriptionItem);
        }
    }
}
