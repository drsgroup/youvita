﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class Users
    {

        public Framework.DTO.DTOWebContract GetUserByLogin(DTO.Users Users)
        {
            string queryString = "select * from viewUsers ";
            queryString += "where userLoginName = " + YouVita.Framework.Data.DBFormat.toFormat(Users.userLoginName, YouVita.Framework.Data.DBFormat.typeValueString);
            queryString += " and userPassword = " + YouVita.Framework.Data.DBFormat.toFormat(Users.userPassword, YouVita.Framework.Data.DBFormat.typeValueString);

            return Framework.DTO.DTOHelper.listDTO<DTO.ListUsers, DTO.Users>(queryString);
        }


        public Framework.DTO.DTOWebContract GetUsersLabs(string userName, string userLogin, string LaboratoryID)
        {
            string queryString = "select u.*, l.LaboratoryFantasyName, l.LaboratoryID from viewUsers u join viewLaboratories l on (u.laboratoryID = l.LaboratoryID) where 1 = 1";

            if (!string.IsNullOrEmpty(userName))
            {
                queryString += " and u.userName like  " + YouVita.Framework.Data.DBFormat.toFormat("%" + userName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(userLogin))
            {
                queryString += " and u.userLoginName like  " + YouVita.Framework.Data.DBFormat.toFormat("%" + userLogin + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (LaboratoryID != "undefined" && LaboratoryID != "0" && LaboratoryID != "")
            {
                queryString += " and l.laboratoryID = " + LaboratoryID;
            }

            queryString += " and u.laboratoryID is not null and UserType = 3 ";           
            return Framework.DTO.DTOHelper.listDTO<DTO.ListUsers, DTO.Users>(queryString);
        }

        public Framework.DTO.DTOWebContract GetUserByLogin(string login)
        {
            string queryString = "select * from Users "; //nao pode usar login repetido, mesmo q excluido
            queryString += "where userLoginName = " + YouVita.Framework.Data.DBFormat.toFormat(login, YouVita.Framework.Data.DBFormat.typeValueString);            

            return Framework.DTO.DTOHelper.listDTO<DTO.ListUsers, DTO.Users>(queryString);
        }

        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewUsers";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where Filter like " + YouVita.Framework.Data.DBFormat.toFormat("%" + filter + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListUsers, DTO.Users>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Users Users)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(Users);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Users Users)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(Users);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Users Users)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(Users);
        }

    }
}
