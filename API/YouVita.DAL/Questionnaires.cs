﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class Questionnaires
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewQuestionnaires";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where Filter like " + YouVita.Framework.Data.DBFormat.toFormat("%" + filter + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListQuestionnaires, DTO.Questionnaires>(queryString);
        }
    }
}
