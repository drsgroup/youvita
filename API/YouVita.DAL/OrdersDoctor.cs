﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;
using YouVita.Framework.Data;
using System.Data;

namespace YouVita.DAL
{
    public class OrdersDoctor
    {
        public Framework.DTO.DTOWebContract List(string doctorName, string CPF, string CRM, string UF_CRM, string laboratoryID, string representativeID)
        {
            string queryString = "select o.*, d.DoctorName, d.CPF, d.CRM, d.UF_CRM, l.laboratoryID , r.representativeID  from viewOrdersDoctor o ";
            queryString += "  join viewdoctors d on (o.DoctorID = d.DoctorID)";
            queryString += "  join viewRepresentatives r on (d.RepresentativeID = r.RepresentativeID) ";
            queryString += "  join viewLaboratories l on (r.laboratoryID = l.LaboratoryID) ";
            queryString += " where 1 = 1 ";

            if (!string.IsNullOrEmpty(doctorName))
            {
                queryString += " and doctorName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + doctorName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(CPF))
            {
                queryString += " and CPF like " + YouVita.Framework.Data.DBFormat.toFormat("%" + CPF + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(CRM))
            {
                queryString += " and CRM like " + YouVita.Framework.Data.DBFormat.toFormat("%" + CRM + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(UF_CRM))
            {
                queryString += " and UF_CRM like " + YouVita.Framework.Data.DBFormat.toFormat("%" + UF_CRM + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (laboratoryID != "undefined" && laboratoryID != "0" && laboratoryID != "")
            {
                queryString += " and laboratoryID = " + laboratoryID;
            }

            if (representativeID != "undefined" && representativeID != "0" && representativeID != "")
            {
                queryString += " and representativeID = " + representativeID;
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrdersDoctor, DTO.OrdersDoctor>(queryString);
        }

        public Framework.DTO.DTOWebContract ListByParams(string userLogin, string laboratoryID, string representativeLogin, string doctorName, string doctorLogin)
        {
            string queryString = "  select o.*, d.DoctorName, r.RepresentativeName, l.LaboratoryFantasyName, oa.projectId ";
            queryString += " from viewOrdersDoctor o  join viewDoctors d on (o.DoctorID = d.DoctorID) ";
            queryString += "  join viewRepresentatives r on (d.RepresentativeID = r.RepresentativeID) ";
            queryString += "  join viewLaboratories l on (r.laboratoryID = l.LaboratoryID) ";
            queryString += "  join viewOrderAvaliable oa on(o.OrderAvaliableID = oa.OrderAvaliableID) ";
            queryString += " where 1 = 1 ";

            //if (!String.IsNullOrEmpty(laboratoryID)&& laboratoryID!= "0")
            //{
            //    queryString += " and l.laboratoryID = " + laboratoryID;
            //}

            if (!String.IsNullOrEmpty(representativeLogin))
            {
                queryString += " and r.login = " + YouVita.Framework.Data.DBFormat.toFormat(representativeLogin, YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!String.IsNullOrEmpty(doctorLogin))
            {
                queryString += " and d.login = " + YouVita.Framework.Data.DBFormat.toFormat(doctorLogin, YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!String.IsNullOrEmpty(doctorName))
            {
                queryString += " and d.DoctorName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + doctorName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            var ret = Framework.DTO.DTOHelper.listDTO<DTO.ListOrdersDoctor, DTO.OrdersDoctor>(queryString);
            return ret;
        }

        public string GetCNPJ(Int32 orderDoctorId)
        {
            string sql = @" SELECT distinct l.CNPJ
                          FROM OrdersDoctor O
                          join viewDoctors vd on(o.DoctorID = vd.DoctorID)
                          JOIN viewRepresentatives r on(vd.RepresentativeID = r.RepresentativeID)
                          join viewLaboratories l on(r.laboratoryID = l.LaboratoryID)
                        WHERE O.OrderDoctorID = " + orderDoctorId.ToString();

            var ret = Framework.DTO.DTOHelper.ExecQuery(sql);

            if (ret.Rows.Count > 0)
            {                
                return ret.Rows[0]["CNPJ"].ToString();
            }
            else
            {
                return null;
            }            
        }

        public Framework.DTO.DTOWebContract Insert(DTO.OrdersDoctor OrderDoctor)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(OrderDoctor);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.OrdersDoctor OrderDoctor)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(OrderDoctor);
        }

        public Framework.DTO.DTOWebContract Update(DTO.OrdersDoctor OrderDoctor)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(OrderDoctor);
        }

        public void Resend(Int32 orderDoctorId)
        {
            string sql = " update OrdersDoctor set status = " + YouVita.Framework.Data.DBFormat.toFormat("A", YouVita.Framework.Data.DBFormat.typeValueString);
            sql += " where OrderDoctorID = " + orderDoctorId.ToString();
            Framework.DTO.DTOHelper.ExecNonQuery(sql); 
        }




        public DataTable GetListOrderDoctor()
        {
            string queryString = String.Empty;
            //   queryString = @" select l.CNPJ, v.CPF, v.DoctorName, dadd.ZipCode, dadd.Address, dadd.AddressNumber as Number, dadd.Neighborhood, 
            //                     dadd.City, p.productCode, po.Qty, vo.OrderDoctorID, pp.ProjectStockType, VO.OrderDoctorDateSend, vo.OrderAvaliableID
            //                     from OrdersDoctor vo
            //                     join viewProductsOrderDoctor po on (vo.OrderDoctorID = po.OrdersDoctorID)
            //                     join viewProduct p on (p.productID = po.ProductID)
            //                     join viewDoctors v on (v.DoctorID = vo.DoctorID)
            //                     join viewRepresentatives r on (v.RepresentativeID = r.RepresentativeID)
            //                     join viewLaboratories l on (r.laboratoryID = l.LaboratoryID)
            //join viewOrderAvaliable va on (va.OrderAvaliableID = vo.OrderAvaliableID)
            //join viewProject pp on (pp.ProjectId = va.projectId)
            //join DoctorAddresses dadd on (dadd.DoctorAddressesId = vo.doctorAddessesId)
            //                   where vo.Status = 'A'  
            //                      and vo.OrderDoctorEnabled = 1							   
            //                     and vo.OrderDoctorDateSend < GETDATE()+5 
            //                   order by vo.OrderDoctorID ";

            queryString = @"select l.CNPJ, v.CPF, v.DoctorName, dadd.ZipCode, dadd.Address, dadd.AddressNumber as Number, dadd.Neighborhood, 
                              dadd.City, p.productCode, po.Qty, vo.OrderDoctorID, pp.ProjectStockType, VO.OrderDoctorDateSend, vo.OrderAvaliableID, vo.doctorAddessesId,
							  'PD' as tipo
                              from OrdersDoctor vo
                              join viewProductsOrderDoctor po on (vo.OrderDoctorID = po.OrdersDoctorID)
                              join viewProduct p on (p.productID = po.ProductID)
                              join viewDoctors v on (v.DoctorID = vo.DoctorID)
                              join viewRepresentatives r on (v.RepresentativeID = r.RepresentativeID)
                              join viewLaboratories l on (r.laboratoryID = l.LaboratoryID)
							  join viewOrderAvaliable va on (va.OrderAvaliableID = vo.OrderAvaliableID)
							  join viewProject pp on (pp.ProjectId = va.projectId)
							  join DoctorAddresses dadd on (dadd.DoctorAddressesId = vo.doctorAddessesId)
                            where vo.Status = 'A'  
                               and vo.OrderDoctorEnabled = 1							   
                              and vo.OrderDoctorDateSend < GETDATE()+5 
                              union all
                            select l.CNPJ, r.cpf, r.RepresentativeName, r.zipcode, r.address, r.addressNumber, r.Neighborhood, r.city, p.productCode,
                                   pr.Qty, v.OrdersRepresentativeID, 
	                               isnull((select top 1 p.ProjectStockType
                                 from viewDoctors d
	                             join viewProjectDoctor pd on (d.DoctorID = pd.DoctorId)
	                             join viewProject p on (p.ProjectId = pd.ProjectId)
	                            where d.RepresentativeID = v.RepresentativeID),
                                (select top 1 vwp.ProjectStockType
								  from viewProduct vp
								  join viewProjectProduct vpp on (vp.productId = vpp.productId)
								  join viewProject vwp on (vpp.ProjectId = vwp.ProjectId)
								 where vp.laboratoryID = r.laboratoryId
								   and vp.productID = pr.ProductID)) as ProjectStockType,	   
	                               v.OrderRepresentativeDateSend, v.OrderRepresentativeAvaliableID, 0,
	                               'PR' as tipo
                              from OrdersRepresentative v
                              join viewProductsOrderRepresentative pr on (v.OrdersRepresentativeID = pr.OrdersRepresentativeID)
                              join viewProduct p on (pr.ProductID = p.productID)
                              join viewRepresentatives r on (v.RepresentativeID = r.RepresentativeID)
                              join viewLaboratories l on (r.laboratoryID = l.LaboratoryID)
                              join viewOrderAvaliableRepresentative va on (v.OrderRepresentativeAvaliableID = va.OrderAvaliableRepresentativeID)
                             where v.OrderRepresentativeEnabled = 1
                               and v.Status = 'A'
                               and v.OrderRepresentativeDateSend < getdate()+5
                               order by tipo, OrderAvaliableID
                    ";
            return Framework.DTO.DTOHelper.ExecQuery(queryString);
        }

        public DataTable GetListOrderDoctorForTracking()
        {
            string queryString = String.Empty;
            queryString = @" select l.CNPJ, v.CPF, v.DoctorName, v.ZipCode, v.Address, v.Number, v.Neighborhood, v.City, p.productCode, po.Qty, vo.OrderDoctorID 
                              from viewOrdersDoctor vo
                              join viewProductsOrderDoctor po on (vo.OrderDoctorID = po.OrdersDoctorID)
                              join viewProduct p on (p.productID = po.ProductID)
                              join viewDoctors v on (v.DoctorID = vo.DoctorID)
                              join viewRepresentatives r on (v.RepresentativeID = r.RepresentativeID)
                              join viewLaboratories l on (r.laboratoryID = l.LaboratoryID)
                            where vo.Status in ('E') 
                              AND vo.OrderDoctorEnabled = 1
                            order by vo.OrderDoctorID ";

            return Framework.DTO.DTOHelper.ExecQuery(queryString);
        }

        public void UpdateStatus(Int32 orderDoctorId, string status, string tipo)
        {
            string queryString = String.Empty;

            if (tipo == "PD")
            {
                queryString = " Update OrdersDoctor set Status = " + YouVita.Framework.Data.DBFormat.toFormat(status, YouVita.Framework.Data.DBFormat.typeValueString);
                queryString += " where OrderDoctorId = " + orderDoctorId.ToString();
            }
            else //representante
            {
                queryString = " Update OrdersRepresentative set Status = " + YouVita.Framework.Data.DBFormat.toFormat(status, YouVita.Framework.Data.DBFormat.typeValueString);
                queryString += " where OrdersRepresentativeID = " + orderDoctorId.ToString();
            }

            Framework.DTO.DTOHelper.ExecNonQuery(queryString);
        }

        
    }
}
