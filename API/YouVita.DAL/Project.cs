﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class Project
    {
        public Framework.DTO.DTOWebContract List(int projectId)
        {
            string queryString = "select * from viewProject p ";
            queryString += " where ProjectId = " + projectId.ToString();
            return Framework.DTO.DTOHelper.listDTO<DTO.ListProject, DTO.Project>(queryString);
        }

        public Int32 GetExistsStock(string stock, int projectId)
        {
            string queryString;
            Int32 qtd = 0;
            var lstStock = stock.Split(',');
            if (lstStock.Length > 0)
            {
                for (var i = 0; i < lstStock.Length; i++)
                {
                    queryString = " select count(*) as qtd from viewProject p where p.projectId <> "+ projectId .ToString()+ " and p.ProjectStockType like " + YouVita.Framework.Data.DBFormat.toFormat("%" + lstStock[i] + "%", YouVita.Framework.Data.DBFormat.typeValueString);
                    var retorno = Framework.DTO.DTOHelper.ExecQuery(queryString);
                    if (Convert.ToInt32(retorno.Rows[0]["qtd"].ToString()) > 0)
                    {
                        return 1;
                    }
                }
            }
            else
            {
                queryString = " select count(*) as qtd from viewProject p where p.ProjectStockType like " + YouVita.Framework.Data.DBFormat.toFormat("%" + lstStock[0] + "%", YouVita.Framework.Data.DBFormat.typeValueString);
                var retorno = Framework.DTO.DTOHelper.ExecQuery(queryString);
                if (Convert.ToInt32(retorno.Rows[0]["qtd"].ToString()) > 0)
                {
                    return 1;
                }
            }
            return qtd;
        }

        public Framework.DTO.DTOWebContract ListFilter(string filter)
        {
            string queryString = "select * from viewProject p ";
            if (!String.IsNullOrEmpty(filter))
            {
                queryString += " where ProjectName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + filter + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }
            queryString += " order by ProjectName ";

            return Framework.DTO.DTOHelper.listDTO<DTO.ListProject, DTO.Project>(queryString);
        }

        public Framework.DTO.DTOWebContract ListLabId(string laboratoryId)
        {
            string queryString = "select * from viewProject p ";
            if (laboratoryId != "0")
            {
                queryString += " where laboratoryId = " + laboratoryId;
            }
            queryString += " order by ProjectName ";

            return Framework.DTO.DTOHelper.listDTO<DTO.ListProject, DTO.Project>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Project project)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(project);            
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Project project)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(project);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Project project)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(project);
        }


    }
}
