﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class Orders
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewOrders";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where Filter like " + DBFormat.toFormat("%" + filter + "%", DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrders, DTO.Orders>(queryString);
        }


        public Framework.DTO.DTOWebContract ListByPatientId(string patientId)
        {
            string queryString = "select * from viewOrders";

            if (!string.IsNullOrEmpty(patientId))
            {
                queryString += " Where PatientId = " + patientId;
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrders, DTO.Orders>(queryString);
        }


        public Framework.DTO.DTOWebContract Insert(DTO.Orders Orders)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(Orders);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Orders Orders)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(Orders);
        }

    }
}
