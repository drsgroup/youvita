﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class Medicines
    {
        public Framework.DTO.DTOWebContract List(string EAN, string MedicineName, string LabName, string ActivePrincipleName, string Presentation)
        {
            string queryString = "select *, 15 as QtyStock from viewMedicines Where 1=1 ";

            if (!string.IsNullOrEmpty(EAN))
            {
                queryString += " And EAN like " + YouVita.Framework.Data.DBFormat.toFormat("%" + EAN + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(MedicineName))
            {
                queryString += " And MedicineName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + MedicineName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(LabName))
            {
                queryString += " And LabName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + LabName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(ActivePrincipleName))
            {
                queryString += " And ActivePrincipleName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + ActivePrincipleName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(Presentation))
            {
                queryString += " And Presentation like " + YouVita.Framework.Data.DBFormat.toFormat("%" + Presentation + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListMedicines, DTO.Medicines>(queryString);
        }

        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select *, 15 as QtyStock from viewMedicines";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where Filter like " + YouVita.Framework.Data.DBFormat.toFormat("%" + filter + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListMedicines, DTO.Medicines>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Medicines Medicines)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(Medicines);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Medicines Medicines)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(Medicines);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Medicines Medicines)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(Medicines);
        }
    }
}
