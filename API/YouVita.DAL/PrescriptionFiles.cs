﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class PrescriptionFiles
    {

        public Framework.DTO.DTOWebContract List(string PrescriptionId)
        {
            string queryString = "select * from viewPrescriptionFiles where PrescriptionId = " + PrescriptionId;

            return Framework.DTO.DTOHelper.listDTO<DTO.ListPrescriptionFiles, DTO.PrescriptionFiles>(queryString);
        }

    }
}
