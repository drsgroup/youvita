﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class ReportOrderDoctor
    {
        public Framework.DTO.DTOWebContract List(string LaboratoryId, string RepresentativeId, string DoctorId, string dtStart, string dtEnd, string type)
        {
            string queryString = @" select prod.productCode as sku,
                                   prod.ProductName,
	                               po.Qty,
	                               vo.OrderDoctorDateSend,
	                               rep.RepresentativeName,
	                               doc.DoctorName,
	                               adr.Address,
	                               adr.AddressNumber,
	                               adr.City,
	                               adr.Neighborhood,
	                               adr.ZipCode,
	                               adr.Complement,
	                               s.StateName,
	                               vo.Status,
	                               rep.LaboratoryId,
	                               rep.RepresentativeId,
	                               doc.doctorId,
                                   vo.OrderDoctorID,
                                   lab.LaboratoryFantasyName,
                                   'Medico' as Type
                              from viewOrdersDoctor vo
                              join viewProductsOrderDoctor po on (vo.OrderDoctorID = po.OrdersDoctorID)
                              join viewDoctors doc on (vo.DoctorId = doc.DoctorId)
                              join viewRepresentatives rep on (doc.RepresentativeID = rep.RepresentativeID)
                              join viewProduct prod on (po.ProductID = prod.ProductID)
                              join viewDoctorAddresses adr on (vo.doctorAddessesId = adr.DoctorAddressesId)
                              join viewStates s on (s.StateId = adr.StateId) 
                              join viewLaboratories lab on (lab.LaboratoryId = rep.laboratoryId)
                             where 1 = 1 ";
            if (!type.Contains("1") && !type.Contains("0"))
            {
                queryString += " and 1 = 2 "; //nao possui o tipo médico                
            }
            if (!String.IsNullOrEmpty(LaboratoryId) && LaboratoryId !="0"){
                queryString += " and rep.LaboratoryId =  " + LaboratoryId;
            }

            if (!String.IsNullOrEmpty(RepresentativeId) && RepresentativeId != "0")
            {
                queryString += " and rep.RepresentativeId =  " + RepresentativeId;
            }

            if (!String.IsNullOrEmpty(DoctorId) && DoctorId != "0")
            {
                queryString += " and doc.doctorId =  " + DoctorId;
            }
           

            if (!String.IsNullOrEmpty(dtStart))
            {
                queryString += " and vo.OrderDoctorDateSend >= "+ YouVita.Framework.Data.DBFormat.toFormat(dtStart, YouVita.Framework.Data.DBFormat.typeValueString) ;
            }

            if (!String.IsNullOrEmpty(dtEnd))
            {
                queryString += " and vo.OrderDoctorDateSend <= " + YouVita.Framework.Data.DBFormat.toFormat(dtEnd, YouVita.Framework.Data.DBFormat.typeValueString);
            }
            queryString += " union all ";
            queryString += @" select prod.productCode as sku,
                                   prod.ProductName,
	                               po.Qty,
	                               vo.OrderRepresentativeDateSend,
	                               rep.RepresentativeName,
	                               '' as DoctorName,
	                               rep.address,
	                               rep.addressNumber,
	                               rep.City,
	                               rep.Neighborhood,
	                               rep.ZipCode,
	                               isnull(rep.Complement, ''),
	                               s.StateName,
	                               vo.Status,
	                               rep.LaboratoryId,
	                               rep.RepresentativeId,
	                               rep.RepresentativeId,
                                   vo.OrdersRepresentativeID,
                                   lab.LaboratoryFantasyName,
                                   'Representante' as Type
                              from viewOrdersRepresentative vo
                              join viewProductsOrderRepresentative po on(vo.OrdersRepresentativeID = po.OrdersRepresentativeID)
                              join viewRepresentatives rep on(vo.RepresentativeID = rep.RepresentativeID)
                              join viewProduct prod on(po.ProductID = prod.ProductID)
                              join viewStates s on(s.StateId = rep.stateID)
                              join viewLaboratories lab on(lab.LaboratoryId = rep.laboratoryId)
                             where 1 = 1";

            if (!String.IsNullOrEmpty(LaboratoryId) && LaboratoryId != "0")
            {
                queryString += " and rep.LaboratoryId =  " + LaboratoryId;
            }

            if (!String.IsNullOrEmpty(RepresentativeId) && RepresentativeId != "0")
            {
                queryString += " and rep.RepresentativeId =  " + RepresentativeId;
            }

            if (!String.IsNullOrEmpty(DoctorId) && DoctorId != "0" && !type.Contains("2") && !type.Contains("0"))
            {
                queryString += " and rep.RepresentativeId = -1 "; //nao buscar representante qdo for selecionado um médico.
            }


            if (!String.IsNullOrEmpty(dtStart))
            {
                queryString += " and vo.OrderRepresentativeDateSend >= " + YouVita.Framework.Data.DBFormat.toFormat(dtStart, YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!String.IsNullOrEmpty(dtEnd))
            {
                queryString += " and vo.OrderRepresentativeDateSend <= " + YouVita.Framework.Data.DBFormat.toFormat(dtEnd, YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!type.Contains("2") && !type.Contains("0"))
            {
                queryString += " and 1 = 2 "; //nao possui o tipo representante
            }

            

            var xpto = Framework.DTO.DTOHelper.listDTO<DTO.ListReportOrderDoctor, DTO.ReportOrderDoctor>(queryString);

            return xpto;

            //select prod.productCode as sku,
            //                       prod.ProductName,
	           //                    po.Qty,
	           //                    vo.OrderRepresentativeDateSend,
	           //                    rep.RepresentativeName,
	           //                    '' as DoctorName,
	           //                    rep.address,
	           //                    rep.addressNumber,
	           //                    rep.City,
	           //                    rep.Neighborhood,
	           //                    rep.ZipCode,
	           //                    rep.Complement,
	           //                    s.StateName,
	           //                    vo.Status,
	           //                    rep.LaboratoryId,
	           //                    rep.RepresentativeId,
	           //                    rep.RepresentativeId,
            //                       vo.OrdersRepresentativeID,
            //                       lab.LaboratoryFantasyName
            //                  from viewOrdersRepresentative vo
            //                  join viewProductsOrderRepresentative po on(vo.OrdersRepresentativeID = po.OrdersRepresentativeID)
            //                  join viewRepresentatives rep on(vo.RepresentativeID = rep.RepresentativeID)
            //                  join viewProduct prod on(po.ProductID = prod.ProductID)
            //                  join viewStates s on(s.StateId = rep.stateID)
            //                  join viewLaboratories lab on(lab.LaboratoryId = rep.laboratoryId)
            //                 where 1 = 1

            //return Framework.DTO.DTOHelper.listDTO<DTO.ListOrdersDoctor, DTO.ReportOrderDoctor>(queryString);
        }
    }
}
