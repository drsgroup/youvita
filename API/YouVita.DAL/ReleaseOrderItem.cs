﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class ReleaseOrderItem
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {

            string queryString = "select r.*,m.medicineName, m.EAN, m.LabName, m.Presentation, s.StateUF from viewReleaseOrderItem r ";
            queryString += "join viewMedicines m on(m.MedicineId = r.MedicineId) ";
            queryString +=  "join viewStates s on(r.StateId = s.StateId)";

            //retorna todos os itens que forem do Id do Pedido
            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where ReleaseOrderId = " + YouVita.Framework.Data.DBFormat.toFormat(filter, YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListReleaseOrderItem, DTO.ReleaseOrderItem>(queryString);
        }
        
        public Framework.DTO.DTOWebContract ListItemsByPatientId(string patientId)
        {
            string queryString = "select i.*, m.*, vs.StateUF from viewReleaseOrder o ";
            queryString += " join viewReleaseOrderItem i on (o.ReleaseOrderId = i.ReleaseOrderId) ";
            queryString += " join viewMedicines m on (i.MedicineId = m.MedicineId) ";
            queryString += " join viewStates vs on (vs.StateId = i.StateId) ";
            queryString += " where o.PatientId = " + patientId;
            queryString += " and i.ReleaseOrderItemEnabled  = 1 ";
            queryString += " order by o.ReleaseOrderId, m.MedicineName ";
            //retorna todos os itens que forem do Id do Pedido
            return Framework.DTO.DTOHelper.listDTO<DTO.ListReleaseOrderItem, DTO.ReleaseOrderItem>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.ReleaseOrderItem ReleaseOrderItem)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(ReleaseOrderItem);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.ReleaseOrderItem ReleaseOrderItem)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(ReleaseOrderItem);
        }
    }
}
