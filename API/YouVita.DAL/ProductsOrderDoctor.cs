﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class ProductsOrderDoctor
    {
        public Framework.DTO.DTOWebContract ListByID(string OrdersDoctorID)
        {
            string queryString = "select p.*, pr.productName from viewProductsOrderDoctor p ";
            queryString += " join viewProduct pr on (p.ProductID = pr.productID) ";
            queryString += " Where p.OrdersDoctorID = " + OrdersDoctorID;           

            return Framework.DTO.DTOHelper.listDTO<DTO.ListProductsOrderDoctor, DTO.ProductsOrderDoctor>(queryString);
        }

        public string GetProductCode(string productID)
        {
            string queryString = "select productCode from viewProduct p ";
            queryString += " Where p.productID = " + productID;

            var ret = Framework.DTO.DTOHelper.ExecQuery(queryString);
            if (ret.Rows.Count > 0){
                return ret.Rows[0]["productCode"].ToString();
            }else{
                return "0000";
            }                
        }
        

        public Framework.DTO.DTOWebContract Insert(DTO.ProductsOrderDoctor ProductsOrderDoctor)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(ProductsOrderDoctor);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.ProductsOrderDoctor OrderDoctor)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(OrderDoctor);
        }

        public Framework.DTO.DTOWebContract Update(DTO.OrdersDoctor OrderDoctor)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(OrderDoctor);
        }
    }
}
