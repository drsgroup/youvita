﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class PatientAdverseEffect
    {

        public Framework.DTO.DTOWebContract Insert(DTO.PatientAdverseEffect effect)
        {
            effect.userLogin = "ok";
            return Framework.DTO.DTOHelper.insertDTODataBase(effect);
        }

        public Framework.DTO.DTOWebContract List(string patientId)
        {
            string queryString = "select e.* , a.AdverseEffectsDescription from PatientAdverseEffect e join AdverseEffects a on (e.AdverseEffectsId = a.AdverseEffectsId) ";
            queryString = queryString + " where PatienteId = " + patientId;


            return Framework.DTO.DTOHelper.listDTO<DTO.ListPatientAdverseEffect, DTO.PatientAdverseEffect>(queryString);
        }



    }
}
