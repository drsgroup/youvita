﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class ReportPatientsByClient
    {

        public Framework.DTO.DTOWebContract List(string clientId)
        {
            string queryString = "select * from viewReportPatientByClient";

            if (!string.IsNullOrEmpty(clientId))
            {
                queryString += " Where ClientId = " + clientId;
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListReportPatientsByClient, DTO.ReportPatientsByClient>(queryString);
        }


    }
}
