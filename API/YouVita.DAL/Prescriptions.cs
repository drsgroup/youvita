﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class Prescriptions
    {
        public Framework.DTO.DTOWebContract ListByFilter(DateTime? prescriptionDate, DateTime? expirationDate, string patientName, string CRM, string UfCRM, string DoctorName)
        {
            string queryString = "select pr.*, p.PatientName, s.StateUF from viewPrescriptions pr ";
            queryString += " join viewPatients p on (pr.PatientId = p.PatientId) ";
            queryString += "left join viewStates s on (pr.ufCrm = s.StateId) ";
            queryString += " Where 1 = 1";

            if (prescriptionDate != DateTime.MinValue && prescriptionDate != null)
            {
                queryString += " and pr.prescriptionDate = " + YouVita.Framework.Data.DBFormat.toFormat(prescriptionDate.ToString(), YouVita.Framework.Data.DBFormat.typeValueDatetime);
            }

            if (expirationDate != DateTime.MinValue && expirationDate != null)
            {
                queryString += " and pr.prescriptionDate = " + YouVita.Framework.Data.DBFormat.toFormat(expirationDate.ToString(), YouVita.Framework.Data.DBFormat.typeValueDatetime);
            }

            if (!string.IsNullOrEmpty(patientName))
            {
                queryString += " and p.PatientName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + patientName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(CRM))
            {
                queryString += " and pr.CRM like " + YouVita.Framework.Data.DBFormat.toFormat("%" + CRM + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(UfCRM))
            {
                queryString += " and pr.UfCRM like " + YouVita.Framework.Data.DBFormat.toFormat("%" + UfCRM + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(DoctorName))
            {
                queryString += " and pr.DoctorName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + DoctorName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListPrescriptions, DTO.Prescriptions>(queryString);
        }

        public Framework.DTO.DTOWebContract ListByPrescription(string PrescriptionId)
        {
            string queryString = "select pr.*, p.PatientName, s.StateUF from viewPrescriptions pr ";
            queryString += " join viewPatients p on (pr.PatientId = p.PatientId) ";
            queryString += "left join viewStates s on (pr.ufCrm = s.StateId) ";
            queryString += " Where PrescriptionId = " + YouVita.Framework.Data.DBFormat.toFormat(PrescriptionId, YouVita.Framework.Data.DBFormat.typeValueString);


            return Framework.DTO.DTOHelper.listDTO<DTO.ListPrescriptions, DTO.Prescriptions>(queryString);
        }

        public Framework.DTO.DTOWebContract ListByPrescriptionPatientId(string patientId)
        {
            string queryString = "select pr.*, p.PatientName, s.StateUF from viewPrescriptions pr ";
            queryString += " join viewPatients p on (pr.PatientId = p.PatientId) ";
            queryString += "left join viewStates s on (pr.ufCrm = s.StateId) ";
            queryString += " Where p.patientId = " + patientId;

            return Framework.DTO.DTOHelper.listDTO<DTO.ListPrescriptions, DTO.Prescriptions>(queryString);
        }

        

        public Framework.DTO.DTOWebContract Insert(DTO.Prescriptions Prescription)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(Prescription);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Prescriptions Prescription)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(Prescription);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Prescriptions Prescription)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(Prescription);
        }
    }
}
