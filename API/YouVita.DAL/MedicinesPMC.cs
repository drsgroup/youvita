﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class MedicinesPMC
    {

        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewMedicinesPMC";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where Filter like " + YouVita.Framework.Data.DBFormat.toFormat("%" + filter + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListDispensationPeriod, DTO.DispensationPeriod>(queryString);
        }


    }
}
