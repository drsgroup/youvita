﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class ReportDetailPatientsByClient
    {

        public Framework.DTO.DTOWebContract List(string clientId)
        {
            string queryString = "select * from viewReportDetailPatientsByClient";

            if (!string.IsNullOrEmpty(clientId))
            {
                queryString += " Where ClientId = " + clientId;
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListReportDetailPatientsByClient, DTO.ReportDetailPatientsByClient>(queryString);
        }

    }
}
