﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class PatientPathologies
    {

        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewPatientPathologies";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where Filter like " + YouVita.Framework.Data.DBFormat.toFormat("%" + filter + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListPatientPathologies, DTO.PatientPathologies>(queryString);
        }

        public Framework.DTO.DTOWebContract GetByIdPatient(string filter)
        {
            string queryString = "select * from viewPatientPathologies";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where PatientId =  " +filter;
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListPatientPathologies, DTO.PatientPathologies>(queryString);
        }

        

        public Framework.DTO.DTOWebContract Insert(DTO.PatientPathologies PatientPathologie)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(PatientPathologie);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.PatientPathologies PatientPathologie)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(PatientPathologie);
        }

        public Framework.DTO.DTOWebContract Update(DTO.PatientPathologies PatientPathologie)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(PatientPathologie);
        }

    }
}
