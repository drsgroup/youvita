﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.Data;
using YouVita.Framework.DTO;

namespace YouVita.DAL
{
    public class Configurations
    {

        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewConfigurations";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where Filter like " + YouVita.Framework.Data.DBFormat.toFormat("%" + filter + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListConfigurations, DTO.Configurations>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Configurations Configuration)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(Configuration);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Configurations Configuration)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(Configuration);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Configurations Configuration)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(Configuration);
        }

    }
}
