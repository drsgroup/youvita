﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class OrderItem
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewOrderItem ";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where Filter like " + DBFormat.toFormat("%" + filter + "%", DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrderItem, DTO.OrderItem>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.OrderItem OrderItem)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(OrderItem);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.OrderItem OrderItem)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(OrderItem);
        }

    }
}
