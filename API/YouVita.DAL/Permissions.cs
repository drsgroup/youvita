﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class Permissions
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewPermissions";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where Filter like " + YouVita.Framework.Data.DBFormat.toFormat("%" + filter + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListPermissions, DTO.Permissions>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Permissions Permission)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(Permission);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Permissions Permission)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(Permission);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Permissions Permission)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(Permission);
        }

    }
}
