﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class ProjectProduct
    {
        public Framework.DTO.DTOWebContract List(int projectId)
        {
            string queryString = "select * from viewProjectProduct p ";
            queryString += " where ProjectId = " + projectId.ToString();
            return Framework.DTO.DTOHelper.listDTO<DTO.ListProjectProduct, DTO.ProjectProduct>(queryString);
        }

        public Framework.DTO.DTOWebContract ListAll(int projectId, int laboratoryId)
        {
            string queryString = "select vp.productID, vp.productName, isnull(vpp.ProjectProductsId, 0) as projectProductsId ";
            queryString += " from viewProduct vp left join viewProjectProduct vpp on (vp.productID = vpp.ProductId and  vpp.projectId = " + projectId.ToString()+ ") ";
            queryString += " left  join viewProject p on (p.ProjectId = vpp.projectid and p.LaboratoryId = "+ laboratoryId.ToString()+ ") ";
            //queryString += "  where (vpp.projectId = " + projectId.ToString() + " or vpp.projectid is null) ";
            queryString += " where vp.LaboratoryId = " + laboratoryId.ToString();
            queryString += " order by vp.productName ";
            return Framework.DTO.DTOHelper.listDTO<DTO.ListProjectProduct, DTO.ProjectProduct>(queryString);
        }

        public Framework.DTO.DTOWebContract ListLabId(string projectId)
        {
            string queryString = "select vp.* ";
            queryString += " from viewProduct vp join viewProjectProduct vpp on (vp.productID = vpp.ProductId ) ";
            queryString += " where vpp.projectId = " + projectId.ToString();
            queryString += " order by vp.productName ";
            return Framework.DTO.DTOHelper.listDTO<DTO.ListProducts, DTO.Product>(queryString);
        }

        

        public Framework.DTO.DTOWebContract Insert(DTO.ProjectProduct project)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(project);
        }

        public void Delete(int projectId)
        {
            var query = "Update ProjectProdutct set ProjectProductEnabled = 0 where ProjectId = " + projectId.ToString();

            Framework.DTO.DTOHelper.ExecNonQuery(query);
        }

        public Framework.DTO.DTOWebContract Update(DTO.ProjectProduct project)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(project);
        }
    }
}
