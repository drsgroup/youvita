﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class ProductsAvaliableRepresentative
    {
        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewProductsAvaliableRepresentative";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where Filter like " + YouVita.Framework.Data.DBFormat.toFormat("%" + filter + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListProductsAvaliable, DTO.ProductsAvaliable>(queryString);
        }

        public Framework.DTO.DTOWebContract GetProductsAvaliable(string login)
        {
            string queryString = "select p.*, pr.ProductName ";
            queryString += " from viewUsers v JOIN viewRepresentatives d on (v.UserLoginName = d.login) ";
            queryString += " JOIN viewProductsAvaliableRepresentative p on (p.RepresentativeID = d.RepresentativeID) ";
            queryString += " JOIN viewproduct pr on (p.productID = pr.productID) ";
            queryString += " where v.UserLoginName = " + YouVita.Framework.Data.DBFormat.toFormat(login, YouVita.Framework.Data.DBFormat.typeValueString);
            queryString += " and p.status = 'P'";

            var dt = Framework.DTO.DTOHelper.ExecQuery(queryString);
            var ret = new Framework.DTO.DTOWebContract();
            ret.ObjectDTO = dt;
            ret.Status = "OK";
            ret.Message = "";
            return ret;
            //return Framework.DTO.DTOHelper.listDTO<DTO.ListProductsAvaliable, DTO.ProductsAvaliable>(queryString);
        }

        public Framework.DTO.DTOWebContract GetProductsAvaliableByID(string OrderAvaliableID)
        {
            string queryString = " select v.*, p.productName from viewProductsAvaliableRepresentative v ";
            queryString += " join viewProduct p on (v.ProductID = p.productID) ";
            queryString += " where v.OrderAvaliableRepresentativeID = " + OrderAvaliableID;

            return Framework.DTO.DTOHelper.listDTO<DTO.ListProductsAvaliableRepresentative, DTO.ProductsAvaliableRepresentative>(queryString);
        }


        public Framework.DTO.DTOWebContract Insert(DTO.ProductsAvaliableRepresentative ProductsAvaliableRepresentative)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(ProductsAvaliableRepresentative);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.ProductsAvaliableRepresentative ProductsAvaliableRepresentative)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(ProductsAvaliableRepresentative);
        }

        public void DeleteByOrderAvaliableRepresentativeID(int OrderAvaliableId)
        {

        }

        public Framework.DTO.DTOWebContract Update(DTO.ProductsAvaliableRepresentative ProductsAvaliableRepresentative)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(ProductsAvaliableRepresentative);
        }
    }
}
