﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class ProjectDoctor
    {
        public Framework.DTO.DTOWebContract List(int projectId)
        {
            string queryString = "select * from viewProjectProduct p ";
            queryString += " where ProjectId = " + projectId.ToString();
            return Framework.DTO.DTOHelper.listDTO<DTO.ListProject, DTO.ProjectDoctor>(queryString);
        }

        public Framework.DTO.DTOWebContract ListAll(int projectId, int laboratoryId)
        {
            string queryString = "select vd.doctorName, vd.doctorId, vd.crm, vd.UF_CRM, isnull(vpd.projectdoctorid, 0) as projectdoctorid ";
            queryString += " from viewDoctors vd join viewRepresentatives r on (vd.RepresentativeID = r.RepresentativeID and r.laboratoryID = "+ laboratoryId.ToString()+" ) left join viewProjectDoctor vpd on (vd.DoctorID = vpd.doctorid and vpd.projectId = " + projectId.ToString() + ") ";
            queryString += " left join viewProject p on (p.ProjectId = vpd.projectid ) ";
            //queryString += " where (vpd.projectId = " + projectId.ToString() + "  or vpd.projectid is null) ";
            //queryString += "  and (p.LaboratoryId = " + laboratoryId.ToString() + " or p.ProjectId is null) ";     
            queryString += " where (r.LaboratoryId = 0 or r.LaboratoryId = " + laboratoryId.ToString()+ ")";
            queryString += " order by vd.DoctorName ";
            return Framework.DTO.DTOHelper.listDTO<DTO.ListProjectDoctor, DTO.ProjectDoctor>(queryString);
        }


        public Framework.DTO.DTOWebContract ListByProjectId(int projectId)
        {
            string queryString = "select vd.doctorName, vd.doctorId, vd.crm, vd.UF_CRM, isnull(vpd.projectdoctorid, 0) as projectdoctorid ";
            queryString += " from viewDoctors vd join viewProjectDoctor vpd on (vd.DoctorID = vpd.doctorid ) ";
            queryString += " where vpd.projectId = " + projectId.ToString();
            queryString += " order by vd.DoctorName ";
            return Framework.DTO.DTOHelper.listDTO<DTO.ListProjectDoctor, DTO.ProjectDoctor>(queryString);
        }


        public Framework.DTO.DTOWebContract Insert(DTO.ProjectDoctor project)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(project);
        }

        public void Delete(int projectId)
        {
            var queryString = "update ProjectDoctor set ProjectDoctorEnabled = 0 where ProjectId = " + projectId.ToString();
            Framework.DTO.DTOHelper.ExecNonQuery(queryString);
        }

        public Framework.DTO.DTOWebContract Update(DTO.ProjectDoctor project)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(project);
        }
    }

    
}
