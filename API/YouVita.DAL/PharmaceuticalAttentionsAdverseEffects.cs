﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class PharmaceuticalAttentionsAdverseEffects
    {

        public Framework.DTO.DTOWebContract List(string filter)
        {
            string queryString = "select * from viewPharmaceuticalAttentionsAdverseEffects";

            if (!string.IsNullOrEmpty(filter))
            {
                queryString += " Where PharmaceuticalAttentionId = " + filter;
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListPharmaceuticalAttentionsAdverseEffects, DTO.PharmaceuticalAttentionsAdverseEffects>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.PharmaceuticalAttentionsAdverseEffects entity)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(entity);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.PharmaceuticalAttentionsAdverseEffects entity)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(entity);
        }

    }
}
