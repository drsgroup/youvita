﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class ReportDetailReleaseOrderByClient
    {
        public Framework.DTO.DTOWebContract List(string clientId, DateTime startDate, DateTime endDate)
        {
            string queryString = "select * from viewReportDetailReleaseOrderByClient";
            queryString += " Where ReleaseOrderDate between " + Framework.Data.DBFormat.toFormat(startDate.ToString(),Framework.Data.DBFormat.typeValueDatetime) + " And " +
                           Framework.Data.DBFormat.toFormat(endDate.ToString(), Framework.Data.DBFormat.typeValueDatetime);

            if (!string.IsNullOrEmpty(clientId))
            {
                queryString += " and ClientId = " + clientId;
            }


            return Framework.DTO.DTOHelper.listDTO<DTO.ListReportDetailReleaseOrderByClient, DTO.ReportDetailReleaseOrderByClient>(queryString);
        }

    }
}
