﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;
using YouVita.Framework.Data;

namespace YouVita.DAL
{
    public class Patients
    {

        public Framework.DTO.DTOWebContract List(string patientName, string CPF, string identificationOnClient, int clientId)
        {
            string queryString = "select p.*, c.ClientFantasyName from viewPatients p join viewClients c on (p.ClientId = c.ClientId) where 1 = 1";

            if (!string.IsNullOrEmpty(patientName))
            {
                queryString += " and patientName like " + YouVita.Framework.Data.DBFormat.toFormat("%" + patientName + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(CPF))
            {
                queryString += " and CPF like " + YouVita.Framework.Data.DBFormat.toFormat("%" + CPF + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!string.IsNullOrEmpty(identificationOnClient))
            {
                queryString += " and identificationOnClient like " + YouVita.Framework.Data.DBFormat.toFormat("%" + identificationOnClient + "%", YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (clientId > 0)
            {
                queryString += " and p.ClientId = " + clientId.ToString();
            }

            return Framework.DTO.DTOHelper.listDTO<DTO.ListPatients, DTO.Patients>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.Patients Patient)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(Patient);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.Patients Patient)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(Patient);
        }

        public Framework.DTO.DTOWebContract Update(DTO.Patients Patient)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(Patient);
        }

    }
}
