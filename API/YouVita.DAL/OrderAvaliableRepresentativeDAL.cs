﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.DAL
{
    public class OrderAvaliableRepresentativeDAL
    {
        public Framework.DTO.DTOWebContract ListByDoctor(string DoctorID)
        {
            string queryString = "select * from viewOrderAvaliableRepresentative ";
            queryString += " Where RepresentativeID = " + DoctorID;

            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrderAvaliableRepresentative, DTO.OrderAvaliableRepresentative>(queryString);
        }


        public Framework.DTO.DTOWebContract ListByRepresentativeName(string doctor)
        {
            string queryString = "SELECT O.*, d.RepresentativeName FROM viewOrderAvaliableRepresentative o ";
            queryString += " join viewRepresentatives d on (o.RepresentativeID = d.RepresentativeID) ";
            queryString += " join viewUsers v on (d.login = v.UserLoginName) where 1 = 1 ";

            if (!string.IsNullOrEmpty(doctor))
            {
                queryString += " and d.RepresentativeName like '%" + doctor + "%'";
            }
            
            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrderAvaliableRepresentative, DTO.OrderAvaliableRepresentative>(queryString);
        }

        public Framework.DTO.DTOWebContract ListByLogin(string login, String status)
        {
            string queryString = "SELECT O.*, d.RepresentativeName FROM viewOrderAvaliableRepresentative o ";
            queryString += " join viewRepresentatives d on (o.RepresentativeID = d.RepresentativeID) ";
            queryString += " join viewUsers v on (d.login = v.UserLoginName) where 1 = 1 ";

            if (!string.IsNullOrEmpty(login))
            {
                queryString += " and v.UserLoginName = " + YouVita.Framework.Data.DBFormat.toFormat(login, YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (!String.IsNullOrEmpty(status))
            {
                queryString += " and o.status = " + YouVita.Framework.Data.DBFormat.toFormat(status, YouVita.Framework.Data.DBFormat.typeValueString);
            }
            
            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrderAvaliableRepresentative, DTO.OrderAvaliableRepresentative>(queryString);
        }

        public Framework.DTO.DTOWebContract ListByRepresLogin(string represLogin, string representativeName, string laboratoryID)
        {
            string queryString = "SELECT O.*, d.RepresentativeName FROM viewOrderAvaliableRepresentative o ";
            queryString += " join viewRepresentatives d on (o.representativeID = d.representativeID) ";
            queryString += " join viewUsers v on (d.login = v.UserLoginName) join viewRepresentatives r on (d.RepresentativeID = r.RepresentativeID) ";
            queryString += " where 1 = 1 ";
            
            if (represLogin != "undefined" && represLogin != "null" && !String.IsNullOrEmpty(represLogin)){
                queryString += " and r.login = " + YouVita.Framework.Data.DBFormat.toFormat(represLogin, YouVita.Framework.Data.DBFormat.typeValueString);
            }

            if (representativeName != "undefined" && representativeName != "null" && !String.IsNullOrEmpty(representativeName))
            {
                queryString += " and d.RepresentativeName like '%" + representativeName + "%'";
            }

            if (laboratoryID != "undefined" && laboratoryID != "null" && !String.IsNullOrEmpty(laboratoryID) && laboratoryID!="0")
            {
                queryString += " and d.laboratoryID =" +laboratoryID;
            }
            
            
            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrderAvaliableRepresentative, DTO.OrderAvaliableRepresentative>(queryString);
        }



        public Framework.DTO.DTOWebContract ListByRepresentative(string RepresentativeID)
        {
            string queryString = "select o.* from viewDoctors d ";
            queryString += "  join viewOrderAvaliable o on (d.DoctorID = o.doctorid) ";
            queryString += " where d.RepresentativeID = " + RepresentativeID;

            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrderAvaliableRepresentative, DTO.OrderAvaliableRepresentative>(queryString);
        }

        public Framework.DTO.DTOWebContract ListByLab(string LabID)
        {
            string queryString = "select o.* from viewOrderAvaliable o ";
            queryString += " where o.laboratoryID = " + LabID;
            
            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrderAvaliableRepresentative, DTO.OrderAvaliableRepresentative>(queryString);
        }

        public Framework.DTO.DTOWebContract List()
        {
            string queryString = "select o.*, d.DoctorName from viewOrderAvaliable o ";
            queryString += " join viewDoctors d on (o.doctorid = d.DoctorID) ";
            //queryString += " where o.laboratoryID = " + LabID;
            
            return Framework.DTO.DTOHelper.listDTO<DTO.ListOrderAvaliableRepresentative, DTO.OrderAvaliableRepresentative>(queryString);
        }

        public Framework.DTO.DTOWebContract Insert(DTO.OrderAvaliableRepresentative OrderAvaliableRepresentative)
        {
            return Framework.DTO.DTOHelper.insertDTODataBase(OrderAvaliableRepresentative);            
        }

        public Framework.DTO.DTOWebContract Update(DTO.OrderAvaliableRepresentative OrderAvaliableRepresentative)
        {
            return Framework.DTO.DTOHelper.updateDTODataBase(OrderAvaliableRepresentative);
        }

        public Framework.DTO.DTOWebContract Delete(DTO.OrderAvaliableRepresentative OrderAvaliableRepresentative)
        {
            return Framework.DTO.DTOHelper.deleteDTODataBase(OrderAvaliableRepresentative);
        }
    }
}
