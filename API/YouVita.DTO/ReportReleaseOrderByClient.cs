﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ReportReleaseOrderByClient
    {
        [DTOAttribute("clientId", "clientId", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int clientId { get; set; }

        [DTOAttribute("clientName", "clientName", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string clientName { get; set; }

        [DTOAttribute("qtyReleaseOrder", "qtyReleaseOrder", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int qtyReleaseOrder { get; set; }

        [DTOAttribute("month", "month", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int month { get; set; }

        [DTOAttribute("year", "year", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int year { get; set; }

    }

    public class ListReportReleaseOrderByClient : List<ReportReleaseOrderByClient>
    { }

}
