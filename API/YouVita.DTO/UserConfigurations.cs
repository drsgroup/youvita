﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class UserConfigurations
    {

        [DTOAttribute("UserId", "Código", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int UserId { get; set;  }


        [DTOAttribute("UserTypeId", "Código Tipo do Usuário", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int UserTypeId { get; set; }


        [DTOAttribute("UserTypeName", "Tipo do Usuário", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string UserTypeName { get; set; }


        [DTOAttribute("SystemId", "Código do Sistema", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int SystemId { get; set; }


        [DTOAttribute("SystemName", "Sistema", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string SystemName { get; set; }


        [DTOAttribute("SystemRouteId", "Código da Rota", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int SystemRouteId { get; set; }


        [DTOAttribute("SystemRoute", "Rota", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string SystemRoute { get; set; }

    }

    public class ListUserConfigurations : List<UserConfigurations>
    { }
}
