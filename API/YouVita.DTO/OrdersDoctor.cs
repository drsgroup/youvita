﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class OrdersDoctor : DTODefault
    {
        [DTOAttribute("OrderDoctorID", "Codigo", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int orderDoctorID { get; set; }

        [DTOAttribute("DoctorID", "ID do Doutor", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int doctorID { get; set; }

        [DTOAttribute("Status", "Status", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string status { get; set; }

        [DTOAttribute("CreateDate", "Data da Criação", DTOPrimaryKey.No, 19, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime createDate { get; set; }

        [DTOAttribute("OrderDoctorDateSend", "Data do Envio", DTOPrimaryKey.No, 19, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime orderDoctorDateSend { get; set; }

        [DTOAttribute("OrderDoctorEnabled", "Ativo", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int orderDoctorEnabled { get; set; }

        [DTOAttribute("OrderAvaliableID", "OrderAvaliableID", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int OrderAvaliableID { get; set; }


        [DTOAttribute("doctorName", "Nome", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        public string doctorName { get; set; }


        [DTOAttribute("representativeName", "Nome", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        public string representativeName { get; set; }


        [DTOAttribute("laboratoryFantasyName", "Nome", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        public string laboratoryFantasyName { get; set; }

        [DTOAttribute("Products", "Products", DTOPrimaryKey.No, 1, DTONotNull.No, DTOParameterProcedure.No)]
        public ListProductsOrderDoctor Products { get; set; }

        [DTOAttribute("doctorAddessesId", "doctorAddessesId", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public Int32 doctorAddessesId { get; set; }

        [DTOAttribute("prjectId", "prjectId", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.No)]
        public Int32 projectId { get; set; }

    }

    public class ListOrdersDoctor : List<OrdersDoctor>
    { }

}
