﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class PhoneType : DTODefault
    {
        [DTOAttribute("PhoneTypeId","Código", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int phoneTypeId { get; set; }

        [DTOAttribute("Description","Descrição", DTOPrimaryKey.No, 50, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string description { get; set; }

        [DTOAttribute("PhoneTypeEnabled","Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int phoneTypeEnabled { get; set; }
    }

    public class ListPhoneType : List<PhoneType>
    { }
}
