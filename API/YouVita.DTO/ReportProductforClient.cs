﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ReportProductforClient
    {
        [DTOAttribute("medicineId", "medicineId", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int medicineId { get; set; }

        [DTOAttribute("clientId", "clientId", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int clientId { get; set; }

        [DTOAttribute("medicineName", "medicineName", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string medicineName { get; set; }

        [DTOAttribute("labName", "labName", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string labName { get; set; }

        [DTOAttribute("quantity", "quantity", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int quantity { get; set; }

        [DTOAttribute("month", "month", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int month { get; set; }

        [DTOAttribute("year", "year", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int year { get; set; }

    }

    public class ListReportProductforClient : List<ReportProductforClient>
    { }
}
