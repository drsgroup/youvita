﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{

    public class ResearchResponses : DTODefault
    {
        [DTOAttribute("ResearchesId", "ResearchesId", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ResearchesId { get; set; }

        [DTOAttribute("QuestionsId", "QuestionId", DTOPrimaryKey.No, 30, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int QuestionsId { get; set; }

        [DTOAttribute("Question", "Question", DTOPrimaryKey.No, 200, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Question { get; set; }

        [DTOAttribute("Complemento", "Complemento", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Complemento { get; set; }        
    }
    public class ListResearchResponses : List<ResearchResponses>
    { }
}
