﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class OrderAvaliableRepresentative : DTODefault
    {
        [DTOAttribute("OrderAvaliableRepresentativeID", "ID", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int OrderAvaliableRepresentativeID { get; set; }

        [DTOAttribute("RepresentativeID", "ID", DTOPrimaryKey.No, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int RepresentativeID { get; set; }

        [DTOAttribute("startValidity", "Validade inicial", DTOPrimaryKey.No, 19, DTONotNull.No, DTOParameterProcedure.Yes)]
        public DateTime startValidity { get; set; }

        [DTOAttribute("endValidity", "Validade final", DTOPrimaryKey.No, 19, DTONotNull.No, DTOParameterProcedure.Yes)]
        public DateTime endValidity { get; set; }

        [DTOAttribute("frequence", "Frequencia", DTOPrimaryKey.No, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int frequence { get; set; }

        [DTOAttribute("dateToSend", "Data primeiro envio", DTOPrimaryKey.No, 19, DTONotNull.No, DTOParameterProcedure.Yes)]
        public DateTime dateToSend { get; set; }

        [DTOAttribute("laboratoryID", "Data primeiro envio", DTOPrimaryKey.No, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int laboratoryID { get; set; }

        [DTOAttribute("OrderAvaliableRepresentativeEnabled", "Data primeiro envio", DTOPrimaryKey.No, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int OrderAvaliableRepresentativeEnabled { get; set; }

        [DTOAttribute("ListProductsAvaliable", "ListProductsAvaliable", DTOPrimaryKey.No, 1, DTONotNull.No, DTOParameterProcedure.No)]
        public ListProductsAvaliableRepresentative Items { get; set; }

        [DTOAttribute("RepresentativeName", "RepresentativeName", DTOPrimaryKey.No, 1, DTONotNull.No, DTOParameterProcedure.No)]
        public string RepresentativeName { get; set; }

        [DTOAttribute("status", "status", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string status { get; set; }

    }
    public class ListOrderAvaliableRepresentative : List<OrderAvaliableRepresentative>
    { }

}
