﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Permissions : DTODefault
    {
        [DTOAttribute("PermissionId","Código", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int permissionId { get; set; }

        [DTOAttribute("PermissionName","Permissão", DTOPrimaryKey.No, 100, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string permissionName { get; set; }

        [DTOAttribute("PermissionEnabled","Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int permissionEnabled { get; set; }

    }

    public class ListPermissions : List<Permissions>
    { }
}