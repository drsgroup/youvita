﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Pathologies
    {
        [DTOAttribute("PathologyId","Código", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int pathologyId { get; set; }

        [DTOAttribute("PathologyDescription","Descrição", DTOPrimaryKey.No, 50, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string pathologyDescription { get; set; }

        [DTOAttribute("PathologiesEnabled","Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int pathologiesEnabled { get; set; }

    }

    public class ListPathologies : List<Pathologies>
    { }
}
