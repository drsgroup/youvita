﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class PatientPathologies : DTODefault
    {
        [DTOAttribute("PatientPathologiesId", "Código", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int PatientPathologiesId { get; set; }

        [DTOAttribute("PatientId","Código Paciente", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int patientId { get; set; }

        [DTOAttribute("PathologyId","Código Patologia", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int pathologyId { get; set; }

        [DTOAttribute("PatientPathologiesEnabled","Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int patientPathologiesEnabled { get; set; }
    }

    public class ListPatientPathologies : List<PatientPathologies>
    { }
}
