﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Medicines : DTODefault
    {

        [DTOAttribute("MedicineId", "Código", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int MedicineId { get; set; }

        [DTOAttribute("EAN", "EAN", DTOPrimaryKey.No, 13, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string EAN { get; set; }

        [DTOAttribute("MedicineName", "Nome do Medicamento", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string MedicineName { get; set; }

        [DTOAttribute("LabId", "Codigo do Laboratorio", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string LabId { get; set; }

        [DTOAttribute("LabName", "Nome do Laboratorio", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string LabName { get; set; }

        [DTOAttribute("ActivePrincipleId", "Codigo Principio Ativo", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string ActivePrincipleId { get; set; }

        [DTOAttribute("ActivePrincipleName", "Nome Principio Ativo", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string ActivePrincipleName { get; set; }

        [DTOAttribute("Presentation", "Apresentação", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string Presentation { get; set; }

        [DTOAttribute("UnitId", "Codigo Unidade", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string UnitId { get; set; }

        [DTOAttribute("UnitName", "Nome Unidade", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string UnitName { get; set; }

        [DTOAttribute("Dosage", "Dosagem", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int Dosage { get; set; }

        [DTOAttribute("QtyTotal", "Quantidade Total", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int QtyTotal { get; set; }

        [DTOAttribute("QtyStock", "Quantidade Estoque", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.No)]
        public int QtyStock { get; set; }

        [DTOAttribute("MedicineType", "Tipo Medicamento", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string MedicineType { get; set; }

        [DTOAttribute("Comments", "Comentario", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string Comments { get; set; }

        [DTOAttribute("MedicineEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int MedicineEnabled { get; set; }

        [DTOAttribute("codClasse", "Ativo", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string codClasse { get; set; }

        [DTOAttribute("DescClasse", "Ativo", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string DescClasse { get; set; }


        [DTOAttribute("ListMedicinesPMC", "ListMedicinesPMC", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.No)]
        public List<MedicinesPMC> ListMedicinesPMC { get; set; }

    }

    public class ListMedicines : List<Medicines>
    { }
}
