﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ProjectProduct : DTODefault
    {
        [DTOAttribute("ProjectProductsId", "ProjectProductsId", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public Int64 ProjectProductsId { get; set; }

        [DTOAttribute("ProjectId", "ProjectId", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public Int64 ProjectId { get; set; }

        [DTOAttribute("ProductId", "ProductId", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public Int64 ProductId { get; set; }

        [DTOAttribute("ProjectProductEnabled", "ProjectProductEnabled", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ProjectProductEnabled { get; set; }

        [DTOAttribute("DisabledDate", "DisabledDate", DTOPrimaryKey.No, 20, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime DisabledDate { get; set; }

        [DTOAttribute("DateIncluded", "DateIncluded", DTOPrimaryKey.No, 20, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime DateIncluded { get; set; }

        [DTOAttribute("ProductName", "ProductName", DTOPrimaryKey.No, 100, DTONotNull.Yes, DTOParameterProcedure.No)]
        public string  ProductName { get; set; }

    }

    public class ListProjectProduct : List<ProjectProduct>
    { }
}
