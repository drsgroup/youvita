﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ClientType : DTODefault
    {
        [DTOAttribute("ClientTypeId","Código", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int clientTypeId { get; set; }

        [DTOAttribute("ClientTypeDescription","Descrição", DTOPrimaryKey.No, 40, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string clientTypeDescription { get; set; }

        [DTOAttribute("ClientTypeEnabled","Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int clientTypeEnabled { get; set; }
    }

    public class ListClientType : List<ClientType>
    { }
}
