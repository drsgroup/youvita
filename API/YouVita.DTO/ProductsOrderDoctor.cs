﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ProductsOrderDoctor : DTODefault
    {
        [DTOAttribute("ProductsOrderDoctorID", "ProductsOrderDoctorID", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ProductsOrderDoctorID { get; set; }


        [DTOAttribute("ProductID", "ProductID", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ProductID { get; set; }

        [DTOAttribute("Qty", "Qty", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int Qty { get; set; }

        [DTOAttribute("OrdersDoctorID", "OrdersDoctorID", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int OrdersDoctorID { get; set; }

        [DTOAttribute("productName", "productName", DTOPrimaryKey.No, 50, DTONotNull.Yes, DTOParameterProcedure.No)]
        public string productName { get; set; }

        [DTOAttribute("productCode", "productCode", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public string productCodeTOTVS { get; set; }

    }

    public class ListProductsOrderDoctor : List<ProductsOrderDoctor>
    { }
}

