﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class AdverseEffects : DTODefault
    {
        [DTOAttribute("AdverseEffectsId", "Codigo", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int AdverseEffectsId { get; set; }

        [DTOAttribute("AdverseEffectsDescription", "Descrição", DTOPrimaryKey.No, 200, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string AdverseEffectsDescription { get; set; }

      
        [DTOAttribute("AdverseEffectsEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int AdverseEffectsEnabled { get; set; }


    }
    public class ListAdverseEffects : List<AdverseEffects>
    { }
}
