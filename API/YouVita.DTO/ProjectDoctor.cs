﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ProjectDoctor :DTODefault
    {
        [DTOAttribute("ProjectDoctorId", "Id", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int projectDoctorId { get; set; }
            
        [DTOAttribute("ProjectId", "ProjectId", DTOPrimaryKey.No, 100, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public Int32 projectId { get; set; }

        [DTOAttribute("DoctorId", "DoctorId", DTOPrimaryKey.No, 100, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public Int32 DoctorId { get; set; }

        [DTOAttribute("projectDoctorEnabled", "projectDoctorEnabled", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int projectDoctorEnabled { get; set; }

        [DTOAttribute("DisabledDate", "DisabledDate", DTOPrimaryKey.No, 30, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime DisabledDate { get; set; }

        [DTOAttribute("DateIncluded", "DateIncluded", DTOPrimaryKey.No, 30, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime DateIncluded { get; set; }

        [DTOAttribute("DoctorName", "DoctorName", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.No)]
        public string doctorName { get; set; }

        [DTOAttribute("crm", "crm", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.No)]
        public string crm { get; set; }
    }

    public class ListProjectDoctor : List<ProjectDoctor>
    { }

}
