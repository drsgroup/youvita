﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class PrescriptionsItems : DTODefault
    {
        [DTOAttribute("PrescriptionItemId", "Código do item da Receita", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int prescriptionItemId { get; set; }

        [DTOAttribute("PrescriptionId", "Código da Receita", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int prescriptionId { get; set; }

        [DTOAttribute("MedicineId", "Código do Medicamento", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int medicineId { get; set; }

        [DTOAttribute("DailyDose", "Dose diária - Posologia", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int dailyDose { get; set; }

        [DTOAttribute("TermDays", "Prazo em dias", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int termDays { get; set; }

        [DTOAttribute("PrescriptionItemEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int prescriptionItemEnabled { get; set; }

        [DTOAttribute("MedicineName", "Nome do Medicamento", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        public string MedicineName { get; set; }

        [DTOAttribute("LabId", "Codigo do Laboratorio", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.No)]
        public string LabId { get; set; }

        [DTOAttribute("LabName", "Nome do Laboratorio", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        public string LabName { get; set; }

        [DTOAttribute("presentatio", "Apresentação", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        public string Presentation { get; set; }

        [DTOAttribute("EAN", "EAN", DTOPrimaryKey.No, 200, DTONotNull.Yes, DTOParameterProcedure.No)]
      public string EAN { get; set; }
    }

    public class ListPrescriptionItems : List<PrescriptionsItems>
    { }
}
