﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Prescriptions : DTODefault
    {
        [DTOAttribute("PrescriptionId", "Código da Receita", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int prescriptionId { get; set; }

        [DTOAttribute("PatientId", "Código do Paciente", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int patientId { get; set; }

        [DTOAttribute("PrescriptionDate", "Data da Receita", DTOPrimaryKey.No, 19, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime prescriptionDate { get; set; }

        [DTOAttribute("ExpirationDate", "Data de Validade", DTOPrimaryKey.No, 19, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime expirationDate { get; set; }

        [DTOAttribute("CRM", "CRM do Médico", DTOPrimaryKey.No, 10, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string crm { get; set; }

        [DTOAttribute("UfCrm", "UF CRM", DTOPrimaryKey.No, 2, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string ufCrm { get; set; }

        [DTOAttribute("DoctorName", "Nome do Médico", DTOPrimaryKey.No, 60, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string doctorName { get; set; }

        [DTOAttribute("PathImagePrescription", "Caminho da Imagem da Receita", DTOPrimaryKey.No, 260, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string pathImagePrescription { get; set; }

        [DTOAttribute("PrescriptionEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int prescriptionEnabled { get; set; }
        
        [DTOAttribute("ListPrescriptionItem", "ListPrescriptionItem", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.No)]
        public List<DTO.PrescriptionsItems> ListPrescriptionItem { get; set; }
        
        [DTOAttribute("PatientName", "PatientName", DTOPrimaryKey.No, 1, DTONotNull.No, DTOParameterProcedure.No)]
        public string patientName { get; set; }

        [DTOAttribute("StateUF", "UF", DTOPrimaryKey.No, 2, DTONotNull.No, DTOParameterProcedure.No)]
        public string stateUF { get; set; }

        [DTOAttribute("fileContent", "fileContent", DTOPrimaryKey.No, 999999999, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string fileContent { get; set; }

        [DTOAttribute("fileType", "fileType", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string fileType { get; set; }

    }

    public class ListPrescriptions : List<Prescriptions>
    { }

}
