﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ProductsAvaliable : DTODefault
    {
        [DTOAttribute("ProductAvaliableID", "Codigo", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int productAvaliableID { get; set; }

        [DTOAttribute("ProductID", "ID do Produto", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int productID { get; set; }

        [DTOAttribute("Qty", "Quantidade", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int qty { get; set; }

        [DTOAttribute("OrderAvaliableID", "OrderAvaliableID", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int OrderAvaliableID { get; set; }

        [DTOAttribute("ProductsAvaliableEnabled", "Ativo", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int productsAvaliableEnabled { get; set; }


        [DTOAttribute("productName", "Ativo", DTOPrimaryKey.No, 50, DTONotNull.Yes, DTOParameterProcedure.No)]
        public string productName { get; set; }

    }

    public class ListProductsAvaliable : List<ProductsAvaliable>
    { }
}
