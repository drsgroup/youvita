﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Users : DTODefault
    {
        [DTOAttribute("UserId","Código", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int userId { get; set; }

        [DTOAttribute("UserName","Nome", DTOPrimaryKey.No, 40, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string userName { get; set; }

        [DTOAttribute("UserLoginName","Login", DTOPrimaryKey.No, 40, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string userLoginName { get; set; }

        [DTOAttribute("userPassword", "Password", DTOPrimaryKey.No, 40, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string userPassword { get; set; }

        [DTOAttribute("UserEnabled","Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int userEnabled { get; set; }

        [DTOAttribute("UserType", "Tipo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int userType { get; set; }

        [DTOAttribute("ClientId", "ClientId", DTOPrimaryKey.No, 10, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ClientId { get; set; }

        [DTOAttribute("laboratoryID", "laboratoryID", DTOPrimaryKey.No, 10, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int laboratoryID { get; set; }

        [DTOAttribute("laboratoryName", "laboratoryName", DTOPrimaryKey.No, 100, DTONotNull.Yes, DTOParameterProcedure.No)]
        public string laboratoryName { get; set; }
 
        [DTOAttribute("LaboratoryFantasyName", "laboratoryName", DTOPrimaryKey.No, 100, DTONotNull.Yes, DTOParameterProcedure.No)]
        public string laboratoryFantasyName { get; set; }

        [DTOAttribute("ListUserConfigurations", "ListUserConfigurations", DTOPrimaryKey.No, 100, DTONotNull.Yes, DTOParameterProcedure.No)]
        public ListUserConfigurations Configurations { get; set; }

    }
    public class ListUsers : List<Users>
    { }
}
