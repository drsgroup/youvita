﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Patients : DTODefault
    {
        [DTOAttribute("PatientId","Código", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int PatientId { get; set; }

        [DTOAttribute("ClientId","Cliente", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ClientId { get; set; }

        [DTOAttribute("PatientName","Nome", DTOPrimaryKey.No, 60, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string PatientName { get; set; }

        [DTOAttribute("BirthDate","Data Aniversário", DTOPrimaryKey.No, 19, DTONotNull.No, DTOParameterProcedure.Yes)]
        public DateTime BirthDate { get; set; }

        [DTOAttribute("CPF","CPF", DTOPrimaryKey.No, 11, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string CPF { get; set; }

        [DTOAttribute("RG","RG", DTOPrimaryKey.No, 15, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string RG { get; set; }

        [DTOAttribute("IdentificationOnClient","Identificador", DTOPrimaryKey.No, 20, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string IdentificationOnClient { get; set; }

        [DTOAttribute("Responsible","Responsável", DTOPrimaryKey.No, 60, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string Responsible { get; set; }

        [DTOAttribute("BestContactPeriod","Melhor Periodo de Contato", DTOPrimaryKey.No, 20, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string BestContactPeriod { get; set; }

        [DTOAttribute("Comments","Comentário", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string Comments { get; set; }

        [DTOAttribute("PatientStatus","Status", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string PatientStatus { get; set; }

        [DTOAttribute("CID1","CID01", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string CID1 { get; set; }

        [DTOAttribute("CID2","CID02", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string CID2 { get; set; }

        [DTOAttribute("CID3","CID03", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string CID3 { get; set; }

        [DTOAttribute("PatientEnabled","Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int PatientEnabled { get; set; }

        [DTOAttribute("PatientType ", "Tipo de Paciente", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string patientType { get; set; }

        [DTOAttribute("ClientFantasyName", "Cliente", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string ClientFantasyName { get; set; }

        [DTOAttribute("ListPatientPhones", "ListPatientPhones", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.No)]
        public DTO.ListPatientPhones ListPatientPhones { get; set; }

        [DTOAttribute("ListPatientAddresses", "ListPatientAddresses", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.No)]
        public DTO.ListPatientAddresses ListPatientAddresses { get; set; }

        [DTOAttribute("ListReleaseOrder", "ListReleaseOrder", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.No)]
        public DTO.ReleaseOrder ReleaseOrder { get; set; }

    }

    public class ListPatients : List<Patients>
    { }
}
