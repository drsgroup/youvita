﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ReportDetailPatientsByClient
    {

        [DTOAttribute("clientId", "clientId", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int clientId { get; set; }

        [DTOAttribute("patientId", "patientId", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int patientId { get; set; }

        [DTOAttribute("patientName", "patientName", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string patientName { get; set; }

        [DTOAttribute("birthdate", "birthdate", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime birthdate { get; set; }

        [DTOAttribute("cpf", "cpf", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string cpf { get; set; }

        [DTOAttribute("rg", "rg", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string rg { get; set; }

        [DTOAttribute("identificationonClient", "identificationonClient", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string identificationonClient { get; set; }



    }

    public class ListReportDetailPatientsByClient : List<ReportDetailPatientsByClient>
    { }
}
