﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class PharmaceuticalAttentionsAdverseEffects : DTODefault
    {
        [DTOAttribute("pharmaceuticalAttentionId", "pharmaceuticalAttentionId", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int pharmaceuticalAttentionId { get; set; }

        [DTOAttribute("AdverseEffectsId", "AdverseEffectsId", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int AdverseEffectsId { get; set; }
    }

    public class ListPharmaceuticalAttentionsAdverseEffects : List<PharmaceuticalAttentionsAdverseEffects>
    { }
}
