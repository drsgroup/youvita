﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Configurations: DTODefault
    {
        [DTOAttribute("ConfigurationId", "Cod. Configuração",DTOPrimaryKey.Yes, 4,DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int configurationId { get; set; }

        [DTOAttribute("ConfigurationName", "Nome Configuração", DTOPrimaryKey.No, 100, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string configurationName { get; set; }

        [DTOAttribute("ConfigurationValue", "Valor Configuração", DTOPrimaryKey.No, 400, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string configurationValue { get; set; }

        [DTOAttribute("ConfigurationEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int configurationEnabled { get; set; }

        [DTOAttribute("PatientPhones", "PatientPhones", DTOPrimaryKey.No, 1, DTONotNull.No, DTOParameterProcedure.No)]
        public List<DTO.PatientPhones> listPatientPhones { get; set; }

    }

    public class ListConfigurations : List<Configurations>
    { }
}
