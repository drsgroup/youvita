﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class QuestionnaireResponses : DTODefault
    {
        [DTOAttribute("QuestionnaireResponsesId", "Codigo", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int QuestionnaireResponsesId { get; set; }

        [DTOAttribute("ResearchesId", "Pesquisa", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ResearchesId { get; set; }

        [DTOAttribute("QuestionsId", "Pergunta", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int QuestionsId { get; set; }

        [DTOAttribute("AnswersId", "Codigo Resposta", DTOPrimaryKey.No, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int AnswersId { get; set; }

        [DTOAttribute("Answer", "Resposta", DTOPrimaryKey.No, 250, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string Answer { get; set; }

        [DTOAttribute("QuestionnaireResponsesEnabled", "Ativo", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.No)]
        public int QuestionnaireResponsesEnabled { get; set; }

        [DTOAttribute("Complement", "Ativo", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string Complement { get; set; }

        [DTOAttribute("Respostas", "Ativo", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        public string Respostas { get; set; }
    }

    public class ListQuestionnaireResponses : List<QuestionnaireResponses>
    { }
}
