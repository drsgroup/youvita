﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ProductsAvaliableRepresentative : DTO.DTODefault
    {
        [DTOAttribute("ProductsAvaliableRepresentativeID", "Codigo", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ProductsAvaliableRepresentativeID { get; set; }

        [DTOAttribute("ProductID", "ID do Produto", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int productID { get; set; }

        [DTOAttribute("Qty", "Quantidade", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int qty { get; set; }

        [DTOAttribute("OrderAvaliableRepresentativeID", "Pedido", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int OrderAvaliableRepresentativeID { get; set; }

        [DTOAttribute("ProductsAvaliableRepresentativeEnabled", "Ativo", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ProductsAvaliableRepresentativeEnabled { get; set; }


        [DTOAttribute("productName", "Ativo", DTOPrimaryKey.No, 50, DTONotNull.Yes, DTOParameterProcedure.No)]
        public string productName { get; set; }

    }

    public class ListProductsAvaliableRepresentative : List<ProductsAvaliableRepresentative>
    { }
}
