﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Questions : DTODefault
    {
        [DTOAttribute("QuestionsId", "Id", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int QuestionsId { get; set; }

        [DTOAttribute("Question", "Pergunta", DTOPrimaryKey.No, 200, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Question { get; set; }

        [DTOAttribute("QuestionType", "Tipo de Pergunta", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string QuestionType { get; set; }

        [DTOAttribute("RequiredAnswer", "Obrigatório a resposta", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string RequiredAnswer { get; set; }

        [DTOAttribute("Complement", "Complemento", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Complement { get; set; }

        [DTOAttribute("QuestionsEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int QuestionsEnabled { get; set; }

        [DTOAttribute("ListAnswers", "ListAnswers", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.No)]
        public ListAnswers ListAnswers { get; set; }
    }

    public class ListQuestions : List<Questions>
    { }
}
