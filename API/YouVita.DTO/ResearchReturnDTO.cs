﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ResearchReturnDTO : DTODefault
    {
        [DTOAttribute("ResearchesId", "ResearchesId", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ResearchesId { get; set; }

        [DTOAttribute("CreateDate", "CreateDate", DTOPrimaryKey.No, 30, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime CreateDate { get; set; }

        [DTOAttribute("CPF", "CPF", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string CPF { get; set; }

        [DTOAttribute("PatientName", "PatientName", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string PatientName { get; set; }
    }
    public class ListResearchReturnDTO : List<ResearchReturnDTO>
    { }
}

