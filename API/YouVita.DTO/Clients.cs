﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Clients : DTODefault
    {
        [DTOAttribute("ClientId","Codigo", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ClientId { get; set; }

        [DTOAttribute("ClientName","Nome", DTOPrimaryKey.No, 200, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string ClientName { get; set; }

        [DTOAttribute("ClientFantasyName","Nome Fantasia", DTOPrimaryKey.No, 60, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string ClientFantasyName { get; set; }

        [DTOAttribute("CNPJ","CNPJ", DTOPrimaryKey.No, 20, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string CNPJ { get; set; }

        [DTOAttribute("Address","Endereço", DTOPrimaryKey.No, 60, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Address { get; set; }

        [DTOAttribute("AddressNumber","Número", DTOPrimaryKey.No, 10, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string AddressNumber { get; set; }

        [DTOAttribute("Neighborhood","Bairro", DTOPrimaryKey.No, 50, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Neighborhood { get; set; }

        [DTOAttribute("City", "Cidade", DTOPrimaryKey.No, 50, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string City { get; set; }

        [DTOAttribute("ZipCode", "CEP", DTOPrimaryKey.No, 9, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string ZipCode { get; set; }

        [DTOAttribute("StateId","Estado", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int StateId { get; set; }

        [DTOAttribute("ResponsibleContact", "Responsavel", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string ResponsibleContact { get; set; }

        [DTOAttribute("Occupation", "Cargo", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string Occupation { get; set; }

        [DTOAttribute("Email","E-mail", DTOPrimaryKey.No, 60, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Email { get; set; }

        [DTOAttribute("Phone", "Telefone", DTOPrimaryKey.No, 60, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Phone { get; set; }

        [DTOAttribute("Ramal", "Telefone", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string Ramal { get; set; }

        [DTOAttribute("CellPhone", "Celular", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string CellPhone { get; set; }

        [DTOAttribute("ClientLogin","Login", DTOPrimaryKey.No, 60, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string ClientLogin { get; set; }

        [DTOAttribute("ClientPassword","Senha", DTOPrimaryKey.No, 40, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string ClientPassword { get; set; }

        [DTOAttribute("ConfirmPassword","Confirmação Senha", DTOPrimaryKey.No, 40, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string ConfirmPassword { get; set; }

        [DTOAttribute("ClientTypeId", "Tipo", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ClientTypeId { get; set; }

        [DTOAttribute("ClientStatus", "Status", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string ClientStatus { get; set; }

        [DTOAttribute("Comments","Comentário", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string Comments { get; set; }

        [DTOAttribute("ClientsEnabled","Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ClientsEnabled { get; set; }

        [DTOAttribute("ListClientsContacts", "ListClientsContacts", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.No)]
        public List<DTO.ClientsContacts> ListClientsContacts { get; set; }

    }
    public class ListClients : List<Clients>
    { }
}
