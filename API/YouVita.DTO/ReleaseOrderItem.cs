﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ReleaseOrderItem : DTODefault
    {
        [DTOAttribute("ReleaseOrderItemId", "Código do item", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int releaseOrderItemId { get; set; }

        [DTOAttribute("ReleaseOrderId", "Código do Pedido", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int releaseOrderId { get; set; }

        [DTOAttribute("MedicineId", "Código do Medicamento", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int medicineId { get; set; }

        [DTOAttribute("Dosage", "Dosagem", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int dosage { get; set; }

        [DTOAttribute("Quantity", "Quantidade", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int quantity { get; set; }

        [DTOAttribute("PatientAuthorization", "Autorização do Paciente", DTOPrimaryKey.No, 20, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string patientAuthorization { get; set; }

        [DTOAttribute("Crm", "Código CRM", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int crm { get; set; }

        [DTOAttribute("StateId", "Código do Estado", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int stateId { get; set; }

        [DTOAttribute("ReleaseOrderItemObservation", "Observação da Ordem de Liberação", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string releaseOrderItemObservation { get; set; }

        [DTOAttribute("ReleaseOrderItemEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int releaseOrderItemEnabled { get; set; }

        [DTOAttribute("medicineName", "Nome do Medicamento", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        public string medicineName { get; set; }

        [DTOAttribute("EAN", "EAN", DTOPrimaryKey.No, 13, DTONotNull.No, DTOParameterProcedure.No)]
        public string EAN { get; set; }

        [DTOAttribute("StateUF", "UF", DTOPrimaryKey.No, 2, DTONotNull.No, DTOParameterProcedure.No)]
        public string stateUF { get; set; }

        [DTOAttribute("LabName", "LabName", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        public string labName { get; set; }

        [DTOAttribute("Presentation", "Presentation", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        public string presentation { get; set; }

    }

    public class ListReleaseOrderItem : List<ReleaseOrderItem>
    { }
}
