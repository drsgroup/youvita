﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class OrderItem : DTODefault
    {
        [DTOAttribute("OrderItemId", "Código do Item da Ordem", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int orderItemId { get; set; }

        [DTOAttribute("OrderId", "Código da Ordem", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int orderId { get; set; }

        [DTOAttribute("ReleaseOrderItemId", "Ordem de Liberação do item", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int releaseOrderItemId { get; set; }

        [DTOAttribute("ReleaseOrderId", "Ordem de Liberação", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int releaseOrderId { get; set; }

        [DTOAttribute("MedicineId", "Código do Remédio", DTOPrimaryKey.No, 19, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int medicineId { get; set; }

        [DTOAttribute("Dosage", "Dosagem", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int dosage { get; set; }

        [DTOAttribute("Quantity", "Quantidade", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int quantity { get; set; }


        [DTOAttribute("OrderItemEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int orderItemEnabled { get; set; }



    }


    public class ListOrderItem : List<OrderItem>
    { }
}
