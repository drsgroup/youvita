﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class DTODefault
    {

        [DTOAttribute("@UserLogin", "Login", DTOPrimaryKey.No, 40,DTONotNull.Yes,DTOParameterProcedure.Yes,true)]
        public string userLogin { get; set; }

    }
}
