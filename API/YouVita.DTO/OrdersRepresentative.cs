﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class OrdersRepresentative : DTO.DTODefault
    {
        [DTOAttribute("OrdersRepresentativeID", "Codigo", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ordersRepresentativeID { get; set; }

        [DTOAttribute("RepresentativeID", "ID do Doutor", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int representativeID { get; set; }

        [DTOAttribute("Status", "Status", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string status { get; set; }

        [DTOAttribute("CreateDate", "Data da Criação", DTOPrimaryKey.No, 19, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime createDate { get; set; }

        [DTOAttribute("OrderRepresentativeDateSend", "Data do Envio", DTOPrimaryKey.No, 19, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime orderRepresentativeDateSend { get; set; }

        [DTOAttribute("OrderRepresentativeEnabled", "Ativo", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int orderRepresentativeEnabled { get; set; }

        [DTOAttribute("OrderRepresentativeAvaliableID", "OrderAvaliableID", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int orderRepresentativeAvaliableID { get; set; }

        [DTOAttribute("representativeName", "Nome", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        public string representativeName { get; set; }

        [DTOAttribute("laboratoryFantasyName", "Nome", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        public string laboratoryFantasyName { get; set; }

        [DTOAttribute("Products", "Products", DTOPrimaryKey.No, 1, DTONotNull.No, DTOParameterProcedure.No)]
        public ListProductsOrderRepresentative Products { get; set; }

    }

    public class ListOrdersRepresentative : List<OrdersRepresentative>
    { }

}