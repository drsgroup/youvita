﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ReportPatientsByClient
    {

        [DTOAttribute("clientId", "clientId", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int clientId { get; set; }

        [DTOAttribute("clientName", "clientName", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string clientName { get; set; }

        [DTOAttribute("qtyPatient", "qtyPatient", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int qtyPatient { get; set; }
    }

    public class ListReportPatientsByClient : List<ReportPatientsByClient>
    { }
}
