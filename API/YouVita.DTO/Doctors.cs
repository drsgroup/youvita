﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Doctors : DTODefault
    {
        [DTOAttribute("DoctorID", "Código", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int doctorID { get; set; }


        [DTOAttribute("DoctorName", "Nome", DTOPrimaryKey.No, 60, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string doctorName { get; set; }

        [DTOAttribute("CRM", "CRM", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string crm { get; set; }

        [DTOAttribute("UF_CRM", "UF CRM", DTOPrimaryKey.No, 2, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string uf_crm { get; set; }

        [DTOAttribute("CPF", "CPF", DTOPrimaryKey.No, 14, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string cpf { get; set; }

        [DTOAttribute("Address", "Endereço", DTOPrimaryKey.No, 60, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string address { get; set; }

        [DTOAttribute("Number", "Número", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string number { get; set; }

        [DTOAttribute("Complement", "Complemento", DTOPrimaryKey.No, 60, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string complement { get; set; }

        [DTOAttribute("City", "Cidade", DTOPrimaryKey.No, 60, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string city { get; set; }

        [DTOAttribute("Neighborhood", "Bairro", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string neighborhood { get; set; }

        [DTOAttribute("StateId", "Estado", DTOPrimaryKey.No, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int stateId { get; set; }

        [DTOAttribute("ZipCode", "CEP", DTOPrimaryKey.No, 9, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string zipCode { get; set; }

        [DTOAttribute("PhoneComercial", "Telefone Comercial", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string phoneComercial { get; set; }

        [DTOAttribute("CellPhone", "Celular", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string cellPhone { get; set; }

        [DTOAttribute("FocalPoint", "Ponto Focal", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string focalPoint { get; set; }

        [DTOAttribute("Email", "Email", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string email { get; set; }

        [DTOAttribute("Login", "Login", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string login { get; set; }

        [DTOAttribute("Password", "Senha", DTOPrimaryKey.No, 40, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string password { get; set; }

        [DTOAttribute("Comments", "Comentários", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string comments { get; set; }

        [DTOAttribute("Status", "Status", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string status { get; set; }

        [DTOAttribute("RepresentativeID", "Identificador do Representante", DTOPrimaryKey.No, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int representativeID { get; set; }

        [DTOAttribute("representativeName", "Identificador do Representante", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        public string representativeName { get; set; }

        [DTOAttribute("DoctorEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int doctorEnabled { get; set; }

        [DTOAttribute("ramal", "Ativo", DTOPrimaryKey.No, 10, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string ramal { get; set; }

        [DTOAttribute("ListDoctorAddresses", "ListDoctorAddresses", DTOPrimaryKey.No, 1, DTONotNull.No, DTOParameterProcedure.No)]
        public DTO.ListDoctorAddresses ListDoctorAddresses { get; set; }

        [DTOAttribute("laboratoryID", "laboratoryID", DTOPrimaryKey.No, 1, DTONotNull.No, DTOParameterProcedure.No)]
        public int laboratoryID { get; set; }

        [DTOAttribute("laboratoryName", "laboratoryName", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        public string laboratoryFantasyName { get; set; }

        //[DTOAttribute("stateUF", "stateUF", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        //public string stateUF { get; set; }

    }

    public class ListDoctors : List<Doctors>
    { }
}
