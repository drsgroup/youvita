﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class PharmaceuticalAttentionsMedicinesInteraction : DTODefault
    {
        [DTOAttribute("pharmaceuticalAttentionId", "pharmaceuticalAttentionId", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int pharmaceuticalAttentionId { get; set; }

        [DTOAttribute("MedicinesInteractionId", "MedicinesInteractionId", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int MedicinesInteractionId { get; set; }
    }

    public class ListPharmaceuticalAttentionsMedicinesInteraction : List<PharmaceuticalAttentionsMedicinesInteraction>
    { }
}
