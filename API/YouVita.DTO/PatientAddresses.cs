﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;


namespace YouVita.DTO
{
    public class PatientAddresses : DTODefault
    {

        [DTOAttribute("PatientAddressesId", "Código", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int PatientAddressesId { get; set; }

        [DTOAttribute("PatientId", "Código Paciente", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int PatientId { get; set; }

        [DTOAttribute("AddressTypeId", "Tipo de Endereço", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int AddressTypeId { get; set; }

        [DTOAttribute("Address", "Endereço", DTOPrimaryKey.No,60 , DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Address { get; set; }

        [DTOAttribute("AddressNumber", "Numero do Endereço", DTOPrimaryKey.No, 10, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string AddressNumber { get; set; }

        [DTOAttribute("City", "Cidade", DTOPrimaryKey.No, 50, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string City { get; set; }

        [DTOAttribute("Neighborhood", "Bairro", DTOPrimaryKey.No, 50, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Neighborhood { get; set; }

        [DTOAttribute("StateId", "Estado", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int StateId { get; set; }

        [DTOAttribute("ZipCode", "CEP", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string ZipCode { get; set; }

        [DTOAttribute("Complement", "Complemento", DTOPrimaryKey.No, 200, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Complement { get; set; }

        [DTOAttribute("PatientAddressesEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int PatientAddressesEnabled { get; set; }

    }

    public class ListPatientAddresses : List<PatientAddresses>
    { }
}
