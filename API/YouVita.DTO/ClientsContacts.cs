﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ClientsContacts : DTODefault
    {
        [DTOAttribute("ContactId","Código Contato", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ContactId { get; set; }

        [DTOAttribute("ClientId","Código Cliente", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ClientId { get; set; }

        [DTOAttribute("ContactName","Nome", DTOPrimaryKey.No, 40, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string ContactName { get; set; }

        [DTOAttribute("Email", "E-mail", DTOPrimaryKey.No, 100, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Email { get; set; }

        [DTOAttribute("Password", "Password", DTOPrimaryKey.No, 60, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Password { get; set; }

        [DTOAttribute("Occupation","Ocupação", DTOPrimaryKey.No, 40, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Occupation { get; set; }


        [DTOAttribute("Phone","Telefone", DTOPrimaryKey.No, 20, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Phone { get; set; }

        [DTOAttribute("Ramal","Ramal", DTOPrimaryKey.No, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string Ramal { get; set; }

        [DTOAttribute("CellPhone", "Telefone", DTOPrimaryKey.No, 20, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string CellPhone { get; set; }

        //[DTOAttribute("ContactTypeId","Tipo", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        //public int ContactTypeId { get; set; }

        [DTOAttribute("ClientsContactsEnabled","Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ClientsContactsEnabled { get; set; }
    }

    public class ListClientsContacts : List<ClientsContacts>
    { }
}
