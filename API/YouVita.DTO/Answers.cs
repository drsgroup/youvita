﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Answers : DTODefault
    {

        [DTOAttribute("AnswersId", "Id", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int AnswersId { get; set; }

        [DTOAttribute("QuestionsId", "Id do questionário", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Quiz { get; set; }

        [DTOAttribute("Answer", "Resposta", DTOPrimaryKey.No, 200, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Answer { get; set; }

        [DTOAttribute("NextStep", "Próximo passo", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int NextStep { get; set; }

        [DTOAttribute("QuestionsEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int QuestionsEnabled { get; set; }

        [DTOAttribute("AnswerOthers", "AnswerOthers", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int AnswerOthers { get; set; }

    }

    public class ListAnswers : List<Answers>
    { }
}
