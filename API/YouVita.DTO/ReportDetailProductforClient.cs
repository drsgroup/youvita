﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ReportDetailProductforClient
    {

        [DTOAttribute("releaseOrderDate", "releaseOrderDate", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime releaseOrderDate { get; set; }

        [DTOAttribute("patientName", "patientName", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string patientName { get; set; }

        [DTOAttribute("cpf", "cpf", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string cpf { get; set; }

        [DTOAttribute("rg", "rg", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string rg { get; set; }

        [DTOAttribute("identificationonClient", "identificationonClient", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string identificationonClient { get; set; }

        [DTOAttribute("medicineId", "medicineId", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int medicineId { get; set; }

        [DTOAttribute("medicineName", "medicineName", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string medicineName { get; set; }

        [DTOAttribute("labName", "labName", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string labName { get; set; }

        [DTOAttribute("quantity", "quantity", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int quantity { get; set; }

        [DTOAttribute("dosage", "dosage", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int dosage { get; set; }

        [DTOAttribute("clientId", "clientId", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int clientId { get; set; }

    }

    public class ListReportDetailProductforClient : List<ReportDetailProductforClient>
    { }
}
