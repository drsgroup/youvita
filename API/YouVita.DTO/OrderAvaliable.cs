﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class OrderAvaliable : DTODefault
    {
        [DTOAttribute("OrderAvaliableID", "ID", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int OrderAvaliableID { get; set; }

        [DTOAttribute("DoctorId", "ID", DTOPrimaryKey.No, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int DoctorId { get; set; }

        [DTOAttribute("startValidity", "Validade inicial", DTOPrimaryKey.No, 19, DTONotNull.No, DTOParameterProcedure.Yes)]
        public DateTime startValidity { get; set; }

        [DTOAttribute("endValidity", "Validade final", DTOPrimaryKey.No, 19, DTONotNull.No, DTOParameterProcedure.Yes)]
        public DateTime endValidity { get; set; }

        [DTOAttribute("frequence", "Frequencia", DTOPrimaryKey.No, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int frequence { get; set; }

        [DTOAttribute("dateToSend", "Data primeiro envio", DTOPrimaryKey.No, 19, DTONotNull.No, DTOParameterProcedure.Yes)]
        public DateTime dateToSend { get; set; }

        [DTOAttribute("laboratoryID", "Data primeiro envio", DTOPrimaryKey.No, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int laboratoryID { get; set; }

        [DTOAttribute("OrderAvaliableEnabled", "Data primeiro envio", DTOPrimaryKey.No, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int OrderAvaliableEnabled { get; set; }

        [DTOAttribute("ListProductsAvaliable", "ListProductsAvaliable", DTOPrimaryKey.No, 1, DTONotNull.No, DTOParameterProcedure.No)]
        public ListProductsAvaliable Items { get; set; }

        [DTOAttribute("DoctorName", "DoctorName", DTOPrimaryKey.No, 1, DTONotNull.No, DTOParameterProcedure.No)]
        public string DoctorName{ get; set; }

        [DTOAttribute("status", "status", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string status { get; set; }

        [DTOAttribute("projectId", "projectId", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public Int32 projectId { get; set; }

        [DTOAttribute("doctorAddessesId", "doctorAddessesId", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.No)]
        public Int32 doctorAddessesId { get; set; }

        [DTOAttribute("lastSend", "lastSend", DTOPrimaryKey.No, 30, DTONotNull.No, DTOParameterProcedure.No)]
        public DateTime lastSend { get; set; }

    }
    public class ListOrderAvaliable : List<OrderAvaliable>
    { }

}
