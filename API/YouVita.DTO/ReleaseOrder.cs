﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ReleaseOrder : DTODefault
    {
        [DTOAttribute("ReleaseOrderId", "Código da Ordem de Liberação", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int releaseOrderId { get; set; }

        [DTOAttribute("ReleaseOrderDate", "Data da Ordem de Liberação", DTOPrimaryKey.No, 19, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime releaseOrderDate { get; set; }

        [DTOAttribute("PatientId", "Código do Paciente", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int patientId { get; set; }

        [DTOAttribute("Dispensation", "Código de Dispensação", DTOPrimaryKey.No, 30, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string dispensation { get; set; }

        [DTOAttribute("ReleaseOrderStatus", "Status da Ordem de Liberação", DTOPrimaryKey.No, 1, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string releaseOrderStatus { get; set; }

        [DTOAttribute("ReleaseOrderEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int releaseOrderEnabled { get; set; }

        [DTOAttribute("QtdEnvios", "Ativo", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int QtdEnvios { get; set; }


        [DTOAttribute("ListReleaseOrderItems", "ListReleaseOrderItems", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.No)]
        public List<DTO.ReleaseOrderItem> ListReleaseOrderItem { get; set; }

        [DTOAttribute("PatientName", "PatientName", DTOPrimaryKey.No, 1, DTONotNull.No, DTOParameterProcedure.No)]
        public string patientName { get; set; }

        [DTOAttribute("ReleaseOrderObservation", "ReleaseOrderObservation", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string ReleaseOrderObservation { get; set; }

        [DTOAttribute("ClientName", "ClientName", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        public string clientName { get; set; }

        [DTOAttribute("ClientFantasyName", "ClientName", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        public string ClientFantasyName { get; set; }


        [DTOAttribute("CPF", "CPF", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.No)]
        public string CPF { get; set; }

    }

    public class ListReleaseOrder : List<ReleaseOrder>
    { }
}
