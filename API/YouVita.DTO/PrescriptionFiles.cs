﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class PrescriptionFiles
    {
        [DTOAttribute("PrescriptionId", "Código da Receita", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int prescriptionId { get; set; }

        [DTOAttribute("fileContent", "fileContent", DTOPrimaryKey.No, 30000, DTONotNull.No, DTOParameterProcedure.No)]
        public string fileContent { get; set; }

        [DTOAttribute("fileType", "fileType", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string fileType { get; set; }

    }

    public class ListPrescriptionFiles : List<PrescriptionFiles>
    { }
}
