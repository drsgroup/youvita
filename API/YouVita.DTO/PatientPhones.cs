﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class PatientPhones : DTODefault
    {

        [DTOAttribute("PatientPhoneId","Código", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int patientPhoneId { get; set; }

        [DTOAttribute("PatientId","Código Paciente", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int patientId { get; set; }

        [DTOAttribute("PhoneNumber","Telefone", DTOPrimaryKey.No, 12, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string phoneNumber { get; set; }

        [DTOAttribute("Ramal","Ramal", DTOPrimaryKey.No, 10, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string ramal { get; set; }

        [DTOAttribute("PhoneTypeId","Tipo", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int phoneTypeId { get; set; }

        [DTOAttribute("PatientPhonesEnabled","Ativo", DTOPrimaryKey.No, 1, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int patientPhonesEnabled { get; set; }

    }

    public class ListPatientPhones : List<PatientPhones>
    { }
}
