﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class DispensationPeriod : DTODefault
    {
        [DTOAttribute("DispensationId","Código", DTOPrimaryKey.Yes, 4, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int dispensationId { get; set; }

        [DTOAttribute("Description","Descrição", DTOPrimaryKey.No, 20, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string description { get; set; }

        [DTOAttribute("QtyDays","Qtde Dias", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int QtyDays { get; set; }

        [DTOAttribute("DispensationPeriodEnabled","Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int dispensationPeriodEnabled { get; set; }
    }

    public class ListDispensationPeriod : List<DispensationPeriod>
    { }
}
