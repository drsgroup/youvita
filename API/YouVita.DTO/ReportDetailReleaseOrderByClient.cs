﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ReportDetailReleaseOrderByClient
    {

        [DTOAttribute("releaseOrderDate", "releaseOrderDate", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime releaseOrderDate { get; set; }

        [DTOAttribute("clientId", "clientId", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int clientId { get; set; }

        [DTOAttribute("patientId", "patientId", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int patientId { get; set; }

        [DTOAttribute("patientName", "patientName", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string patientName { get; set; }

        [DTOAttribute("cpf", "cpf", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string cpf { get; set; }

        [DTOAttribute("rg", "rg", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string rg { get; set; }

        [DTOAttribute("identificationonClient", "identificationonClient", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string identificationonClient { get; set; }

    }

    public class ListReportDetailReleaseOrderByClient : List<ReportDetailReleaseOrderByClient>
    { }


}
