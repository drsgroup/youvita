﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;


namespace YouVita.DTO
{
    public class MedicineInteractionPatient : DTODefault
    {
        [DTOAttribute("MedicineInteractionPatientID", "ID", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int MedicineInteractionPatientID { get; set; }

        [DTOAttribute("PatientID", "PatientID", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int PatientID { get; set; }

        [DTOAttribute("DateInteraction", "DateInteraction", DTOPrimaryKey.No, 25, DTONotNull.No, DTOParameterProcedure.Yes)]
        public DateTime DateInteraction { get; set; }

        [DTOAttribute("Severity", "Severity", DTOPrimaryKey.No, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int Severity { get; set; }

        [DTOAttribute("MedicineInteractionEnabled", "MedicineInteractionEnabled", DTOPrimaryKey.No, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int MedicineInteractionEnabled { get; set; }

        [DTOAttribute("Comments", "Comments", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string Comments { get; set; }

        [DTOAttribute("Items", "Items", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.No)]
        public ListMedicineInteractionPatientItems Items { get; set; }

        [DTOAttribute("patientName", "patientName", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.No)]
        public string patientName { get; set; }


        [DTOAttribute("clientName", "clientName", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.No)]
        public string clientName { get; set; }

    }


    public class ListMedicineInteractionPatient : List<MedicineInteractionPatient>
    { }

}
