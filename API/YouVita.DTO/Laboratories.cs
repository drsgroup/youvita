﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Laboratories : DTODefault
    {
        [DTOAttribute("LaboratoryID", "Codigo", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int laboratoryID { get; set; }

        [DTOAttribute("LaboratoryName", "Laboratório", DTOPrimaryKey.No, 60, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string laboratoryName { get; set; }

        [DTOAttribute("LaboratoryFantasyName", "Nome Fantasia", DTOPrimaryKey.No, 60, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string laboratoryFantasyName { get; set; }

        [DTOAttribute("CNPJ", "CNPJ", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string cnpj { get; set; }

        [DTOAttribute("Address", "Endereço", DTOPrimaryKey.No, 60, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string address { get; set; }

        [DTOAttribute("Number", "Número", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string number { get; set; }

        [DTOAttribute("Neighborhood", "Bairro", DTOPrimaryKey.No, 60, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string neighborhood { get; set; }

        [DTOAttribute("City", "Cidade", DTOPrimaryKey.No, 60, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string city { get; set; }

        [DTOAttribute("StateId", "Estado", DTOPrimaryKey.No, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int stateId { get; set; }

        [DTOAttribute("Complement", "Complemento", DTOPrimaryKey.No, 60, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string complement { get; set; }

        [DTOAttribute("ZipCode", "CEP", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string zipCode { get; set; }

        [DTOAttribute("Pabx", "PABX", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string pabx { get; set; }

        [DTOAttribute("Responsible", "Responsável", DTOPrimaryKey.No, 30, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string responsible { get; set; }

        [DTOAttribute("DeptResponsible", "Departamento Responsável", DTOPrimaryKey.No, 30, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string deptResponsible { get; set; }

        [DTOAttribute("Email", "E-mail", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string email { get; set; }

        [DTOAttribute("ComercialPhone", "Telefone Comercial", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string comercialPhone { get; set; }

        [DTOAttribute("CellPhone", "Celular", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string cellPhone { get; set; }

        [DTOAttribute("Login", "Login", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string login { get; set; }

        [DTOAttribute("Password", "Celular", DTOPrimaryKey.No, 40, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string password { get; set; }

        [DTOAttribute("Status", "Status", DTOPrimaryKey.No, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int status { get; set; }

        [DTOAttribute("Comments", "Comentário", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string Comments { get; set; }

        [DTOAttribute("LaboratoryEnabled", "Celular", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int laboratoryEnabled { get; set; }

    }

    public class ListLaboratories : List<Laboratories>
    { }
}
