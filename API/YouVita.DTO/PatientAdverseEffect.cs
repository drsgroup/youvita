﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;


namespace YouVita.DTO
{
    public class PatientAdverseEffect : DTODefault
    {
                       
        [DTOAttribute("PatientAdverseEffectId", "Código", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int PatientAdverseEffectId { get; set; }

        [DTOAttribute("PatientId", "Código Paciente", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int PatientId { get; set; }

        [DTOAttribute("DateStart", "Tipo de Endereço", DTOPrimaryKey.No, 30, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime DateStart { get; set; }


        [DTOAttribute("DateEnd", "Tipo de Endereço", DTOPrimaryKey.No, 30, DTONotNull.No, DTOParameterProcedure.Yes)]
        public DateTime? DateEnd { get; set; }

        [DTOAttribute("AdverseEffectsId", "Tipo de Endereço", DTOPrimaryKey.No, 30, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public Int32 AdverseEffectsId { get; set; }

        [DTOAttribute("Comments", "Tipo de Endereço", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string Comments { get; set; }

        [DTOAttribute("AdverseEffectsDescription", "Tipo de Endereço", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.No)]
        public String AdverseEffectsDescription { get; set; }
        


    }

    public class ListPatientAdverseEffect : List<PatientAdverseEffect>
    { }
}
