﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class AddressType
    {
        [DTOAttribute("AddressTypeId", "Código", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int addressTypeId { get; set; }

        [DTOAttribute("AddressTypeDescription", "Descrição", DTOPrimaryKey.No, 40, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string addressTypeDescription { get; set; }

        [DTOAttribute("AddressTypeEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int addressTypeEnabled { get; set; }
    }

    public class ListAddressType : List<AddressType>
    { }
}
