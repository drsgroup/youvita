﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Product : DTODefault
    {
        [DTOAttribute("productID", "ProdutoID", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int productID { get; set; }

        [DTOAttribute("productName", "Produto", DTOPrimaryKey.No, 100, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string productName { get; set; }

        [DTOAttribute("productCode", "Código do produto", DTOPrimaryKey.No, 20, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string productCode { get; set; }

        [DTOAttribute("productEnabled", "Ativo", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int productEnabled { get; set; }

        [DTOAttribute("CNPJ", "Código do produto", DTOPrimaryKey.No, 20, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string CNPJ { get; set; }

        [DTOAttribute("lastSend", "Último envio", DTOPrimaryKey.No, 30, DTONotNull.Yes, DTOParameterProcedure.No)]
        public DateTime lastSend { get; set; }

    }    

    public class ListProducts : List<Product>
    { }
}
