﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class States : DTODefault
    {
        [DTOAttribute("StateId","Código", DTOPrimaryKey.Yes, 4, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int stateId { get; set; }

        [DTOAttribute("StateName","Nome", DTOPrimaryKey.No, 50, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string stateName { get; set; }

        [DTOAttribute("StateUF","UF", DTOPrimaryKey.No, 2, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string stateUF { get; set; }

        [DTOAttribute("StateEnabled","Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int stateEnabled { get; set; }
    }

    public class ListStates : List<States>
    { }
}
