﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ReportOrderDoctor : DTODefault
    {

        [DTOAttribute("sku", "sku", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public string sku { get; set; }

        [DTOAttribute("ProductName", "ProductName", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public string ProductName { get; set; }

        [DTOAttribute("Qty", "Qty", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public Int32 Qty { get; set; }

        [DTOAttribute("OrderDoctorDateSend", "OrderDoctorDateSend", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public DateTime OrderDoctorDateSend { get; set; }

        [DTOAttribute("RepresentativeName", "RepresentativeName", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public string RepresentativeName { get; set; }

        [DTOAttribute("DoctorName", "DoctorName", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public string DoctorName { get; set; }

        [DTOAttribute("Address", "Address", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public string Address { get; set; }

        [DTOAttribute("AddressNumber", "AddressNumber", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public string AddressNumber { get; set; }

        [DTOAttribute("City", "City", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public string City { get; set; }

        [DTOAttribute("Neighborhood", "Neighborhood", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public string Neighborhood { get; set; }

        [DTOAttribute("ZipCode", "ZipCode", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public string ZipCode { get; set; }

        [DTOAttribute("Complement", "Complement", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public string Complement { get; set; }

        [DTOAttribute("StateName", "StateName", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public string StateName { get; set; }

        [DTOAttribute("Status", "Status", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public string Status { get; set; }

        [DTOAttribute("LaboratoryId", "LaboratoryId", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public Int32 LaboratoryId { get; set; }

        [DTOAttribute("RepresentativeId", "RepresentativeId", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public Int32 RepresentativeId { get; set; }

        [DTOAttribute("DoctorId", "DoctorId", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public Int32 DoctorId { get; set; }


        [DTOAttribute("OrderDoctorID", "OrderDoctorID", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public Int32 OrderDoctorID { get; set; }

        [DTOAttribute("LaboratoryFantasyName", "LaboratoryFantasyName", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public string LaboratoryFantasyName { get; set; }

        [DTOAttribute("Type", "Type", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.No)]
        public string Type { get; set; }

    }

    public class ListReportOrderDoctor : List<ReportOrderDoctor>
    { }


}


