﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class MedicinesPMC
    {

        [DTOAttribute("MedicienPMCId", "Código", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int MedicienPMCId { get; set; }

        [DTOAttribute("MedicineId", "Código do Medicamento", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int MedicineId { get; set; }

        [DTOAttribute("Value", "Valor", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public float Value { get; set; }

        [DTOAttribute("Aliquot", "Aliquota", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public float Aliquot { get; set; }

        [DTOAttribute("StartDate", "Data Inicio", DTOPrimaryKey.No, 19, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime StartDate { get; set; }

        [DTOAttribute("EndDate", "Data Fim", DTOPrimaryKey.No, 19, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime EndDate { get; set; }

        [DTOAttribute("MedicinesPMCEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int MedicinesPMCEnabled { get; set; }

    }

    public class ListMedicinesPMC : List<MedicinesPMC>
    { }
}
