﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Researches : DTODefault
    {
        [DTOAttribute("ResearchesId", "Id", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ResearchesId { get; set; }

        [DTOAttribute("CreateDate", "Data Criação", DTOPrimaryKey.No, 19, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime CreateDate { get; set; }

        [DTOAttribute("PatientId", "Id paciente", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int PatientId { get; set; }

        //[DTOAttribute("QuestionsId", "Id Perguntas", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        //public int QuestionsId { get; set; }

        [DTOAttribute("QuestionnairesId", "Id questionarios", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int QuestionnairesId { get; set; }

        //[DTOAttribute("Answer", "Resposta", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.Yes)]
        //public string Answer { get; set; }

        [DTOAttribute("ResearchesEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ResearchesEnabled { get; set; }

        [DTOAttribute("ListQuestionnaireResponses", "ListQuestionnaireResponses", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.No)]
        public DTO.ListQuestionnaireResponses ListQuestionnaireResponses { get; set; }

        [DTOAttribute("PatientName", "Nome do Paciente", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string PatientName { get; set; }

        [DTOAttribute("Quiz", "Questionário", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string Quiz { get; set; }
    }

    public class ListResearches : List<Researches>
    { }
}
