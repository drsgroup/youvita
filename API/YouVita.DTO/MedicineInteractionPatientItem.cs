﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class MedicineInteractionPatientItem
    {
        [DTOAttribute("MedicineInteractionPatientItemID", "ID", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int MedicineInteractionPatientItemID { get; set; }

        [DTOAttribute("InteractionMedicineID", "ID", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int InteractionMedicineID { get; set; }

        [DTOAttribute("EAN", "EAN", DTOPrimaryKey.No, 20, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string EAN { get; set; }

        [DTOAttribute("MedicineName", "MedicineName", DTOPrimaryKey.No, 20, DTONotNull.Yes, DTOParameterProcedure.No)]
        public string MedicineName { get; set; }

        [DTOAttribute("ActivePrincipleName", "ActivePrincipleName", DTOPrimaryKey.No, 20, DTONotNull.Yes, DTOParameterProcedure.No)]
        public string ActivePrincipleName { get; set; }

    }

    public class ListMedicineInteractionPatientItems : List<MedicineInteractionPatientItem>
    { }
}
