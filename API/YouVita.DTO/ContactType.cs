﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class ContactType : DTODefault
    {
        [DTOAttribute("ContactTypeId","Tipo", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int contactTypeId { get; set; }

        [DTOAttribute("ContactDescription","Descrição", DTOPrimaryKey.No, 40, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string contactDescription { get; set; }

        [DTOAttribute("ContactTypeEnabled","Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int contactTypeEnabled { get; set; }
    }

    public class ListContactType : List<ContactType>
    { }
}
