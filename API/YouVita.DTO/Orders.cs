﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Orders : DTODefault
    {
        [DTOAttribute("OrderId", "Código da Ordem", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int orderId { get; set; }

        [DTOAttribute("ReleaseOrderId", "Ordem de Liberação", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int releaseOrderId { get; set; }

        [DTOAttribute("OrderDate", "Data da Ordem", DTOPrimaryKey.No, 19, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime orderDate { get; set; }

        [DTOAttribute("PatientId", "Código do Paciente", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int patientId { get; set; }

        [DTOAttribute("PatientAddressId", "Código do Endereço do Paciente", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int patientAddressId { get; set; }

        [DTOAttribute("PatientAuthorization", "Autorização do Paciente", DTOPrimaryKey.No, 20, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string patientAuthorization { get; set; }

        [DTOAttribute("OrderStatus", "Status da Ordem", DTOPrimaryKey.No, 1, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string orderStatus { get; set; }

        [DTOAttribute("OrderObservation", "Observação", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string orderObservation { get; set; }

        [DTOAttribute("ListOrderItems", "ListOrderItems", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.No)]
        public List<DTO.OrderItem> ListOrderItems { get; set; }

        [DTOAttribute("OrdersEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ordersEnabled { get; set; }
    }

    public class ListOrders : List<Orders>
    { }
}
