﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class MedicineInteractionPatientResults : DTODefault
    {
        [DTOAttribute("MedicineInteractionPatientResultsID", "ID", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public Int32 MedicineInteractionPatientResultsID { get; set; }

        [DTOAttribute("MedicineInteractionPatientItemId", "ID", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public Int32 MedicineInteractionPatientItemId { get; set; }

        [DTOAttribute("DateInteraction", "ID", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime DateInteraction { get; set; }

        [DTOAttribute("ActivePrinciple1", "ID", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public Int32 ActivePrinciple1 { get; set; }

        [DTOAttribute("ActivePrinciple2", "ID", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public Int32 ActivePrinciple2 { get; set; }

        [DTOAttribute("Result", "ID", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Result { get; set; }

        [DTOAttribute("Severity", "ID", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public Int32 Severity { get; set; }

        [DTOAttribute("ean1", "ID", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string ean1 { get; set; }

        [DTOAttribute("ean2", "ID", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string ean2 { get; set; }

        [DTOAttribute("Medicine1", "ID", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Medicine1 { get; set; }

        [DTOAttribute("Medicine2", "ID", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Medicine2 { get; set; }
    }

    public class ListMedicineInteractionPatientResults : List<MedicineInteractionPatientResults>
    { }
}
