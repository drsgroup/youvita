﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Questionnaires
    {
        [DTOAttribute("QuestionnairesId", "Id", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int QuestionnairesId { get; set; }

        [DTOAttribute("Quiz", "Questionário", DTOPrimaryKey.No, 200, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string Quiz { get; set; }

        [DTOAttribute("QuestionsEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int QuestionsEnabled { get; set; }
    }

    public class ListQuestionnaires : List<Questionnaires>
    { }
}
