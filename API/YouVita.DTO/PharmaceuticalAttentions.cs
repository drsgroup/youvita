﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class PharmaceuticalAttentions : DTODefault
    {
        [DTOAttribute("pharmaceuticalAttentionId", "pharmaceuticalAttentionId", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int pharmaceuticalAttentionId { get; set; }

        [DTOAttribute("patientId", "patientId", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int patientId { get; set; }

        [DTOAttribute("dateAttention", "dateAttention", DTOPrimaryKey.No, 30, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public DateTime dateAttention { get; set; }

        [DTOAttribute("observation", "observation", DTOPrimaryKey.No, 800, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string observation { get; set; }

        [DTOAttribute("status", "observation", DTOPrimaryKey.No, 800, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string status { get; set; }

        [DTOAttribute("PatientName", "PatientName", DTOPrimaryKey.No, 800, DTONotNull.No, DTOParameterProcedure.No)]
        public string PatientName { get; set; }

        [DTOAttribute("CPF", "CPF", DTOPrimaryKey.No, 800, DTONotNull.No, DTOParameterProcedure.No)]
        public string CPF { get; set; }
    }

    public class ListPharmaceuticalAttentions : List<PharmaceuticalAttentions>
    { }
}
