﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class DoctorAddresses : DTODefault
    {
        [DTOAttribute("DoctorAddressesId", "Código", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int doctorAddressesId { get; set; }

        [DTOAttribute("DoctorId", "Código Doutor", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int doctorId { get; set; }

        [DTOAttribute("AddressTypeId", "Tipo de Endereço", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int addressTypeId { get; set; }

        [DTOAttribute("Address", "Endereço", DTOPrimaryKey.No, 60, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string address { get; set; }

        [DTOAttribute("AddressNumber", "Numero do Endereço", DTOPrimaryKey.No, 10, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string addressNumber { get; set; }

        [DTOAttribute("City", "Cidade", DTOPrimaryKey.No, 50, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string city { get; set; }

        [DTOAttribute("Neighborhood", "Bairro", DTOPrimaryKey.No, 50, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string neighborhood { get; set; }

        [DTOAttribute("StateId", "Estado", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int stateId { get; set; }

        [DTOAttribute("ZipCode", "CEP", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string zipCode { get; set; }

        [DTOAttribute("Complement", "Complemento", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string complement { get; set; }

        [DTOAttribute("DoctorAddressesEnabled", "Ativo", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int doctorAddressesEnabled { get; set; }

        [DTOAttribute("stateName", "stateName", DTOPrimaryKey.No, 20, DTONotNull.Yes, DTOParameterProcedure.No)]
        public string stateName { get; set; }


        [DTOAttribute("defaultAddress", "defaultAddress", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string defaultAddress { get; set; }

    }

    public class ListDoctorAddresses : List<DoctorAddresses>
    { }
}

