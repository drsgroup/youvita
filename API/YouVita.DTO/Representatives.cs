﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Representatives : DTODefault
    {
        [DTOAttribute("RepresentativeID", "Codigo", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int representativeID { get; set; }

        [DTOAttribute("RepresentativeName", "Nome", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string representativeName { get; set; }

        [DTOAttribute("Email", "E-mail", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string email { get; set; }

        [DTOAttribute("Login", "Login", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string login { get; set; }

        [DTOAttribute("Password", "Senha", DTOPrimaryKey.No, 40, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string password { get; set; }

        [DTOAttribute("Status", "Status", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string status { get; set; }

        [DTOAttribute("Comments", "Comentário", DTOPrimaryKey.No, 200, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string comments { get; set; }

        [DTOAttribute("RepresentativeEnabled", "Ativo", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int representativeEnabled { get; set; }

        [DTOAttribute("laboratoryID", "laboratoryID", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int laboratoryID { get; set; }

        [DTOAttribute("cpf", "cpf", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string cpf { get; set; }

        [DTOAttribute("Address", "Endereço", DTOPrimaryKey.No, 60, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string address { get; set; }

        [DTOAttribute("addressNumber", "Número", DTOPrimaryKey.No, 10, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string addressNumber { get; set; }

        [DTOAttribute("Complement", "Complemento", DTOPrimaryKey.No, 60, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string complement { get; set; }

        [DTOAttribute("City", "Cidade", DTOPrimaryKey.No, 60, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string city { get; set; }

        [DTOAttribute("Neighborhood", "Bairro", DTOPrimaryKey.No, 50, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string neighborhood { get; set; }

        [DTOAttribute("StateId", "Estado", DTOPrimaryKey.No, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int stateId { get; set; }

        [DTOAttribute("ZipCode", "CEP", DTOPrimaryKey.No, 9, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string zipCode { get; set; }

        [DTOAttribute("PhoneComercial", "Telefone Comercial", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string phoneComercial { get; set; }

        [DTOAttribute("CellPhone", "Celular", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string cellPhone { get; set; }

        [DTOAttribute("ramal", "Celular", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string ramal { get; set; }

        [DTOAttribute("laboratoryName", "Celular", DTOPrimaryKey.No, 200, DTONotNull.Yes, DTOParameterProcedure.No)]
        public string laboratoryName { get; set; }

        [DTOAttribute("laboratoryFantasyName", "Celular", DTOPrimaryKey.No, 200, DTONotNull.Yes, DTOParameterProcedure.No)]
        public string laboratoryFantasyName { get; set; }

        

    }

    public class ListRepresentatives : List<Representatives>
    { }
}
