﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class PharmaceuticalAttentionsResearches : DTODefault
    {
        [DTOAttribute("pharmaceuticalAttentionId", "pharmaceuticalAttentionId", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int pharmaceuticalAttentionId { get; set; }

        [DTOAttribute("ResearchesId", "ResearchesId", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ResearchesId { get; set; }
    }

    public class ListPharmaceuticalAttentionsResearches : List<PharmaceuticalAttentionsResearches>
    { }
}
