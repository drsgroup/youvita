﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class LogServices : DTODefault
    {

        [DTOAttribute("LogServiceId", "Codigo", DTOPrimaryKey.Yes, 8, DTONotNull.No, DTOParameterProcedure.Yes)]
        public int LogServiceId { get; set; }

        [DTOAttribute("LogServiceDate", "Codigo", DTOPrimaryKey.No, 20, DTONotNull.No, DTOParameterProcedure.Yes)]
        public DateTime LogServiceDate { get; set; }

        [DTOAttribute("LogServiceName", "Codigo", DTOPrimaryKey.No, 100, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string LogServiceName { get; set; }

        [DTOAttribute("LogServiceDescription", "Codigo", DTOPrimaryKey.No, 2000, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string LogServiceDescription { get; set; }

        [DTOAttribute("LogServiceComplement", "Codigo", DTOPrimaryKey.No, 2000, DTONotNull.No, DTOParameterProcedure.Yes)]
        public string LogServiceComplement { get; set; }

    }


    public class ListLogServices : List<LogServices>
    { }
}
