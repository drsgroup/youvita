﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.Framework.DTO;

namespace YouVita.DTO
{
    public class Project : DTODefault
    {

        [DTOAttribute("ProjectId", "Id", DTOPrimaryKey.Yes, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int projectId { get; set; }

        [DTOAttribute("ProjectName", "ProjectName", DTOPrimaryKey.No, 100, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string ProjectName { get; set; }

        [DTOAttribute("LaboratoryId", "Laboratório", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int laboratoryId { get; set; }

        [DTOAttribute("ProjectEnabled", "ProjectEnabled", DTOPrimaryKey.No, 8, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public int ProjectEnabled { get; set; }

        [DTOAttribute("ListProducts", "ListProducts", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.No)]
        public ListProjectProduct listProjectProduct { get; set; }

        [DTOAttribute("ListProjectDoctor", "ListProjectDoctor", DTOPrimaryKey.No, 1, DTONotNull.Yes, DTOParameterProcedure.No)]
        public ListProjectDoctor listProjectDoctor { get; set; }

        [DTOAttribute("ProjectStockType", "ProjectStockType", DTOPrimaryKey.No, 40, DTONotNull.Yes, DTOParameterProcedure.Yes)]
        public string ProjectStockType { get; set; }
    }

    public class ListProject : List<Project>
    { }
}
