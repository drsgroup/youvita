﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouVita.Framework
{
    public static class Application
    {
        //public static readonly string connectionString = @"Persist Security Info=False;User ID=sa;Initial Catalog=YouVita;Data Source=NOTE-KILL\SQLEXPRESS;PassWord=kill;Pooling=false";
        //public static readonly string connectionString = @"Persist Security Info=False;User ID=youvita;Initial Catalog=YOUVITA_hmlg;Data Source=10.0.0.3;PassWord=Y0uV!t@;Pooling=false;";
        //public static readonly string connectionString = @"Persist Security Info=False;User ID=youvita;Initial Catalog=YOUVITA_hmlg;Data Source=10.0.0.49;PassWord=Y0uV!t@;Pooling=false;";
        public static readonly string connectionString = @"Persist Security Info=False;User ID=youvita;Initial Catalog=YOUVITA;Data Source=10.0.0.49;PassWord=Y0uV!t@;Pooling=false;";

        //public static readonly string connectionString = @"Persist Security Info=False;User ID=youvita_read;Initial Catalog=YOUVITA;Data Source=10.0.0.3;PassWord=Youvit@2020;Pooling=false;";



        //public static readonly string connectionString = @"Server=RAFA\SQLEXPRESS;Database=YouVita;Trusted_Connection=True;pooling=false;";
        //public static readonly string connectionString = @"Persist Security Info=False;User ID=YouVitaHML;Initial Catalog=YouVitaHML;Data Source=198.71.225.146;PassWord=Y0uVit@HML;Pooling=false";
        //public static readonly string connectionString = @"Persist Security Info=False;User ID=youvitaHML;Initial Catalog=YouVita;Data Source=198.71.225.146;PassWord=Y0uVit@HML;Pooling=false";
        //public static readonly string connectionString = @"Data Source=sql.freeasphost.net\MSSQL2016;Initial Catalog=euclidesbezerra_YouVita;User ID=euclidesbezerra;PassWord=youvita;Pooling=false;";
        //public static readonly string connectionString = @"Data Source=198.71.225.146;Initial Catalog=youvita;User ID=youvita;PassWord=YouVit@@;Pooling=false;";
        public static readonly string name = "YouVita";

        public static readonly string DTOWebContractStatusOK = "OK";
        public static readonly string DTOWebContractStatusNOK = "NOK";

        public static readonly string DTOStartNameProcedureInsert = "spIns_";
        public static readonly string DTOStartNameProcedureUpdate = "spUpd_";
        public static readonly string DTOStartNameProcedureDelete = "spDel_";

        public static readonly string TokenAPINameConfig = "Token-APIFrontEnd";
        public static readonly string TokenServiceNameConfig = "YouVitaService|Y0uv1t4";
        public static readonly string TokenValidate = "ValidateToken";

        public static readonly int DTODefaultDataEnabledValue = 1;
        public static readonly int DTODefaultDataDisabledValue = 0;
    }
}
