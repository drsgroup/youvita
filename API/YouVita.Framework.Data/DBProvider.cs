﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace YouVita.Framework.Data
{
    public class DBProvider
    {

        public DBProvider()
        {
            openDatabase();
        }
       

        #region Private Methods

        private System.Data.SqlClient.SqlConnection connection;
        
        private void openDatabase()
        {
            try
            {
                connection = new System.Data.SqlClient.SqlConnection();
                connection.ConnectionString = YouVita.Framework.Application.connectionString;
                connection.Open();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void closeDatabase()
        {
            try
            {
                connection.Close();
                connection.Dispose();
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        #endregion



        #region Public Methods

        public DataTable getDatatable(string querySQL)
        {
            try
            {
                DataTable dtbreturn = new DataTable();
                openDatabase();
                SqlDataAdapter dta = new SqlDataAdapter(querySQL, connection);
                dta.Fill(dtbreturn);
                dta.Dispose();
                closeDatabase();
                return dtbreturn;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public DataSet getDataset(string querySQL)
        {
            try
            {
                DataSet dtsreturn = new DataSet();
                openDatabase();
                SqlDataAdapter dta = new SqlDataAdapter(querySQL, connection);
                dta.Fill(dtsreturn);
                dta.Dispose();
                closeDatabase();
                return dtsreturn;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void executeQuery(string querySQL)
        {
            try
            {
                openDatabase();
                SqlCommand comm = new SqlCommand(querySQL, connection);
                comm.ExecuteNonQuery();
                comm.Dispose();
                closeDatabase();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void executeCommand(SqlCommand commandQuery)
        {
            try
            {
                openDatabase();
                commandQuery.Connection = connection;
                commandQuery.ExecuteNonQuery();
                closeDatabase();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void executeCommand(ref SqlCommand commandQuery)
        {
            try
            {
                openDatabase();
                commandQuery.Connection = connection;
                commandQuery.ExecuteNonQuery();
                closeDatabase();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion
    }
}
