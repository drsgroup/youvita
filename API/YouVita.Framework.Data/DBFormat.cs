﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace YouVita.Framework.Data
{
    public static class DBFormat
    {

        public const string typeValueString = "String";
        public const string typeValueDatetime = "DateTime";
        public const string typeValueInteger = "Integer";
        public const string typeValueDouble = "Double";


        public static string toFormat(string valueString, string typeString)
        {

            valueString = valueString.Replace("'", "");

            if (typeString == "System.Int32" || typeString == typeValueInteger)
            {
                
                if (valueString == "")
                {
                    valueString = "0";
                }
            }

            if (typeString == "System.Double" || typeString == typeValueDouble)
            {
                if (valueString == "")
                {
                    valueString = "0";
                }
                else
                {
                    valueString = valueString.Replace(".", "");    
                    valueString = valueString.Replace(",", ".");    
                }
            }


            if (typeString == "System.DateTime" || typeString == typeValueDatetime)
            {
                if (valueString == "" || valueString.Substring(0,10) == "01/01/0001")
                {
                    valueString = "'19000101 00:00:00'";
                }
                else
                {
                    valueString = "'" + Convert.ToDateTime(valueString).ToString("yyyyMMdd HH:mm:ss") + "'";
                }
            }

            if (typeString == "System.String" || typeString == typeValueString)
            {
                valueString = "'" + valueString + "'";
            }

            return valueString;
        }


    }
}
