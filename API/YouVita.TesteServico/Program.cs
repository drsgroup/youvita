﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouVita.WinService;

namespace YouVita.TesteServico
{
    class Program
    {
        static void Main(string[] args)
        {
            //Simular geracao dos pedidos
            new YouVitaServiceProcess().GenerateFile();

            //simular o erro
            new YouVitaServiceProcess().GetReturn();


            //simular a importacao de produtos
            //new YouVitaServiceProcess().ImportFile();


        }
    }
}
