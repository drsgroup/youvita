import { LoginService } from '../../../services/negocio/login/login.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatProgressBar, MatButton, MatSnackBar } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Md5 } from "md5-typescript";


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  @ViewChild(MatProgressBar) progressBar: MatProgressBar;
  @ViewChild(MatButton) submitButton: MatButton;
  dataformatada: string;

  signinForm: FormGroup;

  constructor(
    private _router: Router,
    private _login: LoginService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    localStorage.clear();
    this.signinForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', Validators.required),
      rememberMe: new FormControl(false)
    });
  }

  signin() {
    const signinData = this.signinForm.value;

    this._login.FazerLogin(signinData).subscribe(res => {
      if (res.Status == "OK") {
        if (res.ObjectDTO.length > 0) {
          let user: any;
          user = new Object();
          user = signinData;          
          user.password="";
          user.userType = res.ObjectDTO[0].userType;
          user.ClientId = res.ObjectDTO[0].ClientId;
          user.laboratoryID = res.ObjectDTO[0].laboratoryID;
          localStorage.setItem("UsuarioAtual", JSON.stringify(user));
          
          switch (user.userType) {
            case 1:
              this._router.navigate(['/home']);              
              break;
            case 3: //laboratorio
              this._router.navigate(['/cadastros/homelab']);              
              break;      
            case 4: //medico
              this._router.navigate(['/cadastros/homemedico']);                            
              break;
              case 5: //medico
              this._router.navigate(['/home']);                            
              break;              
          }         
        }
        else {
          this.snackBar.open(res.Message, "Usuário ou senha inválido.", { duration: 4000 });
          this.signinForm.reset();
          this.progressBar.mode = 'determinate';
        }
      } else {
        this.snackBar.open(res.Message, "", { duration: 4000 });
        this.signinForm.reset();
        this.progressBar.mode = 'determinate';
      }


    });


    this.submitButton.disabled = true;
    this.progressBar.mode = 'indeterminate';

  }

}






