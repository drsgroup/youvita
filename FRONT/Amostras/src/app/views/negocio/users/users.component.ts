import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar, MatDialogRef } from '@angular/material';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { PatientsService } from 'app/services/negocio/patients/patients.service';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';
import { LoginService } from 'app/services/negocio/login/login.service';
import { PacientesFormularioComponent } from '../pacientes/pacientes-formulario/pacientes-formulario.component';
import { UsersFormComponent } from './users-form/users-form.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  colunas = [
    {
      Propriedade: 'userId',
      Titulo: 'ID Usuário',
      Visivel: true,
      Largura : 50
    },
    {
      Propriedade: 'userName',
      Titulo: 'Nome',
      Visivel: true,
      Largura : 200
    },
    {
      Propriedade: 'userLoginName',
      Titulo: 'Login',
      Visivel: true,
      Largura : 100
    },    
    {
      Propriedade: 'laboratoryFantasyName',
      Titulo: 'Laboratório',
      Visivel: true, 
      Largura : 300
    },
  ];

  linhas = [];
  user : any;

  constructor(private dialog: MatDialog,
    private confirm: AppConfirmService,
    private loader: AppLoaderService,
    private snackBar: MatSnackBar,
    private crud : CRUDService, 
    private appInformationService: AppInformationService,
    private loginService : LoginService
  ) { }

  ngOnInit() {    
    this.getUsers("");
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Novo Usuairo" ;
    } else {
      titulo = "Editar Usuário: " + dados.userId;
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(UsersFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.getUsers("");
        
        return;
      });
  }

  getUsers(parametro : string){
    this.user = this.loginService.getNomeUsuarioDetalhes();
    let params : any = new Object();
    params.userLogin = "";
    params.laboratoryID = this.user.laboratoryID;
    params.userName = parametro;
    this.crud.GetParams(params, "/user").subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO;
    }); 
  }

  deleteUser(a){
    
  }
}
