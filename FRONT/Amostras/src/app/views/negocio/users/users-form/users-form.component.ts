import { LoginService } from './../../../../services/negocio/login/login.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { WebContract } from 'app/models/base/Contrato';
import { Md5 } from 'md5-typescript';

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.css']
})
export class UsersFormComponent implements OnInit {
  userForm: FormGroup;
  listLabs = [];

  constructor
    (
      @Inject(MAT_DIALOG_DATA) public data: any,
      public dialogRef: MatDialogRef<UsersFormComponent>,
      public crud : CRUDService,
      public information : AppInformationService,
      public loader : AppLoaderService,
      public login : LoginService,
      public snackBar : MatSnackBar
    ) { }

  ngOnInit() {
    this.montarForm(this.data.payload);
  }

  montarForm(item) {
    this.getLabs();
    this.userForm = new FormGroup({
      userId : new FormControl(item.userId),
      userName: new FormControl(item.userName, [Validators.required]),
      userLoginName: new FormControl(item.userLoginName, [Validators.required]),
      userPassword: new FormControl("", [Validators.required]),
      userConfirmPassword: new FormControl("", [Validators.required]),
      userType: new FormControl("3"),
      laboratoryID: new FormControl(item.laboratoryID, [Validators.required])
    });

    if (!this.data.novo){
      this.userForm.controls['userLoginName'].disable();
      this.userForm.controls['laboratoryID'].disable();
    }
  }

  getLabs(){
    this.crud.List("", "/laboratory").subscribe(res => {
      this.listLabs =[];
      this.listLabs = res.ObjectDTO;
    });
  }

  IsValid(form: any) {
    if (form.userPassword !== form.userConfirmPassword) {
      this.information.information("YouVita", "Confirmação de senha incorreta.");
      return false;
    }
    return true;

  }

  SaveUser() {
    this.loader.open();
    let formulario = this.userForm.value;
    if (this.data.novo) {
      this.login.IsExists(formulario.userLoginName).subscribe(res => {
        if (res.Status == "OK" && res.ObjectDTO.length > 0) {
          this.information.information("YouVita", "Este login já existe, por favor informar um diferente");
          this.loader.close();
          return false;
        }
        if (this.IsValid(formulario)) {
          formulario.userPassword = Md5.init(formulario.userPassword);          
          this.crud.Save(formulario, this.data.novo, "/user").subscribe(res => {
            var cnt = new WebContract();
            cnt = res;
            if (cnt.Status == "OK") {
              this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });
            } else {
              this.snackBar.open(cnt.Message, "", { duration: 3000 });
            }
            this.dialogRef.close("OK");
            this.loader.close();
          });
        }
        this.loader.close();
      });
    } else {
      if (this.IsValid(formulario)) {
        
        if (formulario.userPassword != "*****") {
          formulario.userPassword = Md5.init(formulario.userPassword);
        }else{
          formulario.userPassword =null;
        }
        formulario.userLoginName = this.data.payload.userLoginName;

        this.crud.Save(formulario, this.data.novo, "/user").subscribe(res => {
          var cnt = new WebContract();
          cnt = res;
          if (cnt.Status == "OK") {
            this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });
          } else {
            this.snackBar.open(cnt.Message, "", { duration: 3000 });
          }
          this.dialogRef.close("OK");
          this.loader.close();
        });
      }
      this.loader.close();

    }
  }  

}
