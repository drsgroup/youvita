import { OrdersDetailComponent } from './orders-detail/orders-detail.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { LoginService } from 'app/services/negocio/login/login.service';
import { DoctorOrderDetailComponent } from '../doctor/doctor-orders/doctor-order-detail/doctor-order-detail.component';
import { Router } from '@angular/router';
import { GradeComponent } from 'app/components/common/grade/grade.component';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  colunas = [ 
    {
      Propriedade: 'OrderAvaliableID',
      Titulo: 'ID Pedido',
      Visivel: true,
      Largura: 40
    }, 
    {
      Propriedade: 'DoctorName',
      Titulo: 'Médico ',
      Visivel: true,
      Largura : 200
    },
    {
      Propriedade: 'startValidity',
      Titulo: 'Início Vigência',
      Visivel: true,
      Largura : 100,
      Tipo : "DATA"
    },
    {
      Propriedade: 'endValidity',
      Titulo: 'Fim Vigência',
      Visivel: true,
      Largura : 100,
      Tipo: "DATA"
    }    
  ];

  linhas = [];  
  addPedido : boolean =false;
  
  @ViewChild("grade") grd: GradeComponent;

  constructor(private dialog: MatDialog,
    private crudServices : CRUDService,
    private login : LoginService,
    private router : Router) { }

  ngOnInit() {
    let user : any = this.login.getNomeUsuarioDetalhes();
    if (user.userType == 5){
      this.grd.BtnIncluir = false;
    }else{
      this.grd.BtnIncluir = true;
    }  
    this.consultarPedido("");
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo = "Resumo do pedido";
    
    let dialogRef: MatDialogRef<any> = this.dialog.open(OrdersDetailComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        //this.consultarCliente("");
        this.consultarPedido("");
        return;
      });
  }

  consultarPedido(pa){
    let userLogged = this.login.getNomeUsuarioDetalhes();
    if (userLogged.userType == 5){ //busca a partir do representante
      let params : any = new Object();
      params.userLogin = "teste";
      params.representativeLogin = userLogged.username;
      params.status = "A"; //aprovado
      params.doctor =pa;     
  
      this.crudServices.GetParams(params, "/orderavaliable").subscribe(res => {
        this.linhas = [];
        this.linhas = res.ObjectDTO;
      })
    }
    
    if (userLogged.userType == 4){ //busca a partir do médico
      let params : any = new Object();
      params.userLogin = "teste";
      params.loginDoctor = userLogged.username;
      params.status = "A"; //aprovado
  
      this.crudServices.GetParams(params, "/orderavaliable").subscribe(res => {
        this.linhas = [];
        this.linhas = res.ObjectDTO;
      })
    }    
  }

  deletePedido(pa){

  }

}
