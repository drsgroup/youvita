import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ReleaseOrderFormComponent } from './release-order-form/release-order-form.component';
import { ReleaseOrderService } from 'app/services/negocio/releaseOrder/release-order.service';


@Component({
  selector: 'app-release-order',
  templateUrl: './release-order.component.html',
  styleUrls: ['./release-order.component.css']
})
export class ReleaseOrderComponent implements OnInit {

  private releaseOrderForm : FormGroup;

  constructor(
    private dialog: MatDialog,
    private releaseOrderService : ReleaseOrderService,
    private loader: AppLoaderService,
    private confirm: AppConfirmService,
    private snackBar: MatSnackBar,
    private appInformationService: AppInformationService    
    ) { }
    

  colunas = [
    {
      Propriedade: 'releaseOrderId',
      Titulo: 'ID Ordem de Liberação',
      Visivel: true,
      Largura : 50
    },
    {
      Propriedade: 'releaseOrderDate',
      Titulo: 'Data da Ordem',
      Visivel: true,
      Largura : 80,
      Tipo : 'DATA'
    },
    {
      Propriedade: 'patientName',
      Titulo: 'Nome do Paciente',
      Visivel: true,
      Largura : 300
    },
    {
      Propriedade: 'patientAuthorization',
      Titulo: 'Autorização do Paciente',
      Visivel: true,
      Largura : 200
    }
  ];

  linhas = [];


  ngOnInit() {
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Novo Pedido do Paciente";
    } else {
      titulo = "Visualização do Pedido : "+dados.releaseOrderId;
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(ReleaseOrderFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.consultarReleaseOrder("")
        return;
      });
  }

  consultarReleaseOrder(parametro : string) {
    this.releaseOrderService.getReleaseOrderByFilter(parametro).subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO      
    })
  } 

  deleteReleaseOrder(parametro: any) {
    this.confirm.confirm("Desativação do Pedido", "Tem certeza que deseja desativar o pedido " + parametro.releaseOrderId + "?").subscribe(result => {
      if (result === true) {
        this.loader.open("Excluindo pedido");
        this.releaseOrderService.deleteReleaseOrder(parametro).subscribe(res => {
          if (res.Status == "OK") {
            this.snackBar.open("Pedido desativado com sucesso!", "", { duration: 3000 });
            this.consultarReleaseOrder("");
          }
          else {
            this.appInformationService.information("YouVita", res.Message);
          }
          this.loader.close();
        })
      }
    })
  }

}
