import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReleaseOrderFormComponent } from './release-order-form.component';

describe('ReleaseOrderFormComponent', () => {
  let component: ReleaseOrderFormComponent;
  let fixture: ComponentFixture<ReleaseOrderFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReleaseOrderFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReleaseOrderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
