import { CRUDService } from './../../../../services/negocio/CRUDService/CRUDService';
import { AppInformationService } from './../../../../services/dialogs/app-information/app-information.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSelect, MatOption, MatSnackBar } from '@angular/material';
import { stringify } from '@angular/core/src/util';
import { ARIA_DESCRIBER_PROVIDER } from '@angular/cdk/a11y';
import { PatientsService } from 'app/services/negocio/patients/patients.service';
import { LoginService } from 'app/services/negocio/login/login.service';


@Component({
  selector: 'app-release-order-form',
  templateUrl: './release-order-form.component.html',
  styleUrls: ['./release-order-form.component.css']
})
export class ReleaseOrderFormComponent implements OnInit {
  public releaseOrderForm: FormGroup;
  public itensPedido: FormGroup;
  @ViewChild("qtyEdit") private qtyEdit: ElementRef;
  @ViewChild("estoqueAtual") private estoqueEdit: ElementRef;
  patients = [];
  dispensations = [];
  listMedicine = [];
  itensPed = [];


  constructor(
    @Inject(MAT_DIALOG_DATA) 
    public data: any,
    public dialogRef: MatDialogRef<ReleaseOrderFormComponent>,
    private patientsService: PatientsService,
    private snack: MatSnackBar,
    private loader: AppLoaderService,
    private information: AppInformationService,
    private crudService : CRUDService,
    private loginService : LoginService
  ) { }

  ngOnInit() {
    this.montarForm(this.data.payload);
    }

    montarForm(item) {
      this.releaseOrderForm = new FormGroup({
        patientId: new FormControl(item.patientId, [Validators.required]),
        patientAuthorization: new FormControl(item.patientAuthorization, [Validators.required, Validators.minLength(5), Validators.maxLength(20)]),
        dispensationId: new FormControl(item.dispensationId, [Validators.required]),
        releaseOrderObservation: new FormControl(item.releaseOrderObservation, [Validators.maxLength(200)])
      });
  
      this.itensPedido = new FormGroup({
        medicineId: new FormControl("", [Validators.required]),
        dosage: new FormControl(0, [Validators.required, Validators.min(1)]),
        quantity: new FormControl(0, [Validators.required, Validators.min(1)])
      });

    let user = this.loginService.getNomeUsuarioDetalhes();
    let params : any =new Object();
    params.userLogin ="";
    //params.clientID = user.clientID;
    params.clientId = 1;
    //params.nok = "nok";
    //params.ok="ok";


      this.crudService.GetParams(params, "/patient").subscribe(res => {
        this.patients = [];
        this.patients = res.ObjectDTO;
      });

      let params2 : any =new Object();
      params2.userLogin = "";
        
      this.crudService.GetParams(params2, "/dispensationPeriod").subscribe(res => {
        this.dispensations = [];
        this.dispensations = res.ObjectDTO;
      });
      
      let params3 : any =new Object();
      params3.userLogin = "";
        
      this.crudService.GetParams(params3, "/medicine").subscribe(res => {
        this.listMedicine = [];
        this.listMedicine = res.ObjectDTO;
      });


      if(!this.data.novo){
        this.releaseOrderForm.disable();
        this.itensPed = item.ListReleaseOrderItem;
      }
    }  


    SavePedido() {
      let ListReleaseOrderItem = [];
      let pedido: any;
      this.loader.open();
  
      pedido = new Object();
      pedido.releaseOrderDate = "";
      pedido.patientId = this.releaseOrderForm.controls["patientId"].value;
      pedido.patientAuthorization = this.releaseOrderForm.controls["patientAuthorization"].value;
      pedido.dispensationId = this.releaseOrderForm.controls["dispensationId"].value;
      pedido.releaseOrderObservation = this.releaseOrderForm.controls["releaseOrderObservation"].value;
      pedido.ListReleaseOrderItem = [];
  
      let itemPedido : any;
      for (let i = 0; i < this.itensPed.length; i++) {
        itemPedido = new Object();
        itemPedido.medicineId = this.itensPed[i].medicineId;
        itemPedido.dosage = this.itensPed[i].dosage; 
        itemPedido.quantity = this.itensPed[i].quantity;            
        pedido.ListReleaseOrderItem.push(itemPedido);
      }
  
      this.crudService.Save(pedido, true, "/releaseOrder").subscribe(res => {
          if (res.Status == "OK") {
            this.loader.close();
            this.snack.open("Registro gravado com sucesso", "", { duration: 3000 });
            this.dialogRef.close('');
          } else {
            this.loader.close();
            this.snack.open("Erro ao gravar registro:" + res.Message, "", { duration: 5000 });
            this.dialogRef.close('');
          }
        });
      }

    getStock(event) {  }

    addProduct() {
      const formulario = this.itensPedido.value;
      let medicineName = "";
  
     /*  if (parseInt(formulario.quantity) > parseInt(this.estoqueEdit.nativeElement.value)) {
        this.information.information("YouVita", "Quantidade solicitada maior que o estoque atual");
        return;
      } */
  
      for (let i = 0; i < this.listMedicine.length; i++) {
        if (this.listMedicine[i].MedicineId == formulario.medicineId) {
          medicineName = this.listMedicine[i].MedicineName;
        }
      }
      formulario.medicineName = medicineName;
    
      this.itensPed.push(formulario);
      this.itensPed = [...this.itensPed];
      this.itensPedido.reset();
      this.qtyEdit.nativeElement.value = "";
  
    }

    removeItem(registro) {
      let aux = this.itensPed.filter(item => {
        if (item === registro) {
          return false;
        } else {
          return true;
        }
      });
      this.itensPed = [...aux];
    }

    formValido(): boolean {
      let v = this.releaseOrderForm.invalid;
      if (this.itensPed.length == 0) {
        v = true; //invalido
      }
      return v;
    }
}
