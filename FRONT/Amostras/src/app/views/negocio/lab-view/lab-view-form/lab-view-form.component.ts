import { LoginService } from 'app/services/negocio/login/login.service';
import { CRUDService } from './../../../../services/negocio/CRUDService/CRUDService';
import { AppInformationService } from './../../../../services/dialogs/app-information/app-information.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { ProductService } from './../../../../services/negocio/productService/productService';
import { DoctorService } from './../../../../services/negocio/doctor/doctor.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSelect, MatOption, MatSnackBar } from '@angular/material';
import { stringify } from '@angular/core/src/util';
import { ARIA_DESCRIBER_PROVIDER } from '@angular/cdk/a11y';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';

@Component({
  selector: 'app-lab-view-form',
  templateUrl: './lab-view-form.component.html',
  styleUrls: ['./lab-view-form.component.css']
})
export class LabViewFormComponent implements OnInit {
  public pedidoForm: FormGroup;
  public itensPedido: FormGroup;
  listState = [];
  doctors = [];
  itensPed = [];
  listProduct = [];
  projects = [];
  user: any;
  minDate: any;
  editMode: boolean = false;
  editable: boolean = false;
  @ViewChild("qtyEdit") private qtyEdit: ElementRef;
  @ViewChild("estoqueAtual") private estoqueEdit: ElementRef;
  @ViewChild("medico") private medico: MatSelect;



  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<LabViewFormComponent>,
    private doctorService: DoctorService,
    private productService: ProductService,
    private snack: MatSnackBar,
    private loader: AppLoaderService,
    private information: AppInformationService,
    private crudService: CRUDService,
    private loginService: LoginService,
    private confirm: AppConfirmService) { }

  ngOnInit() {
    this.montarForm(this.data.payload);
  }

  async setarMedico(id) {
    this.medico.value = id;
  }

  montarForm(item) {
    this.user = this.loginService.getNomeUsuarioDetalhes();

    this.pedidoForm = new FormGroup({
      projectId: new FormControl(item.projectId),
      doctorID: new FormControl(item.DoctorId, [Validators.required]),
      startValidity: new FormControl(item.startValidity, [Validators.required]),
      endValidity: new FormControl(item.endValidity, [Validators.required]),
      frequence: new FormControl(item.frequence, [Validators.required]),
      dateToSend: new FormControl(item.dateToSend, [Validators.required]),
      frequenceInDays: new FormControl(item.frequence, [Validators.required, Validators.min(1)]),
      status: new FormControl(item.status || "A"),
      OrderAvaliableID: new FormControl(item.OrderAvaliableID)
    });

    if (!this.data.novo) {
      this.carregar(item.projectId, item.DoctorId, true);
    }
    //obter os projetos
    let p: any = new Object();
    p.laboratoryId = this.user.laboratoryID;
    this.crudService.GetParams(p, "/project").subscribe(res => {
      this.projects = [];
      this.projects = res.ObjectDTO;
    });



    this.itensPedido = new FormGroup({
      productID: new FormControl("", [Validators.required]),
      laboratoryID: new FormControl(""),
      qty: new FormControl(0, [Validators.required, Validators.min(1)])
    });


    let params: any = new Object();
    params.userLogin = "ok";
    params.laboratoryID = this.user.laboratoryID;
    params.nok = "nok";
    params.ok = "ok";

    if (!this.data.novo) {
      this.pedidoForm.disable();
      this.itensPed = item.Items;
      let pars: any = new Object();
      pars.userLogin = this.user.username;
      pars.OrderAvaliableId = item.OrderAvaliableID;
      this.crudService.GetParams(pars, "/OrderAvaliableCheck").subscribe(res => {
        if (res.Status == "OK") {
          this.editable = true;
        } else {
          this.editable = false;
        }
      })
    }
    let dt = new Date();
    this.minDate = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate());
  }



  carregar(projectId, docId, load) {
    let self = this;
    let pjId = 0;
    if (this.data.novo) {
      pjId = projectId.value;
    } else {
      pjId = projectId;
    }

    let params: any = new Object();
    params.userLogin = this.loginService.getNomeUsuarioDetalhes().username;
    params.projectId = pjId;

    this.crudService.GetParams(params, "/projectdoctor").subscribe(res => {
      this.doctors = [];
      this.doctors = res.ObjectDTO;
    });

    this.crudService.GetParams(params, "/projectproduct").subscribe(res => {
      this.listProduct = [];
      this.listProduct = res.ObjectDTO;

      this.setarMedico(docId);
    });

    //limpar produtos e medico já selecionados, qdo troca o projeto
    if (!load) {
      this.pedidoForm.controls.doctorID.setValue("");
      this.itensPed = [];
    }


  }

  SavePedido() {
    let items = [];
    let pedido: any;
    this.loader.open();

    pedido = new Object();
    pedido.DoctorID = this.pedidoForm.controls["doctorID"].value;
    pedido.startValidity = this.pedidoForm.controls["startValidity"].value;
    pedido.endValidity = this.pedidoForm.controls["endValidity"].value;
    pedido.frequence = this.pedidoForm.controls["frequenceInDays"].value;
    pedido.dateToSend = this.pedidoForm.controls["dateToSend"].value;
    pedido.status = this.pedidoForm.controls["status"].value;
    pedido.laboratoryID = this.user.laboratoryID;
    pedido.Items = [];
    pedido.projectId = this.pedidoForm.controls["projectId"].value;

    if (this.pedidoForm.controls["dateToSend"].value < this.pedidoForm.controls["startValidity"].value) {
      this.loader.close();
      this.information.information("YouVita", "A data do primeiro envio não pode ser anterior ao início da vigência");
      return;

    }

    let itemPedido: any;
    for (let i = 0; i < this.itensPed.length; i++) {
      itemPedido = new Object();
      itemPedido.productID = this.itensPed[i].productID;
      itemPedido.qty = this.itensPed[i].qty;
      itemPedido.productName = "";
      itemPedido.productCode = "";
      itemPedido.productEnabled = 1;
      pedido.Items.push(itemPedido);
    }

    this.crudService.Save(pedido, true, "/orderavaliable").subscribe(res => {
      if (res.Status == "OK") {
        this.loader.close();
        this.snack.open("Registro gravado com sucesso", "", { duration: 3000 });
        this.dialogRef.close('');
      } else {
        this.loader.close();
        this.snack.open("Erro ao gravar registro:" + res.Message, "", { duration: 5000 });
        this.dialogRef.close('');
      }
    });



    //console.log(JSON.stringify(pedido));
    //aqui enviar para a api
    // this.productService.saveProductAvaliable(items).subscribe(res => {
    //   if (res.Status == "OK") {
    //     this.loader.close();
    //     this.snack.open("Registro gravado com sucesso", "", { duration: 3000 });
    //     this.dialogRef.close('');
    //   } else {
    //     this.loader.close();
    //     this.snack.open("Erro ao gravar registro:" + res.Message, "", { duration: 5000 });
    //     this.dialogRef.close('');
    //   }
    // });
  }

  getStock(event) {
    this.loader.open();
    let stockType = "";

    for (let i = 0; i < this.projects.length; i++) {
      if (this.projects[i].projectId == this.pedidoForm.controls.projectId.value) {
        stockType = this.projects[i].ProjectStockType;
      }
    }

    try {
      this.productService.getStock(1, event.value, stockType).subscribe(res => {
        if (res.Status == "OK") {
          let x: any;
          x = res.ObjectDTO;
          this.estoqueEdit.nativeElement.value = x.qtd_produto;
          this.loader.close();
        } else {
          this.loader.close();
          this.snack.open("Erro ao obter o estoque: " + res.Message, "", { duration: 5000 });
          this.estoqueEdit.nativeElement.value = "0";
        }
      });
    } catch (e) {
      this.snack.open("Erro ao obter o estoque: " + JSON.stringify(e), "", { duration: 5000 });
      this.loader.close();
    }
  }

  addProduct() {
    
      const formulario = this.itensPedido.value;
      let productName = "";

      if (parseInt(formulario.qty) > parseInt(this.estoqueEdit.nativeElement.value)) {
        this.information.information("YouVita", "Quantidade solicitada maior que o estoque atual");
        return;
      }

      for (let i = 0; i < this.listProduct.length; i++) {
        if (this.listProduct[i].productID == formulario.productID) {
          productName = this.listProduct[i].productName;
        }
      }
      formulario.productName = productName;
      let descFreq = "";

      switch (formulario.frequence) {
        case 1:
          descFreq = "Pontual";
          break;
        case 7:
          descFreq = "Semanal";
          break;
        case 14:
          descFreq = "Quinzenal";
          break;
        case 30:
          descFreq = "Mensal";
          break;
        case 60:
          descFreq = "Bimestral";
          break;
        case 90:
          descFreq = "Trimestral";
          break;
      }

      formulario.descFreq = descFreq;
      this.itensPed.push(formulario);      
      this.itensPed = [...this.itensPed];
      let x = [];
      formulario.OrderAvaliableID = this.pedidoForm.controls.OrderAvaliableID.value;
      x.push(formulario);
      if (!this.data.novo){
        this.crudService.Save(x, true, "/ProductAvaliable").subscribe(res => {
          if (res.Status == "OK") {
            this.snack.open("Produto adicionado com sucesso. Pedidos já enviados não foram alterados, alteração válida somente para os próximos pedidos.", "", { duration: 4000 });  
          }
        });

      }
      this.itensPedido.reset();
      this.qtyEdit.nativeElement.value = "";
    
  }

  removeItem(registro) {
    if (this.data.novo) {
      let aux = this.itensPed.filter(item => {
        if (item === registro) {
          return false;
        } else {
          return true;
        }
      });
      this.itensPed = [...aux];
    } else {
      //fazer a exclusao
      this.confirm.confirm("YouVita", "Tem certeza que deseja excluir este item de todos os pedidos programados?").subscribe(res => {
        if (res) {
          //fazer a exclusao fisica
          let doc: any = new Object();
          doc.userLogin = "login";
          doc.productAvaliableID = registro.productAvaliableID;
          this.crudService.Delete(doc, "/ProductAvaliable").subscribe(res => {
            if (res.Status == "OK") {
              this.snack.open("Item cancelados de todas as programações com sucesso", "", { duration: 4000 });
              let aux = this.itensPed.filter(item => {
                if (item === registro) {
                  return false;
                } else {
                  return true;
                }
              });
              this.itensPed = [...aux];
            }
          })
        }
      })

    }
  }

  formValido(): boolean {
    let v = this.pedidoForm.invalid;
    if (this.itensPed.length == 0) {
      v = true; //invalido
    }
    return v;
  }

  outroPeriodo(evento) {
    if (evento.value == "0") {
      this.pedidoForm.controls["frequenceInDays"].enable();
      this.pedidoForm.controls["frequenceInDays"].setValue("");
    } else {
      this.pedidoForm.controls["frequenceInDays"].setValue(evento.value)
      this.pedidoForm.controls["frequenceInDays"].disable();
    }
  }

  Editar() {
    this.editMode = true;
    //this.pedidoForm.enable();
  }

  Cancel() {
    this.confirm.confirm("YouVita", "Tem certeza que deseja cancelar este pedido e todos as suas programações?").subscribe(res => {
      if (res) {
        let doc: any = new Object();
        doc.userLogin = "user";
        doc.OrderAvaliableId = this.pedidoForm.value.OrderAvaliableID;
        this.crudService.Delete(doc, "/OrderAvaliable").subscribe(r => {
          if (r.Status == "OK") {
            this.snack.open("Pedidos cancelados com sucesso", "", { duration: 4000 });
          }
        });
      }
    });
  }
}
