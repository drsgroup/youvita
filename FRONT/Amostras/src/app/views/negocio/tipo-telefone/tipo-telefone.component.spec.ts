import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoTelefoneComponent } from './tipo-telefone.component';

describe('TipoTelefoneComponent', () => {
  let component: TipoTelefoneComponent;
  let fixture: ComponentFixture<TipoTelefoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoTelefoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoTelefoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
