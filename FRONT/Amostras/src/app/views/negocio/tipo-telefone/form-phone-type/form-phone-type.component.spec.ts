import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormPhoneTypeComponent } from './form-phone-type.component';

describe('FormPhoneTypeComponent', () => {
  let component: FormPhoneTypeComponent;
  let fixture: ComponentFixture<FormPhoneTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormPhoneTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPhoneTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
