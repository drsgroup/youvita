import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { ClientesFormularioComponent } from '../clientes-formulario.component';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { ClientService } from 'app/services/negocio/client/client.service';
import { StateService } from 'app/services/state.service';
import { ClientTypeService } from 'app/services/client-type.service';
import { DispensationPeriodService } from 'app/services/dispensation-period.service';
import { ContactTypeService } from 'app/services/contact-type.service';

@Component({
  selector: 'app-clientes-contatos',
  templateUrl: './clientes-contatos.component.html',
  styleUrls: ['./clientes-contatos.component.css']
})
export class ClientesContatosComponent implements OnInit {
  public contatosForm: FormGroup;
  listContactType = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  public dialogRef: MatDialogRef<ClientesContatosComponent>,
  public loader: AppLoaderService,
  private fb: FormBuilder,
  private snackBar: MatSnackBar,  
  private contactTypeService : ContactTypeService
  ) { }

  ngOnInit() {
    this.montarForm();
  }

  addContato(){
    let newContato = this.contatosForm.value;

    let x = this.listContactType.filter(value => {
      if (value.contactTypeId == newContato.contactTypeId) {
        return value;
      }
    });
    if (x.length > 0) {
      newContato.contactDescription = x[0].contactDescription;
    }   
    this.dialogRef.close(newContato);
  }    
  

  getContactType() {
    this.contactTypeService.getContactType().subscribe(res => {
    this.listContactType = [];
    this.listContactType = res.ObjectDTO
    console.log(res.ObjectDTO);
  })
}

  montarForm(){
    this.getContactType();
    this.contatosForm = new FormGroup({      
      contactName: new FormControl("", [Validators.required, Validators.minLength(5),Validators.maxLength(40)]),
      occupation: new FormControl("", [Validators.required, Validators.minLength(5),Validators.maxLength(40)]),
      email: new FormControl("", [Validators.required, Validators.email,Validators.maxLength(100)]),
      phone: new FormControl("", [Validators.required, Validators.minLength(10),Validators.maxLength(12)]),
      ramal: new FormControl("",[Validators.maxLength(8)]),
      contactTypeId: new FormControl("", [Validators.required, Validators.min(0)])
    });
  }

}
