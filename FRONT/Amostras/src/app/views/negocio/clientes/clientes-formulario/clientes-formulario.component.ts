import { ClientesContatosComponent } from './clientes-contatos/clientes-contatos.component';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatSnackBar, MatDialogRef, MatDialog } from '@angular/material';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ClientService } from 'app/services/negocio/client/client.service';
import { WebContract } from 'app/models/base/Contrato';
import { StateService } from 'app/services/state.service';
import { ContactTypeService } from 'app/services/contact-type.service';
import { ClientTypeService } from 'app/services/client-type.service';
import { DispensationPeriodService } from 'app/services/dispensation-period.service';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';
import { Md5 } from "md5-typescript";

@Component({
  selector: 'app-clientes-formulario',
  templateUrl: './clientes-formulario.component.html',
  styleUrls: ['./clientes-formulario.component.css']
})
export class ClientesFormularioComponent implements OnInit {
  public clientForm: FormGroup;
  public contatosForm: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ClientesFormularioComponent>,
    public loader: AppLoaderService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private serviceClient: ClientService,
    private serviceStates: StateService,
    private serviceClientType: ClientTypeService,
    private serviceDispensationPeriod: DispensationPeriodService,
    private dialog: MatDialog,
    private contactTypeService: ContactTypeService,
    private confirmation: AppConfirmService
  ) { }

  linhasStates = [];
  linhasClientTypes = [];
  linhasDispensationPeriod = [];
  linhasStatus = [];
  ListClientsContacts = [];
  listContactType = [];


  ngOnInit() {
    this.montarForm(this.data.payload);
  }

  consultarStates() {
    this.serviceStates.listarEstados().subscribe(res => {
      this.linhasStates = [];
      this.linhasStates = res.ObjectDTO
      //console.log(res.ObjectDTO);
    })
  }

  consultarClientTypes() {
    this.serviceClientType.listarClientType().subscribe(res => {
      this.linhasClientTypes = [];
      this.linhasClientTypes = res.ObjectDTO
      //console.log(res.ObjectDTO);
    })
  }


  consultarDispensationPeriod() {
    this.serviceDispensationPeriod.listarDispensationPeriod().subscribe(res => {
      this.linhasDispensationPeriod = [];
      this.linhasDispensationPeriod = res.ObjectDTO
      //console.log(res.ObjectDTO);
    })
  }

  consultarStatus() {
    this.linhasStatus = [];
    let myobj: any;

    myobj = new Object();
    myobj.statusId = "1";
    myobj.statusDescription = "Ativo";
    this.linhasStatus.push(myobj);

    myobj = new Object();
    myobj.statusId = "2";
    myobj.statusDescription = "Suspenso";
    this.linhasStatus.push(myobj);
    //console.log(this.linhasAtivo);
  }

  montarForm(item) {
    this.consultarStates();
    this.consultarClientTypes();
    this.consultarDispensationPeriod();
    this.consultarStatus();

    this.clientForm = new FormGroup({
      ClientId: new FormControl(item.ClientId),
      ClientName: new FormControl(item.ClientName, [Validators.required, Validators.minLength(5), Validators.maxLength(60)]),
      ClientFantasyName: new FormControl(item.ClientFantasyName, [Validators.required, Validators.minLength(5), Validators.maxLength(60)]),
      CNPJ: new FormControl(item.CNPJ, [Validators.required, Validators.minLength(14)]),
      Address: new FormControl(item.Address, [Validators.required, Validators.minLength(5), Validators.maxLength(60)]),
      AddressNumber: new FormControl(item.AddressNumber, [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
      City: new FormControl(item.City, [Validators.required, Validators.minLength(5), Validators.maxLength(50)]),
      Neighborhood: new FormControl(item.Neighborhood, [Validators.required, Validators.minLength(5), Validators.maxLength(50)]),
      StateId: new FormControl(item.StateId, [Validators.required]),
      ZipCode: new FormControl(item.ZipCode, [Validators.required, Validators.minLength(5), Validators.maxLength(8)]),
      ResponsibleContact: new FormControl(item.ResponsibleContact, [Validators.minLength(8), Validators.maxLength(50)]),
      Occupation: new FormControl(item.Occupation, [Validators.required]),
      Email: new FormControl(item.Email, [Validators.required, Validators.email, Validators.maxLength(60)]),
      Phone: new FormControl(item.Phone, [Validators.required]),
      Ramal: new FormControl(item.Ramal),
      CellPhone: new FormControl(item.Phone, [Validators.required]),
      ClientLogin: new FormControl(item.ClientLogin, [Validators.required, Validators.minLength(5), Validators.maxLength(60)]),
      ClientPassword: new FormControl(item.ClientPassword, [Validators.required, Validators.minLength(5), Validators.maxLength(20)]),
      ConfirmPassword: new FormControl(item.ConfirmPassword, [Validators.required, Validators.minLength(5), Validators.maxLength(20)]),
      ClientStatus: new FormControl(item.ClientStatus, [Validators.required]),
      ClientTypeId: new FormControl(item.ClientTypeId, [Validators.required]),
      Comments: new FormControl(item.Comments, [Validators.maxLength(200)])
    });

    this.getContactType();
    this.contatosForm = new FormGroup({
      ContactName: new FormControl("", [Validators.required, Validators.minLength(5), Validators.maxLength(40)]),
      Email: new FormControl("", [Validators.required, Validators.email, Validators.maxLength(100)]),
      Password: new FormControl("", [Validators.required, Validators.maxLength(20), Validators.minLength(5)]),
      Confirmpassword: new FormControl("", [Validators.required, Validators.maxLength(20), Validators.minLength(5)]),
      Occupation: new FormControl("", [Validators.required, Validators.minLength(5), Validators.maxLength(40)]),
      Phone: new FormControl("", [Validators.required, Validators.minLength(10), Validators.maxLength(14)]),
      Ramal: new FormControl("", [Validators.maxLength(8)]),
      CellPhone: new FormControl("", [Validators.maxLength(14)]),
      ContactTypeId: new FormControl("", [Validators.required, Validators.min(0)])
    });

    if ((item.ListClientsContacts != null) && (item.ListClientsContacts != undefined)) {
      this.ListClientsContacts = [...item.ListClientsContacts];

      for (let i = 0; i < this.ListClientsContacts.length; i++) {
        this.ListClientsContacts[i].ContactDescription = this.getContactTypeDescription(this.ListClientsContacts[i].ContactTypeId);
      }
    }
  }

  getContactType() {
    this.contactTypeService.getContactType().subscribe(res => {
      this.listContactType = [];
      this.listContactType = res.ObjectDTO
      if ((this.ListClientsContacts != null) && (this.ListClientsContacts != undefined)) {
        this.ListClientsContacts = [...this.ListClientsContacts];

        for (let i = 0; i < this.ListClientsContacts.length; i++) {
          this.ListClientsContacts[i].ContactDescription = this.getContactTypeDescription(this.ListClientsContacts[i].ContactTypeId);
        }
      }
    });
  }

  checkPass(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmpassword.value;
    return pass === confirmPass ? null : { notSame: true }
  }


  GravarCliente() {
    let formulario = this.clientForm.value;
    formulario.ListClientsContacts = this.ListClientsContacts;
    this.serviceClient.GravarClient(formulario, this.data.novo).subscribe(res => {
      var cnt = new WebContract();
      cnt = res;
      if (cnt.Status == "OK") {
        this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });
      } else {
        this.snackBar.open(cnt.Message, "", { duration: 3000 });
      }
      this.dialogRef.close("OK");
    });
  }

  addClientContact() {
    if (this.data.novo == true) {
      let newContato = this.contatosForm.value;
      newContato.Password = Md5.init(newContato.Password.toUpperCase());
      let x = this.listContactType.filter(value => {
        if (value.contactTypeId == newContato.ContactTypeId) {
          return value;
        }
      });
      if (x.length > 0) {
        newContato.ContactDescription = x[0].contactDescription;
      }

      this.ListClientsContacts.push(newContato);
      this.ListClientsContacts = [...this.ListClientsContacts];
    } else {
      let newContato = this.contatosForm.value;
      newContato.ClientId = this.data.payload.ClientId;
      newContato.Password = Md5.init(newContato.Password.toUpperCase());
      this.serviceClient.addClientContact(newContato).subscribe(res => {
        if (res.Status == "OK") {
          let x = this.listContactType.filter(value => {
            if (value.contactTypeId == newContato.ContactTypeId) {
              return value;
            }
          });
          if (x.length > 0) {
            newContato.ContactDescription = x[0].contactDescription;
          }

          this.ListClientsContacts.push(newContato);
          this.ListClientsContacts = [...this.ListClientsContacts];
          this.snackBar.open("Contato adicionado com sucesso.", "", { duration: 3000 });
        }
      });
    }
  }

  getContactTypeDescription(id: number): string {
    let x = this.listContactType.filter(value => {
      if (value.contactTypeId == id) {
        return value;
      }
    });
    if (x.length > 0) {
      return x[0].contactDescription;
    } else {
      return "N/A";
    }
  }

  deleteContact(registro) {
    if (this.data.novo == false) {
      this.confirmation.confirm("YouVita", "Tem certeza que deseja exlcuir este contato?").subscribe(res => {
        if (res === true) {
          this.serviceClient.deleteClientContact(registro.ContactId).subscribe(res => {
            if (res.Status == "OK") {
              let aux = this.ListClientsContacts.filter(item => {
                if (item === registro) {
                  return false;
                } else {
                  return true;
                }
              });
              this.ListClientsContacts = [...aux];
              this.snackBar.open("Contato desativado com sucesso", "", { duration: 3000 });
            }
          })
        }
      })
    }else{
      let aux = this.ListClientsContacts.filter(item => {
        if (item === registro) {
          return false;
        } else {
          return true;
        }
      });
      this.ListClientsContacts = [...aux];
    }
  }

}