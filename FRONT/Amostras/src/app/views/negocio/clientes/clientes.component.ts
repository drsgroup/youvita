import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { AppConfirmService } from '../../../services/dialogs/app-confirm/app-confirm.service';
import { AppLoaderService } from '../../../services/dialogs/app-loader/app-loader.service';
import { ClientesFormularioComponent } from './clientes-formulario/clientes-formulario.component';
import { ClientService } from 'app/services/negocio/client/client.service';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {
  colunas = [
    {
      Propriedade: 'ClientId',
      Titulo: 'ID Cliente',
      Visivel: true
    },
    {
      Propriedade: 'ClientName',
      Titulo: 'Razão Social',
      Visivel: true
    },
    {
      Propriedade: 'ClientFantasyName',
      Titulo: 'Nome Fantasia',
      Visivel: true
    },
    {
      Propriedade: 'CNPJ',
      Titulo: 'CNPJ',
      Visivel: true
    }
  ];

  linhas = [];


  constructor(private dialog: MatDialog,
    private confirm: AppConfirmService,
    private snack: MatSnackBar,
    private serviceClient: ClientService,
    private confirmacao: AppConfirmService,
    private loader: AppLoaderService,
    private snackBar: MatSnackBar,
    private appInformationService: AppInformationService) { }

  ngOnInit() {
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Novo Cliente";
    } else {
      titulo = "Editar Cliente : " + dados.ClientId;
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(ClientesFormularioComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.consultarCliente("");
        return;
      });
  }

  // consultarCliente(parametro : any){
  //   console.log("busca:"+ parametro);
  //  this.linhas = [];
  //   let myobj : any;
  //  myobj = new Object();
  //  myobj.ClientId = 1;
  //  myobj.ClientName = "Jorge 1";
  //  myobj.DataCadastro ="cadastro jorge";
  //  myobj.ClientFantasyName = "Jorge";
  //  myobj.CNPJ = "85.022.602/0001-00";

  //  this.linhas.push(myobj);

  //  myobj = new Object();
  //  myobj.ClientId = 2;
  //  myobj.ClientName = "Joao Paulo";
  //  myobj.DataCadastro ="cadastro Joao Paulo";
  //  myobj.ClientFantasyName = "Joao";
  //  myobj.CNPJ = "96.237.386/0001-29";
  //  this.linhas.push(myobj);

  // }

  consultarCliente(parametro : string) {
    this.serviceClient.listarClient(parametro).subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO
      //console.log(res.ObjectDTO);
    })
  }


  deleteClient(client: any) {
    this.confirm.confirm("Desativação do Cliente", "Tem certeza que deseja desativar o Cliente " + client.clientName + "?").subscribe(result => {
      if (result === true) {
        this.loader.open("Excluindo Cliente");
        this.serviceClient.deleteClient(client).subscribe(res => {
          if (res.Status == "OK") {
            this.snackBar.open("Cliente desativado com sucesso!", "", { duration: 3000 });
            this.consultarCliente("");
          }
          else {
            this.appInformationService.information("YouVita", res.Message);
          }
          this.loader.close();
        })
      }
    })
  }


}
