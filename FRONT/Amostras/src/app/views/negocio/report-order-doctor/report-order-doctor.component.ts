import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { parseTimelineCommand } from '@angular/animations/browser/src/render/shared';
import { formatDate } from '@angular/common';
import { ExportToCsv } from 'export-to-csv';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';
import { MatSnackBar } from '@angular/material';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';

@Component({
  selector: 'app-report-order-doctor',
  templateUrl: './report-order-doctor.component.html',
  styleUrls: ['./report-order-doctor.component.css']
})
export class ReportOrderDoctorComponent implements OnInit {

  constructor(
    public crud: CRUDService,
    public information: AppInformationService,
    private snack: MatSnackBar,
    private loader: AppLoaderService
  ) { }
  public reportForm: FormGroup;
  listLab = [];
  listRep = [];
  listDoc = [];
  listReport = [];

  ngOnInit() {
    this.montarForm();
  }

  montarForm() {
    this.reportForm = new FormGroup({
      laboratoryID : new FormControl("0"),
      representativeId : new FormControl("0"),
      doctorId : new FormControl("0"),
      dtStart : new FormControl(), 
      dtEnd : new FormControl(),
      type : new FormControl("0")
    });    
    let params : any = new Object();    
    params.userLogin = ";"
    params.laboratoryName = "";
    params.laboratoryFantasyName = "";
    params.CNPJ = "";
    
    this.crud.GetParams(params, "/Laboratory").subscribe(res => {
      if (res.Status  =="OK"){
        this.listLab = res.ObjectDTO;
      }
    });

    // this.reportForm.controls.dtStart.valueChanges.subscribe(res => {
    //   this.listReport = [];
    // });

    // this.reportForm.controls.dtEnd.valueChanges.subscribe(res => {
    //   this.listReport = [];
    // });

    this.reportForm.valueChanges.subscribe(res => {
      this.listReport = [];
    })
    
  }

  getRepresentative(event){    
    let params : any = new Object();
    params.userLogin = "";
    params.laboratoryID = event.value;
    params.ok = "ok";

    this.crud.GetParams(params, "/representative").subscribe(res => {
      if (res.Status =="OK"){
        this.listRep = [];
        this.listRep = res.ObjectDTO;        
        this.reportForm.controls.doctorId.setValue("0");
        this.reportForm.controls.representativeId.setValue("0");
        this.listDoc = [];
      } 

    });
  }

  getDoctor(event){    
    let params : any = new Object();
    params.userLogin = "";
    params.doctorName = "";
    params.CPF ="";
    params.CRM = "";
    params.UF_CRM = "";
    params.representativeID = event.value;
    params.laboratoryID = "0";       
    this.crud.GetParams(params, "/doctor").subscribe(res => {
      if (res.Status =="OK"){
        this.listDoc = [];
        this.listDoc = res.ObjectDTO;        
        this.reportForm.controls.doctorId.setValue("0");        
      } 
    });

  }



  getReport(){
    this.loader.open();
    let params : any = new Object();
    this.listReport = [];

    if (this.reportForm.controls.dtStart.value > this.reportForm.controls.dtEnd.value ){
      this.loader.close();
      this.information.information("YouVita", "Data final deve ser maior que a data inicial");
      return;
    }
    params.LaboratoryId = this.reportForm.controls.laboratoryID.value;
    params.RepresentativeId = this.reportForm.controls.representativeId.value;
    params.DoctorId = this.reportForm.controls.doctorId.value;
    params.dtStart = formatDate(this.reportForm.controls.dtStart.value, "yyyyMMdd",  "pt-BR");    
    params.dtEnd = formatDate(this.reportForm.controls.dtEnd.value, "yyyyMMdd",  "pt-BR"); 
    params.type = this.reportForm.controls.type.value;
    this.crud.GetParams(params, "/ReportOrderDoctor").subscribe(res => {
      if (res.Status =="OK"){
        this.listReport = [];
        this.listReport = res.ObjectDTO.map(item => {
          let i : any = new Object();
          i.OrderDoctorID = item.OrderDoctorID;
          i.sku = item.sku;
          i.ProductName = item.ProductName;
          i.Qty = item.Qty;
          i.OrderDoctorDateSend = formatDate(item.OrderDoctorDateSend, "dd/MM/yyyy", "pt-BR");
          i.LaboratoryFantasyName = item.LaboratoryFantasyName;
          i.RepresentativeName = item.RepresentativeName;
          i.DoctorName = item.DoctorName;
          i.Address = item.Address;
          i.AddressNumber = item.AddressNumber;
          i.City = item.City;
          i.Neighborhood = item.Neighborhood;
          i.ZipCode = item.ZipCode;
          i.Complement = item.Complement;
          i.StateName = item.StateName;
          i.Status = item.Status;
          i.Type = item.Type;
          return i;
        });        
      }
      this.loader.close();
      if (this.listReport.length ==0){
        this.snack.open("Não foram localizados pedidos para esta busca", "", {duration:4000});
      }
    });    
  }

  ExportExportar() {    
    let heads = [];
    heads = ["IdPedido", "sku", "Produto", "Quantidade", "DataEnvio",
    "Laboratório","Representante", "Médico", "Logradouro", "Número", "Cidade",
    "Bairro", "CEP", "Complemento", "Estado", "Status", "Tipo" ];

    const options = {
      filename :"Pedidos",
      fieldSeparator: ';',
      quoteStrings: '',
      decimalseparator: ',',
      showLabels: true,
      showTitle: true,
      title: 'Pedidos',
      useBom: true,
      useKeysAsHeaders: false,
      headers: heads
    };

    const exportToCsv = new ExportToCsv(options);

    exportToCsv.generateCsv(this.listReport, false);
  }

  clear(){
    this.reportForm.reset();
  }

}
