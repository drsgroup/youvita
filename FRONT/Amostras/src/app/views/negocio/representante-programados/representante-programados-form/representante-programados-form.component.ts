import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { formatDate } from '@angular/common';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { LoginService } from 'app/services/negocio/login/login.service';
import { ProductService } from 'app/services/negocio/productService/productService';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';

@Component({
  selector: 'app-representante-programados-form',
  templateUrl: './representante-programados-form.component.html',
  styleUrls: ['./representante-programados-form.component.css']
})
export class RepresentanteProgramadosFormComponent implements OnInit {
  pedidoForm: FormGroup;
  public itensPedido: FormGroup;
  produtos = [];
  listProduct = [];
  editable: boolean = false;
  btnResend : boolean = false;
  @ViewChild("estoqueAtual") private estoqueEdit: ElementRef;
  @ViewChild("qtyEdit") private qtyEdit: ElementRef;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<RepresentanteProgramadosFormComponent>,
    public crudService: CRUDService,
    private snack: MatSnackBar,
    private loginService: LoginService,
    private productService: ProductService,
    private loader: AppLoaderService,
    private information: AppInformationService,
    private confirm: AppConfirmService
  ) { }

  ngOnInit() {
    this.montarForm(this.data.payload);
  }

  montarForm(item) {
    this.pedidoForm = new FormGroup({
      ordersRepresentativeID: new FormControl(item.ordersRepresentativeID),
      representativeName: new FormControl(item.representativeName),
      laboratoryFantasyName: new FormControl(item.laboratoryFantasyName),
      orderRepresentativeDateSend: new FormControl(formatDate(item.orderRepresentativeDateSend, "dd/MM/yyyy", "pt-BR")),
      status : new FormControl(item.status)
    });
    this.produtos = item.Products;
    this.pedidoForm.disable();

    this.itensPedido = new FormGroup({
      productID: new FormControl("", [Validators.required]),
      laboratoryID: new FormControl(""),
      Qty: new FormControl(0, [Validators.required, Validators.min(1)]),
      ordersRepresentativeID: new FormControl(item.ordersRepresentativeID)
    });

    this.productService.getProducts(1).subscribe(res => {
      this.listProduct = [];
      this.listProduct = res.ObjectDTO;
    });

    if (item.status.includes("Erro")){
      this.btnResend = true; 
    }

  }

  Editar() {
    let hj = new Date();
    hj.setHours(0, 0, 0, 0);
    hj.setDate(hj.getDate() + 5);
    let aux = this.pedidoForm.controls.orderRepresentativeDateSend.value;

    let dtPedido = new Date(aux.split("/")[2], parseInt(aux.split("/")[1]) - 1, aux.split("/")[0], 0, 0, 0, 0);
    if (dtPedido <= hj) {
      this.information.information("YouVita", "Não é possível editar este pedido.");
      return;
    }

    this.editable = true;
    //this.pedidoForm.enable();

    // let params : any = new Object();
    // params.userLogin = this.loginService.getNomeUsuarioDetalhes().username;
    // params.projectId = this.pedidoForm.value.projectId;    
  }

  addProduct() {
    const formulario = this.itensPedido.value;
    let productName = "";

    if (parseInt(formulario.Qty) > parseInt(this.estoqueEdit.nativeElement.value)) {
      this.information.information("YouVita", "Quantidade solicitada maior que o estoque atual");
      return;
    }

    for (let i = 0; i < this.listProduct.length; i++) {
      if (this.listProduct[i].productID == formulario.productID) {
        productName = this.listProduct[i].productName;
      }
    }
    formulario.productName = productName;
    let descFreq = "";

    switch (formulario.frequence) {
      case 1:
        descFreq = "Pontual";
        break;
      case 7:
        descFreq = "Semanal";
        break;
      case 14:
        descFreq = "Quinzenal";
        break;
      case 30:
        descFreq = "Mensal";
        break;
      case 60:
        descFreq = "Bimestral";
        break;
      case 90:
        descFreq = "Trimestral";
        break;
    }

    formulario.descFreq = descFreq;
    this.produtos.push(formulario);
    this.produtos = [...this.produtos];
    let x = [];
    //formulario.OrderAvaliableRepresentativeID = this.pedidoForm.controls.OrderAvaliableRepresentativeID.value;
    formulario.ordersRepresentativeID = this.pedidoForm.controls.ordersRepresentativeID.value;
    formulario.userLogin = "ok";
    x.push(formulario);

    if (!this.data.novo) {
      this.crudService.Save(x, true, "/ProductOrderRepresentative").subscribe(res => {
        if (res.Status == "OK") {
          this.snack.open("Produto adicionado com sucesso", "", { duration: 4000 });
        }
      });
    }

    this.itensPedido.reset();
    this.qtyEdit.nativeElement.value = "";

  }

  getStock(event) {
    this.loader.open();
    try {
      this.productService.getStock(1, event.value, "").subscribe(res => {
        if (res.Status == "OK") {
          let x: any;
          x = res.ObjectDTO; //res.ObjectDTO.ITEM[0].QUANTITY          
          this.estoqueEdit.nativeElement.value = x.qtd_produto;
          this.loader.close();
        } else {
          this.loader.close();
          this.snack.open("Erro ao obter o estoque: " + res.Message, "", { duration: 5000 });
          this.estoqueEdit.nativeElement.value = "0";
        }
      });
    } catch (e) {
      this.snack.open("Erro ao obter o estoque: " + JSON.stringify(e), "", { duration: 5000 });
      this.loader.close();
    }
  }

  Cancel() {
    let hj = new Date();
    hj.setHours(0, 0, 0, 0);
    hj.setDate(hj.getDate() + 5);
    let aux = this.pedidoForm.controls.orderRepresentativeDateSend.value;
    let dtPedido = new Date(aux.split("/")[2], parseInt(aux.split("/")[1]) - 1, aux.split("/")[0], 0, 0, 0, 0);

    if (dtPedido <= hj) {
      this.information.information("YouVita", "Não é possível cancelar este pedido.");
      return;
    }
    this.confirm.confirm("YouVita", "Tem certeza que deseja cancelar este pedido?").subscribe(res => {
      if (res) {
        let doc: any = new Object();
        doc.userLogin = "user";
        doc.ordersRepresentativeID = this.pedidoForm.controls.ordersRepresentativeID.value;
        this.crudService.Delete(doc, "/OrderRepresentative").subscribe(r => {
          if (r.Status == "OK") {
            this.snack.open("Pedido cancelado com sucesso", "", { duration: 4000 });
            this.dialogRef.close('OK');
          }
        });
      }
    });
  }

  resend(){
    let form = this.pedidoForm.value;   
    
    this.crudService.Save(form, true, "/OrderRepresentativeResend").subscribe(res => {
      if (res.Status == "OK"){
        this.snack.open("Pedido Reenviado", "", {duration : 3000});
        this.dialogRef.close('OK');
      }else{
        this.snack.open("Erro ao reenviar o pedido", "", {duration : 5000});
      }
    })
  }

  removeItem(registro) {

    this.confirm.confirm("YouVita", "Tem certeza que deseja excluir este item de todos os pedidos programados?").subscribe(res => {
      if (res) {
        //fazer a exclusao fisica
        let doc: any = new Object();
        doc.userLogin = "login";
        doc.ProductsOrderDoctorRepresentativeID = registro.ProductsOrderDoctorRepresentativeID;
        this.crudService.Delete(doc, "/ProductOrderRepresentative").subscribe(res => {
          if (res.Status == "OK") {
            this.snack.open("Item excluído com sucesso", "", { duration: 4000 });
            let aux = this.produtos.filter(item => {
              if (item === registro) {
                return false;
              } else {
                return true;
              }
            });
            this.produtos = [...aux];
          }
        })
      }
    })

  }

}