import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { LoginService } from 'app/services/negocio/login/login.service';
import { ProductService } from 'app/services/negocio/productService/productService';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';
import { httpFactory } from '@angular/http/src/http_module';

@Component({
  selector: 'app-list-doctor-order-form',
  templateUrl: './list-doctor-order-form.component.html',
  styleUrls: ['./list-doctor-order-form.component.css']
})
export class ListDoctorOrderFormComponent implements OnInit {
  pedidoForm : FormGroup;
  produtos = [];
  btnResend : boolean = false;
  strTracking : string;
  editMode : boolean = false;
  public itensPedido: FormGroup;
  listProduct = [];
  projects = [];
  @ViewChild("estoqueAtual") private estoqueEdit: ElementRef;  

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ListDoctorOrderFormComponent>,
    public crudService : CRUDService,
    private snack: MatSnackBar,
    private loginService : LoginService,
    private productService: ProductService,
    private loader: AppLoaderService,    
    private information: AppInformationService,        
    private confirm: AppConfirmService
  ) { }

  ngOnInit() {
    this.montarForm(this.data.payload);
    this.getTracking();
  }

  montarForm(item){
    this.pedidoForm = new FormGroup({
      OrderAvaliableID : new FormControl(item.OrderAvaliableID),
      doctorName : new FormControl(item.doctorName),
      representativeName : new FormControl(item.representativeName),      
      laboratoryFantasyName : new FormControl(item.laboratoryFantasyName),
      orderDoctorDateSend : new FormControl(formatDate(item.orderDoctorDateSend, "dd/MM/yyyy", "pt-BR")),
      status : new FormControl(item.status),
      orderDoctorId : new FormControl(item.orderDoctorID),
      projectId : new FormControl(item.projectId)
    });
    //console.log(item);
    this.produtos = item.Products;

    this.itensPedido = new FormGroup({
      productID: new FormControl("", [Validators.required]),
      laboratoryID: new FormControl(""),
      qty: new FormControl(0, [Validators.required, Validators.min(1)])
    });    

    console.log(JSON.stringify(this.data.payload))

    this.pedidoForm.disable();
    if (item.status.includes("Erro")){
      this.btnResend = true; 
    }

    let p : any = new Object();    
    let user = this.loginService.getNomeUsuarioDetalhes();
    p.laboratoryId = user.laboratoryID;
    this.crudService.GetParams(p, "/project").subscribe(res => {
      this.projects = [];
      this.projects = res.ObjectDTO;    
    });       
  }

  removeItem(row){
    this.confirm.confirm("YouVita", "Tem certeza que deseja excluir este produto?").subscribe(res => {
      if (res) {
        //fazer a exclusao fisica
        let doc: any = new Object();
        doc.userLogin = "login";
        doc.ProductsOrderDoctorID = row.ProductsOrderDoctorID;
        this.crudService.Delete(doc, "/ProductsOrderDoctor").subscribe(res => {
          if (res.Status == "OK") {
            this.snack.open("Item excluídos de todas as programações com sucesso", "", { duration: 4000 });
            let aux = this.produtos.filter(item => {
              if (item.ProductsOrderDoctorID === row.ProductsOrderDoctorID) {
                return false;
              } else {
                return true;
              }
            });
            this.produtos = [...aux];
          }
        })
      }
    })
  }

  resend(){
    let form = this.pedidoForm.value;   
    
    this.crudService.Save(form, true, "/OrderDoctorResend").subscribe(res => {
      if (res.Status == "OK"){
        this.snack.open("Pedido Reenviado", "", {duration : 3000});
        this.dialogRef.close('OK');
      }else{
        this.snack.open("Erro ao reenviar o pedido", "", {duration : 5000});
      }
    })
  }

  getTracking(){
    
    this.strTracking = "";
    let params : any = new Object();
    let form = this.pedidoForm.value; 
    params.OrderDoctorId = form.orderDoctorId;
    params.pedido = "PD"+form.OrderAvaliableID+"-PD"+form.orderDoctorId;
    this.crudService.GetParams(params, "/orderdoctorTracking").subscribe(res => {
      if(res.Status =="OK"){
        this.strTracking = " - " + res.Message;
      }else{
        this.strTracking = " ";
      }
    });
  }

  Editar(){
    let hj = new Date();
    hj.setHours(0,0,0,0);
    hj.setDate(hj.getDate() + 5);
    let aux = this.pedidoForm.controls.orderDoctorDateSend.value;
    

    let dtPedido = new Date(aux.split("/")[2], parseInt(aux.split("/")[1])-1, aux.split("/")[0], 0 ,0 ,0 ,0);

    if (dtPedido <= hj){
      this.information.information("YouVita", "Não é possível editar este pedido.");
      return; 
    }

    this.editMode = true;
    //this.pedidoForm.enable();

    let params : any = new Object();
    params.userLogin = this.loginService.getNomeUsuarioDetalhes().username;
    params.projectId = this.pedidoForm.value.projectId;

    this.crudService.GetParams(params, "/projectproduct").subscribe(res => {
      this.listProduct = [];
      this.listProduct = res.ObjectDTO;      
    });

  }

  getStock(event) {
    this.loader.open();
    let stockType = "";

    for(let i = 0; i < this.projects.length; i++){
      if (this.projects[i].projectId ==  this.pedidoForm.controls.projectId.value){
        stockType = this.projects[i].ProjectStockType;
      }
    }

    try {
      this.productService.getStock(1, event.value, stockType).subscribe(res => {
        if (res.Status == "OK") {
          let x: any;
          x = res.ObjectDTO; 
          this.estoqueEdit.nativeElement.value = x.qtd_produto;
          this.loader.close();
        } else {
          this.loader.close();
          this.snack.open("Erro ao obter o estoque: " + res.Message, "", { duration: 5000 });
          this.estoqueEdit.nativeElement.value = "0";
        }
      });
    } catch (e) {
      this.snack.open("Erro ao obter o estoque: " + JSON.stringify(e), "", { duration: 5000 });
      this.loader.close();
    }
  }

  addProduct(){    
    const formulario = this.itensPedido.value;
    let productName = "";
    formulario.OrdersDoctorID = this.pedidoForm.controls.orderDoctorId.value;
    if (parseInt(formulario.qty) > parseInt(this.estoqueEdit.nativeElement.value)) {
      this.information.information("YouVita", "Quantidade solicitada maior que o estoque atual");
      return;
    }

    for (let i = 0; i < this.listProduct.length; i++) {
      if (this.listProduct[i].productID == formulario.productID) {
        productName = this.listProduct[i].productName;
      }
    }
    formulario.productName = productName;
    formulario.Qty = formulario.qty;
    this.produtos.push(formulario);      
    this.produtos = [...this.produtos];

    this.crudService.Save(formulario, true, "/ProductsOrderDoctor").subscribe(res => {
      if (res.Status =="OK"){
        this.snack.open("Produto incluído com sucess neste pedido", "", {duration : 4000});
        this.itensPedido.reset();
      }
    })


  }

  Cancel(){
    let hj = new Date();
    hj.setHours(0,0,0,0);
    hj.setDate(hj.getDate() + 5);
    let aux = this.pedidoForm.controls.orderDoctorDateSend.value;
    

    let dtPedido = new Date(aux.split("/")[2], parseInt(aux.split("/")[1])-1, aux.split("/")[0], 0 ,0 ,0 ,0);




    if (dtPedido <= hj){
      this.information.information("YouVita", "Não é possível cancelar este pedido.");
      return; 
    }
    this.confirm.confirm("YouVita", "Tem certeza que deseja cancelar este pedido?").subscribe(res => {
      if (res) {
        let doc: any = new Object();
        doc.userLogin = "user";
        doc.orderDoctorId = this.pedidoForm.controls.orderDoctorId.value;
        this.crudService.Delete(doc, "/OrderDoctor").subscribe(r => {
          if (r.Status == "OK") {
            this.snack.open("Pedido cancelado com sucesso", "", { duration: 4000 });
            this.dialogRef.close('OK');
          }
        });
      }
    });
  }
  

}
