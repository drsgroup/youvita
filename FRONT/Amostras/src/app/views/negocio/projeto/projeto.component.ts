import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { LoginService } from 'app/services/negocio/login/login.service';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { ProjetoFormularioComponent } from './projeto-formulario/projeto-formulario.component';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';


@Component({
  selector: 'app-projeto',
  templateUrl: './projeto.component.html',
  styleUrls: ['./projeto.component.css']
})
export class ProjetoComponent implements OnInit {
  colunas = [ 
    {
      Propriedade: 'projectId',
      Titulo: 'ID Projeto',
      Visivel: true,
      Largura : 50
    },
    {
      Propriedade: 'ProjectName',
      Titulo: 'Projeto ',
      Visivel: true,
      Largura : 400
    },  
    {
      Propriedade: 'ProjectStockType',
      Titulo: 'Tipo de Estoque ',
      Visivel: true,
      Largura : 150
    },  
     
    
  ];

  linhas = [];   

  constructor(private login : LoginService,
    private crud : CRUDService,
    private dialog: MatDialog,
    private confirm: AppConfirmService,
    private loader: AppLoaderService,
    private snackBar: MatSnackBar,    
    private loginservice : LoginService,    ) { }

  ngOnInit() {
    this.consultarProjeto("");
  }

  consultarProjeto(pa){
    let params : any = new Object();
    params.userLogin ="ok";
    params.filter = pa;
    this.crud.GetParams(params, "/project").subscribe(res => {
      if (res.Status == "OK"){
        this.linhas = res.ObjectDTO;
      }

    });
  }

  deleteProjeto(linha){
    this.confirm.confirm("Desativação do Projeto", "Tem certeza que deseja desativar o Projeto " +
    linha.ProjectName + "?").subscribe(result => {

        if (result === true) {
          this.loader.open("Excluindo Projeto");
          let params : any = new Object();
          params.userLogin = "ok";
          params.projectId = linha.projectId;
          this.crud.Delete(params, "/project").subscribe(res => {
            if (res.Status = "OK"){
              this.loader.close();
              this.snackBar.open("Projecto desativado com sucesso", "", {duration : 3000});
              this.consultarProjeto("");
            }else{
              this.loader.close();
              this.snackBar.open("Erro ao efetuar Desativação do projeto "+ res.Message, "", {duration : 5000});
            }
          })
        }
      })

  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo = (novo ==true )? "Novo projeto" : " Editar projeto "+ dados.projectId;
  
    
    let dialogRef: MatDialogRef<any> = this.dialog.open(ProjetoFormularioComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.consultarProjeto("");
        return;
      });
  }  

}
