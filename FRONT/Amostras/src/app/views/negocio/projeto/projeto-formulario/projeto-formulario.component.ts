import { Component, OnInit, Inject, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar, MatSelectionList } from '@angular/material';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { LoginService } from 'app/services/negocio/login/login.service';
import { WebContract } from 'app/models/base/Contrato';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';

@Component({
  selector: 'app-projeto-formulario',
  templateUrl: './projeto-formulario.component.html',
  styleUrls: ['./projeto-formulario.component.css']
})
export class ProjetoFormularioComponent implements OnInit {
  public projetoForm: FormGroup;

  laboratories = [];
  products = [];
  doctors = [];
  selectLab = false;
  idProject = 0;
  @ViewChild("listDoctors") private listDoctors: MatSelectionList;
  @ViewChild("listProducts") private listProducts: MatSelectionList;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ProjetoFormularioComponent>,
    public crud: CRUDService,
    public loader: AppLoaderService,
    public login: LoginService,
    public snackBar: MatSnackBar,
    public confirmation: AppConfirmService,
    public information: AppInformationService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.montarForm(this.data.payload);
  }

  montarForm(item) {
    this.projetoForm = new FormGroup({
      projectId: new FormControl(item.projectId || ""),
      projectName: new FormControl(item.ProjectName, [Validators.required]),
      laboratoryID: new FormControl(item.laboratoryId, [Validators.required]),
      projectStockType: new FormControl(item.ProjectStockType, [Validators.required])
    });

    this.idProject = item.projectId||"0";

    this.projetoForm.controls.projectId.disable();

    if (!this.data.novo) {
      this.projetoForm.controls.laboratoryID.disable();
      //forcar a busca dos dados
      this.obterDados(item.projectId, item.laboratoryId, "edicao");
    }

    this.crud.List("", "/laboratory").subscribe(res => {
      this.laboratories = res.ObjectDTO;
    });
  }

  onChange(): void {
    this.cdr.detectChanges();
  }

  salvarProjeto() {
    this.loader.open();
    let formulario = this.projetoForm.value;

    //if (this.data.novo) {
      let params: any = new Object();
      params.userLogin = "ok";
      params.stock = this.projetoForm.controls.projectStockType.value;
      params.projectId = this.idProject;
      this.crud.GetParams(params, "/project").subscribe(res => {
        if (res.Status != "OK") {
          this.information.information("YouVita", "Estoque já utilizando em outro projeto");
          this.loader.close();
          return;
        }else{
          let doctors = this.listDoctors.selectedOptions.selected.map(item => {
            let x: any = new Object();
            x.DoctorId = item.value;
            return x;
          });
      
          let products = this.listProducts.selectedOptions.selected.map(item => {
            let y: any = new Object();
            y.ProductId = item.value;
            return y;
          });
      
          if (doctors.length == 0 || products.length == 0) {
            this.information.information("Cadastro de Projetos", "O projeto deve possui pelo menos um médico e um produto");
            this.loader.close();
            return;
      
          }
      
          formulario.listProjectProduct = products;
          formulario.listProjectDoctor = doctors;
          if (!this.data.novo) {
            formulario.projectId = this.data.payload.projectId;
          }
      
          this.crud.Save(formulario, this.data.novo, "/project").subscribe(res => {
            if (res.Status == "OK") {
              this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });
            } else {
              this.snackBar.open(res.Message, "", { duration: 3000 });
            }
            this.dialogRef.close("OK");
            this.loader.close();
          });

        }
      });
    //}    
  }

  obterDados(event, laboratoryId, origem) {
    this.loader.open();
    let params: any = new Object();
    let user = this.login.getNomeUsuarioDetalhes();

    if (origem == "evento") {
      params.projectId = 0;
      params.laboratoryId = event.value;
    } else {
      params.projectId = event;
      params.laboratoryId = laboratoryId;
    }

    params.userLogin = "ok"
    try {
      this.crud.GetParams(params, "/ProjectProduct").subscribe(res => {
        this.selectLab = true;
        if (res.Status == "OK") {
          this.products = [];
          this.products = res.ObjectDTO;
          console.table(this.products);
          this.loader.close();
        } else {
          this.snackBar.open("Erro ao obter os produtos: " + res.Message, "", { duration: 5000 });
        }
      });
    } catch (e) {
      this.snackBar.open("Erro ao obter os produtos: " + JSON.stringify(e), "", { duration: 5000 });
      this.loader.close();

    }

    try {
      this.crud.GetParams(params, "/ProjectDoctor").subscribe(res => {
        if (res.Status == "OK") {
          this.doctors = [];
          this.doctors = res.ObjectDTO;
          console.table(this.doctors);
          this.loader.close();
        } else {
          this.snackBar.open("Erro ao obter os médicos: " + res.Message, "", { duration: 5000 });
        }
      });
    } catch (e) {
      this.snackBar.open("Erro ao obter os médicos: " + JSON.stringify(e), "", { duration: 5000 });
      this.loader.close();
    }
  }

}
