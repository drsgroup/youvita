import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctoViewFormComponent } from './docto-view-form.component';

describe('DoctoViewFormComponent', () => {
  let component: DoctoViewFormComponent;
  let fixture: ComponentFixture<DoctoViewFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctoViewFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctoViewFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
