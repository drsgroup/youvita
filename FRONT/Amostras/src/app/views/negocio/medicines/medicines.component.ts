import { ClientService } from 'app/services/negocio/client/client.service';
import { MedicinesFormComponent } from './medicines-form/medicines-form.component';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';


@Component({
  selector: 'app-medicines',
  templateUrl: './medicines.component.html',
  styleUrls: ['./medicines.component.css']
})
export class MedicinesComponent implements OnInit {
  linhas = [];
  colunas = [
    {
      Propriedade: 'MedicineId',
      Titulo: 'Id.Medicamento',
      Visivel: true
    },
    {
      Propriedade: 'EAN',
      Titulo: 'EAN',
      Visivel: true
    },
    {
      Propriedade: 'MedicineName',
      Titulo: 'Medicamento',
      Visivel: true
    },
    {
      Propriedade: 'LabName',
      Titulo: 'Laboratório',
      Visivel: true
    },
    {
      Propriedade: 'ActivePrincipleName',
      Titulo: 'Princípio ativo',
      Visivel: true
    },
    {
      Propriedade: 'Presentation',
      Titulo: 'Apresentção',
      Visivel: true
    },
    {
      Propriedade: 'UnitName',
      Titulo: 'Unidade',
      Visivel: true
    },
    {
      Propriedade: 'Dosage',
      Titulo: 'Dosagem',
      Visivel: true
    },
  ];

  constructor(
    private dialog: MatDialog,
    private serviceCliente : ClientService ) { }

  ngOnInit() {
  }

  openForm(medicine: any, novo: boolean) {
    let dialogRef: MatDialogRef<any> = this.dialog.open(MedicinesFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: "Medicamento", payload: medicine, novo: false }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.queryMedicine("");
        return;
      });
  }

  queryMedicine(value) {
    this.serviceCliente.buscarMedicamentos('').subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO;

    });
  }

}
