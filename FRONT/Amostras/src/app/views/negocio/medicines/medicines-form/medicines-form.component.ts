import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-medicines-form',
  templateUrl: './medicines-form.component.html',
  styleUrls: ['./medicines-form.component.css']
})
export class MedicinesFormComponent implements OnInit {
  medicineForm : FormGroup;
  
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  public dialogRef: MatDialogRef<MedicinesFormComponent>
  ) { }

  ngOnInit() {
    this.montarForm(this.data.payload);
    this.medicineForm.controls['EAN'].disable();
  }  

  montarForm(item){
    
    this.medicineForm = new FormGroup({      
      MedicineId : new FormControl({value : item.MedicineId, disabled: true}),      
      EAN : new FormControl({value : item.EAN, disabled: true}),
      MedicineName : new FormControl({value : item.MedicineName, disabled: true}),
      LabName : new FormControl({value : item.LabName, disabled: true}),
      ActivePrincipleName : new FormControl({value :item.ActivePrincipleName, disabled: true}),
      Presentation : new FormControl({value : item.Presentation, disabled: true}),
      UnitName : new FormControl({value : item.UnitName, disabled: true}),
      Dosage : new FormControl({value : item.Dosage, disabled: true})      
    });    

  }
}
