import { LoginService } from './../../../../services/negocio/login/login.service';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { DoctorOrderDetailComponent } from './doctor-order-detail/doctor-order-detail.component';


@Component({
  selector: 'app-doctor-orders',
  templateUrl: './doctor-orders.component.html',
  styleUrls: ['./doctor-orders.component.css']
}) 
export class DoctorOrdersComponent implements OnInit {

  colunas = [ 
    {
      Propriedade: 'OrderAvaliableID',
      Titulo: 'ID Pedido',
      Visivel: true,
      Largura : 50
    }, 
    {
      Propriedade: 'DoctorName',
      Titulo: 'Médico ',
      Visivel: true,
      Largura : 300
    },
    {
      Propriedade: 'startValidity',
      Titulo: 'Inicio Vigência',
      Visivel: true,
      Largura : 80,
      Tipo : 'DATA'

    },
    {
      Propriedade: 'endValidity',
      Titulo: 'Fim Vigência',
      Visivel: true,
      Largura : 80,
      Tipo : 'DATA'
    }    
  ];

  linhas = [];  

  constructor(private dialog: MatDialog,
    private crudServices : CRUDService,
    private login : LoginService) { }

  ngOnInit() {
    this.consultarPedido("");
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Novo Pedido Médico";
    } else {
      titulo = "Editar Pedido Médico : ";
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(DoctorOrderDetailComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    }); 

    dialogRef.afterClosed()
      .subscribe(res => {
        this.consultarPedido("");
        return;
      });
  }

  consultarPedido(pa){
    let userLogged = this.login.getNomeUsuarioDetalhes();
    let params : any = new Object();
    params.userLogin = "teste";
    params.loginDoctor = userLogged.username;
    params.status = "P"; //pendente

    this.crudServices.GetParams(params, "/orderavaliable").subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO;
    })
  }

  deletePedido(pa){

  }

}
