import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatSnackBar, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { LoginService } from 'app/services/negocio/login/login.service';
import { ProductService } from 'app/services/negocio/productService/productService';
import { StateService } from 'app/services/state.service';
import { DoctorService } from 'app/services/negocio/doctor/doctor.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LabViewFormComponent } from 'app/views/negocio/lab-view/lab-view-form/lab-view-form.component';

@Component({
  selector: 'app-doctor-order-detail',
  templateUrl: './doctor-order-detail.component.html',
  styleUrls: ['./doctor-order-detail.component.css']
})
export class DoctorOrderDetailComponent implements OnInit {
  public pedidoForm: FormGroup;
  public itensPedido: FormGroup;
  listState = [];
  doctors = [];
  itensPed = [];
  listProduct = [];
  listAddress = [];
  minDate: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DoctorOrderDetailComponent>,
    private dialog: MatDialog,
    private loginService: LoginService,
    private productService: ProductService,
    private serviceStates: StateService,
    private doctorService: DoctorService,
    private snackBar: MatSnackBar,
    private loader: AppLoaderService,
    private crud: CRUDService
  ) { }
  doctorForm: FormGroup;
  addressForm: FormGroup;
  public linhasStates = [];
  public doctor: any;

  colunas = [
    {
      Propriedade: 'MedicoID',
      Titulo: 'ID Médico',
      Visivel: true
    },
    {
      Propriedade: 'Nome',
      Titulo: 'Nome do Médico',
      Visivel: true
    },
    {
      Propriedade: 'CRM',
      Titulo: 'CRM',
      Visivel: true
    },
    {
      Propriedade: 'UF',
      Titulo: 'UF do CRM',
      Visivel: true
    }
  ];

  linhas = [];
  listItens = [];

  ngOnInit() {
    this.montarForm(this.data.payload);
  }

  delete(pa) {

  }

  consultarStates() {
    this.serviceStates.listarEstados().subscribe(res => {
      this.linhasStates = [];
      this.linhasStates = res.ObjectDTO;
      this.doctorForm.controls["uf_crm"].setValue(parseInt(this.doctor.uf_crm));
      this.doctorForm.controls["stateId"].setValue(parseInt(this.doctor.stateId));
    })
  }

  montarForm(item) {

    //buscar medico pelo login
    this.doctorForm = new FormGroup({
      doctorID: new FormControl(""),
      doctorName: new FormControl("", [Validators.required]),
      crm: new FormControl("", [Validators.required]),
      uf_crm: new FormControl("", [Validators.required]),
      cpf: new FormControl("", [Validators.required]),
      address: new FormControl("", [Validators.required]),
      number: new FormControl("", [Validators.required]),
      complement: new FormControl(""),
      city: new FormControl("", [Validators.required]),
      neighborhood: new FormControl("", [Validators.required]),
      stateId: new FormControl("", [Validators.required]),
      zipCode: new FormControl("", [Validators.required]),
      phoneComercial: new FormControl("", [Validators.required]),
      ramal: new FormControl(""),
      cellPhone: new FormControl("", [Validators.required]),
      comments: new FormControl(""),
      addressFormId: new FormControl()

    });

    this.addressForm = new FormGroup({
      address: new FormControl("", [Validators.required]),
      number: new FormControl("", [Validators.required]),
      complement: new FormControl(""),
      city: new FormControl("", [Validators.required]),
      neighborhood: new FormControl("", [Validators.required]),
      stateId: new FormControl("", [Validators.required]),
      zipCode: new FormControl("", [Validators.required]),
      phoneComercial: new FormControl("", [Validators.required]),
      ramal: new FormControl(""),
      cellPhone: new FormControl("", [Validators.required]),
      comments: new FormControl(""),
      addressFormId: new FormControl()
    });


    this.doctorForm.disable();
    //this.addressForm.disable();

    let user = this.loginService.getNomeUsuarioDetalhes();

    this.doctorService.getDoctorsByLogin(user.username).subscribe(res => {
      if (res.ObjectDTO.length > 0) {
        this.doctor = res.ObjectDTO[0];
        this.doctorForm.controls["doctorID"].setValue(this.doctor.doctorID);
        this.doctorForm.controls["doctorName"].setValue(this.doctor.doctorName);
        this.doctorForm.controls["crm"].setValue(this.doctor.crm);
        this.doctorForm.controls["uf_crm"].setValue(this.doctor.uf_crm);
        this.doctorForm.controls["cpf"].setValue(this.doctor.cpf);
        this.doctorForm.controls["address"].setValue(this.doctor.address);
        this.doctorForm.controls["number"].setValue(this.doctor.number);
        this.doctorForm.controls["city"].setValue(this.doctor.city);
        this.doctorForm.controls["neighborhood"].setValue(this.doctor.neighborhood);
        this.doctorForm.controls["zipCode"].setValue(this.doctor.zipCode);
        this.doctorForm.controls["phoneComercial"].setValue(this.doctor.phoneComercial);
        this.doctorForm.controls["ramal"].setValue(this.doctor.ramal);
        this.doctorForm.controls["cellPhone"].setValue(this.doctor.cellPhone);
        this.doctorForm.controls["comments"].setValue(this.doctor.comments);

        this.consultarStates();
      }
    });

    this.pedidoForm = new FormGroup({
      doctorID: new FormControl(item.DoctorId, [Validators.required]),
      startValidity: new FormControl(item.startValidity, [Validators.required]),
      endValidity: new FormControl(item.endValidity, [Validators.required]),
      frequence: new FormControl(item.frequence, [Validators.required, Validators.min(1)]),
      dateToSend: new FormControl(item.dateToSend, [Validators.required]),
      frequenceInDays: new FormControl(item.frequence)
    });

    this.itensPedido = new FormGroup({
      productID: new FormControl("", [Validators.required]),
      laboratoryID: new FormControl(""),
      qty: new FormControl(0, [Validators.required, Validators.min(1)])
    });

    this.doctorService.getDoctors("").subscribe(res => {
      this.doctors = [];
      this.doctors = res.ObjectDTO;
    });

    this.productService.getProducts(1).subscribe(res => {
      this.listProduct = [];
      this.listProduct = res.ObjectDTO;
    });

    if (!this.data.novo) {
      this.pedidoForm.disable();
      this.itensPed = item.Items;
    }
    let dt = new Date();
    this.minDate = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate());

    this.getAllAddress(item.DoctorId);
    this.disableAddress();    
  }

  selectAddress(evento, padrao) {
    if (padrao =="S") {
      for (let i = 0; i < this.listAddress.length; i++) {
        if (this.listAddress[i].defaultAddress == "S") {
          this.addressForm.controls['address'].setValue(this.listAddress[i].address);
          this.addressForm.controls['number'].setValue(this.listAddress[i].addressNumber);
          this.addressForm.controls['city'].setValue(this.listAddress[i].city);
          this.addressForm.controls['stateId'].setValue(this.listAddress[i].stateId);
          this.addressForm.controls['zipCode'].setValue(this.listAddress[i].zipCode);
          this.addressForm.controls['neighborhood'].setValue(this.listAddress[i].neighborhood);
          this.addressForm.controls['complement'].setValue(this.listAddress[i].complement);
          this.addressForm.controls['addressFormId'].setValue(this.listAddress[i].doctorAddressesId);
        } 
      }    
    } else {
      for (let i = 0; i < this.listAddress.length; i++) {
        if (this.listAddress[i].doctorAddressesId == evento.value) {
          this.addressForm.controls['address'].setValue(this.listAddress[i].address);
          this.addressForm.controls['number'].setValue(this.listAddress[i].addressNumber);
          this.addressForm.controls['city'].setValue(this.listAddress[i].city);
          this.addressForm.controls['stateId'].setValue(this.listAddress[i].stateId);
          this.addressForm.controls['zipCode'].setValue(this.listAddress[i].zipCode);
          this.addressForm.controls['neighborhood'].setValue(this.listAddress[i].neighborhood);
          this.addressForm.controls['complement'].setValue(this.listAddress[i].complement);
        }
      }
    }
  }

  getAllAddress(doctorID) {
    let params: any = new Object();
    params.userLogin = "ok";
    params.doctorID = doctorID;
    this.crud.GetParams(params, "/DoctorAddresses").subscribe(res => {
      if (res.Status == "OK") {
        this.listAddress = [];
        this.listAddress = res.ObjectDTO;

        this.selectAddress(undefined, "S"); 
      }
    });
  }

  SaveAddress() {
    this.loader.open();
    let formulario = this.doctorForm.value;    
    this.doctorService.saveDoctor(formulario, false).subscribe(res => {
      //this.addressForm.disable();
      if (res.Status == "OK") {
        this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });
      } else {
        this.snackBar.open(res.Message, "", { duration: 3000 });
      }
      this.loader.close();
    });
  }

  disableAddress() {
    this.addressForm.controls['address'].disable();
    this.addressForm.controls['number'].disable();
    this.addressForm.controls['city'].disable();
    this.addressForm.controls['stateId'].disable();
    this.addressForm.controls['zipCode'].disable();
    this.addressForm.controls['neighborhood'].disable();
    this.addressForm.controls['complement'].disable();

  }

  edit() {
    this.addressForm.enable();
  }

  SaveOrder() {
    let Order: any;
    Order = new Object();
    Order.doctorID = this.doctor.doctorID;
    Order.addressDoctorID = 0;
    Order.status = "E";
    Order.comments = "nenhum";
    Order.createDate = new Date();
    Order.orderDoctorDateSend = new Date();

    this.crud.Save(Order, true, "/OrderDoctor").subscribe(res => {
      if (res.Status == "OK") {
        this.snackBar.open("Pedido realizado com sucesso", "", { duration: 3000 });
      } else {
        this.snackBar.open("Erro ao realizar o pedido: " + res.Message, "", { duration: 5000 });
      }
      //this.montarForm();
    });
  }

  consultaPedidos(pa) {

  }

  ApproveOrder() {
    let aux : any = new Object();
    aux = this.data.payload;
    aux.doctorAddessesId = this.addressForm.controls.addressFormId.value;
    this.crud.Save(aux, true, "/OrderApprove").subscribe(res => {
      if (res.Status == "OK") {
        this.snackBar.open("Pedido aprovado com sucesso", "", { duration: 3000 })
      } else {
        this.snackBar.open("Erro ao aprovar o pedido: " + res.Message, "", { duration: 5000 })
      }
      this.dialogRef.close('OK')
    })
  }

}
