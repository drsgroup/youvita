import { Component, OnInit, ViewChild, SimpleChanges } from '@angular/core';
import { RelatorioService } from 'app/services/negocio/relatorio/relatorio.service';
import { BaseChartDirective } from 'ng2-charts';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {  

  @ViewChild(BaseChartDirective) private grafico: BaseChartDirective; 

  lojas = [];
  barChartOptions: any ={
    scaleShowVerticalLines: true,
    responsive: true,
    scales: {
      xAxes: [{
        gridLines: {
          color: 'rgba(0,0,0,0.02)',
          zeroLineColor: 'rgba(0,0,0,0.02)'
        }
      }],
      yAxes: [{
        gridLines: {
          color: 'rgba(0,0,0,0.02)',
          zeroLineColor: 'rgba(0,0,0,0.02)'
        },
        position: 'left',
        ticks: {
          beginAtZero: true           
        }
      }]
    }
  }

  chartColors: Array <any> = [{
    backgroundColor: '#3f51b5',
    borderColor: '#3f51b5',
    pointBackgroundColor: '#3f51b5',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(148,159,177,0.8)'
  }, {
    backgroundColor: '#eeeeee',
    borderColor: '#e0e0e0',
    pointBackgroundColor: '#e0e0e0',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(77,83,96,1)'
  }, {
    backgroundColor: 'rgba(148,159,177,0.2)',
    borderColor: 'rgba(148,159,177,1)',
    pointBackgroundColor: 'rgba(148,159,177,1)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(148,159,177,0.8)'
  }];  

  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  

  public dadosTela:any[] = [
      {data: [], label: ''}];
  
  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private relatorioService: RelatorioService) { }

  ngOnInit() {
    let data = new Date();    
    let ano = data.getFullYear().toString();
    let mes = data.getMonth()+1;

    let dtIni = ano;
    let dtFim = ano;
    if (mes < 10){
      dtIni = dtIni + '0'+mes.toString()+"01";
      dtFim = dtFim + '0'+mes.toString()+"31";
    }else{
      dtIni = dtIni + mes.toString()+"01";
      dtFim = dtFim + mes.toString()+"31";
    }
    
    // let x = this.relatorioService.listarLavagens(parseInt(dtIni), parseInt(dtFim)).subscribe(res => {
    //   let array = res.Dados;
    //   let groupByName = {};

    //   array.forEach(function (a) {
    //     groupByName[a.NomeLoja] = groupByName[a.NomeLoja] || [];
    //     groupByName[a.NomeLoja].push({ Placa: a.Placa });
    //   }, []);

    //   var chaves = Object.keys(groupByName);
    //   this.lojas = [];  
    //   this.grafico.ngOnChanges({} as SimpleChanges);
    // });
  }
}