import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { LoginService } from 'app/services/negocio/login/login.service';
import { ConfirmReleaseOrderFormComponent } from './confirm-release-order-form/confirm-release-order-form.component';

@Component({
  selector: 'app-confirm-release-order',
  templateUrl: './confirm-release-order.component.html',
  styleUrls: ['./confirm-release-order.component.css']
})
export class ConfirmReleaseOrderComponent implements OnInit {

  colunas = [ 
    {
      Propriedade: 'releaseOrderId',
      Titulo: 'ID Pedido',
      Visivel: true, 
      Largura: 80
    },
    {
      Propriedade: 'releaseOrderDate',
      Titulo: 'Data',
      Visivel: true,
      Largura: 80,
      Tipo: 'DATA'
    }, 
    {
      Propriedade: 'patientName',
      Titulo: 'Paciente',
      Visivel: true,
      Largura: 200
    },
    {
      Propriedade: 'patientAuthorization',
      Titulo: 'Autorização',
      Visivel: true,
      Largura: 80
    },
  ];

  linhas = [];

  constructor(private dialog: MatDialog,
    private crudServices: CRUDService,
    private login: LoginService) { }

  ngOnInit() {
    this.consultarReleaseOrder("");
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo = "Confirmação da liberação de Pedido";

    let dialogRef: MatDialogRef<any> = this.dialog.open(ConfirmReleaseOrderFormComponent, {
      width: '90%', height: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });
    
    dialogRef.afterClosed()
    .subscribe(res => {
      this.consultarReleaseOrder("");
      return;
    });
  }

  consultarReleaseOrder(parametro : string){
    let params : any = new Object();
    params.userLogin = "teste";
    this.crudServices.GetParams(params, "/releaseOrder").subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO;
    })
  }

  deleteConfirm(p : any){

  }
}
