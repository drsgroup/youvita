import { RepresentantePedidosComponent } from './representante-pedidos/representante-pedidos.component';
import { UsersComponent } from './users/users.component';
import { ListDoctorOrdersComponent } from './list-doctor-orders/list-doctor-orders.component';
import { LabViewComponent } from './lab-view/lab-view.component';
import { DoctorComponent } from './doctor/doctor.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PacientesComponent } from './pacientes/pacientes.component';
import { ClientesComponent } from './clientes/clientes.component';
import { TipoTelefoneComponent } from 'app/views/negocio/tipo-telefone/tipo-telefone.component';
import { MedicinesComponent } from './medicines/medicines.component';
import { AuthGuard } from 'app/services/auth/auth.guard';
import { LaboratorioComponent } from './laboratorio-component/laboratorio.component';
import { RepresentativeComponent } from './representative/representative.component';
import { DoctorViewComponent } from './doctor-view/doctor-view.component';
import { DoctorOrdersComponent } from './doctor/doctor-orders/doctor-orders.component';
import { OrdersComponent } from './orders/orders.component';
import { RepresentanteProgramadosComponent } from './representante-programados/representante-programados.component';
import { ReleaseOrderComponent } from './release-order/release-order.component';
import { ConfirmReleaseOrderComponent } from './confirm-release-order/confirm-release-order.component';
import { ProjetoComponent } from './projeto/projeto.component';
import { ReportOrderDoctorComponent } from './report-order-doctor/report-order-doctor.component';


const routes: Routes = [
 
  {
    path: '',
    children: [{
      path: 'clientes',
      component: ClientesComponent,
      data: { title: 'Cadastro de clientes', breadcrumb: 'Cadastro de Clientes' }
      
    }]
  }, 
  {
    path: '',
    children: [{
      path: 'pacientes',
      component: PacientesComponent,
      data: { title: 'Cadastro de pacientes', breadcrumb: 'Cadastro de Pacientes' }
      
    }]
  },   
  {
    path: '',
    children: [{
      path: 'tipoTelefone',
      component: TipoTelefoneComponent,
      data: { title: 'Cadastro de Tipo de Telefone', breadcrumb: 'TipoTelefone' }
      
    }]
  }, 
  {
    path: '',    
    children: [{
      path: 'medicamentos',
      component: MedicinesComponent,
      data: { title: 'Consulta de medicamentos', breadcrumb: 'Consulta de Medicamentos' }
      
    }]
  }, 
  {
    path: '',
    children: [{
      path: 'laboratorios',
      component: LaboratorioComponent,
      data: { title: 'Cadastro de Laboratórios', breadcrumb: 'Cadastro de Laboratórios' }
      
    }]
  },       
  {
    path: '',
    children: [{
      path: 'medicos',
      component: DoctorComponent,
      data: { title: 'Cadastro de Médicos', breadcrumb: 'Cadastro de Médicos' }
      
    }]
  },
  {
    path: '',
    children: [{
      path: 'representantes',
      component: RepresentativeComponent,
      data: { title: 'Cadastro de Representantes', breadcrumb: 'Cadastro de Representantes' }
      
    }]
  },  
  {
    path: '',
    children: [{
      path: 'homelab',
      component: LabViewComponent,
      data: { title: 'Liberar material para o médico', breadcrumb: 'Liberar material para o médico' }
      
    }]
  },
  {
    path: '',
    children: [{
      path: 'visaomedico',
      component: DoctorViewComponent,
      data: { title: 'Aceitar pedido', breadcrumb: 'Pedido' }
      
    }]
  },
  {
    path: '',
    children: [{
      path: 'homemedico',
      component: DoctorOrdersComponent,
      data: { title: 'Pedidos disponíveis', breadcrumb: 'Pedidos disponíveis' }
      
    }]
  },
  {
    path: '',
    children: [{
      path: 'pedidos',
      component: OrdersComponent,
      data: { title: 'Pedidos Aprovados', breadcrumb: 'Pedidos Aprovados' }
      
    }]
  },
  {
    path: '',
    children: [{
      path: 'programados',
      component: ListDoctorOrdersComponent,
      data: { title: 'Pedidos Programados', breadcrumb: 'Pedidos Programados' }      
    }]
  }, 
  {
    path: '',
    children: [{
      path: 'usuarios',
      component: UsersComponent,
      data: { title: 'Usuários', breadcrumb: 'Usuário' }      
    }]
  }, 
  {
    path: '',
    children: [{
      path: 'pedidorepresentante',
      component: RepresentantePedidosComponent,
      data: { title: 'Pedidos Representante', breadcrumb: 'Pedidos Representante' }      
    }]
  },
  {
    path: '',
    children: [{
      path: 'representanteprogramados',
      component: RepresentanteProgramadosComponent,
      data: { title: 'Pedidos Programados Representante', breadcrumb: 'Pedidos Programados Representante' }      
    }]
  },
  {
    path: '',
    children: [{
      path: 'ordemliberacao',
      component: ReleaseOrderComponent,
      data: { title: 'Pedidos Programados Representante', breadcrumb: 'Pedidos Programados Representante' }      
    }]
  },  
  {
    path: '',
    children: [{
      path: 'confirmpedidos',
      component: ConfirmReleaseOrderComponent,
      data: { title: 'Confirmação de Pedidos', breadcrumb: 'Liberação de Pedidos' }
      
    }]
  },  
  {
    path: '',
    children: [{
      path: 'projeto',
      component: ProjetoComponent,
      data: { title: 'Projeto do laboratório', breadcrumb: 'Projeto do laboratório' }      
    }]
  }, 
  {
    path: '',
    children: [{
      path: 'reportOrderDoctor',
      component: ReportOrderDoctorComponent,
      data: { title: 'Relatório de Pedidos', breadcrumb: 'Relatório de Pedidos' }      
    }]
  }, 
  
    
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NegocioRoutingModule { }
