import { LoginService } from 'app/services/negocio/login/login.service';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { PacientesFormularioComponent } from './pacientes-formulario/pacientes-formulario.component';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { PatientsService } from 'app/services/negocio/patients/patients.service';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';


@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.component.html',
  styleUrls: ['./pacientes.component.css']
})
export class PacientesComponent implements OnInit {

  colunas = [
    {
      Propriedade: 'PatientId',
      Titulo: 'Identificador',
      Visivel: true
    },
    {
      Propriedade: 'ClientFantasyName',
      Titulo: 'Cliente',
      Visivel: true
    },
    {
      Propriedade: 'PatientName',
      Titulo: 'Nome do Paciente',
      Visivel: true
    },
    {
      Propriedade: 'CPF',
      Titulo: 'CPF',
      Visivel: true
    },
    {
      Propriedade: 'PatientStatus',
      Titulo: 'Status',
      Visivel: true
    },
  ];

  linhas = [];

  constructor(private dialog: MatDialog,
    private confirm: AppConfirmService,
    private loader: AppLoaderService,
    private snackBar: MatSnackBar,
    private patients: PatientsService,
    private appInformationService: AppInformationService,
    private loginService : LoginService
  ) { }

  ngOnInit() {
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Novo Paciente" ;
    } else {
      titulo = "Editar Paciente: " + dados.PatientId;
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(PacientesFormularioComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        //this.consultarLoja();
        this.getPatients("");
        return;
      });
  }

  getPatients(parametro : string) {
    let usr = this.loginService.getNomeUsuarioDetalhes();
    this.patients.getPatients(parametro, usr.ClientId).subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO;      
    });
  }

  deletePatient(patient: any) {
    this.confirm.confirm("Desativação do Paciente", "Tem certeza que deseja desativar o Paciente " +
      patient.patientName + "?").subscribe(result => {

        if (result === true) {
          this.loader.open("Excluindo Paciente");
          this.patients.deletePatient(patient).subscribe(res => {
            if (res.Status == "OK") {
              this.snackBar.open("Paciente desativado com sucesso!", "", { duration: 3000 });
              this.getPatients("");
            }
            else {
              this.appInformationService.information("YouVita", res.Message);
            }
            this.loader.close();
          })
        }
      })
  }
}
