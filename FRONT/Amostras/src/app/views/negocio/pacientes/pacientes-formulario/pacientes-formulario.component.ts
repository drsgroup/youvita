import { PacientePatologiasComponent } from './paciente-patologias/paciente-patologias.component';
import { PacienteEnderecosComponent } from './paciente-enderecos/paciente-enderecos.component';
import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatInput } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { PatientsService } from 'app/services/negocio/patients/patients.service';
import { ClientService } from 'app/services/negocio/client/client.service';
import { WebContract } from 'app/models/base/Contrato';
import { PhoneTypeService } from 'app/services/phone-type.service';
import { StateService } from 'app/services/state.service';
import { AddressTypeService } from 'app/services/address-type.service';
import { PathologiesService } from 'app/services/pathologies.service';
import { PacientesFoneComponent } from './pacientes-fone/pacientes-fone.component';
import { LoginService } from 'app/services/negocio/login/login.service';

@Component({
  selector: 'app-pacientes-formulario',
  templateUrl: './pacientes-formulario.component.html',
  styleUrls: ['./pacientes-formulario.component.css']
})
export class PacientesFormularioComponent implements OnInit {
  public pacienteForm: FormGroup;
  public foneForm: FormGroup;
  public pathologieForm: FormGroup;
  public addressForm: FormGroup;
  public medicinesForm : FormGroup;
  public medicinesFind : FormGroup;
  public ClientId: number;
  public telaCliente: boolean;
  public NomeCliente: string;
  @ViewChild("telefone") private tel: ElementRef;


  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<PacientesFormularioComponent>,
    public loader: AppLoaderService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private patientService: PatientsService,
    private clientService: ClientService,
    private dialog: MatDialog,
    private phoneTypeService: PhoneTypeService,
    private stateService: StateService,
    private addressTypeService: AddressTypeService,
    private pathologiesService: PathologiesService,
    private loginService: LoginService) { }

  listClients = [];
  listPhoneType = [];
  listPhones = [];
  listState = [];
  listAddressType = [];
  listAddress = [];
  listPathologies = [];
  listPatientPathologies = [];
  listPatientMedicines = [];
  listMedicines = [];
  selected = [];

  ngOnInit() {
    let usuario = this.loginService.getNomeUsuarioDetalhes();
    this.ClientId = usuario.ClientId;

    if (usuario.userType > 1) {
      this.telaCliente = true;
    } else {
      this.telaCliente = false;
    }
    this.montarForm(this.data.payload);
  }

  ftelaCliente(): Boolean {
    let usr = this.loginService.getNomeUsuarioDetalhes();
    if (usr.userType > 1) {
      return true;
    } else {
      return false;
    }
  }

  getPathologies() {
    this.pathologiesService.getPathologies().subscribe(res => {
      this.listPathologies = [];
      this.listPathologies = res.ObjectDTO;
      this.getPathologyDescription();
    })
  }

  getPathologyDescription() {
    if (this.listPatientPathologies.length > 0) {
      for (let i = 0; i < this.listPatientPathologies.length; i++) {

        let x = this.listPathologies.filter(value => {
          if (value.pathologyId == this.listPatientPathologies[i].pathologyId) {
            return value;
          }
        });
        if (x.length > 0) {
          this.listPatientPathologies[i].pathologyDescription = x[0].pathologyDescription;
        }
      }
    }
  }

  getPhoneType() {
    this.phoneTypeService.getPhoneType().subscribe(res => {
      this.listPhoneType = [];
      this.listPhoneType = res.ObjectDTO;
      this.getPhoneDescription();
    })
  }

  montarForm(item) {
    this.consultarClients();
    this.pacienteForm = new FormGroup({
      PatientId: new FormControl(item.PatientId),
      ClientName: new FormControl({ value: "", disabled: true }),
      ClientId: new FormControl(item.ClientId),
      PatientName: new FormControl(item.PatientName, [Validators.required, Validators.minLength(5), Validators.maxLength(60)]),
      BirthDate: new FormControl(item.BirthDate, [Validators.required]),
      CPF: new FormControl(item.CPF, [Validators.required, Validators.minLength(11)]),
      RG: new FormControl(item.RG, [Validators.required, Validators.minLength(5), Validators.maxLength(15)]),
      IdentificationOnClient: new FormControl(item.IdentificationOnClient||"", [Validators.required, Validators.minLength(5), Validators.maxLength(20)]),
      Responsible: new FormControl(item.Responsible||"", [Validators.required, Validators.minLength(5), Validators.maxLength(60)]),
      BestContactPeriod: new FormControl(item.BestContactPeriod||"", [Validators.required]),
      PatientStatus: new FormControl(item.PatientStatus||"", [Validators.required]),
      CID1: new FormControl(item.CID1||"", [Validators.maxLength(20), Validators.required]),
      CID2: new FormControl(item.CID2||"", [Validators.maxLength(20)]),
      CID3: new FormControl(item.CID3||"", [Validators.maxLength(20)]),
      Comments: new FormControl(item.Comments||"", [Validators.maxLength(200)]),
      PatientEnabled : new FormControl(item.PatientEnabled||"1")
    });

    this.foneForm = new FormGroup({
      phoneNumber: new FormControl("", [Validators.required, Validators.minLength(10)]),
      ramal: new FormControl(""),
      phoneTypeId: new FormControl("", [Validators.required, Validators.min(0)])
    });

    this.pathologieForm = new FormGroup({
      patientId: new FormControl(""),
      pathologyId: new FormControl("", [Validators.required]),
    });

    this.addressForm = new FormGroup({
      patientId: new FormControl(""),
      addressTypeId: new FormControl("", [Validators.required]),
      Address: new FormControl("", [Validators.required]),
      AddressNumber: new FormControl("", [Validators.required]),
      City: new FormControl("", [Validators.required]),
      Neighborhood: new FormControl("", [Validators.required]),
      stateId: new FormControl("", [Validators.required]),
      ZipCode: new FormControl("", [Validators.required]),
      Complement: new FormControl(""),
    });

    this.medicinesForm = new FormGroup({
      PatientId : new FormControl(item.PatientId),
      Dosage : new FormControl("", [Validators.required]),
      EAN : new FormControl("", [Validators.required]),
      Qty : new FormControl("", [Validators.required]),
      Dispensation : new FormControl("", [Validators.required, Validators.min(0)]),
      Authorization : new FormControl("", [Validators.required, Validators.minLength(3)])      
    });

    this.listPhones = (item.ListPatientPhones || []);
    this.listPatientPathologies = (item.ListPatientPathologies || []);
    this.listAddress = (item.ListPatientAddresses || []);

    this.getPhoneType();
    this.getState();
    this.getAddressType();
    this.getPathologies();
  }

  savePatient() {
    let formPatient = this.pacienteForm.value;
    formPatient.ListPatientPhones = this.listPhones;
    formPatient.ListPatientPathologies = this.listPatientPathologies;
    formPatient.ListPatientAddresses = this.listAddress;

    this.patientService.savePatient(formPatient, this.data.novo).subscribe(res => {
      var cnt = new WebContract();
      cnt = res;
      if (cnt.Status == "OK") {
        this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });

      } else {
        this.snackBar.open(cnt.Message, "", { duration: 3000 });
      }
      this.dialogRef.close("OK");
    });

  }

  addPatientMedicine(){
    let newMedicine = this.medicinesForm.value;
    this.listPatientMedicines.push(newMedicine);
    this.listPatientMedicines = [...this.listPatientMedicines];
  }

  getState() {
    this.stateService.listarEstados().subscribe(res => {
      this.listState = [];
      this.listState = res.ObjectDTO;
      this.adjustAddress();
    })
  }

  getAddressType() {
    this.addressTypeService.getAddressType().subscribe(res => {
      this.listAddressType = [];
      this.listAddressType = res.ObjectDTO;
      this.adjustAddress();
    })
  }

  adjustAddress() {
    if (this.listAddress.length > 0) {
      for (let i = 0; i < this.listAddress.length; i++) {
        let x = this.listAddressType.filter(value => {
          if (value.addressTypeId == this.listAddress[i].AddressTypeId) {
            return value;
          }
        });
        if (x.length > 0) {
          this.listAddress[i].addressTypeDescription = x[0].addressTypeDescription;
        }

        let auxState = this.listState.filter(value => {
          if (value.stateId == this.listAddress[i].StateId) {
            return value;
          }
        });
        if (auxState.length > 0) {
          this.listAddress[i].stateName = auxState[0].StateName;
        }
      }
    }
  }

  getPhoneDescription() {
    if (this.listPhones.length > 0) {
      for (let i = 0; i < this.listPhones.length; i++) {
        let x = this.listPhoneType.filter(value => {
          if (value.phoneTypeId == this.listPhones[i].phoneTypeId) {
            return value;
          }
        });
        if (x.length > 0) {
          this.listPhones[i].description = x[0].description;
        }
      }
    }
  }

  consultarClients() {
    this.clientService.listarClient("").subscribe(res => {
      this.listClients = [];
      this.listClients = res.ObjectDTO;

      //NomeCliente
      for (let x = 0; x < this.listClients.length; x++) {
        if (this.listClients[x].ClientId == this.ClientId) {
          this.pacienteForm.controls["ClientName"].setValue(this.listClients[x].ClientFantasyName);
          this.pacienteForm.controls["ClientId"].setValue(this.ClientId);
        }
      }
    })
  }

  addPhone() {
    let newPhone: any;
    newPhone = this.foneForm.value;
    newPhone.userLogin = "testejoaodokko";
    newPhone.patientsEnabled = 1;

    let x = this.listPhoneType.filter(value => {
      if (value.phoneTypeId == newPhone.phoneTypeId) {
        return value;
      }
    });
    if (x.length > 0) {
      newPhone.description = x[0].description;
    }


    this.listPhones.push(newPhone);
    this.listPhones = [...this.listPhones]
    this.foneForm.reset();
    this.tel.nativeElement.value = "";


  }

  // addPhone() {
  //   let dados: any;
  //   dados = new Object();

  //   dados.titulo = "Novo telefone"
  //   let dialogRef: MatDialogRef<any> = this.dialog.open(PacientesFoneComponent, {
  //     width: '720px',
  //     disableClose: true,
  //     data: { title: "Novo Telefone", payload: dados, novo: this.data.novo }
  //   });

  //   dialogRef.afterClosed()
  //     .subscribe(res => {
  //       //this.consultarLoja();
  //       if (res != "NOK") {
  //         this.listPhones.push(res);
  //         this.listPhones = [...this.listPhones];
  //       }
  //       return;
  //     });
  // }

  // addAddress() {
  //   let dados: any;
  //   dados = new Object();

  //   let dialogRef: MatDialogRef<any> = this.dialog.open(PacienteEnderecosComponent, {
  //     width: '720px',
  //     disableClose: true,
  //     data: { title: "Novo Endereço", payload: dados, novo: this.data.novo }
  //   });

  //   dialogRef.afterClosed()
  //     .subscribe(res => {
  //       //this.consultarLoja();
  //       if (res != "NOK") {
  //         this.listAddress.push(res);
  //         this.listAddress = [...this.listAddress];
  //       }
  //       return;
  //     });
  // }

  removePhone(phone: any) {
    let aux = this.listPhones.filter(item => {
      if (item === phone) {
        return false;
      } else {
        return true;
      }
    });
    this.listPhones = [...aux]
    this.foneForm.reset();
  }



  addAddress() {
    let newAddress = this.addressForm.value;
    newAddress.userLogin = "testejoaodokko";
    newAddress.patientsEnabled = 1;

    let x = this.listAddressType.filter(value => {
      if (value.addressTypeId == newAddress.addressTypeId) {
        return value;
      }
    });
    if (x.length > 0) {
      newAddress.addressTypeDescription = x[0].addressTypeDescription;
    }


    let y = this.listState.filter(value => {
      if (value.stateId == newAddress.stateId) {
        return value;
      }
    });
    if (y.length > 0) {
      newAddress.stateName = y[0].stateName;
    }

    this.listAddress.push(newAddress);
    this.listAddress = [...this.listAddress]
    this.addressForm.reset();
  }

  removeAddress(address: any) {
    let aux = this.listAddress.filter(item => {
      if (item === address) {
        return false;
      } else {
        return true;
      }
    });
    this.listAddress = [...aux]
    //this.addressForm.reset();
  }



  addPatientPathologies() {
    let newPathologie: any;
    newPathologie = this.pathologieForm.value;
    newPathologie.userLogin = "testejoaodokko";
    newPathologie.patientsEnabled = 1;

    let x = this.listPathologies.filter(value => {
      if (value.pathologyId == newPathologie.pathologyId) {
        return value;
      }
    });
    if (x.length > 0) {
      newPathologie.pathologyDescription = x[0].pathologyDescription;
    }

    this.listPatientPathologies.push(newPathologie);
    this.listPatientPathologies = [...this.listPatientPathologies]
    this.pathologieForm.reset();
  }

  // addPatientPathologies() {

  //   let dados: any;
  //   dados = new Object();

  //   let dialogRef: MatDialogRef<any> = this.dialog.open(PacientePatologiasComponent, {
  //     width: '720px',
  //     disableClose: true,
  //     data: { title: "Nova Patologia", payload: dados, novo: this.data.novo }
  //   });

  //   dialogRef.afterClosed()
  //     .subscribe(res => {
  //       //this.consultarLoja();
  //       if (res != "NOK") {
  //         this.listPatientPathologies.push(res);
  //         this.listPatientPathologies = [...this.listPatientPathologies]
  //       }
  //       return;
  //     });
  // }


  removePathologies(pathology: any) {
    if (this.data.novo == true) {
      let aux = this.listPatientPathologies.filter(item => {
        if (item === pathology) {
          return false;
        } else {
          return true;
        }
      });
      this.listPatientPathologies = [...aux]
    } else {
      //excluir do banco
      null;
    }
  }
  onSelect({ selected }) {
    this.medicinesForm.controls["EAN"].setValue(selected[0].EAN);
    
  }

  procurarMedicamento(){
    this.clientService.buscarMedicamentos('').subscribe(res => {
      this.listMedicines = [];
      this.listMedicines = res.ObjectDTO; 
    });
  }

  removeMedicine(medicine : any){
    let aux = this.listPatientMedicines.filter(item => {
      if (item === medicine) {
        return false;
      } else {
        return true;
      }
    });
    this.listPatientMedicines = [...aux]    

  }

}
