import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { PathologiesService } from 'app/services/pathologies.service';
import { PacientesFoneComponent } from '../pacientes-fone/pacientes-fone.component';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { PhoneTypeService } from 'app/services/phone-type.service';

@Component({
  selector: 'app-paciente-patologias',
  templateUrl: './paciente-patologias.component.html',
  styleUrls: ['./paciente-patologias.component.css']
})
export class PacientePatologiasComponent implements OnInit {
  public pathologieForm: FormGroup;
  listPathologies = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private pathologiesService: PathologiesService,
    public dialogRef: MatDialogRef<PacientePatologiasComponent>,
    public loader: AppLoaderService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.montarForm();
  }

  montarForm(){
    this.getPathologies();

    this.pathologieForm = new FormGroup({
      patientId: new FormControl(""),
      pathologyId: new FormControl("", [Validators.required]),
    });
  }

  getPathologies() {
    this.pathologiesService.getPathologies().subscribe(res => {
      this.listPathologies = [];
      this.listPathologies = res.ObjectDTO
    })
  }  

  addPathology() {
    let newPathology = this.pathologieForm.value;

    let x = this.listPathologies.filter(value => {
      if (value.pathologyId == newPathology.pathologyId) {
        return value;
      }
    });
    if (x.length > 0) {
      newPathology.pathologyDescription = x[0].pathologyDescription;
    }   


    this.dialogRef.close(newPathology);
  }  

}
