import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';
import { StateService } from 'app/services/state.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { LaboratoryService } from 'app/services/negocio/laboratory/laboratory.service';
import { WebContract } from 'app/models/base/Contrato';
import { LoginService } from 'app/services/negocio/login/login.service';
import { Md5 } from 'md5-typescript';
import { validarCNPJ } from 'app/validators/CNPJValidator';

@Component({
  selector: 'app-laboratio-formulario',
  templateUrl: './laboratio-formulario.component.html',
  styleUrls: ['./laboratio-formulario.component.css']
})
export class LaboratioFormularioComponent implements OnInit {
  public laboratorioForm: FormGroup;
  linhasStates = [];
  campoObrigatorio ="Campo obrigatório";
  emailInvalido ="Email inválido";

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<LaboratioFormularioComponent>,
    public labService: LaboratoryService,
    private snackBar: MatSnackBar,
    private serviceStates: StateService,
    private login: LoginService,
    private information: AppInformationService,
    private loader: AppLoaderService) { }

  ngOnInit() {
    this.montarForm(this.data.payload);
  }

  montarForm(item) {
    this.laboratorioForm = new FormGroup({
      laboratoryID: new FormControl(item.laboratoryID),
      laboratoryName: new FormControl(item.laboratoryName, [Validators.required]),
      laboratoryFantasyName: new FormControl(item.laboratoryFantasyName, [Validators.required]),
      cnpj: new FormControl(item.cnpj, [Validators.required, Validators.minLength(14)]),
      address: new FormControl(item.address, [Validators.required]),
      number: new FormControl(item.number, [Validators.required]),
      neighborhood: new FormControl(item.neighborhood, [Validators.required]),
      city: new FormControl(item.city, [Validators.required]),
      stateId: new FormControl(item.stateId, [Validators.required]),
      complement: new FormControl(item.complement || ""),
      zipCode: new FormControl(item.zipCode, [Validators.required]),
      pabx: new FormControl(item.pabx, [Validators.required]),
      responsible: new FormControl(item.responsible, [Validators.required]),
      deptResponsible: new FormControl(item.deptResponsible),
      email: new FormControl(item.email, [Validators.required, Validators.email]),
      comercialPhone: new FormControl(item.comercialPhone, [Validators.required]),
      cellPhone: new FormControl(item.cellPhone),
      login: new FormControl(item.login, [Validators.required]),
      password: new FormControl("*****", [Validators.required]),
      confirmPassword: new FormControl("*****", [Validators.required]),
      status: new FormControl(1, [Validators.required]),
      Comments: new FormControl(item.Comments || ""),
      ramal: new FormControl(item.ramal),
    });

    if (!this.data.novo){
      this.laboratorioForm.controls["login"].disable();
      this.laboratorioForm.controls["password"].clearValidators();
      this.laboratorioForm.controls["confirmPassword"].clearValidators();
    }else{
      this.laboratorioForm.controls["password"].setValue("");
      this.laboratorioForm.controls["confirmPassword"].setValue("");
    }

    this.getStates();
  }

  SaveLaboratory() {
    let formulario = this.laboratorioForm.value;
    this.loader.open();

    if (!validarCNPJ(formulario.cnpj)){
      this.loader.close();
      this.information.information("YouVita", "CNPJ inválido, por favor verificar.");
      return;

    }

    if (this.data.novo) {
      this.login.IsExists(formulario.login).subscribe(res => {
        if (res.Status == "OK" && res.ObjectDTO.length > 0) {
          this.information.information("YouVita", "Este login já existe, por favor informar um diferente");
          this.loader.close();
          return false;
        }
        if (this.isValid(formulario)) {
          formulario.password = Md5.init(formulario.password);
          this.labService.saveLaboratory(formulario, this.data.novo).subscribe(res => {
            var cnt = new WebContract();
            cnt = res;
            if (cnt.Status == "OK") {
              this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });
            } else {
              this.snackBar.open(cnt.Message, "", { duration: 3000 });
            }   
            this.loader.close();         
            this.dialogRef.close("OK");
          });
        }
        this.loader.close();
      });
    } else {
      if (this.isValid(formulario)) {
        if (formulario.password != "*****") {
          formulario.password = Md5.init(formulario.password);
        }else{
          formulario.password =null;
        }        
        formulario.login = this.data.payload.login;
        this.labService.saveLaboratory(formulario, this.data.novo).subscribe(res => {
          var cnt = new WebContract();
          cnt = res;
          if (cnt.Status == "OK") {
            this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });
          } else {
            this.snackBar.open(cnt.Message, "", { duration: 3000 });
          }
          this.loader.close();
          this.dialogRef.close("OK");
        });
      }
      
    }
  }

  isValid(form: any) {
    if (form.password !== form.confirmPassword) {
      this.information.information("YouVita", "Confirmação de senha incorreta.");
      return false;
    }
    return true;
  }

  getStates() {
    this.serviceStates.listarEstados().subscribe(res => {
      this.linhasStates = [];
      this.linhasStates = res.ObjectDTO
      //console.log(res.ObjectDTO);
    })
  }

}
