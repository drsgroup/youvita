import { MedicinesComponent } from './medicines/medicines.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';
import {
  MatInputModule,
  MatIconModule,
  MatCardModule,
  MatMenuModule,
  MatButtonModule,
  MatChipsModule,
  MatListModule,
  MatTooltipModule,
  MatDialogModule,
  MatSnackBarModule,
  MatSlideToggleModule,
  MatToolbarModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MAT_DATE_FORMATS,
  DateAdapter,
  MAT_DATE_LOCALE,
  MatAutocompleteModule,
  MatRadioModule

} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { NegocioRoutingModule } from './negocio-routing.module';
import { NgxMaskModule } from 'ngx-mask';
import { NgxCurrencyModule } from "ngx-currency";
import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from "ngx-currency/src/currency-mask.config";
import { GradeComponent } from '../../components/common/grade/grade.component';
import { GradeVisualizarComponent } from '../../components/common/grade-visualizar/grade-visualizar.component';
import { LoginService } from '../../services/negocio/login/login.service';
import { CommonPipesModule } from '../../pipes/common/common-pipes.module';
import { NgxUpperCaseDirectiveModule } from 'ngx-upper-case-directive';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ClientesComponent } from './clientes/clientes.component';
import { PacientesComponent } from './pacientes/pacientes.component';
import { ClientesFormularioComponent } from './clientes/clientes-formulario/clientes-formulario.component';
import { PacientesFormularioComponent } from './pacientes/pacientes-formulario/pacientes-formulario.component';
import { TipoTelefoneComponent } from './tipo-telefone/tipo-telefone.component';
import { FormPhoneTypeComponent } from './tipo-telefone/form-phone-type/form-phone-type.component';
import { PacientesFoneComponent } from './pacientes/pacientes-formulario/pacientes-fone/pacientes-fone.component';
import { PacienteEnderecosComponent } from './pacientes/pacientes-formulario/paciente-enderecos/paciente-enderecos.component';
import { PacientePatologiasComponent } from './pacientes/pacientes-formulario/paciente-patologias/paciente-patologias.component';
import { ClientesContatosComponent } from './clientes/clientes-formulario/clientes-contatos/clientes-contatos.component';
import { MedicinesFormComponent } from './medicines/medicines-form/medicines-form.component';
import { LaboratorioComponent } from './laboratorio-component/laboratorio.component';
import { LaboratioFormularioComponent } from './laboratorio-component/laboratio-formulario/laboratio-formulario.component';
import { DoctorComponent } from './doctor/doctor.component';
import { DoctorFormComponent } from './doctor/doctor-form/doctor-form.component';
import { RepresentativeComponent } from './representative/representative.component';
import { RepresentativeFormComponent } from './representative/representative-form/representative-form.component';
import { DoctorViewComponent } from './doctor-view/doctor-view.component';
import { DoctoViewFormComponent } from './doctor-view/docto-view-form/docto-view-form.component';
import { LabViewComponent } from './lab-view/lab-view.component';
import { LabViewFormComponent } from './lab-view/lab-view-form/lab-view-form.component';
import { GradeComponent2 } from 'app/components/common/grade2/grade2.component';
import { DoctorOrdersComponent } from './doctor/doctor-orders/doctor-orders.component';
import { DoctorOrderDetailComponent } from './doctor/doctor-orders/doctor-order-detail/doctor-order-detail.component';
import { OrdersComponent } from './orders/orders.component';
import { OrdersDetailComponent } from './orders/orders-detail/orders-detail.component';
import { ListDoctorOrdersComponent } from './list-doctor-orders/list-doctor-orders.component';
import { ListDoctorOrderFormComponent } from './list-doctor-orders/list-doctor-order-form/list-doctor-order-form.component';
import { UsersComponent } from './users/users.component';
import { UsersFormComponent } from './users/users-form/users-form.component';
import { RepresentantePedidosComponent } from './representante-pedidos/representante-pedidos.component';
import { ReleaseOrderComponent } from './release-order/release-order.component';
import { ReleaseOrderFormComponent } from './release-order/release-order-form/release-order-form.component';
import { RepresentantePedidosFormComponent } from './representante-pedidos/representante-pedidos-form/representante-pedidos-form.component';
import { RepresentanteProgramadosComponent } from './representante-programados/representante-programados.component';
import { RepresentanteProgramadosFormComponent } from './representante-programados/representante-programados-form/representante-programados-form.component';
import { ConfirmReleaseOrderComponent } from './confirm-release-order/confirm-release-order.component';
import { ConfirmReleaseOrderFormComponent } from './confirm-release-order/confirm-release-order-form/confirm-release-order-form.component';
import { ProjetoComponent } from './projeto/projeto.component';
import { ProjetoFormularioComponent } from './projeto/projeto-formulario/projeto-formulario.component';
import { OnlyValid } from 'app/directives/common/onlyValid.directive';
import { ReportOrderDoctorComponent } from './report-order-doctor/report-order-doctor.component';



export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: "right",
  allowNegative: true,
  allowZero: true,
  decimal: ",",
  precision: 2,
  prefix: "R$ ",
  suffix: "",
  thousands: "."
};

@NgModule({
  imports: [NgxUpperCaseDirectiveModule,
    CommonModule,
    ReactiveFormsModule,
    CommonPipesModule,
    FormsModule,
    FlexLayoutModule,
    NgxDatatableModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatMenuModule,
    MatButtonModule,
    MatChipsModule,
    MatListModule,
    MatTooltipModule,
    MatDialogModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    MatToolbarModule,
    NegocioRoutingModule,
    MatSelectModule,
    MatCheckboxModule,
    MatStepperModule,
    MatRadioModule,
    NgxCurrencyModule,
    MatDatepickerModule,
    MatNativeDateModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatAutocompleteModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [
    GradeComponent,
    ClientesComponent,
    PacientesComponent,
    ClientesFormularioComponent,
    PacientesFormularioComponent,
    TipoTelefoneComponent,
    FormPhoneTypeComponent,
    PacientesFoneComponent,
    PacienteEnderecosComponent,
    PacientePatologiasComponent,
    ClientesContatosComponent,
    MedicinesComponent,
    MedicinesFormComponent,
    LaboratorioComponent,
    LaboratioFormularioComponent,
    DoctorComponent,
    DoctorFormComponent,
    RepresentativeComponent,
    RepresentativeFormComponent,
    DoctorViewComponent,
    DoctoViewFormComponent,
    LabViewComponent,
    LabViewFormComponent,
    GradeComponent2,
    DoctorOrdersComponent,
    DoctorOrderDetailComponent,
    OrdersComponent,
    OrdersDetailComponent,
    ListDoctorOrdersComponent,
    ListDoctorOrderFormComponent,
    UsersComponent,
    UsersFormComponent,
    RepresentantePedidosComponent,
    ReleaseOrderComponent,
    ReleaseOrderFormComponent,
    RepresentantePedidosFormComponent,
    RepresentanteProgramadosComponent,
    RepresentanteProgramadosFormComponent,
    ConfirmReleaseOrderComponent,
    ConfirmReleaseOrderFormComponent,
    ProjetoComponent,
    ProjetoFormularioComponent,
    OnlyValid,
    ReportOrderDoctorComponent

  ],
  exports: [MatAutocompleteModule],
  entryComponents: [
    ClientesFormularioComponent,
    PacientesFormularioComponent,
    FormPhoneTypeComponent,
    PacientesFoneComponent,
    PacienteEnderecosComponent,
    PacientePatologiasComponent,
    ClientesContatosComponent,
    MedicinesFormComponent,
    LaboratioFormularioComponent,
    DoctorFormComponent,
    RepresentativeFormComponent,
    DoctoViewFormComponent,
    LabViewFormComponent,
    DoctorOrderDetailComponent,
    OrdersDetailComponent,
    ListDoctorOrderFormComponent,
    UsersFormComponent,
    ReleaseOrderFormComponent,
    RepresentantePedidosFormComponent,
    RepresentanteProgramadosFormComponent,
    ProjetoFormularioComponent
  ],
  providers: [LoginService,

    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' }]

})
export class NegocioModule { }
