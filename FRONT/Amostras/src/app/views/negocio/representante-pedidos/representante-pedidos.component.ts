import { Component, OnInit, ViewChild } from '@angular/core';
import { GradeComponent } from 'app/components/common/grade/grade.component';
import { MatDialog, MatDialogRef } from '@angular/material';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { LoginService } from 'app/services/negocio/login/login.service';
import { Router } from '@angular/router';
import { OrdersDetailComponent } from '../orders/orders-detail/orders-detail.component';
import { RepresentantePedidosFormComponent } from './representante-pedidos-form/representante-pedidos-form.component';

@Component({
  selector: 'app-representante-pedidos',
  templateUrl: './representante-pedidos.component.html',
  styleUrls: ['./representante-pedidos.component.css']
})
export class RepresentantePedidosComponent implements OnInit {
  
  colunas = [ 
    {
      Propriedade: 'OrderAvaliableRepresentativeID',
      Titulo: 'ID Pedido',
      Visivel: true,
      Largura : 40
    },
    {
      Propriedade: 'RepresentativeName',
      Titulo: 'Representante',
      Visivel: true,
      Largura : 300
    },
    {
      Propriedade: 'startValidity',
      Titulo: 'Início Vigência',
      Visivel: true,
      Largura : 100,
      Tipo : "DATA"
    },
    {
      Propriedade: 'endValidity',
      Titulo: 'Fim Vigência',
      Visivel: true,
      Largura : 100,
      Tipo : "DATA"
    },
    {
      Propriedade: 'dateToSend',
      Titulo: 'Data envio ',
      Visivel: true,
      Largura : 100,
      Tipo : "DATA"
    }
    
  ];

  linhas = [];  

  constructor(private dialog: MatDialog,
    private crudServices : CRUDService,
    private login : LoginService) { }

  ngOnInit() {
    this.consultarPedido(""); 
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Novo Pedido Representante";
    } else {
      titulo = "Visualizar Pedido Representante: " + dados.OrderAvaliableRepresentativeID;
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(RepresentantePedidosFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.consultarPedido("");
        return;
      });
  }

  consultarPedido(pa){
    let userLogged = this.login.getNomeUsuarioDetalhes();

    if (userLogged.userType == 1){ //busca a partir do admin
      let params : any = new Object();
      params.userLogin = "teste";
      params.representativeLogin = "";
      params.status = "A"; //aprovado      
      params.representativeName = pa;
      params.laboratoryID = "";
  
      this.crudServices.GetParams(params, "/OrderAvaliableRepresentative").subscribe(res => {
        this.linhas = [];
        this.linhas = res.ObjectDTO;
      })
    }

    if (userLogged.userType == 5){ //busca a partir do representante
      let params : any = new Object();
      params.userLogin = "teste";
      params.representativeLogin = userLogged.username;
      params.status = "A"; //aprovado
      params.representativeName = "";
      params.laboratoryID = "";
 
      this.crudServices.GetParams(params, "/OrderAvaliableRepresentative").subscribe(res => {
        this.linhas = [];
        this.linhas = res.ObjectDTO;
      })
    }

    if (userLogged.userType == 3){ //busca a partir do representante
      let params : any = new Object();
      params.userLogin = "teste";
      params.representativeLogin = "";
      params.status = "A"; //aprovado
      params.representativeName = "";
      params.laboratoryID = userLogged.laboratoryID;
 
      this.crudServices.GetParams(params, "/OrderAvaliableRepresentative").subscribe(res => {
        this.linhas = [];
        this.linhas = res.ObjectDTO;
      })
    }    
    
    // if (userLogged.userType == 4){ //busca a partir do médico
    //   let params : any = new Object();
    //   params.userLogin = "teste"; 
    //   params.loginDoctor = userLogged.username;
    //   params.status = "A"; //aprovado
  
    //   this.crudServices.GetParams(params, "/OrderAvaliableRepresentative").subscribe(res => {
    //     this.linhas = [];
    //     this.linhas = res.ObjectDTO;
    //   })
    // }    
  }


  deletePedido(pa){

  }

}
