import { BaseService } from './../base/base.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WebContract } from 'app/models/base/Contrato';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RelatorioService extends BaseService {

  private _api = "/relatorio";

  constructor(public _http: HttpClient) {
    super();
  }

  public listarLavagens(dtIni: number, dtFim: number): Observable<WebContract> {
    return this._http.get<WebContract>(this._Url + this._api, {
      observe: 'response',
      params: { dataIni: dtIni.toString(), dataFim: dtFim.toString() }
    })
      .do(res => this.getToken(res))
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body;        
        return cnt;
      })
      .catch(this.handleError);
  }

}
