import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { WebContract } from 'app/models/base/Contrato';

@Injectable({
  providedIn: 'root'
})
export class DoctorService extends BaseService{
  private _api = "/doctor";

  constructor(private _http: HttpClient) {
    super();
  }  

  public saveDoctor(doc: any, novo: boolean): Observable<WebContract> {
    if (novo === true) {
      return this._http.post<WebContract>(this._Url + this._api, doc, { observe: 'response', params: { userLogin: "joaodokko" } })
        //.do(res => this.getToken(res))
        .map(res => { var cnt = new WebContract(); cnt = res.body; return cnt; })
        .catch(this.handleError);
    }
    else {
      return this._http.put<WebContract>(this._Url + this._api, doc, { observe: 'response', params: { userLogin: "joaodokko" } })
        //.do(res => this.getToken(res))
        .map(res => {
          var cnt = new WebContract();
          cnt = res.body; 
          return cnt;
        })
        .catch(this.handleError);
    }
  }  

  public deleteDoctor(doc: any): Observable<WebContract> {
    let htppParams = new HttpParams();

    return this._http.delete<WebContract>(this._Url + this._api,
      { observe: 'response', params: { "userLogin": "jao", "doctorID": doc.doctorID.toString() } })
      //.do(res => this.getToken(res))
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body; return cnt;
      })
      .catch(this.handleError);
  }  

  public getDoctors(parametro:string): Observable<WebContract> {  
    return this._http.get<WebContract>(this._Url+this._api, { observe: 'response',params:{userLogin:"rafaelTeste", filter : parametro} })
    //.do(res => this.getToken(res))
    .map(res => {
      var cnt = new WebContract();
      cnt = res.body;
      return cnt;
      })
      .catch(this.handleError);            
  }

  public getDoctorsByLogin(login:string): Observable<WebContract> {             
    return this._http.get<WebContract>(this._Url+this._api, { observe: 'response',params:{"login" : login, "login_2":"nada", "login_3":"nada" } })
    //.do(res => this.getToken(res))
    .map(res => {
      var cnt = new WebContract();
      cnt = res.body;
      return cnt;
      })
      .catch(this.handleError);            
  }
  

  public addAddress(address : any): Observable<WebContract> {
    let htppParams = new HttpParams();

    return this._http.post<WebContract>(this._Url + "/DoctorAddresses", address,
      { observe: 'response', params: { "userLogin": "jao" } })
      //.do(res => this.getToken(res))
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body; 
        return cnt;
      })
      .catch(this.handleError);
  }
  
  public deleteAddress(doctorAddressesId : number): Observable<WebContract> {
    let htppParams = new HttpParams();

    return this._http.delete<WebContract>(this._Url + "/DoctorAddresses",
      { observe: 'response', params: { "userLogin": "jao", "doctorAddressesId": doctorAddressesId.toString() } })
      //.do(res => this.getToken(res))
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body; 
        return cnt;
      })
      .catch(this.handleError);
  }      


}
