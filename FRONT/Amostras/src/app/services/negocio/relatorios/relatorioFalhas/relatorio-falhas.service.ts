import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../../base/base.service';
import { formatDate } from '@angular/common';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RelatorioFalhasService extends BaseService {

  private _api = "/ReportEventFaild";


  constructor(private _http : HttpClient) { 
    super();
  }

  public ObterDadosFalhas(dtIni: Date, dtFim: Date, _partnerId : number): Observable<any> {
    //alert(formatDate(dtIni, "yyyy-MM-dd HH:mm:ss", "pt-BR"));
    return this._http.get<any>(this._Url + this._api, {
      observe: 'response',
      params: {partnerId : _partnerId.toString(), startDate: formatDate(dtIni, "yyyy/MM/dd HH:mm:ss", "pt-BR"), endDate: formatDate(dtFim, "yyyy/MM/dd HH:mm:ss", "pt-BR") }
    })
      //.do(res => this.getToken(res))
      .map(res => {                
        return res.body;
      })
      .catch(this.handleError);
  }
}
