import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BaseService } from '../../base/base.service';
import { formatDate } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class RelatorioEventosService extends BaseService{

  private _api = "/ReportEventLog";


  constructor(private _http : HttpClient) { 
    super();
  }

  public ObterDadosFalhas(_clientId : number, _eventCode : string, _partnerId : number, _dtIni: Date, _dtFim: Date): Observable<any> {
    //alert(formatDate(_dtIni, "yyyy-MM-dd HH:mm:00", "pt-BR"));
    return this._http.get<any>(this._Url + this._api, {
      observe: 'response',
      params: { clientId : _clientId.toString(), partnerId: _partnerId.toString(), eventCode : _eventCode, startDate: formatDate(_dtIni, "yyyy/MM/dd HH:mm:ss", "pt-BR"), endDate: formatDate(_dtFim, "yyyy/MM/dd HH:mm:ss", "pt-BR") }
    })
      //.do(res => this.getToken(res))
      .map(res => {                
        return res.body;
      })
      .catch(this.handleError);
  }

}
