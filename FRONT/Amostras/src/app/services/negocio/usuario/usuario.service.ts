import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { BaseService } from '../base/base.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppLoaderService } from '../../dialogs/app-loader/app-loader.service';
import { Usuario } from '../../../models/base/negocio/Usuario';
import { WebContract } from 'app/models/base/Contrato';
;

@Injectable({
  providedIn: 'root' 
})
export class UsuarioService extends BaseService {  
  private _api = "/usuarios";

  constructor(public _http: HttpClient,
    private loader: AppLoaderService) {
    super();
  }

  public GravarUsuario(usuario: Usuario, novo: boolean): Observable<WebContract> {
    if (novo === true) {
      return this._http.post<WebContract>(this._Url + this._api, usuario, { observe: 'response' })
        .do(res => this.getToken(res))
        .map(res => {
          var cnt = new WebContract();
          cnt = res.body;
          return cnt;
        })
        .catch(this.handleError);
    }else{
      return this._http.put<WebContract>(this._Url + this._api, usuario, { observe: 'response' })
        .do(res => this.getToken(res))
        .map(res => {
          var cnt = new WebContract();
          cnt = res.body;
          return cnt;
        })
        .catch(this.handleError);      
    }
  }  

  public ListarUsuarios(): Observable<WebContract> {
    return this._http.get<WebContract>(this._Url + this._api, { observe: 'response' })
      .do(res => this.getToken(res))
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body;
        return cnt;
      })
      .catch(this.handleError); 
  }

  public ExcluirUsuario(usuarioKey : string): Observable<WebContract> { 
    return this._http.delete<WebContract>(this._Url + this._api, { observe: 'response', params: new HttpParams().set('usuarioKey', usuarioKey) })
      .do(res => this.getToken(res))
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body;
        return cnt;
      })
      .catch(this.handleError); 
  }

}
