import { Injectable } from '@angular/core';
import { BaseService } from 'app/services/negocio/base/base.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { WebContract } from 'app/models/base/Contrato';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PatientsService extends BaseService {


  private _api = "/patient";

  constructor(private _http: HttpClient) {

    super();

  }

  public getPatients(parametro: string, ClientId : number): Observable<WebContract> {    
    return this._http.get<WebContract>(this._Url + this._api, { observe: 'response', params: { userLogin: "joaodokko", filter: parametro, clientId : ClientId.toString() } })
      //.do(res => this.getToken(res))
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body;
        return cnt;
      })
      .catch(this.handleError);
  }

  public savePatient(patient: any, novo: boolean): Observable<WebContract> {
    if (novo === true) {
      return this._http.post<WebContract>(this._Url + this._api, patient, { observe: 'response', params: { userLogin: "joaodokko" } })
        //.do(res => this.getToken(res))
        .map(res => { var cnt = new WebContract(); cnt = res.body; return cnt; })
        .catch(this.handleError);
    }
    else {
      return this._http.put<WebContract>(this._Url + this._api, patient, { observe: 'response', params: { userLogin: "joaodokko" } })
        //.do(res => this.getToken(res))
        .map(res => {
          var cnt = new WebContract();
          cnt = res.body; return cnt;
        })
        .catch(this.handleError);
    }
  }

  public deletePatient(patient: any): Observable<WebContract> {
    let htppParams = new HttpParams();

    return this._http.delete<WebContract>(this._Url + this._api,
      { observe: 'response', params: { "userLogin": "jao", "patientId": patient.patientId.toString() } })
      //.do(res => this.getToken(res))
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body; return cnt;
      })
      .catch(this.handleError);
  }

}
