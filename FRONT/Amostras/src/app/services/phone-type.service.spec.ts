import { TestBed, inject } from '@angular/core/testing';

import { PhoneTypeService } from './phone-type.service';

describe('PhoneTypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PhoneTypeService]
    });
  });

  it('should be created', inject([PhoneTypeService], (service: PhoneTypeService) => {
    expect(service).toBeTruthy();
  }));
});
