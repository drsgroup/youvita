import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { JwtModule, JwtHelperService } from '@auth0/angular-jwt';
import { WebContract } from '../../models/base/Contrato';

@Injectable()
export class AuthGuard implements CanActivate {
  private isAuthenticated = true; // Set this value dynamically

  constructor(private router: Router) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    //var StrUsuario = localStorage.getItem("Token");

    // if (StrUsuario !== null) {
    //   const helper = new JwtHelperService();
    //   var token = helper.decodeToken(StrUsuario);

    //   if (token) {
    //     this.isAuthenticated = true;
    //     return true;
    //   }
    // }
    //this.router.navigate(['/sessao/entrar']);
    return true;
  }

  public getToken(): string {
    // console.log("obtendo token: "+localStorage.getItem('Token'));
    return localStorage.getItem('Token');
  }

}