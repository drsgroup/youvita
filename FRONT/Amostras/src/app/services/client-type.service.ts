import { Injectable } from '@angular/core';
import { BaseService } from './negocio/base/base.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { WebContract } from 'app/models/base/Contrato';

@Injectable({
  providedIn: 'root'
})
export class ClientTypeService extends BaseService{

  private _api = "/clienttype";

  constructor(public _http: HttpClient) {
    super();
  }

  public listarClientType(): Observable<WebContract> {    
         
    return this._http.get<WebContract>(this._Url+this._api, { observe: 'response',params:{userLogin:"rafaelTeste"} })
    //.do(res => this.getToken(res))
    .map(res => {
      var cnt = new WebContract();
      cnt = res.body;
      return cnt;
      })
      .catch(this.handleError);            
  }

  
}
