import { Component, OnInit } from '@angular/core';
import { NavigationService } from "../../../services/navigation/navigation.service";
import { WebContract } from '../../../models/base/Contrato';
import { LoginService } from 'app/services/negocio/login/login.service';

@Component({
  selector: 'navigation',
  templateUrl: './navigation.template.html'
})
export class NavigationComponent {
  hasIconTypeMenuItem;
  iconTypeMenuTitle: string;
  menuItems: any[];

  constructor(private navService: NavigationService,
    private loginService: LoginService) { }
  ngOnInit() {
    this.iconTypeMenuTitle = this.navService.iconTypeMenuTitle;
    // Loads menu items from NavigationService    

    /*
    this.navService.getMenus().subscribe(contrato => {
      this.menuItems = contrato.Dados;      
      this.hasIconTypeMenuItem = !!this.menuItems.filter(item => item.type === 'icon').length;
    });
    */
    let user = this.loginService.getNomeUsuarioDetalhes();

    let x = [];

    switch (user.userType) {
      case 1:
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "supervisor_account", "Name": "Cadastro Cliente", "Ordem": 1, "State": "cadastros/clientes", "Type": "link" });
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "accessibility", "Name": "Cadastro Paciente", "Ordem": 1, "State": "cadastros/pacientes", "Type": "link" });
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "assignment", "Name": "Cadastro de Receita", "Ordem": 1, "State": "cadastros/receitas", "Type": "link" });
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "local_hospital", "Name": "Consulta Medicamentos", "Ordem": 1, "State": "cadastros/medicamentos", "Type": "link" });
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "style", "Name": "Anamnese", "Ordem": 1, "State": "cadastros/anamnese", "Type": "link" });        
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "assignment_ind", "Name": "Enviar Pedido", "Ordem": 1, "State": "cadastros/confirmpedidos", "Type": "link" });
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "add_to_queue", "Name": "Cadastro de Efeitos Adversos", "Ordem": 1, "State": "cadastros/efeitosadversos", "Type": "link" });
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "assignment_turned_in", "Name": "Pedido Paciente", "Ordem": 1, "State": "cadastros/ordemliberacao", "Type": "link" });
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "event_note", "Name": "Atenção Farmacêutica", "Ordem": 1, "State": "cadastros/atencao", "Type": "link" });
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "compare_arrows", "Name": "Interação Medicamentosa", "Ordem": 1, "State": "cadastros/interacao", "Type": "link" });
        /*
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "shopping_cart", "Name": "Pedidos Programados Médico", "Ordem": 1, "State": "cadastros/programados", "Type": "link" });        
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "shop", "Name": "Pedidos Representante", "Ordem": 1, "State": "cadastros/pedidorepresentante", "Type": "link" });        
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "domain", "Name": "Pedidos Programados Representante", "Ordem": 1, "State": "cadastros/representanteprogramados", "Type": "link" });                        
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "domain", "Name": "Criar Novo Usuário", "Ordem": 1, "State": "cadastros/usuarios", "Type": "link" });                
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "store", "Name": "Cotação \\ Autorização de Medicamentos", "Ordem": 1, "State": "cadastros/ordemliberacao", "Type": "link" });
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "store", "Name": "Confirmação de Pedido", "Ordem": 1, "State": "cadastros/confirmpedidos", "Type": "link" });       
        */
        break;
        case 2: //operadora de saude          
          x.push({ "Ativo": "S", "Disable": 0, "Icon": "accessibility", "Name": "Cadastro Paciente", "Ordem": 1, "State": "cadastros/pacientes", "Type": "link" });
          x.push({ "Ativo": "S", "Disable": 0, "Icon": "assignment", "Name": "Cadastro de Receita", "Ordem": 1, "State": "cadastros/receitas", "Type": "link" });
          x.push({ "Ativo": "S", "Disable": 0, "Icon": "local_hospital", "Name": "Cadastro Medicamentos", "Ordem": 1, "State": "cadastros/medicamentos", "Type": "link" });
          //x.push({ "Ativo": "S", "Disable": 0, "Icon": "style", "Name": "Anamnese", "Ordem": 1, "State": "cadastros/anamnese", "Type": "link" });                  
          x.push({ "Ativo": "S", "Disable": 0, "Icon": "assignment_turned_in", "Name": "Pedido Paciente", "Ordem": 1, "State": "cadastros/ordemliberacao", "Type": "link" });                    
          break;
      case 3: //laboratorio
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "local_hospital", "Name": "Cadastro Médico", "Ordem": 1, "State": "cadastros/medicos", "Type": "link" });
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "domain", "Name": "Pedido Médico", "Ordem": 1, "State": "cadastros/homelab", "Type": "link" });
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "shopping_cart", "Name": "Pedidos Programados Médico", "Ordem": 1, "State": "cadastros/programados", "Type": "link" });

        x.push({ "Ativo": "S", "Disable": 0, "Icon": "supervisor_account", "Name": "Cadastro Representante", "Ordem": 1, "State": "cadastros/representantes", "Type": "link" });
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "shop", "Name": "Pedido Representante", "Ordem": 1, "State": "cadastros/pedidorepresentante", "Type": "link" });
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "domain", "Name": "Pedidos Programados Representante", "Ordem": 1, "State": "cadastros/representanteprogramados", "Type": "link" });
        break;

      case 4: //medico
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "assignment", "Name": "Pedidos para Aprovação", "Ordem": 1, "State": "cadastros/homemedico", "Type": "link" });
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "shopping_cart", "Name": "Pedidos Recebidos", "Ordem": 1, "State": "cadastros/pedidos", "Type": "link" });
        x.push({ "Ativo": "S", "Disable": 0, "Icon": "shopping_cart", "Name": "Pedidos Programados", "Ordem": 1, "State": "cadastros/programados", "Type": "link" });
        break;

      case 5: //representante 
        let i: any = new Object();
        i.Sub = [];
        i.Sub.push({ "Ativo": "S", "Disable": 0, "Icon": "shopping_cart", "Name": "Pedidos", "Ordem": 1, "State": "cadastros/pedidos", "Type": "link" })
        i.Sub.push({ "Ativo": "S", "Disable": 0, "Icon": "shopping_cart", "Name": "Pedidos Programados", "Ordem": 1, "State": "cadastros/programados", "Type": "link" });
        i.Ativo = "S";
        i.Disable = 0;
        i.Icon = "assignment_ind";
        i.Name = "Médicos";
        i.Ordem = 1;
        i.State = "cadastros/pedidos";
        i.Type = "dropDown";
        i.Tooltip = "Médicos";
        x.push(i);

        i = new Object();
        i.Sub = [];
        i.Sub.push({ "Ativo": "S", "Disable": 0, "Icon": "shopping_cart", "Name": "Pedidos", "Ordem": 1, "State": "cadastros/pedidorepresentante", "Type": "link" })
        i.Sub.push({ "Ativo": "S", "Disable": 0, "Icon": "shopping_cart", "Name": "Pedidos Programados", "Ordem": 1, "State": "cadastros/representanteprogramados", "Type": "link" });
        i.Ativo = "S";
        i.Disable = 0;
        i.Icon = "supervisor_account";
        i.Name = "Representante";
        i.Ordem = 1;
        i.State = "cadastros/pedidos";
        i.Type = "dropDown";
        i.Tooltip = "Médicos";
        x.push(i);
        break;


    }

    this.menuItems = x;
    this.hasIconTypeMenuItem = !!this.menuItems.filter(item => item.type === 'icon').length;
  }

}