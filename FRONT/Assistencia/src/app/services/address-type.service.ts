import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { WebContract } from 'app/models/base/Contrato';
import { HttpClientModule, HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from 'app/services/negocio/base/base.service';

@Injectable({
  providedIn: 'root'
})
export class AddressTypeService extends BaseService {

  private _api = "/addressType";

  constructor(private _http: HttpClient) {
    super();
  }

  public getAddressType(): Observable<WebContract> {

    return this._http.get<WebContract>(this._Url + this._api, { observe: 'response', params: { userLogin: "rafaelTeste" } })
      //.do(res => this.getToken(res))
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body;
        return cnt;
      })
      .catch(this.handleError);
  }


}
