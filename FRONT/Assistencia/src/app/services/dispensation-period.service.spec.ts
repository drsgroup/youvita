import { TestBed, inject } from '@angular/core/testing';

import { DispensationPeriodService } from './dispensation-period.service';

describe('DispensationPeriodService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DispensationPeriodService]
    });
  });

  it('should be created', inject([DispensationPeriodService], (service: DispensationPeriodService) => {
    expect(service).toBeTruthy();
  }));
});
