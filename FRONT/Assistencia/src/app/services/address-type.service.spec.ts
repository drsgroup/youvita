import { TestBed, inject } from '@angular/core/testing';

import { AddressTypeService } from './address-type.service';

describe('AddressTypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddressTypeService]
    });
  });

  it('should be created', inject([AddressTypeService], (service: AddressTypeService) => {
    expect(service).toBeTruthy();
  }));
});
