import { TestBed, inject } from '@angular/core/testing';

import { PathologiesService } from './pathologies.service';

describe('PathologiesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PathologiesService]
    });
  });

  it('should be created', inject([PathologiesService], (service: PathologiesService) => {
    expect(service).toBeTruthy();
  }));
});
