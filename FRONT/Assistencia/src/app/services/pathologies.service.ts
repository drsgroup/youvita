import { Injectable } from '@angular/core';
import { BaseService } from 'app/services/negocio/base/base.service';
import { HttpClient } from '@angular/common/http';
import { WebContract } from 'app/models/base/Contrato';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class PathologiesService  extends BaseService {

  private _api = "/Pathologie";

  constructor(private _http: HttpClient) {
    super();
  } 

  public getPathologies(): Observable<WebContract> {    
         
    return this._http.get<WebContract>(this._Url+this._api, { observe: 'response',params:{userLogin:"joaodokko"} })
    //.do(res => this.getToken(res))
    .map(res => {
      var cnt = new WebContract();
      cnt = res.body;
      return cnt;
      })
      .catch(this.handleError);            
  }

}
