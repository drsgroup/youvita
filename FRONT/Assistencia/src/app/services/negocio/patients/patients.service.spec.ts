import { TestBed, inject } from '@angular/core/testing';

import { PatientsServiceService } from './patients-service.service';

describe('PatientsServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PatientsServiceService]
    });
  });

  it('should be created', inject([PatientsServiceService], (service: PatientsServiceService) => {
    expect(service).toBeTruthy();
  }));
});
