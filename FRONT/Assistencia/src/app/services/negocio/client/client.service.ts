import { BaseService } from './../base/base.service';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { WebContract } from 'app/models/base/Contrato';


@Injectable({
  providedIn: 'root'
})
export class ClientService extends BaseService {
  private _api = "/client";
  private _apiConatct = "/ClientContact";
  private _apiMedicine ="/medicine";

  constructor(public _http: HttpClient) {
    super();
  }

  // public listarClient(partnerId : number): Observable<any> {

  //   return this._http.get<any>(this._Url + this._api, { observe: 'response', params: new HttpParams().set('partnerId', partnerId.toString()) })
  //     .map(res => {
  //       return res.body;
  //     })
  //     .catch(this.handleError);
  // }

  public listarClient(parametro: string): Observable<WebContract> {

    return this._http.get<WebContract>(this._Url + this._api, { observe: 'response', params: { userLogin: "rafaelTeste", filter: parametro } })
      //.do(res => this.getToken(res))
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body;
        return cnt;
      })
      .catch(this.handleError);
  }

  public GravarClient(client: any, novo: boolean): Observable<WebContract> {
    if (novo === true) {
      return this._http.post<WebContract>(this._Url + this._api, client, { observe: 'response', params: { userLogin: "rafaelTeste" } })
        //.do(res => this.getToken(res)) 
        .map(res => {
          var cnt = new WebContract();
          cnt = res.body;
          return cnt;
        })
        .catch(this.handleError);
    }
    else {
      return this._http.put<WebContract>(this._Url + this._api, client, { observe: 'response', params: { userLogin: "rafaelTeste" } })
        //.do(res => this.getToken(res)) 
        .map(res => {
          var cnt = new WebContract();
          cnt = res.body;
          return cnt;
        })
        .catch(this.handleError);
    }
  }

  public deleteClient(client: any): Observable<WebContract> {
    let htppParams = new HttpParams();

    return this._http.delete<WebContract>(this._Url + this._api,
      { observe: 'response', params: { "userLogin": "jao", "clientId": client.clientId.toString() } })
      //.do(res => this.getToken(res))
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body; return cnt;
      })
      .catch(this.handleError);
  }  

  public deleteClientContact(contactId : number): Observable<WebContract> {
    let htppParams = new HttpParams();

    return this._http.delete<WebContract>(this._Url + this._apiConatct,
      { observe: 'response', params: { "userLogin": "jao", "contactId": contactId.toString() } })
      //.do(res => this.getToken(res))
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body; 
        return cnt;
      })
      .catch(this.handleError);
  }    

  public addClientContact(contact : any): Observable<WebContract> {
    let htppParams = new HttpParams();

    return this._http.post<WebContract>(this._Url + this._apiConatct, contact,
      { observe: 'response', params: { "userLogin": "jao" } })
      //.do(res => this.getToken(res))
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body; 
        return cnt;
      })
      .catch(this.handleError);
  }    

  public buscarMedicamentos(parametro: string): Observable<WebContract> {
    return this._http.get<WebContract>(this._Url + this._apiMedicine, { observe: 'response', params: { userLogin: "rafaelTeste", filter: parametro } })    
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body;
        return cnt;
      })
      .catch(this.handleError);
  }  
}
