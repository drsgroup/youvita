import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { WebContract } from 'app/models/base/Contrato';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReleaseOrderService extends BaseService{

  private _api = "/releaseOrder";

  constructor(private _http: HttpClient) {
    super();
  }  

  public getReleaseOrderByFilter(parametro:string): Observable<WebContract> {    
         
    return this._http.get<WebContract>(this._Url+this._api, { observe: 'response',params:{userLogin:"rafaelTeste", filter : parametro} })
    //.do(res => this.getToken(res))
    .map(res => {
      var cnt = new WebContract();
      cnt = res.body;
      return cnt;
      })
      .catch(this.handleError);            
  }

  public getReleaseOrderByOrder(parametro:string): Observable<WebContract> {    
         
    return this._http.get<WebContract>(this._Url+this._api, { observe: 'response',params:{userLogin:"rafaelTeste", releaseOrderId : parametro, ok : "OK" } })
    //.do(res => this.getToken(res))
    .map(res => {
      var cnt = new WebContract();
      cnt = res.body;
      return cnt;
      })
      .catch(this.handleError);            
  }

  public deleteReleaseOrder(releaseOrder: any): Observable<WebContract> {
    let htppParams = new HttpParams();

    return this._http.delete<WebContract>(this._Url + this._api,
      { observe: 'response', params: { "userLogin": "ok", "releaseOrderId": releaseOrder.releaseOrderId.toString() } })
      //.do(res => this.getToken(res))
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body; return cnt;
      })
      .catch(this.handleError);
  }



}
