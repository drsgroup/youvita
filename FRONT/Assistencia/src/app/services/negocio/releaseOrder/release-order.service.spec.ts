import { TestBed, inject } from '@angular/core/testing';

import { ReleaseOrderService } from './release-order.service';

describe('ReleaseOrderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReleaseOrderService]
    });
  });

  it('should be created', inject([ReleaseOrderService], (service: ReleaseOrderService) => {
    expect(service).toBeTruthy();
  }));
});
