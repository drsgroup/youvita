import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { WebContract } from 'app/models/base/Contrato';

@Injectable({
  providedIn: 'root'
})
export class RepresentativeService extends BaseService {
  private _api = "/representative";

  constructor(private _http: HttpClient) {
    super();
  }  

  public saveRepresentative(representative: any, novo: boolean): Observable<WebContract> {
    if (novo === true) {
      return this._http.post<WebContract>(this._Url + this._api, representative, { observe: 'response', params: { userLogin: "joaodokko" } })
        //.do(res => this.getToken(res))
        .map(res => { var cnt = new WebContract(); cnt = res.body; return cnt; })
        .catch(this.handleError);
    }
    else {
      return this._http.put<WebContract>(this._Url + this._api, representative, { observe: 'response', params: { userLogin: "joaodokko" } })
        //.do(res => this.getToken(res))
        .map(res => {
          var cnt = new WebContract();
          cnt = res.body; return cnt;
        })
        .catch(this.handleError);
    }
  }  

  public deleteRepresentative(representative: any): Observable<WebContract> {
    let htppParams = new HttpParams();

    return this._http.delete<WebContract>(this._Url + this._api,
      { observe: 'response', params: { "userLogin": "jao", "RepresentativeID": representative.representativeID.toString() } })
      //.do(res => this.getToken(res))
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body; return cnt;
      })
      .catch(this.handleError);
  }  

  public getRepresentatives(parametro:string): Observable<WebContract> {    
         
    return this._http.get<WebContract>(this._Url+this._api, { observe: 'response',params:{userLogin:"rafaelTeste", filter : parametro} })
    //.do(res => this.getToken(res))
    .map(res => {
      var cnt = new WebContract();
      cnt = res.body;
      return cnt;
      })
      .catch(this.handleError);            
  }


}


