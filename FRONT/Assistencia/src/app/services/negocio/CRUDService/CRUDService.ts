import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { WebContract } from 'app/models/base/Contrato';

@Injectable({
  providedIn: 'root'
})
export class CRUDService extends BaseService{  

  constructor(private _http: HttpClient) {
    super();
  }  

  public Save(doc: any, novo: boolean, api : string): Observable<WebContract> {
    if (novo === true) {
      return this._http.post<WebContract>(this._Url + api, doc, { observe: 'response', params: { userLogin: "joaodokko" } })
        //.do(res => this.getToken(res))
        .map(res => { var cnt = new WebContract(); cnt = res.body; return cnt; })
        .catch(this.handleError);
    }
    else {
      return this._http.put<WebContract>(this._Url + api, doc, { observe: 'response', params: { userLogin: "joaodokko" } })
        //.do(res => this.getToken(res))
        .map(res => {
          var cnt = new WebContract();
          cnt = res.body; 
          return cnt;
        })
        .catch(this.handleError);
    }
  }  

  public Delete(doc: any, api : string): Observable<WebContract> {
    let htppParams = new HttpParams();

    return this._http.delete<WebContract>(this._Url + api,
      { observe: 'response', params: { "userLogin": "jao", "doctorID": doc.doctorID.toString() } })
      //.do(res => this.getToken(res))
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body; return cnt;
      })
      .catch(this.handleError);
  }  

  public List(parametro:string, api : string): Observable<WebContract> {    
         
    return this._http.get<WebContract>(this._Url+api, { observe: 'response',params:{userLogin:"rafaelTeste", filter : parametro} })
    //.do(res => this.getToken(res))
    .map(res => {
      var cnt = new WebContract();
      cnt = res.body;
      return cnt;
      })
      .catch(this.handleError);            
  }  

  public GetParams(parametros : any, api : string): Observable<WebContract> {             
    return this._http.get<WebContract>(this._Url+api, { observe: 'response', params : parametros })
    //.do(res => this.getToken(res))
    .map(res => {
      var cnt = new WebContract();
      cnt = res.body;
      return cnt;
      })
      .catch(this.handleError);            
  }   

  public DeleteParams(parametros : any, api : string): Observable<WebContract> {             
    return this._http.delete<WebContract>(this._Url+api, { observe: 'response', params : parametros })
    //.do(res => this.getToken(res))
    .map(res => {
      var cnt = new WebContract();
      cnt = res.body;
      return cnt;
      })
      .catch(this.handleError);            
  }   

}
