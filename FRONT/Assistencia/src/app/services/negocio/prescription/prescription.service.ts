import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { WebContract } from 'app/models/base/Contrato';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PrescriptionService extends BaseService{
  
  private _api = "/prescription";

  constructor(private _http: HttpClient ) {
    super();
   }

   public getPrescriptionByFilter(parametro:string): Observable<WebContract> {    
         
    return this._http.get<WebContract>(this._Url+this._api, { observe: 'response',params:{userLogin:"rafaelTeste", filter : parametro} })
    //.do(res => this.getToken(res))
    .map(res => {
      var cnt = new WebContract();
      cnt = res.body;
      return cnt;
      })
      .catch(this.handleError);            
  }

  public getPrescriptionByOrder(parametro:string): Observable<WebContract> {    
         
    return this._http.get<WebContract>(this._Url+this._api, { observe: 'response',params:{userLogin:"rafaelTeste", PrescriptionId : parametro, ok : "OK" } })
    //.do(res => this.getToken(res))
    .map(res => {
      var cnt = new WebContract();
      cnt = res.body;
      return cnt;
      })
      .catch(this.handleError);            
  }

  public savePrescription(prescription: any, novo: boolean): Observable<WebContract> {
    if (novo === true) {
      return this._http.post<WebContract>(this._Url + this._api, prescription, { observe: 'response', params: { userLogin: "rafael" } })
        //.do(res => this.getToken(res))
        .map(res => { var cnt = new WebContract(); cnt = res.body; return cnt; })
        .catch(this.handleError);
    }
    else {
      return this._http.put<WebContract>(this._Url + this._api, prescription, { observe: 'response', params: { userLogin: "rafael" } })
        //.do(res => this.getToken(res))
        .map(res => {
          var cnt = new WebContract();
          cnt = res.body; return cnt;
        })
        .catch(this.handleError);
    }
  }

  public deletePrescription(prescription: any): Observable<WebContract> {
    let htppParams = new HttpParams();

    return this._http.delete<WebContract>(this._Url + this._api,
      { observe: 'response', params: { "userLogin": "ok", "prescriptionId": prescription.prescriptionId.toString() } })
      //.do(res => this.getToken(res))
      .map(res => {
        var cnt = new WebContract();
        cnt = res.body; return cnt;
      })
      .catch(this.handleError);
  }
}
