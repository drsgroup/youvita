import { BaseService } from './../base/base.service';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class EventService extends BaseService{
  private _api = "/event";

  constructor(private _http : HttpClient) { 
    super();
  }


  public listarEvent(): Observable<any> {    
    return this._http.get<any>(this._Url + this._api, { observe: 'response' })
      .map(res => {
        return res.body;
      })
      .catch(this.handleError);
  }


}
