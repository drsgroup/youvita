import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { map, take } from 'rxjs/operators';

import { environment } from 'environments/environment';
import { BaseService } from '../base/base.service';
import { WebContract } from '../../../models/base/Contrato';
import { Usuario } from 'app/models/base/negocio/Usuario';
import { Md5 } from "md5-typescript";


@Injectable({
  providedIn: 'root'
})
export class LoginService extends BaseService {
  private _api = "/login";

  constructor(public _http: HttpClient) {
    super();
  }

  // public FazerLogin(usuario: string, senha: string): Observable<any> {    
  //   let head = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
  //   return this._http.post(this._Url + this._api, { "PartnerUserName": usuario, "PartnerPassword": senha }, { observe: 'response' })
  //     //.do(res => this.getToken(res))
  //     .map(res => {       
  //       return res.body;
  //     })
  //     .catch(this.handleError);
  // }

  public FazerLogin(login : any): Observable<any> {    
    let head = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let loginAux : any;
    loginAux = new Object();
    loginAux.userEnabled = 0;
    loginAux.userId = 0;
    loginAux.userLoginName = login.username;
    loginAux.userName = "";
    loginAux.userPassword = Md5.init(login.password);

    
    return this._http.post(this._Url + this._api, loginAux, { observe: 'response' })
      //.do(res => this.getToken(res))
      .map(res => {       
        return res.body;
      })
      .catch(this.handleError);
  }

  public IsExists(login : string) : Observable<any>{
    let head = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });   
    return this._http.get<WebContract>(this._Url+"/user", { observe: 'response',params:{userLogin:"rafaelTeste", login : login} })
    //.do(res => this.getToken(res))
    .map(res => {
      var cnt = new WebContract();
      cnt = res.body;
      return cnt;
      })
      .catch(this.handleError);            
  }

  

  public getUsuarioLogado(): string {
    var StrUsuario = localStorage.getItem("UsuarioAtual");
    if (StrUsuario !== null) {
      var cnt = new WebContract();
      cnt = JSON.parse(StrUsuario);
      //return cnt.Dados[0].usuarioKey;
      return "OK";
    } else {
      return "";
    }
  }

  public getNomeUsuarioLogado(): string {
    var StrUsuario = localStorage.getItem("UsuarioAtual");
    if (StrUsuario !== null) {
      var cnt = new WebContract();
      cnt = JSON.parse(StrUsuario);
      //return cnt.Dados[0].Nome;
      return "OK";
    } else {
      return " ";
    }
  }

  public getNomeUsuarioDetalhes(): any {
    let usr = new Usuario();
    var StrUsuario = localStorage.getItem("UsuarioAtual");
    if (StrUsuario !== null) {  
      usr =  JSON.parse(StrUsuario);                     
      return usr;
    } else {
      return usr;
    }
  }

}
