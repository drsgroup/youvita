import { Injectable } from '@angular/core';
import { BaseService } from './negocio/base/base.service';
import { HttpClient } from '@angular/common/http';
import { WebContract } from 'app/models/base/Contrato';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StateService  extends BaseService{

  private _api = "/state";

  constructor(public _http: HttpClient) {
    super();
  }


  public listarEstados(): Observable<WebContract> {    
         
    return this._http.get<WebContract>(this._Url+this._api, { observe: 'response',params:{userLogin:"rafaelTeste"} })
    //.do(res => this.getToken(res))
    .map(res => {
      var cnt = new WebContract();
      cnt = res.body;
      return cnt;
      })
      .catch(this.handleError);            
  }








}
