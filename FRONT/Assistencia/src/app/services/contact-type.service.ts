import { Injectable } from '@angular/core';
import { WebContract } from 'app/models/base/Contrato';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseService } from './negocio/base/base.service';

@Injectable({
  providedIn: 'root'
})
export class ContactTypeService extends BaseService {

  private _api = "/contactType";

  constructor(private _http: HttpClient) {
    super();
  } 

  public getContactType(): Observable<WebContract> {    
         
    return this._http.get<WebContract>(this._Url+this._api, { observe: 'response',params:{userLogin:"rafaelTeste"} })
    //.do(res => this.getToken(res))
    .map(res => {
      var cnt = new WebContract();
      cnt = res.body;
      return cnt;
      })
      .catch(this.handleError);            
  }

}
