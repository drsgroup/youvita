import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar, MatDialog } from '@angular/material';
import { LabViewFormComponent } from '../lab-view/lab-view-form/lab-view-form.component';
import { DoctorService } from 'app/services/negocio/doctor/doctor.service';
import { ProductService } from 'app/services/negocio/productService/productService';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { LoginService } from 'app/services/negocio/login/login.service';
import { RepresentanteProgramadosFormComponent } from './representante-programados-form/representante-programados-form.component';

@Component({
  selector: 'app-representante-programados',
  templateUrl: './representante-programados.component.html',
  styleUrls: ['./representante-programados.component.css']
})
export class RepresentanteProgramadosComponent implements OnInit {
  colunas = [ 
    {
      Propriedade: 'ordersRepresentativeID',
      Titulo: 'ID Pedido',
      Visivel: true,
      Largura : 50
    },
    {
      Propriedade: 'representativeName',
      Titulo: 'Representante',
      Visivel: true,
      Largura : 200     
    },
    {
      Propriedade: 'laboratoryFantasyName',
      Titulo: 'Laboratório',
      Visivel: true,
      Largura : 200      
    },
    {
      Propriedade: 'orderRepresentativeDateSend',
      Titulo: 'Data envio',
      Visivel: true,
      Largura : 100,
      Tipo : "DATA"
    }
  ];

  linhas = [];    

  constructor(
    private login : LoginService,
    private crud : CRUDService,
    private dialog: MatDialog
  ) { }  

  ngOnInit() {
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
     titulo = "Pedido Programado Representante: "+ dados.ordersRepresentativeID;
    
    let dialogRef: MatDialogRef<any> = this.dialog.open(RepresentanteProgramadosFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.consultarPedido("");
        return; 
      });
  }  

  consultarPedido(parametro){
    let user = this.login.getNomeUsuarioDetalhes();
    let params : any = new Object();
    
    if (user.userType == 1){ //admin
      params.userLogin = user.username;
      params.laboratoryID = "0"; //todos laboratorios
      params.representativeLogin = ""; //todos representante
      params.representativeName = parametro; //medicos conforme parametro      
    }

    if (user.userType == 3) { //laboratorio
      params.userLogin = user.username;
      params.laboratoryID = user.laboratoryID;
      params.representativeLogin = ""; //todos representante
      params.representativeName = parametro; //medicos conforme parametro      
    }

    if (user.userType == 4){
      params.userLogin = user.username;
      params.laboratoryID = user.laboratoryID;
      params.representativeLogin = ""; //todos representante
      params.representativeName = parametro; //medicos conforme parametro      
    }        

    if (user.userType == 5){ //representante
      params.userLogin = user.username;
      params.laboratoryID = user.laboratoryID;
      params.representativeLogin = user.username; //todos representante
      params.representativeName = ""; //medicos conforme parametro      
    }      
    
    
    this.crud.GetParams(params, "/OrderRepresentative").subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO; 
    });  
  }

  deletePedido(ps){

  }
}
