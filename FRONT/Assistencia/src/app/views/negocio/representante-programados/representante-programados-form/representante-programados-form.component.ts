import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-representante-programados-form',
  templateUrl: './representante-programados-form.component.html',
  styleUrls: ['./representante-programados-form.component.css']
})
export class RepresentanteProgramadosFormComponent implements OnInit {
  pedidoForm : FormGroup;
  produtos = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<RepresentanteProgramadosFormComponent>
  ) { }

  ngOnInit() {
    this.montarForm(this.data.payload);
  }

  montarForm(item){
    this.pedidoForm = new FormGroup({
      ordersRepresentativeID : new FormControl(item.ordersRepresentativeID),
      representativeName : new FormControl(item.representativeName),      
      laboratoryFantasyName : new FormControl(item.laboratoryFantasyName),
      orderDoctorDateSend : new FormControl(formatDate(item.orderRepresentativeDateSend, "dd/MM/yyyy", "pt-BR"))      
    });
    this.produtos = item.Products;  
    this.pedidoForm.disable();
  }

}