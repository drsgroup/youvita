import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { AppConfirmService } from '../../../services/dialogs/app-confirm/app-confirm.service';
import { AppLoaderService } from '../../../services/dialogs/app-loader/app-loader.service';
import { ClientesFormularioComponent } from './clientes-formulario/clientes-formulario.component';
import { ClientService } from 'app/services/negocio/client/client.service';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';
import { CampoBusca } from 'app/models/base/negocio/CampoBusca';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {
  colunas = [
    {
      Propriedade: 'ClientId',
      Titulo: 'ID Cliente',
      Visivel: true,
      Largura: 60
    },
    {
      Propriedade: 'ClientName',
      Titulo: 'Razão Social',
      Visivel: true,
      Largura: 250
    },
    {
      Propriedade: 'ClientFantasyName',
      Titulo: 'Nome Fantasia',
      Visivel: true,
      Largura: 250
    },
    {
      Propriedade: 'CNPJ',
      Titulo: 'CNPJ',
      Visivel: true,
      Largura: 100
    }
  ];

  linhas = [];
  public lastFind: any;

  configBusca = [
    new CampoBusca("clientName", "Razão Social", 50, "", "string"),
    new CampoBusca("clientFantasyName", "Nome Fantasia", 50, "", "string"),
    new CampoBusca("clientCNPJ", "CNPJ", 20, "00.000.000/0000-00", "string")]


  constructor(private dialog: MatDialog,
    private confirm: AppConfirmService,
    private snack: MatSnackBar,
    private serviceClient: ClientService,
    private confirmacao: AppConfirmService,
    private loader: AppLoaderService,
    private snackBar: MatSnackBar,
    private appInformationService: AppInformationService,
    private crudServide: CRUDService) { }

  ngOnInit() {
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Novo Cliente";
    } else {
      titulo = "Editar Cliente : " + dados.ClientId;
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(ClientesFormularioComponent, {
      width: '90%', height: '100%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.consultarCliente(this.lastFind);
        return;
      });
  }


  consultarCliente(parametro: any) {
    this.lastFind = parametro;
    this.crudServide.GetParams(parametro, "/client").subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO;
    });
  }


  deleteClient(client: any) {
    this.confirm.confirm("Exclusão do Cliente", "Tem certeza que deseja excluir o Cliente " + client.clientName + "?").subscribe(result => {
      if (result === true) {
        this.loader.open("Excluindo Cliente");
        this.serviceClient.deleteClient(client).subscribe(res => {
          if (res.Status == "OK") {
            this.snackBar.open("Cliente excluído com sucesso!", "", { duration: 3000 });
            this.consultarCliente("");
          }
          else {
            this.appInformationService.information("YouVita", res.Message);
          }
          this.loader.close();
        })
      }
    })
  }


}
