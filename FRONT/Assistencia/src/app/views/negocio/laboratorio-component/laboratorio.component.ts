import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';
import { AppConfirmService } from './../../../services/dialogs/app-confirm/app-confirm.service';
import { LaboratoryService } from 'app/services/negocio/laboratory/laboratory.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { LaboratioFormularioComponent } from './laboratio-formulario/laboratio-formulario.component';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-laboratorio-component',
  templateUrl: './laboratorio.component.html'
  
})
export class LaboratorioComponent implements OnInit {

  private laboratorioForm : FormGroup;

  constructor(
    private dialog: MatDialog,
    private labService : LaboratoryService,
    private confirm : AppConfirmService,
    private appInformationService : AppInformationService,
    private loader : AppLoaderService,
    private snackBar : MatSnackBar) { }

  colunas = [
    {
      Propriedade: 'laboratoryID',
      Titulo: 'ID Lab',
      Visivel: true,
      Largura : 40
    },
    {
      Propriedade: 'laboratoryName',
      Titulo: 'Razão Social',
      Visivel: true,
      Largura :300
    },
    {
      Propriedade: 'laboratoryFantasyName',
      Titulo: 'Nome Fantasia',
      Visivel: true,
      Largura : 300
    },
    {
      Propriedade: 'cnpj',
      Titulo: 'CNPJ',
      Visivel: true,
      Largura : 100
    }
  ];

  linhas = [];

  ngOnInit() {
    
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Novo Laboratório";
    } else {
      titulo = "Editar Laboratório : "+dados.laboratoryFantasyName;
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(LaboratioFormularioComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.consultarLab("")
        //this.consultarCliente("");
        return;
      });
  }

  consultarLab(parametro : string) {
    this.labService.getLabs(parametro).subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO      
    })
  }  

  deleteLab(lab: any) {
    this.confirm.confirm("Exclusão do Laboratório", "Tem certeza que deseja excluir o Laboratório " +
      lab.laboratoryName + "?").subscribe(result => {

        if (result === true) {
          this.loader.open("Excluindo Paciente");
          this.labService.deleteLab(lab).subscribe(res => {
            if (res.Status == "OK") {
              this.snackBar.open("Laboratório Excluído com sucesso!", "", { duration: 3000 });
              this.consultarLab("");
            }
            else {
              this.appInformationService.information("YouVita", res.Message);
            }
            this.loader.close();
          })
        }
      })
  }

  SaveLab(){

  }

}
