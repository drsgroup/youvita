import { Component, OnInit } from '@angular/core';
import { LoginService } from 'app/services/negocio/login/login.service';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { MatDialog, MatDialogRef } from '@angular/material';
import { PharmaceuticalAttentionAnamnesisFormComponent } from './pharmaceutical-attention-anamnesis-form/pharmaceutical-attention-anamnesis-form.component';
import { CampoBusca } from 'app/models/base/negocio/CampoBusca';

@Component({
  selector: 'app-pharmaceutical-attention-anamnesis',
  templateUrl: './pharmaceutical-attention-anamnesis.component.html',
  styleUrls: ['./pharmaceutical-attention-anamnesis.component.css']
})
export class PharmaceuticalAttentionAnamnesisComponent implements OnInit {
  colunas = [
    {
      Propriedade: 'PatientName',
      Titulo: 'Paciente',
      Visivel: true,
      Largura: 300
    },
    {
      Propriedade: 'CPF',
      Titulo: 'CPF',
      Visivel: true,
      Largura: 200
    },
    {
      Propriedade: 'CreateDate',
      Titulo: 'Data do Questionário',
      Visivel: true,
      Largura: 100,
      Tipo: "DATA"
    }
  ];

  linhas = [];
  listClients = [];
  public configBusca = [
    new CampoBusca("patientName", "Nome Paciente", 50, "", "string"),
    new CampoBusca("CPF", "CPF", 15, "000.000.000-00", "string"),
    new CampoBusca("identificationOnClient", "Identificação no cliente", 50, "", "string")
  ];  
  public lastFind : any;

  constructor(private login: LoginService,
    private crud: CRUDService,
    private dialog: MatDialog) {

  }

  ngOnInit() {    
  }

  getResearches(parametro: any) {
    this.lastFind = parametro;
    let usr = this.login.getNomeUsuarioDetalhes();
    // let params: any = new Object();
    // params.userLogin = usr.userLogin;
    // params.Filter = parametro.filter;
    // params.ClientId = usr.ClientId;
    this.crud.GetParams(parametro, "/researches").subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO;
    })
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Nova Anamnese";
    } else {
      titulo = "Visualizar Anamnese: " + dados.PatientId;
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(PharmaceuticalAttentionAnamnesisFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        //this.consultarLoja();
        this.getResearches(this.lastFind);
        return;
      });
  }

  public deletePedido(ok: any) {

  }


}


