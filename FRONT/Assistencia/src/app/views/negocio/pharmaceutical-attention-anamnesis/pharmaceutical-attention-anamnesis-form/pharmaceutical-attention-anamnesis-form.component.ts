import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatSnackBar, MatOptionSelectionChange } from '@angular/material';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { LoginService } from 'app/services/negocio/login/login.service';
import { FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';

@Component({
  selector: 'app-pharmaceutical-attention-anamnesis-form',
  templateUrl: './pharmaceutical-attention-anamnesis-form.component.html',
  styleUrls: ['./pharmaceutical-attention-anamnesis-form.component.css']
})
export class PharmaceuticalAttentionAnamnesisFormComponent implements OnInit {
  public researchForm: FormGroup;
  public responsesForm: FormGroup;
  questionnaireResponse = [];
  listClients = [];
  patients = [];
  listQuestions = [];
  gambiarra = "";
  listPerguntas = [];


  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<PharmaceuticalAttentionAnamnesisFormComponent>,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private loader: AppLoaderService,
    private crud: CRUDService,
    private loginService: LoginService,
    private formBuilder: FormBuilder
  ) { }


  async ngOnInit() {
    //this.questionnaireResponse = this.data.payload.ListQuestionnaireResponses;
    // this.loader.open();    
    if (this.data.novo) {
      await this.montarForm(this.data.payload);
    } else {
      this.montaConsulta(this.data.payload);

    }
    // this.loader.close();
  }

  montaConsulta(item) {
    this.listPerguntas = [];
    let params: any = new Object();
    params.ResearchId = item.ResearchesId;
    this.crud.GetParams(params, "/researches").subscribe(res => {
      let perguntas = res.ObjectDTO;
      this.listPerguntas = [];
      let questionId = 0;
      let q: any;
      //q.Resps = new Array();
      for (let i = 0; i < perguntas.length; i++) {
        if (questionId != perguntas[i].QuestionsId ) {
          if (q != null) {
            this.listPerguntas.push(q);
          }
          questionId = perguntas[i].QuestionsId
          q = new Object();
          q.Question = perguntas[i].Question;
          q.Resps = new Array();
          q.Resps.push(perguntas[i].Complemento);
          //this.listPerguntas.push(q);
        } else {
          if (questionId > 0) {
            q.Resps.push(perguntas[i].Complemento);
          }
        }


        // if (questionId != perguntas[i].QuestionsId) {
        //   if (i > 0) {
        //     this.listPerguntas.push(q);
        //     q = new Object();
        //     q.Resps = new Array();
        //   }
        //   //q = new Object();
        //   q.Question = perguntas[i].Question;          
        //   q.Resps.push(perguntas[i].Complemento);
        // } else {
        //   q.Resps.push(perguntas[i].Complemento);
        // }
      }
      console.table(this.listPerguntas);

    });

  }


  // favoriteSeason: string;
  // seasons: string[] = ['Winter', 'Spring', 'Summer', 'Autumn'];


  async montarForm(item) {
    // this.loader.open();
    this.getPatients();
    this.researchForm = new FormGroup({
      PatientId: new FormControl(item.PatientId),
      QuestionsId: new FormControl(item.QuestionsId),
      QuestionnairesId: new FormControl(item.QuestionnairesId),
      Answer: new FormControl(item.Answer),
      Quiz: new FormControl(item.Quiz)
    });
    await this.getQuestions();

    this.responsesForm = new FormGroup({});
    for (let i = 0; i < this.listQuestions.length; i++) {
      this.responsesForm.addControl(this.listQuestions[i].QuestionsId.toString(), new FormControl(""));
      if (this.listQuestions[i].Complement == "S") {
        let other = this.listQuestions[i].QuestionsId.toString() + "T";
        this.responsesForm.addControl(this.listQuestions[i].QuestionsId.toString() + "T", new FormControl(""));
        this.responsesForm.controls[other].disable();
      }
    }

    this.responsesForm.valueChanges.subscribe(res => {
      this.gambiarra = JSON.stringify(this.responsesForm.value)
    })
  }

  changeValue(evento: MatOptionSelectionChange, answer, questionId, complement) {
    //console.log(JSON.stringify(answer));
    if (evento.source.selected) {
      if (complement == 'S') {
        if (answer.AnswerOthers == 1) {
          console.log('habilitar o campo outros: ' + questionId)
          this.responsesForm.controls[questionId + 'T'].enable();
        } else {
          this.responsesForm.controls[questionId + 'T'].reset();
          this.responsesForm.controls[questionId + 'T'].disable();
        }
      }
    }
    // if (complement =="S"){
    //   this.responsesForm.controls[control].enable();    
    // }
  }

  getPatients() {
    let usr = this.loginService.getNomeUsuarioDetalhes();
    let params: any = new Object();
    params.patientName = "";
    params.cpf = "";
    params.identificationOnClient = "";
    params.ClientId = usr.ClientId;
    this.crud.GetParams(params, "/patient").subscribe(res => {
      this.patients = [];
      this.patients = res.ObjectDTO;
    });
  }

  async getQuestions() {
    let usr = this.loginService.getNomeUsuarioDetalhes();
    let params: any = new Object();
    params.userLogin = "teste";
    params.filter = "";
    let res = await this.crud.GetParams(params, "/questions").toPromise();
    if (res.Status == "OK") {
      this.listQuestions = [];
      this.listQuestions = res.ObjectDTO;
    };
  }

  private addQuestions() {
    this.listQuestions.map((o, i) => {
      const control = new FormControl(i === 0); // if first item set to true, else false
      (this.researchForm.controls.listQuestions as FormArray).push(control);
    });
  }

  saveResearch() {
    //let x = this.responsesForm.value;
    let researches: any = new Object();
    //gabarito.header = this.researchForm.value;
    researches.ResearchesId = 0;
    researches.CreateDate = new Date();
    researches.PatientId = 1;
    researches.QuestionnairesId = 0;
    researches.PatientName = 'nom';
    researches.Quiz = "0";

    let responses = [];
    let response: any;
    for (let i = 0; i < this.listQuestions.length; i++) {
      response = new Object();
      response.QuestionnaireResponsesId = 0;
      response.ResearchesId = 0;
      response.QuestionsId = this.listQuestions[i].QuestionsId;
      if (!Array.isArray(this.responsesForm.controls[this.listQuestions[i].QuestionsId.toString()].value)) {

        if (this.listQuestions[i].QuestionType == 'T') {
          response.AnswersId = 0;
          response.Respostas = this.responsesForm.controls[this.listQuestions[i].QuestionsId.toString()].value;
          response.Answer = this.responsesForm.controls[this.listQuestions[i].QuestionsId.toString()].value;

        } else {
          response.AnswersId = this.responsesForm.controls[this.listQuestions[i].QuestionsId.toString()].value.toString();
          response.Respostas = this.responsesForm.controls[this.listQuestions[i].QuestionsId.toString()].value;
          response.Answer = '';
        }
      } else {
        response.AnswersId = 0;
        response.Respostas = this.responsesForm.controls[this.listQuestions[i].QuestionsId.toString()].value.join(',');
        response.Answer = '';
      }
      if (this.listQuestions[i].Complement == "S") {
        if (this.responsesForm.controls[this.listQuestions[i].QuestionsId.toString() + 'T'].enabled) {
          response.Complement = this.responsesForm.controls[this.listQuestions[i].QuestionsId.toString() + 'T'].value.toString();
        } else {
          response.Complement = '';
        }
      }
      //response.Answer = "";
      response.QuestionnaireResponsesEnabled = 0;
      responses.push(response);
    }
    researches.ListQuestionnaireResponses = responses;
    //console.log(JSON.stringify(researches));

    this.crud.Save(researches, true, "/Researches").subscribe(res => {
      this.snackBar.open("Questionário gravado com sucesso", "", { duration: 10000 });
      this.dialogRef.close("OK");
    })
  }
}
