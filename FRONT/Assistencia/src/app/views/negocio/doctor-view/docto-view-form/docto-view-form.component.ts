import { FormControl, FormGroup } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-docto-view-form',
  templateUrl: './docto-view-form.component.html',
  styleUrls: ['./docto-view-form.component.css']
})
export class DoctoViewFormComponent implements OnInit {
  pedidoForm : FormGroup;
  addressForm : FormGroup;
  
  listItens = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  public dialogRef: MatDialogRef<DoctoViewFormComponent>) { }

  listState=[];

  ngOnInit() {
    this.montarForm("");
  }

  montarForm(ite){
    this.pedidoForm = new FormGroup({
      ID : new FormControl("")
    });

    this.addressForm = new FormGroup({
      ID : new FormControl("")
    })
  }

}
;