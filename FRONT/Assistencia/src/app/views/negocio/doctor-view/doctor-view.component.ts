import { CRUDService } from './../../../services/negocio/CRUDService/CRUDService';
import { AppLoaderService } from './../../../services/dialogs/app-loader/app-loader.service';
import { StateService } from './../../../services/state.service';
import { DoctorService } from './../../../services/negocio/doctor/doctor.service';
import { ProductService } from './../../../services/negocio/productService/productService';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { DoctoViewFormComponent } from './docto-view-form/docto-view-form.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from 'app/services/negocio/login/login.service';

@Component({
  selector: 'app-doctor-view',
  templateUrl: './doctor-view.component.html',
  styleUrls: ['./doctor-view.component.css']
})
export class DoctorViewComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private loginService: LoginService,
    private productService: ProductService,
    private serviceStates: StateService,
    private doctorService: DoctorService,
    private snackBar: MatSnackBar,
    private loader: AppLoaderService,
    private crud : CRUDService) { }
  doctorForm: FormGroup;
  public linhasStates = [];
  public doctor: any;

  colunas = [
    {
      Propriedade: 'MedicoID',
      Titulo: 'ID Médico',
      Visivel: true
    },
    {
      Propriedade: 'Nome',
      Titulo: 'Nome do Médico',
      Visivel: true
    },
    {
      Propriedade: 'CRM',
      Titulo: 'CRM',
      Visivel: true
    },
    {
      Propriedade: 'UF',
      Titulo: 'UF do CRM',
      Visivel: true
    }
  ];

  linhas = [];
  listItens = [];

  ngOnInit() {
    this.montarForm();
  }

  delete(pa) {

  }

  consultarStates() {
    this.serviceStates.listarEstados().subscribe(res => {
      this.linhasStates = [];
      this.linhasStates = res.ObjectDTO;
      this.doctorForm.controls["uf_crm"].setValue(parseInt(this.doctor.uf_crm));
      this.doctorForm.controls["stateId"].setValue(parseInt(this.doctor.stateId));
    })
  }

  montarForm() {

    //buscar medico pelo login
    this.doctorForm = new FormGroup({
      doctorID: new FormControl(""),
      doctorName: new FormControl("", [Validators.required]),
      crm: new FormControl("", [Validators.required]),
      uf_crm: new FormControl("", [Validators.required]),
      cpf: new FormControl("", [Validators.required]),
      address: new FormControl("", [Validators.required]),
      number: new FormControl("", [Validators.required]),
      complement: new FormControl(""),
      city: new FormControl("", [Validators.required]),
      neighborhood: new FormControl("", [Validators.required]),
      stateId: new FormControl("", [Validators.required]),
      zipCode: new FormControl("", [Validators.required]),
      phoneComercial: new FormControl("", [Validators.required]),
      ramal: new FormControl(""),
      cellPhone: new FormControl("", [Validators.required]),
      comments: new FormControl("")
    });
    this.doctorForm.disable();

    let user = this.loginService.getNomeUsuarioDetalhes();

    this.doctorService.getDoctorsByLogin(user.username).subscribe(res => {
      if (res.ObjectDTO.length > 0) {
        this.doctor = res.ObjectDTO[0];
        this.doctorForm.controls["doctorID"].setValue(this.doctor.doctorID);
        this.doctorForm.controls["doctorName"].setValue(this.doctor.doctorName);
        this.doctorForm.controls["crm"].setValue(this.doctor.crm);
        this.doctorForm.controls["uf_crm"].setValue(this.doctor.uf_crm);
        this.doctorForm.controls["cpf"].setValue(this.doctor.cpf);
        this.doctorForm.controls["address"].setValue(this.doctor.address);
        this.doctorForm.controls["number"].setValue(this.doctor.number);
        this.doctorForm.controls["city"].setValue(this.doctor.city);
        this.doctorForm.controls["neighborhood"].setValue(this.doctor.neighborhood);
        this.doctorForm.controls["zipCode"].setValue(this.doctor.zipCode);
        this.doctorForm.controls["phoneComercial"].setValue(this.doctor.phoneComercial);
        this.doctorForm.controls["ramal"].setValue(this.doctor.ramal);
        this.doctorForm.controls["cellPhone"].setValue(this.doctor.cellPhone);
        this.doctorForm.controls["comments"].setValue(this.doctor.comments);

        this.consultarStates();
      }
    });

    this.productService.getProductAvaliable(user.username).subscribe(res => {
      let itens = [];
      this.listItens = [];
      itens = res.ObjectDTO;
      let descFreq = "";
      for (let i = 0; i < itens.length; i++) {
        switch (itens[i].Frequence) {
          case 1:
            descFreq = "Pontual";
            break;
          case 7:
            descFreq = "Semanal";
            break;
          case 14:
            descFreq = "Quinzenal";
            break;
          case 30:
            descFreq = "Mensal";
            break;
          case 60:
            descFreq = "Bimestral";
            break;
          case 90:
            descFreq = "Trimestral";
            break;
        }
        let item: any;
        item = new Object();
        item = itens[i];
        item.FrequenceDesc = descFreq;
        this.listItens.push(item);
      }
      this.listItens = [...this.listItens];
      //console.table(this.linhas);
    });
  }

  SaveDoctor() {
    this.loader.open();
    let formulario = this.doctorForm.value;
    this.doctorService.saveDoctor(formulario, false).subscribe(res => {
      this.doctorForm.disable();
      if (res.Status == "OK") {
        this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });
      } else {
        this.snackBar.open(res.Message, "", { duration: 3000 });
      }
      this.loader.close();
    });
  }

  edit() {
    this.doctorForm.enable();
  }

  SaveOrder(){
    let Order : any;
    Order = new Object();
    Order.doctorID = this.doctor.doctorID;
    Order.addressDoctorID = 0;
    Order.status = "E";
    Order.comments = "nenhum";
    Order.createDate = new Date();
    Order.orderDoctorDateSend = new Date();

    this.crud.Save(Order, true, "/OrderDoctor").subscribe(res =>{
      if (res.Status =="OK"){
        this.snackBar.open("Pedido realizado com sucesso", "", { duration : 3000});
      }else{
        this.snackBar.open("Erro ao realizar o pedido: " + res.Message, "", {duration: 5000});
      }
      this.montarForm();
    });
  }

  consultaPedidos(pa) {

  }

}
