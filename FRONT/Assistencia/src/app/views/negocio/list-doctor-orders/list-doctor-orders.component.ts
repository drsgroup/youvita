import { ListDoctorOrderFormComponent } from './list-doctor-order-form/list-doctor-order-form.component';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { LoginService } from './../../../services/negocio/login/login.service';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';

@Component({
  selector: 'app-list-doctor-orders',
  templateUrl: './list-doctor-orders.component.html',
  styleUrls: ['./list-doctor-orders.component.css']
})
export class ListDoctorOrdersComponent implements OnInit { 

  colunas = [ 
    {
      Propriedade: 'OrderAvaliableID',
      Titulo: 'ID Pedido',
      Visivel: true,
      Largura : 50
    },
    {
      Propriedade: 'doctorName',
      Titulo: 'Médico ',
      Visivel: true,
      Largura : 300
    },
    {
      Propriedade: 'representativeName',
      Titulo: 'Representante',
      Visivel: true,
      Largura : 200     
    },
    {
      Propriedade: 'laboratoryFantasyName',
      Titulo: 'Laboratório',
      Visivel: true,
      Largura : 100      
    },
    {
      Propriedade: 'orderDoctorDateSend',
      Titulo: 'Data envio',
      Visivel: true,
      Largura : 100,
      Tipo : "DATA"
    }
  ];

  linhas = [];    

  constructor(
    private login : LoginService,
    private crud : CRUDService,
    private dialog: MatDialog
  ) { }  

  ngOnInit() {
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
     titulo = "Pedido Programado Médico: "+ dados.OrderAvaliableID;
    
    let dialogRef: MatDialogRef<any> = this.dialog.open(ListDoctorOrderFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.consultarPedido("");
        return;
      });
  }  

  consultarPedido(parametro){
    let user = this.login.getNomeUsuarioDetalhes();
    let params : any = new Object();
    if (user.userType == 1){ //admin
      params.userLogin = user.username;
      params.laboratoryID = "0"; //todos laboratorios
      params.representativeLogin = ""; //todos representante
      params.doctorName = parametro; //medicos conforme parametro
      params.doctorLogin = "";
    }

    if (user.userType == 3) { //laboratorio
      params.userLogin = user.username;
      params.laboratoryID = user.laboratoryID;
      params.representativeLogin = ""; //todos representante
      params.doctorName = parametro; //medicos conforme parametro
      params.doctorLogin = "";
    }

    if (user.userType == 4){
      params.userLogin = user.username;
      params.laboratoryID = user.laboratoryID;
      params.representativeLogin = ""; //todos representante
      params.doctorName = ""; //medicos conforme parametro
      params.doctorLogin = user.username;
    }        

    if (user.userType == 5){ //representante
      params.userLogin = user.username;
      params.laboratoryID = user.laboratoryID;
      params.representativeLogin = user.username; //todos representante
      params.doctorName = parametro; //medicos conforme parametro
      params.doctorLogin = "";
    }      
    
    
    this.crud.GetParams(params, "/OrderDoctor").subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO;
    });  
  }

  deletePedido(ps){

  }
}
