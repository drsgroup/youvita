import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-list-doctor-order-form',
  templateUrl: './list-doctor-order-form.component.html',
  styleUrls: ['./list-doctor-order-form.component.css']
})
export class ListDoctorOrderFormComponent implements OnInit {
  pedidoForm : FormGroup;
  produtos = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ListDoctorOrderFormComponent>
  ) { }

  ngOnInit() {
    this.montarForm(this.data.payload);
  }

  montarForm(item){
    this.pedidoForm = new FormGroup({
      OrderAvaliableID : new FormControl(item.OrderAvaliableID),
      doctorName : new FormControl(item.doctorName),
      representativeName : new FormControl(item.representativeName),      
      laboratoryFantasyName : new FormControl(item.laboratoryFantasyName),
      orderDoctorDateSend : new FormControl(formatDate(item.orderDoctorDateSend, "dd/MM/yyyy", "pt-BR"))      
    });
    this.produtos = item.Products;

    console.log(JSON.stringify(this.data.payload))

    this.pedidoForm.disable();
  }

}
