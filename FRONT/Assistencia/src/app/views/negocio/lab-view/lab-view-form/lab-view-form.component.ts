import { LoginService } from 'app/services/negocio/login/login.service';
import { CRUDService } from './../../../../services/negocio/CRUDService/CRUDService';
import { AppInformationService } from './../../../../services/dialogs/app-information/app-information.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { ProductService } from './../../../../services/negocio/productService/productService';
import { DoctorService } from './../../../../services/negocio/doctor/doctor.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSelect, MatOption, MatSnackBar } from '@angular/material';
import { stringify } from '@angular/core/src/util';
import { ARIA_DESCRIBER_PROVIDER } from '@angular/cdk/a11y';

@Component({
  selector: 'app-lab-view-form',
  templateUrl: './lab-view-form.component.html',
  styleUrls: ['./lab-view-form.component.css']
})
export class LabViewFormComponent implements OnInit {
  public pedidoForm: FormGroup;
  public itensPedido: FormGroup;
  listState = [];
  doctors = [];
  itensPed = [];
  listProduct = [];
  user: any;
  minDate: any;
  @ViewChild("qtyEdit") private qtyEdit: ElementRef;
  @ViewChild("estoqueAtual") private estoqueEdit: ElementRef;


  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<LabViewFormComponent>,
    private doctorService: DoctorService,
    private productService: ProductService,
    private snack: MatSnackBar,
    private loader: AppLoaderService,
    private information: AppInformationService,
    private crudService: CRUDService,
    private loginService: LoginService) { }

  ngOnInit() {
    this.montarForm(this.data.payload);
  }

  montarForm(item) {
    this.user = this.loginService.getNomeUsuarioDetalhes();

    this.pedidoForm = new FormGroup({
      doctorID: new FormControl(item.DoctorId, [Validators.required]),
      startValidity: new FormControl(item.startValidity, [Validators.required]),
      endValidity: new FormControl(item.endValidity, [Validators.required]),
      frequence: new FormControl(item.frequence, [Validators.required]),
      dateToSend: new FormControl(item.dateToSend, [Validators.required]),
      frequenceInDays: new FormControl(item.frequence, [Validators.required, Validators.min(1)]),
      status: new FormControl(item.approved || "A")
    });

    this.itensPedido = new FormGroup({
      productID: new FormControl("", [Validators.required]),
      laboratoryID: new FormControl(""),
      qty: new FormControl(0, [Validators.required, Validators.min(1)])
    });

    let user = this.loginService.getNomeUsuarioDetalhes();
    let params: any = new Object();
    params.userLogin = "ok";
    params.laboratoryID = user.laboratoryID;
    params.nok = "nok";
    params.ok = "ok";

    //this.doctorService.getDoctors("").subscribe(res => {
    this.crudService.GetParams(params, "/doctor").subscribe(res => {
      this.doctors = [];
      this.doctors = res.ObjectDTO;
    });

    this.productService.getProducts(1).subscribe(res => {
      this.listProduct = [];
      this.listProduct = res.ObjectDTO;
    });

    if (!this.data.novo) {
      this.pedidoForm.disable();
      this.itensPed = item.Items;
    }
    let dt = new Date();
    this.minDate = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate());
  }

  SavePedido() {
    let items = [];
    let pedido: any;
    this.loader.open();

    pedido = new Object();
    pedido.DoctorID = this.pedidoForm.controls["doctorID"].value;
    pedido.startValidity = this.pedidoForm.controls["startValidity"].value;
    pedido.endValidity = this.pedidoForm.controls["endValidity"].value;
    pedido.frequence = this.pedidoForm.controls["frequenceInDays"].value;
    pedido.dateToSend = this.pedidoForm.controls["dateToSend"].value;
    pedido.status = this.pedidoForm.controls["status"].value;
    pedido.laboratoryID = this.user.laboratoryID;
    pedido.Items = [];

    let itemPedido: any;
    for (let i = 0; i < this.itensPed.length; i++) {
      itemPedido = new Object();
      itemPedido.productID = this.itensPed[i].productID;
      itemPedido.qty = this.itensPed[i].qty;
      pedido.Items.push(itemPedido);
    }

    this.crudService.Save(pedido, true, "/orderavaliable").subscribe(res => {
      if (res.Status == "OK") {
        this.loader.close();
        this.snack.open("Registro gravado com sucesso", "", { duration: 3000 });
        this.dialogRef.close('');
      } else {
        this.loader.close();
        this.snack.open("Erro ao gravar registro:" + res.Message, "", { duration: 5000 });
        this.dialogRef.close('');
      }
    });
  }

  getStock(event) {
    this.loader.open();
    try {
      this.productService.getStock(1, event.value).subscribe(res => {
        if (res.Status == "OK") {
          let x: any;
          x = res.ObjectDTO; //res.ObjectDTO.ITEM[0].QUANTITY
          this.estoqueEdit.nativeElement.value = x.ITEM[0].QUANTITY;
          this.loader.close();
        } else {
          this.loader.close();
          this.snack.open("Erro ao obter o estoque: " + res.Message, "", { duration: 5000 });
          this.estoqueEdit.nativeElement.value = "0";
        }
      });
    } catch (e) {
      this.snack.open("Erro ao obter o estoque: " + JSON.stringify(e), "", { duration: 5000 });
      this.loader.close();
    }
  }

  addProduct() {
    const formulario = this.itensPedido.value;
    let productName = "";

    if (parseInt(formulario.qty) > parseInt(this.estoqueEdit.nativeElement.value)) {
      this.information.information("YouVita", "Quantidade solicitada maior que o estoque atual");
      return;
    }

    for (let i = 0; i < this.listProduct.length; i++) {
      if (this.listProduct[i].productID == formulario.productID) {
        productName = this.listProduct[i].productName;
      }
    }
    formulario.productName = productName;
    let descFreq = "";

    switch (formulario.frequence) {
      case 1:
        descFreq = "Pontual";
        break;
      case 7:
        descFreq = "Semanal";
        break;
      case 14:
        descFreq = "Quinzenal";
        break;
      case 30:
        descFreq = "Mensal";
        break;
      case 60:
        descFreq = "Bimestral";
        break;
      case 90:
        descFreq = "Trimestral";
        break;
    }

    formulario.descFreq = descFreq;
    this.itensPed.push(formulario);
    this.itensPed = [...this.itensPed];
    this.itensPedido.reset();
    this.qtyEdit.nativeElement.value = "";

  }

  removeItem(registro) {
    let aux = this.itensPed.filter(item => {
      if (item === registro) {
        return false;
      } else {
        return true;
      }
    });
    this.itensPed = [...aux];
  }

  formValido(): boolean {
    let v = this.pedidoForm.invalid;
    if (this.itensPed.length == 0) {
      v = true; //invalido
    }
    return v;
  }

  outroPeriodo(evento) {
    if (evento.value == "0") {
      this.pedidoForm.controls["frequenceInDays"].enable();
      this.pedidoForm.controls["frequenceInDays"].setValue("");
    } else {
      this.pedidoForm.controls["frequenceInDays"].setValue(evento.value)
      this.pedidoForm.controls["frequenceInDays"].disable();
    }
  }
}
