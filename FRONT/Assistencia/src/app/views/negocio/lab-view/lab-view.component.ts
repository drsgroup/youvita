import { LabViewFormComponent } from './lab-view-form/lab-view-form.component';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';

@Component({
  selector: 'app-lab-view',
  templateUrl: './lab-view.component.html',
  styleUrls: ['./lab-view.component.css']
})
export class LabViewComponent implements OnInit {
  
  
  colunas = [ 
    {
      Propriedade: 'OrderAvaliableID',
      Titulo: 'ID Pedido',
      Visivel: true,
      Largura : 50
    },
    {
      Propriedade: 'DoctorName',
      Titulo: 'Médico ',
      Visivel: true,
      Largura : 300
    },
    {
      Propriedade: 'startValidity',
      Titulo: 'Início Vigência',
      Visivel: true,
      Largura : 100,
      Tipo : "DATA"
    },
    {
      Propriedade: 'endValidity',
      Titulo: 'Fim Vigência',
      Visivel: true,
      Largura : 100,
      Tipo : "DATA"
    }
  ];

  linhas = [];  

  constructor(private dialog: MatDialog,
    private crudServices : CRUDService) { }

  ngOnInit() {
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Novo Pedido Médico";
    } else {
      titulo = "Visualizar Pedido Médico";
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(LabViewFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.consultarPedido("");
        return;
      });
  }

  consultarPedido(pa){
    let params : any = new Object();
    params.userLogin = "teste";
    params.doctorName = pa;    
    this.crudServices.GetParams(params, "/orderavaliable").subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO;
    })
  }

  deletePedido(pa){

  }

}
