import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { WebContract } from 'app/models/base/Contrato';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';

@Component({
  selector: 'app-adverse-effects-form',
  templateUrl: './adverse-effects-form.component.html',
  styleUrls: ['./adverse-effects-form.component.css']
})
export class AdverseEffectsFormComponent implements OnInit {
  public formAdverseEffects: FormGroup;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  public dialogRef: MatDialogRef<AdverseEffectsFormComponent>,
  private snackBar: MatSnackBar,
  private loader: AppLoaderService,
  private crudService : CRUDService) {}

  ngOnInit() {

    this.montarForm(this.data.payload);
  }

  montarForm(item) {
    
    this.formAdverseEffects = new FormGroup({
      AdverseEffectsId: new FormControl(item.AdverseEffectsId),
      AdverseEffectsDescription: new FormControl(item.AdverseEffectsDescription,
        [Validators.required, Validators.maxLength(200)]),
    });
  }


  SaveAdverseEffects() {

    let adverseEffects = this.formAdverseEffects.value;
    this.loader.open();
    this.crudService.Save(adverseEffects, this.data.novo, "/adverseEffects").subscribe(res => {
        if (res.Status == "OK") {
          this.loader.close();
          this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });
          this.dialogRef.close('');
        } else {
          this.loader.close();
          this.snackBar.open("Erro ao gravar registro:" + res.Message, "", { duration: 5000 });
          this.dialogRef.close('');
        }
      });
    }



}
