import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdverseEffectsFormComponent } from './adverse-effects-form.component';

describe('AdverseEffectsFormComponent', () => {
  let component: AdverseEffectsFormComponent;
  let fixture: ComponentFixture<AdverseEffectsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdverseEffectsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdverseEffectsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
