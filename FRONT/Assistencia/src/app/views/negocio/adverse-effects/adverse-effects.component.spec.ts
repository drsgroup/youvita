import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdverseEffectsComponent } from './adverse-effects.component';

describe('AdverseEffectsComponent', () => {
  let component: AdverseEffectsComponent;
  let fixture: ComponentFixture<AdverseEffectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdverseEffectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdverseEffectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
