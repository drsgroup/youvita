import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { LoginService } from 'app/services/negocio/login/login.service';
import { AdverseEffectsFormComponent } from './adverse-effects-form/adverse-effects-form.component';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';
import { CampoBusca } from 'app/models/base/negocio/CampoBusca';

@Component({
  selector: 'app-adverse-effects',
  templateUrl: './adverse-effects.component.html',
  styleUrls: ['./adverse-effects.component.css']
})
export class AdverseEffectsComponent implements OnInit {
  colunas = [
    {
      Propriedade: 'AdverseEffectsId',
      Titulo: 'ID Efeitos Adversos',
      Visivel: false,
      Largura: 80
    },
    {
      Propriedade: 'AdverseEffectsDescription',
      Titulo: 'Descrição',
      Visivel: true,
      Largura:600
    }
  ];

  linhas = [];
  public lastFind : any;
  configBusca = [
    new CampoBusca("filter", "Efeito Adverso", 50, "", "string")
  ];

  constructor(private login: LoginService,
    private crud: CRUDService,
    private dialog: MatDialog,
    private confirm: AppConfirmService,
    private snackBar: MatSnackBar,
    private loader: AppLoaderService,
    private crudService : CRUDService,
    private appInformationService: AppInformationService) { }

  ngOnInit() {
    
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Novo Efeito Adverso";
    } else {
      titulo = "Editar Efeito Adverso";
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(AdverseEffectsFormComponent, {
      width: '720px',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
    .subscribe(res => {      
      this.getAdverseEffects(this.lastFind);
      return;
    });
  }

  getAdverseEffects(parametro: any) {
    this.lastFind = parametro;
    this.crud.GetParams(parametro, "/adverseeffects").subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO;
    })
  }

  deleteAdverseEffects(parametro: any) {
    let params: any = new Object();
    params.userLogin = "teste";
    params.AdverseEffectsId = parametro.AdverseEffectsId;
    this.confirm.confirm("Exclusão - Efeitos Adversos", "Tem certeza que deseja excluir?").subscribe(result => {
      if (result === true) {
        this.loader.open("Excluindo - Efeito Adverso");
        this.crud.DeleteParams(params, "/adverseEffects").subscribe(res => {
          if (res.Status == "OK") {
            this.snackBar.open("Efeitos Adversos excluído com sucesso!", "", { duration: 3000 });
            this.getAdverseEffects(this.lastFind);
          }
          else {
            this.appInformationService.information("YouVita", res.Message);
          }
          this.loader.close();
        })
      }
    })
  }
}
