import { CRUDService } from './../../../../services/negocio/CRUDService/CRUDService';
import { AppInformationService } from './../../../../services/dialogs/app-information/app-information.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSelect, MatOption, MatSnackBar } from '@angular/material';
import { stringify } from '@angular/core/src/util';
import { ARIA_DESCRIBER_PROVIDER } from '@angular/cdk/a11y';
import { PatientsService } from 'app/services/negocio/patients/patients.service';
import { LoginService } from 'app/services/negocio/login/login.service';
import { ClientService } from 'app/services/negocio/client/client.service';
import { StateService } from 'app/services/state.service';


@Component({
  selector: 'app-release-order-form',
  templateUrl: './release-order-form.component.html',
  styleUrls: ['./release-order-form.component.css']
})
export class ReleaseOrderFormComponent implements OnInit {
  public releaseOrderForm: FormGroup;
  public itensPedido: FormGroup;
  public medicinesForm: FormGroup;
  public findMedicinesForm: FormGroup;
  public showPrevision : boolean = false;
  @ViewChild("qtyEdit") private qtyEdit: ElementRef;
  @ViewChild("estoqueAtual") private estoqueEdit: ElementRef;
  @ViewChild("crm") private crm: ElementRef;
  @ViewChild("dosage") private dosage: ElementRef;
  @ViewChild("qty") private qty: ElementRef;
  @ViewChild("ean") private ean: ElementRef;
  patients = [];
  dispensations = [];
  listMedicine = [];
  itensPed = [];
  listPatientMedicines = [];
  listMedicines = [];
  selected = [];
  listState = [];


  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: any,
    public dialogRef: MatDialogRef<ReleaseOrderFormComponent>,
    private patientsService: PatientsService,
    private snack: MatSnackBar,
    private loader: AppLoaderService,
    private information: AppInformationService,
    private crudService: CRUDService,
    private loginService: LoginService,
    private clientService: ClientService,
    private stateService: StateService,
  ) { }

  ngOnInit() {

    this.montarForm(this.data.payload);
  }

  montarForm(item) {

    this.releaseOrderForm = new FormGroup({
      patientId: new FormControl(item.patientId, [Validators.required]),
      Dispensation: new FormControl(item.dispensation),
      QtdEnvios : new FormControl("")

    });

    this.medicinesForm = new FormGroup({
      PatientId: new FormControl(item.patientId),
      MedicineId: new FormControl(""),
      MedicineName: new FormControl("", [Validators.required]),
      LabName : new FormControl(""),
      Presentation : new FormControl(""),
      Dosage: new FormControl("", [Validators.required]),
      EAN: new FormControl("", [Validators.required]),
      Qty: new FormControl("", [Validators.required]),
      Authorization: new FormControl("", [Validators.required, Validators.minLength(3)]),
      Crm: new FormControl("", [Validators.required,Validators.maxLength(6)]),
      StateId: new FormControl("", [Validators.required]),
      ReleaseOrderObservation: new FormControl("")
    });

    this.findMedicinesForm = new FormGroup({
      MedicineFind: new FormControl("",[Validators.required, Validators.minLength(3)])
    });

    this.itensPedido = new FormGroup({
      medicineId: new FormControl("", [Validators.required]),
      dosage: new FormControl(0, [Validators.required, Validators.min(1)]),
      quantity: new FormControl(0, [Validators.required, Validators.min(1)])
    });

    let user = this.loginService.getNomeUsuarioDetalhes(); 
    let params: any = new Object();
    params.patientName  ="";
    params.cpf = "";
    params.identificationOnClient = "";    
    params.ClientId = user.ClientId;

    this.crudService.GetParams(params, "/patient").subscribe(res => {
      this.patients = [];
      this.patients = res.ObjectDTO;
    });

    let params2: any = new Object();
    params2.userLogin = "";

    // this.crudService.GetParams(params2, "/dispensationPeriod").subscribe(res => {
    //   this.dispensations = [];
    //   this.dispensations = res.ObjectDTO;
    // });

    // let params3: any = new Object();
    // params3.userLogin = "";

    // this.crudService.GetParams(params3, "/medicine").subscribe(res => {
    //   this.listMedicine = [];
    //   this.listMedicine = res.ObjectDTO;
    // });



    if (!this.data.novo) {
      this.releaseOrderForm.disable();

      let orderItem: any;
      for (let i = 0; i < item.ListReleaseOrderItem.length; i++) {
        orderItem = new Object();
        orderItem.EAN = item.ListReleaseOrderItem[i].EAN;
        orderItem.MedicineName = item.ListReleaseOrderItem[i].medicineName;
        orderItem.Crm = item.ListReleaseOrderItem[i].crm;
        orderItem.UFCRM = item.ListReleaseOrderItem[i].stateUF;
        orderItem.LabName = item.ListReleaseOrderItem[i].labName;
        orderItem.Presentation = item.ListReleaseOrderItem[i].presentation;
        orderItem.Dosage = item.ListReleaseOrderItem[i].dosage;
        orderItem.Qty = item.ListReleaseOrderItem[i].quantity;
        orderItem.Authorization = item.ListReleaseOrderItem[i].patientAuthorization;
        this.listPatientMedicines.push(orderItem);
      }
    }

    this.getState();
  }

  SavePedido() {
    let ListReleaseOrderItem = [];
    let pedido: any;
    this.loader.open();

    pedido = new Object();
    pedido.releaseOrderDate = "";
    pedido.patientId = this.releaseOrderForm.controls["patientId"].value;
    pedido.dispensation = this.releaseOrderForm.controls["Dispensation"].value;
    pedido.ListReleaseOrderItem = [];
    pedido.QtdEnvios = this.releaseOrderForm.controls["QtdEnvios"].value;

    let itemPedido: any;
    for (let i = 0; i < this.listPatientMedicines.length; i++) {
      itemPedido = new Object();
      itemPedido.medicineId = this.listPatientMedicines[i].MedicineId;
      itemPedido.dosage = this.listPatientMedicines[i].Dosage;
      itemPedido.quantity = this.listPatientMedicines[i].Qty;
      itemPedido.crm = this.listPatientMedicines[i].Crm;
      itemPedido.stateId = this.listPatientMedicines[i].StateId;
      itemPedido.patientAuthorization = this.listPatientMedicines[i].Authorization;
      itemPedido.ReleaseOrderItemObservation = this.listPatientMedicines[i].ReleaseOrderObservation;
      pedido.ListReleaseOrderItem.push(itemPedido);
    }

    this.crudService.Save(pedido, true, "/releaseOrder").subscribe(res => {
      if (res.Status == "OK") {
        this.loader.close();
        this.snack.open("Registro gravado com sucesso", "", { duration: 3000 });
        this.dialogRef.close('');
      } else {
        this.loader.close();
        this.snack.open("Erro ao gravar registro:" + res.Message, "", { duration: 5000 });
        this.dialogRef.close('');
      }
    });
  }

  getStock(event) { }

  addProduct() {
    const formulario = this.itensPedido.value;
    let medicineName = "";

    /*  if (parseInt(formulario.quantity) > parseInt(this.estoqueEdit.nativeElement.value)) {
       this.information.information("YouVita", "Quantidade solicitada maior que o estoque atual");
       return;
     } */

    for (let i = 0; i < this.listMedicine.length; i++) {
      if (this.listMedicine[i].MedicineId == formulario.medicineId) {
        medicineName = this.listMedicine[i].MedicineName;
      }
    }
    formulario.medicineName = medicineName;

    this.itensPed.push(formulario);
    this.itensPed = [...this.itensPed];
    this.itensPedido.reset();
    this.qtyEdit.nativeElement.value = "";

  }

  removeItem(registro) {
    let aux = this.itensPed.filter(item => {
      if (item === registro) {
        return false;
      } else {
        return true;
      }
    });
    this.itensPed = [...aux];
  }

  formValido(): boolean {
    let v = this.releaseOrderForm.invalid;
    if (this.listPatientMedicines.length == 0) {
      v = true; //invalido
    }
    return v;
  }

  addPatientMedicine() {
    let newMedicine = this.medicinesForm.value;

    for (let i = 0; i < this.listState.length; i++) {
      if(this.medicinesForm.value.StateId == this.listState[i].stateId){
        newMedicine.UFCRM = this.listState[i].stateUF;
        break;
      }
    }
    this.listPatientMedicines.push(newMedicine);
    this.listPatientMedicines = [...this.listPatientMedicines];
    this.medicinesForm.reset();
    this.crm.nativeElement.value = "";
    this.dosage.nativeElement.value = "";
    this.qty.nativeElement.value = "";
    this.ean.nativeElement.value = "";
  }




  removeMedicine(medicine: any) {
    let aux = this.listPatientMedicines.filter(item => {
      if (item === medicine) {
        return false;
      } else {
        return true;
      }
    });
    this.listPatientMedicines = [...aux]

  }

  procurarMedicamento() {
    let filter = this.findMedicinesForm.controls["MedicineFind"].value;
    this.clientService.buscarMedicamentos(filter).subscribe(res => {
      this.listMedicines = [];
      this.listMedicines = res.ObjectDTO;
    });
  }

  onSelect({ selected }) {
    this.medicinesForm.controls["MedicineId"].setValue(selected[0].MedicineId);
    this.medicinesForm.controls["EAN"].setValue(selected[0].EAN);
    this.medicinesForm.controls["MedicineName"].setValue(selected[0].MedicineName);
    this.medicinesForm.controls["LabName"].setValue(selected[0].LabName);
    this.medicinesForm.controls["Presentation"].setValue(selected[0].Presentation);
    
  }

  getState() {
    this.stateService.listarEstados().subscribe(res => {
      this.listState = [];
      this.listState = res.ObjectDTO;
    })
  }

  changePrev(evento){
    this.showPrevision = !this.showPrevision;

  }
}
