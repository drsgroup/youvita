import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ReleaseOrderFormComponent } from './release-order-form/release-order-form.component';
import { ReleaseOrderService } from 'app/services/negocio/releaseOrder/release-order.service';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { CampoBusca } from 'app/models/base/negocio/CampoBusca';


@Component({
  selector: 'app-release-order',
  templateUrl: './release-order.component.html',
  styleUrls: ['./release-order.component.css']
})
export class ReleaseOrderComponent implements OnInit {

  private releaseOrderForm: FormGroup;

  constructor(
    private dialog: MatDialog,
    private releaseOrderService: ReleaseOrderService,
    private loader: AppLoaderService,
    private confirm: AppConfirmService,
    private snackBar: MatSnackBar,
    private appInformationService: AppInformationService,
    private crudService: CRUDService
  ) { }


  colunas = [
    {
      Propriedade: 'releaseOrderId',
      Titulo: 'ID Liberação',
      Visivel: true,
      Largura: 50
    },
    {
      Propriedade: 'releaseOrderDate',
      Titulo: 'Data da Ordem',
      Visivel: true,
      Largura: 80,
      Tipo: 'DATA'
    },
    {
      Propriedade: 'patientName',
      Titulo: 'Nome do Paciente',
      Visivel: true,
      Largura: 300
    },
    {
      Propriedade: 'dispensation',
      Titulo: 'Prazo em dias',
      Visivel: true,
      Largura: 200
    }
  ];

  linhas = [];
  configBusca = [
    new CampoBusca("ReleaseOrderDate", "Data Pedido", 10, "", "DATA"),
    new CampoBusca("PatientName", "Nome Paciente", 50, "", "string")
  ]

  public lastFind: any;

  ngOnInit() {

  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Novo Pedido do Paciente";
    } else {
      titulo = "Visualização do Pedido: " + dados.releaseOrderId;
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(ReleaseOrderFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.consultarReleaseOrder(this.lastFind)
        return;
      });
  }

  consultarReleaseOrder(parametro: any) {
    this.lastFind = parametro;
    this.crudService.GetParams(parametro, "/releaseOrder").subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO
    })
    // this.releaseOrderService.getReleaseOrderByFilter(parametro).subscribe(res => {
    //   this.linhas = [];
    //   this.linhas = res.ObjectDTO
    // })
  }

  deleteReleaseOrder(parametro: any) {
    this.confirm.confirm("Exclusão do Pedido", "Tem certeza que deseja excluir o pedido " + parametro.releaseOrderId + "?").subscribe(result => {
      if (result === true) {
        this.loader.open("Excluindo pedido");
        this.releaseOrderService.deleteReleaseOrder(parametro).subscribe(res => {
          if (res.Status == "OK") {
            this.snackBar.open("Pedido excluído com sucesso!", "", { duration: 3000 });
            this.consultarReleaseOrder(this.lastFind);
          }
          else {
            this.appInformationService.information("YouVita", res.Message);
          }
          this.loader.close();
        })
      }
    })
  }

}
