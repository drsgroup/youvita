import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';

@Component({
  selector: 'app-medicines-form',
  templateUrl: './medicines-form.component.html',
  styleUrls: ['./medicines-form.component.css']
})
export class MedicinesFormComponent implements OnInit {
  medicineForm : FormGroup;
  
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  public dialogRef: MatDialogRef<MedicinesFormComponent>,
  public crud : CRUDService,
  private snack : MatSnackBar
  ) { }

  ngOnInit() {
    this.montarForm(this.data.payload);
    this.medicineForm.controls['EAN'].disable();
  }  

  montarForm(item){
    
    this.medicineForm = new FormGroup({      
      MedicineId : new FormControl({value : item.MedicineId, disabled: true}),      
      EAN : new FormControl({value : item.EAN, disabled: true}),
      MedicineName : new FormControl({value : item.MedicineName, disabled: true}),
      LabName : new FormControl({value : item.LabName, disabled: true}),
      ActivePrincipleName : new FormControl({value :item.ActivePrincipleName, disabled: true}),
      Presentation : new FormControl({value : item.Presentation, disabled: true}),
      UnitName : new FormControl({value : item.UnitName, disabled: true}),
      Dosage : new FormControl({value : item.Dosage, disabled: true}),
      codClasse : new FormControl(item.codClasse||""),
      DescClasse : new FormControl(item.DescClasse||"")

    });    
    console.log(JSON.stringify(item))

  }

  salvar(){
    let form = this.medicineForm.value;
    form.MedicineId = this.data.payload.MedicineId;
    this.crud.Save(form, false, "/Medicine").subscribe(res => {
      if (res.Status =="OK"){
        this.snack.open("Medicamento atualizado com sucesso", "", { duration : 3000});
        this.dialogRef.close("OK");

      }
    })


  }
}
