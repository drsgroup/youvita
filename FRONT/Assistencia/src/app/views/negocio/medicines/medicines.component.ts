import { ClientService } from 'app/services/negocio/client/client.service';
import { MedicinesFormComponent } from './medicines-form/medicines-form.component';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { CampoBusca } from 'app/models/base/negocio/CampoBusca';


@Component({
  selector: 'app-medicines',
  templateUrl: './medicines.component.html',
  styleUrls: ['./medicines.component.css']
})
export class MedicinesComponent implements OnInit {
  linhas = [];
  colunas = [
    {
      Propriedade: 'MedicineId',
      Titulo: 'Id.Medicamento',
      Visivel: true,
      Largura: 80
    },
    {
      Propriedade: 'EAN',
      Titulo: 'EAN',
      Visivel: true,
      Largura: 100
    },
    {
      Propriedade: 'MedicineName',
      Titulo: 'Medicamento',
      Visivel: true,
      Largura: 200
    },
    {
      Propriedade: 'LabName',
      Titulo: 'Laboratório',
      Visivel: true,
      Largura: 200
    },
    {
      Propriedade: 'ActivePrincipleName',
      Titulo: 'Princípio ativo',
      Visivel: true,
      Largura: 200
    },
    {
      Propriedade: 'QtyStock',
      Titulo: 'Estoque',
      Visivel: true,
      Largura: 100
    },    
    {
      Propriedade: 'codClasse',
      Titulo: 'Cód.Classe',
      Visivel: true,
      Largura: 100
    }    ,
    {
      Propriedade: 'DescClasse',
      Titulo: 'Classe Terapêutica',
      Visivel: true,
      Largura: 200
    }
  ];

  configBusca = [
    new CampoBusca("EAN", "EAN", 15, "", "string"),
    new CampoBusca("MedicineName", "Medicamento", 50, "", "string"),
    new CampoBusca("LabName", "Laboratório", 50, "", "string"),
    new CampoBusca("ActivePrincipleName", "Princípio Ativo", 50, "", "string"),
    new CampoBusca("Presentation", "Apresentação", 50, "", "string")   
  ]
  public lastFind: any;

  constructor(
    private dialog: MatDialog,
    private serviceCliente: ClientService,
    private information: AppInformationService,
    private crudService: CRUDService) { }

  ngOnInit() {
  }

  openForm(medicine: any, novo: boolean) {
    let dialogRef: MatDialogRef<any> = this.dialog.open(MedicinesFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: "Medicamento", payload: medicine, novo: false }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.queryMedicine(this.lastFind);
        return;
      });
  }

  queryMedicine(value: any) {
    //   if (value) {
    //     if(value.length <=2){
    //       this.information.information("YouVita  - Assistência", "Para consultar medicamentos é necessário no mínimo 3 caracteres.");
    //       return;
    //     }
    //     this.serviceCliente.buscarMedicamentos(value).subscribe(res => {
    //       this.linhas = [];
    //       this.linhas = res.ObjectDTO;
    //     });
    //   }else{
    //     this.information.information("YouVita  - Assistência", "Para consultar medicamentos é necessário no mínimo 3 caracteres.");
    //   }
    // 
    this.lastFind = value;
    this.crudService.GetParams(value, "/medicine").subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO;
    })
  }

}
