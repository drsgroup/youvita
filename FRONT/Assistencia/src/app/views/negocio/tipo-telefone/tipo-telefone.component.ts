import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar, MatDialogRef } from '@angular/material';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { FormPhoneTypeComponent } from 'app/views/negocio/tipo-telefone/form-phone-type/form-phone-type.component';
import { PhoneTypeService } from 'app/services/phone-type.service';

@Component({
  selector: 'app-tipo-telefone',
  templateUrl: './tipo-telefone.component.html',
  styleUrls: ['./tipo-telefone.component.css']
})
export class TipoTelefoneComponent implements OnInit {

  colunas = [
    {
      Propriedade: 'phoneTypeId',
      Titulo: 'Identificador',
      Visivel: true
    },
    {
      Propriedade: 'description',
      Titulo: 'Tipo de Telefone',
      Visivel: true
    }
  ];

  linhas = [];
                  
  constructor(private dialog: MatDialog,
    private snack: MatSnackBar,    
    private confirmacao: AppConfirmService,
    private loader: AppLoaderService,
    private snackBar: MatSnackBar,
    private phoneTypeService: PhoneTypeService) { }

  ngOnInit() {
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Novo Tipo de Telefone";
    } else {
      titulo = "Editar Tipo de Telefone";
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(FormPhoneTypeComponent, {
      width: '720px',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        //this.consultarLoja();
        return;
      });

  }

  getPhoneType(){
      this.phoneTypeService.getPhoneType().subscribe(res => {
        this.linhas = [];
        this.linhas = res.ObjectDTO;
      });
  }
  
  ExcluirPaciente(parametro){

  }
}