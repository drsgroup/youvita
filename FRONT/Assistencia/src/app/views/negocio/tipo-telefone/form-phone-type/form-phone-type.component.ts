import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-form-phone-type',
  templateUrl: './form-phone-type.component.html',
  styleUrls: ['./form-phone-type.component.css']
})
export class FormPhoneTypeComponent implements OnInit {

  public formPhoneType: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  public dialogRef: MatDialogRef<FormPhoneTypeComponent>,
  public loader: AppLoaderService,
  private fb: FormBuilder,
  private snackBar: MatSnackBar) { }

  ngOnInit() {

    this.montarForm(this.data.payload);
  }

  montarForm(item) {   
    this.formPhoneType = new FormGroup({      
      PhoneTypeId: new FormControl(item.phoneTypeId, [Validators.required, Validators.maxLength(8)]),
      Description : new FormControl(item.description, [Validators.required, Validators.maxLength(200)]),
    });    
  }  

  InputPhoneType(){
    this.snackBar.open("Gravando o Tipo de Telefone", "", { duration: 3000 });
    
    this.dialogRef.close("OK");

  }

}
