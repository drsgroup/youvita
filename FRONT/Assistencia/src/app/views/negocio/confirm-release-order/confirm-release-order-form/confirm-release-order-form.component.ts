import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatSnackBar } from '@angular/material';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { CRUDService } from '../../../../services/negocio/CRUDService/CRUDService';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from 'app/services/negocio/login/login.service';
import { WebContract } from 'app/models/base/Contrato';

@Component({
  selector: 'app-confirm-release-order-form',
  templateUrl: './confirm-release-order-form.component.html',
  styleUrls: ['./confirm-release-order-form.component.css']
})
export class ConfirmReleaseOrderFormComponent implements OnInit {
  public ClientId: number;
  confirmReleaseOrderForm: FormGroup;
  confirmReleaseOrderAddressForm: FormGroup;
  doctorForm: FormGroup;
  addressForm: FormGroup;
  listAddress = [];
  linhasStates = [];
  listPhones = [];
  listReleaseOrdem = [];
  listMedicine = [];
  public newAddr : boolean = false;


  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ConfirmReleaseOrderFormComponent>,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private loader: AppLoaderService,
    private crud: CRUDService,
    private loginService: LoginService
  ) { }



  ngOnInit() {
    let usuario = this.loginService.getNomeUsuarioDetalhes();
    this.ClientId = usuario.ClientId;
    this.montarForm(this.data.payload);
    this.getAddress(this.data.payload.patientId);
    this.getStates();
    this.getPatientPhones(this.data.payload.patientId);
    this.listReleaseOrdem = this.data.payload.ListReleaseOrderItem;

  }

  montarForm(item) {

    this.confirmReleaseOrderForm = new FormGroup({
      releaseOrderId: new FormControl(item.releaseOrderId),
      patientName: new FormControl(item.patientName),
      patientAuthorization: new FormControl(item.patientAuthorization),
      dispensationId: new FormControl(item.dispensationId),
      releaseOrderStatus: new FormControl(item.releaseOrderStatus),
      releaseOrderObservation: new FormControl(item.releaseOrderObservation)
    });

    this.confirmReleaseOrderAddressForm = new FormGroup({
      patientAddressesId: new FormControl(item.patientAddressesId),
      patientId: new FormControl(item.patientId),
      addressTypeId: new FormControl(""),
      address: new FormControl(""),
      addressNumber: new FormControl(""),
      city: new FormControl(""),
      neighborhood: new FormControl(""),
      stateId: new FormControl(""),
      zipCode: new FormControl(""),
      complement: new FormControl("")
    });

    this.addressForm = new FormGroup({
      PatientAddressesId: new FormControl(""),
    });

    this.confirmReleaseOrderForm.disable();
    this.confirmReleaseOrderAddressForm.disable();
  }

  getAddress(patientId: string) {
    let params : any = new Object();    
    params.patientId  = patientId;        
    this.crud.GetParams(params, "/patientAddress").subscribe(res => {
      this.listAddress = [];
      this.listAddress = res.ObjectDTO;
    })
  }

  getStates() {
    this.crud.List("", "/state").subscribe(res => {
      this.linhasStates = [];
      this.linhasStates = res.ObjectDTO;
    })
  }

  selectAddress(evento) {
    for (let i = 0; i < this.listAddress.length; i++) {

      if (this.listAddress[i].PatientAddressesId == evento.value) {
        this.confirmReleaseOrderAddressForm.controls["address"].setValue(this.listAddress[i].Address);
        this.confirmReleaseOrderAddressForm.controls["addressNumber"].setValue(this.listAddress[i].AddressNumber);
        this.confirmReleaseOrderAddressForm.controls["complement"].setValue(this.listAddress[i].Complement);
        this.confirmReleaseOrderAddressForm.controls["city"].setValue(this.listAddress[i].City);
        this.confirmReleaseOrderAddressForm.controls["neighborhood"].setValue(this.listAddress[i].Neighborhood);
        this.confirmReleaseOrderAddressForm.controls["stateId"].setValue(this.listAddress[i].StateId);
        this.confirmReleaseOrderAddressForm.controls["zipCode"].setValue(this.listAddress[i].ZipCode);
      }
    }
  }

  newAddress() {
    this.confirmReleaseOrderAddressForm.controls["address"].setValue("");
    this.confirmReleaseOrderAddressForm.controls["addressNumber"].setValue("");
    this.confirmReleaseOrderAddressForm.controls["complement"].setValue("");
    this.confirmReleaseOrderAddressForm.controls["city"].setValue("");
    this.confirmReleaseOrderAddressForm.controls["neighborhood"].setValue("");
    this.confirmReleaseOrderAddressForm.controls["stateId"].setValue("");
    this.confirmReleaseOrderAddressForm.controls["zipCode"].setValue("");
    this.confirmReleaseOrderAddressForm.enable();
    this.newAddr = true;
  }

  editAddress() {
    this.confirmReleaseOrderAddressForm.enable();
    this.newAddr = false;
  }

  saveAddress() {
    let address = this.confirmReleaseOrderAddressForm.value;
    address.patientAddressesId = this.addressForm.value.PatientAddressesId;
    this.crud.Save(address, this.newAddr, "/patientAddress").subscribe(res => {      
      var cnt = new WebContract();
      cnt = res;
      if (cnt.Status == "OK") {
        this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });
        this.newAddr = false;
        this.getAddress(this.data.payload.patientId);
        this.confirmReleaseOrderAddressForm.disable();
      } else {
        this.snackBar.open(cnt.Message, "", { duration: 3000 });
      }      
    });
  }

  cancelEdit() {
    this.confirmReleaseOrderAddressForm.reset();
    this.confirmReleaseOrderAddressForm.disable();

  }

  getPatientPhones(patientId: string) {
    let params : any = new Object();
    params.userLogin = "x";
    params.patientId  = patientId;    
    params.ok = "0";
    this.crud.GetParams(params, "/patientPhone").subscribe(res => {
      this.listPhones = [];
      this.listPhones = res.ObjectDTO;
    })
  }

  saveOrder() {
    let order: any;
    this.loader.open();

    order = new Object();
    order.releaseOrderId = this.confirmReleaseOrderForm.value.releaseOrderId;
    order.patientId = this.confirmReleaseOrderAddressForm.value.patientId;
    order.patientAddressId = this.addressForm.value.PatientAddressesId;
    order.patientAuthorization = this.confirmReleaseOrderForm.value.patientAuthorization;
    order.orderObservation = this.confirmReleaseOrderForm.value.releaseOrderObservation;
    order.ListOrderItems = this.listReleaseOrdem;

    this.crud.Save(order, true, "/orders").subscribe(res => {
      if (res.Status == "OK") {
        this.loader.close();
        this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });
        this.dialogRef.close('');
      } else {
        this.loader.close();
        this.snackBar.open("Erro ao gravar registro:" + res.Message, "", { duration: 5000 });
        this.dialogRef.close('');
      }
    });

  }


}


