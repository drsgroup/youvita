import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { LoginService } from 'app/services/negocio/login/login.service';
import { ConfirmReleaseOrderFormComponent } from './confirm-release-order-form/confirm-release-order-form.component';
import { CampoBusca } from 'app/models/base/negocio/CampoBusca';

@Component({
  selector: 'app-confirm-release-order',
  templateUrl: './confirm-release-order.component.html',
  styleUrls: ['./confirm-release-order.component.css']
})
export class ConfirmReleaseOrderComponent implements OnInit {

  colunas = [
    {
      Propriedade: 'releaseOrderId',
      Titulo: 'ID Liberação',
      Visivel: true,
      Largura: 50
    },
    {
      Propriedade: 'releaseOrderDate',
      Titulo: 'Data da Ordem',
      Visivel: true,
      Largura: 80,
      Tipo: 'DATA'
    },
    {
      Propriedade: 'patientName',
      Titulo: 'Nome do Paciente',
      Visivel: true,
      Largura: 300
    },
    {
      Propriedade: 'dispensation',
      Titulo: 'Prazo em dias',
      Visivel: true,
      Largura: 200
    }
  ];

  linhas = [];

  configBusca = [
    new CampoBusca("ReleaseOrderDate", "Data do Pedido", 10, "", "DATA"),
    new CampoBusca("PatientName", "Nome Paciente", 50, "", "string")
  ]

  public lastFind : any;

  constructor(private dialog: MatDialog,
    private crudServices: CRUDService,
    private login: LoginService) { }

  ngOnInit() {
    
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo = "Confirmação da liberação de Pedido";

    let dialogRef: MatDialogRef<any> = this.dialog.open(ConfirmReleaseOrderFormComponent, {
      width: '90%', 
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });
    
    dialogRef.afterClosed()
    .subscribe(res => {
      this.consultarReleaseOrder(this.lastFind);
      return;
    });
  }

  consultarReleaseOrder(parametro : string){
    this.lastFind = parametro;
    this.crudServices.GetParams(parametro, "/releaseOrder").subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO;
    })
  }
  deleteConfirm(xx){

  }
}
