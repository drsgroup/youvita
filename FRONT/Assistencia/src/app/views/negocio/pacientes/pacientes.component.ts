import { LoginService } from 'app/services/negocio/login/login.service';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { PacientesFormularioComponent } from './pacientes-formulario/pacientes-formulario.component';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { PatientsService } from 'app/services/negocio/patients/patients.service';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { CampoBusca } from 'app/models/base/negocio/CampoBusca';


@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.component.html',
  styleUrls: ['./pacientes.component.css']
})
export class PacientesComponent implements OnInit {

  colunas = [
    {
      Propriedade: 'PatientId',
      Titulo: 'Identificador',
      Visivel: true,
      Largura: 50
    },
    {
      Propriedade: 'ClientFantasyName',
      Titulo: 'Cliente',
      Visivel: true,
      Largura : 250
    },
    {
      Propriedade: 'PatientName',
      Titulo: 'Nome do Paciente',
      Visivel: true,
      Largura: 250
    },
    {
      Propriedade: 'CPF',
      Titulo: 'CPF',
      Visivel: true,
      Largura: 100
    },    
  ];

  linhas = [];  
  public configBusca = [
    new CampoBusca("patientName", "Nome Paciente", 50, "", "string"),
    new CampoBusca("CPF", "CPF", 15, "000.000.000-00", "string"),
    new CampoBusca("identificationOnClient", "Identificação no cliente", 50, "", "string")

  ];
  public lastFind : any;

  constructor(private dialog: MatDialog,
    private confirm: AppConfirmService,
    private loader: AppLoaderService,
    private snackBar: MatSnackBar,
    private patients: PatientsService,
    private appInformationService: AppInformationService,
    private loginService : LoginService,
    private crudService : CRUDService

  ) { }

  ngOnInit() {    
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Novo Paciente" ;
    } else {
      titulo = "Editar Paciente: " + dados.PatientId;
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(PacientesFormularioComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        //this.consultarLoja();
        this.getPatients(this.lastFind);
        return;
      });
  }

  getPatients(parametro : any) {
    this.lastFind = parametro;
    let usr = this.loginService.getNomeUsuarioDetalhes();
    let params: any = new Object();
    params.patientName = parametro.patientName;
    params.cpf = parametro.CPF;
    params.identificationOnClient = parametro.identificationOnClient;
    params.clientId = usr.ClientId;    
    
    this.crudService.GetParams(params, "/patient").subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO;      
    });
  }

  deletePatient(patient: any) {
    this.confirm.confirm("Exclusão do Paciente", "Tem certeza que deseja excluir o Paciente " +
      patient.patientName + "?").subscribe(result => {

        if (result === true) {
          this.loader.open("Excluindo Paciente");
          this.patients.deletePatient(patient).subscribe(res => {
            if (res.Status == "OK") {
              this.snackBar.open("Paciente Excluído com sucesso!", "", { duration: 3000 });
              this.getPatients(this.lastFind);
            }
            else {
              this.appInformationService.information("YouVita", res.Message);
            }
            this.loader.close();
          })
        }
      })
  }
}
