import { PacientePatologiasComponent } from './paciente-patologias/paciente-patologias.component';
import { PacienteEnderecosComponent } from './paciente-enderecos/paciente-enderecos.component';
import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatInput, MatCheckbox, MatSelect } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { PatientsService } from 'app/services/negocio/patients/patients.service';
import { ClientService } from 'app/services/negocio/client/client.service';
import { WebContract } from 'app/models/base/Contrato';
import { PhoneTypeService } from 'app/services/phone-type.service';
import { StateService } from 'app/services/state.service';
import { AddressTypeService } from 'app/services/address-type.service';
import { PathologiesService } from 'app/services/pathologies.service';
import { PacientesFoneComponent } from './pacientes-fone/pacientes-fone.component';
import { LoginService } from 'app/services/negocio/login/login.service';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';

@Component({
  selector: 'app-pacientes-formulario',
  templateUrl: './pacientes-formulario.component.html',
  styleUrls: ['./pacientes-formulario.component.css']
})
export class PacientesFormularioComponent implements OnInit {
  public pacienteForm: FormGroup;
  public foneForm: FormGroup;
  public addressForm: FormGroup;
  public medicinesForm: FormGroup;
  public medicinesFind: FormGroup;
  public findMedicinesForm: FormGroup;
  public releaseForm : FormGroup;
  public ClientId: number;
  public telaCliente: boolean;
  public NomeCliente: string;
  public fileContent: string = "";
  public showPrevision: boolean = false;
  @ViewChild("telefone") private tel: ElementRef;
  @ViewChild("dosage") private dosage: ElementRef;
  @ViewChild("qty") private qty: ElementRef;
  @ViewChild("ean") private ean: ElementRef;
  @ViewChild("crm") private crm: ElementRef;
  @ViewChild("chkNovos") private chkNovos: MatCheckbox;
  @ViewChild("chkQtdEnvios") private chkQtdEnvios: MatSelect;
  @ViewChild("periodos") private periodos: MatSelect;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<PacientesFormularioComponent>,
    public loader: AppLoaderService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private patientService: PatientsService,
    private clientService: ClientService,
    private dialog: MatDialog,
    private phoneTypeService: PhoneTypeService,
    private stateService: StateService,
    private addressTypeService: AddressTypeService,
    private loginService: LoginService,
    private crudService: CRUDService) { }

  listClients = [];
  listPhoneType = [];
  listPhones = [];
  listState = [];
  listAddressType = [];
  listAddress = [];
  listPatientMedicines = [];
  listMedicines = [];
  selected = [];
  patientTypes = [
    { value: 'T', viewValue: 'Titular' },
    { value: 'D', viewValue: 'Dependente' }
  ];

  patientStatus = [
    { value: '1', viewValue: 'Ativo' },
    { value: '0', viewValue: 'Suspenso' }
  ];

  maxDate: any;

  ngOnInit() {
    let usuario = this.loginService.getNomeUsuarioDetalhes();
    this.ClientId = usuario.ClientId;

    if (usuario.userType > 1) {
      this.telaCliente = true;
    } else {
      this.telaCliente = false;
    }
    this.montarForm(this.data.payload);
  }

  ftelaCliente(): Boolean {
    let usr = this.loginService.getNomeUsuarioDetalhes();
    if (usr.userType > 1) {
      return true;
    } else {
      return false;
    }
  }

  getPhoneType() {
    this.phoneTypeService.getPhoneType().subscribe(res => {
      this.listPhoneType = [];
      this.listPhoneType = res.ObjectDTO;
      this.getPhoneDescription();
    })
  }

  changeShowPrev() {
    this.showPrevision = !this.showPrevision;
  }

  montarForm(item) {
    this.consultarClients();
    this.pacienteForm = new FormGroup({
      PatientId: new FormControl(item.PatientId),
      ClientName: new FormControl({ value: "", disabled: true }),
      ClientId: new FormControl(item.ClientId),
      PatientName: new FormControl(item.PatientName, [Validators.required, Validators.minLength(5), Validators.maxLength(60)]),
      BirthDate: new FormControl(item.BirthDate, [Validators.required]),
      CPF: new FormControl(item.CPF, [Validators.required, Validators.minLength(11)]),
      RG: new FormControl(item.RG, [Validators.required, Validators.maxLength(15)]),
      patientType: new FormControl(item.patientType, [Validators.required]),
      IdentificationOnClient: new FormControl(item.IdentificationOnClient || "", [Validators.required]),
      Responsible: new FormControl(item.Responsible || "", [Validators.required]),
      BestContactPeriod: new FormControl(item.BestContactPeriod || "", [Validators.required]),
      PatientStatus: new FormControl(item.PatientStatus, [Validators.required]),
      CID1: new FormControl(item.CID1 || "", [Validators.maxLength(20), Validators.required]),
      CID2: new FormControl(item.CID2 || ""),
      CID3: new FormControl(item.CID3 || ""),
      Comments: new FormControl(item.Comments || ""),
      PatientEnabled: new FormControl(item.PatientEnabled || "1")
    });

    this.foneForm = new FormGroup({
      phoneNumber: new FormControl("", [Validators.required, Validators.minLength(10)]),
      ramal: new FormControl(""),
      phoneTypeId: new FormControl("", [Validators.required, Validators.min(0)])
    });

    this.addressForm = new FormGroup({
      patientId: new FormControl(""),
      addressTypeId: new FormControl("", [Validators.required]),
      Address: new FormControl("", [Validators.required]),
      AddressNumber: new FormControl("", [Validators.required]),
      City: new FormControl("", [Validators.required]),
      Neighborhood: new FormControl("", [Validators.required]),
      stateId: new FormControl("", [Validators.required]),
      ZipCode: new FormControl("", [Validators.required]),
      Complement: new FormControl(""),
    });

    this.medicinesForm = new FormGroup({
      PatientId: new FormControl(item.PatientId),
      MedicineName: new FormControl(""),
      LabName: new FormControl(""),
      Presentation: new FormControl(""),
      Dosage: new FormControl("", [Validators.required]),
      EAN: new FormControl("", [Validators.required]),
      Qty: new FormControl("", [Validators.required]),
      Authorization: new FormControl("", [Validators.required]),
      CRM: new FormControl("", [Validators.required]),
      UF_CRM: new FormControl("", [Validators.required]),
      DatePrescription: new FormControl("", [Validators.required]),
      MedicineId : new FormControl("")

    });

    this.findMedicinesForm = new FormGroup({
      MedicineFind: new FormControl("", [Validators.required, Validators.minLength(3)])
    });

    this.releaseForm = new FormGroup({
      Dispensation : new FormControl(""),
      QtdEnvios : new FormControl("")
    });

    this.listPhones = (item.ListPatientPhones || []);
    this.listAddress = (item.ListPatientAddresses || []);

    this.getPhoneType();
    this.getState();
    this.getAddressType();

    let dt = new Date();
    this.maxDate = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate());


    if (!this.data.novo) { //desabilitar alterar ClientId
      this.pacienteForm.controls["ClientId"].disable();
    }
  }

  savePatient() {
    let formPatient = this.pacienteForm.value;
    formPatient.ListPatientPhones = this.listPhones;
    formPatient.ListPatientAddresses = this.listAddress;
    formPatient.chkNovos = this.chkNovos.value;
    formPatient.QtdEnvios !== undefined ? this.chkQtdEnvios.value : 1;
    formPatient.Frequency !== undefined ? this.periodos.value : 1;


    let pedido: any = new Object();
    pedido.releaseOrderDate = "";
    pedido.patientId = this.pacienteForm.controls["PatientId"].value;
    pedido.QtdEnvios = this.releaseForm.controls['QtdEnvios'].value;
    pedido.Dispensation = this.releaseForm.controls['Dispensation'].value;    
    pedido.releaseOrderDate = new Date();
    pedido.ListReleaseOrderItem = [];

    let itemPedido: any;
    for (let i = 0; i < this.listPatientMedicines.length; i++) {
      itemPedido = new Object();
      itemPedido.medicineId = this.listPatientMedicines[i].MedicineId;
      itemPedido.dosage = this.listPatientMedicines[i].Dosage;
      itemPedido.quantity = this.listPatientMedicines[i].Qty;
      itemPedido.crm = this.listPatientMedicines[i].CRM;
      itemPedido.stateId = this.listPatientMedicines[i].UF_CRM;
      itemPedido.ean = this.listPatientMedicines[i].EAN;
      itemPedido.patientAuthorization = this.listPatientMedicines[i].Authorization;
      itemPedido.ReleaseOrderItemObservation = "";
      pedido.ListReleaseOrderItem.push(itemPedido);
    }

    if (this.listPatientMedicines.length > 0) {
      formPatient.ReleaseOrder = pedido;
    }

    this.patientService.savePatient(formPatient, this.data.novo).subscribe(res => {
      var cnt = new WebContract();
      cnt = res;
      if (cnt.Status == "OK") {
        this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });

      } else {
        this.snackBar.open(cnt.Message, "", { duration: 3000 });
      }
      this.dialogRef.close("OK");
    });

  }

  addPatientMedicine() {
    let newMedicine = this.medicinesForm.value;
    this.listPatientMedicines.push(newMedicine);
    this.listPatientMedicines = [...this.listPatientMedicines];
    this.medicinesForm.reset();


    this.dosage.nativeElement.value = "";
    this.qty.nativeElement.value = "";
    this.ean.nativeElement.value = "";
    this.crm.nativeElement.value = "";
  }

  getState() {
    this.stateService.listarEstados().subscribe(res => {
      this.listState = [];
      this.listState = res.ObjectDTO;
      this.adjustAddress();
    })
  }

  getAddressType() {
    this.addressTypeService.getAddressType().subscribe(res => {
      this.listAddressType = [];
      this.listAddressType = res.ObjectDTO;
      this.adjustAddress();
    })
  }

  adjustAddress() {
    if (this.listAddress.length > 0) {
      for (let i = 0; i < this.listAddress.length; i++) {
        let x = this.listAddressType.filter(value => {
          if (value.addressTypeId == this.listAddress[i].AddressTypeId) {
            return value;
          }
        });
        if (x.length > 0) {
          this.listAddress[i].addressTypeDescription = x[0].addressTypeDescription;
        }

        let auxState = this.listState.filter(value => {
          if (value.stateId == this.listAddress[i].StateId) {
            return value;
          }
        });
        if (auxState.length > 0) {
          this.listAddress[i].stateName = auxState[0].StateName;
        }
      }
    }
  }

  getPhoneDescription() {
    if (this.listPhones.length > 0) {
      for (let i = 0; i < this.listPhones.length; i++) {
        let x = this.listPhoneType.filter(value => {
          if (value.phoneTypeId == this.listPhones[i].phoneTypeId) {
            return value;
          }
        });
        if (x.length > 0) {
          this.listPhones[i].description = x[0].description;
        }
      }
    }
  }

  consultarClients() {
    //this.clientService.listarClient("").subscribe(res => {
    let params: any = new Object();
    params.clientName = "";
    params.ClientFantasyName = "";
    params.clientCNPJ = "";

    this.crudService.GetParams(params, "/client").subscribe(res => {
      this.listClients = [];
      this.listClients = res.ObjectDTO;

      //NomeCliente
      for (let x = 0; x < this.listClients.length; x++) {
        if (this.listClients[x].ClientId == this.ClientId) {
          this.pacienteForm.controls["ClientName"].setValue(this.listClients[x].ClientFantasyName);
          this.pacienteForm.controls["ClientId"].setValue(this.ClientId);
        }
      }
    })
  }

  addPhone() {
    let newPhone: any;
    newPhone = this.foneForm.value;
    newPhone.userLogin = "testejoaodokko";
    newPhone.patientsEnabled = 1;

    let x = this.listPhoneType.filter(value => {
      if (value.phoneTypeId == newPhone.phoneTypeId) {
        return value;
      }
    });
    if (x.length > 0) {
      newPhone.description = x[0].description;
    }
    this.listPhones.push(newPhone);
    this.listPhones = [...this.listPhones]
    this.foneForm.reset();
    this.tel.nativeElement.value = "";
  }

  removePhone(phone: any) {
    let aux = this.listPhones.filter(item => {
      if (item === phone) {
        return false;
      } else {
        return true;
      }
    });
    this.listPhones = [...aux]
    this.foneForm.reset();
  }

  addAddress() {
    let newAddress = this.addressForm.value;
    newAddress.userLogin = "testejoaodokko";
    newAddress.patientsEnabled = 1;

    let x = this.listAddressType.filter(value => {
      if (value.addressTypeId == newAddress.addressTypeId) {
        return value;
      }
    });
    if (x.length > 0) {
      newAddress.addressTypeDescription = x[0].addressTypeDescription;
    }

    let y = this.listState.filter(value => {
      if (value.stateId == newAddress.stateId) {
        return value;
      }
    });
    if (y.length > 0) {
      newAddress.stateName = y[0].stateName;
    }

    this.listAddress.push(newAddress);
    this.listAddress = [...this.listAddress]
    this.addressForm.reset();
  }

  onSelect({ selected }) {
    this.medicinesForm.controls["EAN"].setValue(selected[0].EAN);
    this.medicinesForm.controls["MedicineName"].setValue(selected[0].MedicineName);
    this.medicinesForm.controls["LabName"].setValue(selected[0].LabName);
    this.medicinesForm.controls["Presentation"].setValue(selected[0].Presentation);
    this.medicinesForm.controls["MedicineId"].setValue(selected[0].MedicineId);
  }

  removeAddress(address: any) {
    let aux = this.listAddress.filter(item => {
      if (item === address) {
        return false;
      } else {
        return true;
      }
    });
    this.listAddress = [...aux]
    //this.addressForm.reset();
  }

  procurarMedicamento() {
    let filter = this.findMedicinesForm.controls["MedicineFind"].value;
    this.clientService.buscarMedicamentos(filter).subscribe(res => {
      this.listMedicines = [];
      this.listMedicines = res.ObjectDTO;
    });
  }

  removeMedicine(medicine: any) {
    let aux = this.listPatientMedicines.filter(item => {
      if (item === medicine) {
        return false;
      } else {
        return true;
      }
    });
    this.listPatientMedicines = [...aux]
  }

  changePrev(evento) {
    this.showPrevision = !this.showPrevision;

  }

}
