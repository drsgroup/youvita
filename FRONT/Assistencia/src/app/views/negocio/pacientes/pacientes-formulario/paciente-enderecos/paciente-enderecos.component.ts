import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { AddressTypeService } from 'app/services/address-type.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { StateService } from 'app/services/state.service';

@Component({
  selector: 'app-paciente-enderecos',
  templateUrl: './paciente-enderecos.component.html',
  styleUrls: ['./paciente-enderecos.component.css']
})
export class PacienteEnderecosComponent implements OnInit {
  public addressForm: FormGroup;
  listAddressType = [];
  listState = [];

  constructor(private addressTypeService: AddressTypeService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<PacienteEnderecosComponent>,
    public loader: AppLoaderService,
    private fb: FormBuilder,
    private stateService: StateService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.montarForm();
  }


  montarForm() {
    this.getAddressType();
    this.getState();

    this.addressForm = new FormGroup({
      patientId: new FormControl(""),
      addressTypeId: new FormControl(""),
      address: new FormControl("", [Validators.required]),
      addressNumber: new FormControl("", [Validators.required]),
      city: new FormControl("", [Validators.required]),
      neighborhood: new FormControl("", [Validators.required]),
      stateId: new FormControl(""),
      zipCode: new FormControl("", [Validators.required]),
      complement: new FormControl(""),

    });
  }

  getState() {
    this.stateService.listarEstados().subscribe(res => {
      this.listState = [];
      this.listState = res.ObjectDTO
    })
  }

  getAddressType() {
    this.addressTypeService.getAddressType().subscribe(res => {
      this.listAddressType = [];
      this.listAddressType = res.ObjectDTO
    })
  }

  addAddress() {
    let newAddress = this.addressForm.value;

    let x = this.listAddressType.filter(value => {
      if (value.addressTypeId == newAddress.addressTypeId) {
        return value;
      }
    });
    if (x.length > 0) {
      newAddress.addressTypeDescription = x[0].addressTypeDescription;
    }

    let auxState = this.listState.filter(value => {
      if (value.stateId == newAddress.stateId) {
        return value;
      }
    });
    if (auxState.length > 0) {
      newAddress.stateName = auxState[0].stateName;
    }


    this.dialogRef.close(newAddress);
  }

}
