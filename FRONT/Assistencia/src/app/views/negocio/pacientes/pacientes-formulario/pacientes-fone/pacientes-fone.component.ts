import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { PhoneTypeService } from 'app/services/phone-type.service';

@Component({
  selector: 'app-paciente-fone',
  templateUrl: './pacientes-fone.component.html'
})
export class PacientesFoneComponent implements OnInit {
  public foneForm: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<PacientesFoneComponent>,
    public loader: AppLoaderService,
    private fb: FormBuilder,
    private phoneTypeService: PhoneTypeService,
    private snackBar: MatSnackBar) { }

  listPhoneType = [];

  ngOnInit() {
    this.montarForm();
    this.getPhoneType();
  }

  getPhoneType() {
    this.phoneTypeService.getPhoneType().subscribe(res => {
      this.listPhoneType = [];
      this.listPhoneType = res.ObjectDTO
    })
  }

  montarForm() {
    this.foneForm = new FormGroup({
      phoneNumber: new FormControl("", [Validators.required, Validators.minLength(10)]),
      ramal: new FormControl(""),
      phoneTypeId: new FormControl("", [Validators.required, Validators.min(0)])
    });
  }

  addPhone() {
    let newPhone = this.foneForm.value;

    let x = this.listPhoneType.filter(value => {
      if (value.phoneTypeId == newPhone.phoneTypeId) {
        return value;
      }
    });
    if (x.length > 0) {
      newPhone.description = x[0].description;
    }

    this.dialogRef.close(newPhone);
  }
}
