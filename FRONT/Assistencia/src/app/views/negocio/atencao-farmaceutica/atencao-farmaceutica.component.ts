import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { ClientesFormularioComponent } from '../clientes/clientes-formulario/clientes-formulario.component';
import { AtencaoFarmaceuticaFormComponent } from './atencao-farmaceutica-form/atencao-farmaceutica-form.component';
import { CampoBusca } from 'app/models/base/negocio/CampoBusca';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { LoginService } from 'app/services/negocio/login/login.service';

@Component({
  selector: 'app-atencao-farmaceutica',
  templateUrl: './atencao-farmaceutica.component.html',
  styleUrls: ['./atencao-farmaceutica.component.css']
})
export class AtencaoFarmaceuticaComponent implements OnInit {
  colunas = [
    {
      Propriedade: 'patientId',
      Titulo: 'Id Paciente',
      Visivel: true,
      Largura : 60
    },
    {
      Propriedade: 'PatientName',
      Titulo: 'Paciente',
      Visivel: true,
      Largura : 250
    },
    {
      Propriedade: 'CPF',
      Titulo: 'CPF',
      Visivel: true,
      Largura : 100
    }

  ];

  linhas = [];

  public configBusca = [
    new CampoBusca("patientName", "Nome Paciente", 50, "", "string"),
    new CampoBusca("CPF", "CPF", 15, "000.000.000-00", "string"),
    new CampoBusca("identificationOnClient", "Identificação no cliente", 50, "", "string")
  ];  
  public lastFind : any;  

  constructor(private dialog: MatDialog, private crud : CRUDService, private login : LoginService) { }

  ngOnInit() {
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Nova Assistência Farmacêutica";
    } else {
      titulo = "Visualiar Assistência Farmacêutica: " + dados.ClientId;
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(AtencaoFarmaceuticaFormComponent, {
      width: '90%', height: '80%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.getAtencao(this.lastFind);
        return;
      });
  }

  getAtencao(parametro: any) {
    this.lastFind = parametro;
    let usr = this.login.getNomeUsuarioDetalhes();
    // let params: any = new Object();
    // params.userLogin = usr.userLogin;
    // params.Filter = parametro.filter;
    // params.ClientId = usr.ClientId;
    this.crud.GetParams(parametro, "/PharmaceuticalAttentions").subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO;
    })
  }



  visualizarAtencao(evento){

  }

}

