import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar, MatDialog } from '@angular/material';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { OrdersDetailComponent } from '../../orders/orders-detail/orders-detail.component';
import { ReleaseOrderFormComponent } from '../../release-order/release-order-form/release-order-form.component';
import { PrescriptionsFormComponent } from '../../prescriptions/prescriptions-form/prescriptions-form.component';
import { PharmaceuticalAttentionAnamnesisFormComponent } from '../../pharmaceutical-attention-anamnesis/pharmaceutical-attention-anamnesis-form/pharmaceutical-attention-anamnesis-form.component';
import { InteracaoMedicamentosaFormComponent } from '../../interacao-medicamentosa/interacao-medicamentosa-form/interacao-medicamentosa-form.component';

@Component({
  selector: 'app-atencao-farmaceutica-form',
  templateUrl: './atencao-farmaceutica-form.component.html',
  styleUrls: ['./atencao-farmaceutica-form.component.css']
})
export class AtencaoFarmaceuticaFormComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AtencaoFarmaceuticaFormComponent>,
    private snackBar: MatSnackBar,
    private loader: AppLoaderService,
    private crudService: CRUDService,
    private dialog: MatDialog) { }

  public formAtencao: FormGroup;
  public formEfeito: FormGroup;
  public listPatients = [];
  public listReceitas = [];
  public listReleaseOrder = [];
  public listEffects = [];
  public linhasInteracoes = [];
  public effectList = [];
  public questionarios = [];
  public titulo = "";
  public finalizado : boolean;

  ngOnInit() {
    this.montarForm(this.data.payload);
  }

  async montarForm(item) {
    this.formAtencao = new FormGroup({
      PharmaceuticalAttentionId : new FormControl(item.PharmaceuticalAttentionId||""),
      patientId: new FormControl(item.patientId||"", [Validators.required]),
      observation: new FormControl(item.observation||""),
      dateAttention : new FormControl(item.dateAttention||"", [Validators.required])
    });

    this.formEfeito = new FormGroup({
      patientId: new FormControl("", [Validators.required]),
      AdverseEffectsId: new FormControl("", [Validators.required]),
      DateStart: new FormControl("", [Validators.required]),
      DateEnd: new FormControl(""),
      Comments: new FormControl("")

    });    

    if (this.data.payload.status =="FINALIZADA"){
      this.finalizado = true;
    }else{
      this.finalizado = false;
    }

    await this.getEffects();
    this.getPatients();
  }

  getPatients() {
    let params: any = new Object();
    params.patientName = "";
    params.cpf = "";
    params.identificationOnClient = "";
    params.clientId = 0;

    this.crudService.GetParams(params, "/patient").subscribe(res => {
      this.listPatients = [];
      this.listPatients = res.ObjectDTO;
    })
  }

  getQuestionarios() {
    let params: any = new Object();
    params.userlogin ="xxx";
    params.patientId = this.formAtencao.controls['patientId'].value;
    this.crudService.GetParams(params, "/researches").subscribe(res => {
      this.questionarios = res.ObjectDTO;
    })
  }

  getPatientEffects() {
    let params: any = new Object();
    params.PatientId = this.formAtencao.controls['patientId'].value;
    this.crudService.GetParams(params, "/PatientAdverseEffect").subscribe(res => {
      this.effectList = [];
      this.effectList = res.ObjectDTO;
    })
  }

  trocaPaciente(evento) {
    this.formEfeito.controls['patientId'].setValue(evento.value);
    this.loadReceitas();
  }

  async getEffects() {
    let params: any = new Object();
    params.userLogin = "ok";
    params.filter = "";
    let res = await this.crudService.GetParams(params, "/AdverseEffects").toPromise();
    this.listEffects = [];
    this.listEffects = res.ObjectDTO;

  }

  showReceita(prescriptionId) {
    let dados: any;
    for (let i = 0; i < this.listReceitas.length; i++) {
      if (this.listReceitas[i].prescriptionId == prescriptionId) {
        dados = this.listReceitas[i];
        break;
      }
    }
    var titulo = "Resumo da Receita";
    let dialogRef: MatDialogRef<any> = this.dialog.open(PrescriptionsFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: false }
    });
  }

  showQuestionario(researchId) {
    let dados: any;
    for (let i = 0; i < this.questionarios.length; i++) {
      if (this.questionarios[i].ResearchesId == researchId) {
        dados = this.questionarios[i];
        break;
      }
    }
    var titulo = "Resumo da Anamnese";
    let dialogRef: MatDialogRef<any> = this.dialog.open(PharmaceuticalAttentionAnamnesisFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: false }
    });
  }

  addEffectAdverse() {
    let form = this.formEfeito.value;
    this.crudService.Save(form, true, '/PatientAdverseEffect').subscribe(res => {
      if (res.Status == "OK") {
        this.snackBar.open("Efeito gravado com sucesso", "", { duration: 3000 })
        this.formEfeito.reset();
        this.getPatientEffects();

        //buscar efeitos
      }
    });
  }

  showReleaseOrder(releaseOrderId) {
    let dados: any;
    for (let i = 0; i < this.listReleaseOrder.length; i++) {
      if (this.listReleaseOrder[i].releaseOrderId == releaseOrderId) {
        dados = this.listReleaseOrder[i];
        break;
      }
    }
    var titulo = "Resumo do pedido";
    let dialogRef: MatDialogRef<any> = this.dialog.open(ReleaseOrderFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: false }
    });
  }

  selectTab(evento) {
    switch (evento.index) {
      case 0: //receitas
        this.loadReceitas();
        break;
      case 1: //Pedidos
        this.loadPedidosLiberados();
        break;
      case 2://Anamnese
        this.getQuestionarios();
        break;
      case 3: //interacao medicamentosa
        this.loadInteracoes();
        break;
      case 4: //efitos adversos
        this.getPatientEffects();
        break;
    }
  }

  loadInteracoes() {
    let params: any = new Object();
    params.patientid = this.formAtencao.controls['patientId'].value;
    this.crudService.GetParams(params, "/InterationMedicine").subscribe(res => {
      this.linhasInteracoes = [];
      this.linhasInteracoes = res.ObjectDTO;

    });
  }

  verInteracao(row) {
    var titulo;
    titulo = "Visualiar Interação Medicamentosa ";

    let dialogRef: MatDialogRef<any> = this.dialog.open(InteracaoMedicamentosaFormComponent, {
      width: '90%', height: '80%',
      disableClose: true,
      data: { title: titulo, payload: row, novo: false }
    });
  }

  loadReceitas() {
    let params: any = new Object();
    params.userLogin = "teste";
    params.prescriptionId = "0";
    params.patientId = this.formAtencao.controls['patientId'].value;
    params.ok = "ok";
    if (params.patientId != null && params.patientId != undefined && params.patientId != "") {
      this.crudService.GetParams(params, "/Prescription").subscribe(res => {
        this.listReceitas = [];
        this.listReceitas = res.ObjectDTO;
      });
    }
  }

  loadPedidosLiberados() {
    let params: any = new Object();
    params.userLogin = "teste";
    params.releaseOrderId = "0";
    params.patientId = this.formAtencao.controls['patientId'].value;
    params.ok = "ok";

    if (params.patientId != null && params.patientId != undefined && params.patientId != "") {
      this.crudService.GetParams(params, "/ReleaseOrder").subscribe(res => {
        this.listReleaseOrder = [];
        this.listReleaseOrder = res.ObjectDTO;
      });
    }
  }

  novaReceita() {
    let dialogRef: MatDialogRef<any> = this.dialog.open(PrescriptionsFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: "Nova receita", payload: {}, novo: true }
    });
  }

  novaAnamnese() {
    let dialogRef: MatDialogRef<any> = this.dialog.open(PharmaceuticalAttentionAnamnesisFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: "Nova Anamnese", payload: {}, novo: true }
    });
  }

  novaInteracao() {
    let dialogRef: MatDialogRef<any> = this.dialog.open(InteracaoMedicamentosaFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: "Nova Interação Medicamentosa", payload: {}, novo: true }
    });
  }

  salvar(){
    let form = this.formAtencao.value;
    form.userLogin = "ok";
    form.Status = "FINALIZADA";
    this.titulo = "Finalizar"
    this.crudService.Save(form, true, "/PharmaceuticalAttentions").subscribe(res => {
      if (res.Status =="OK"){
        this.snackBar.open("Atenção farmacêutica gravada com sucesso", "", { duration : 3000});
        this.finalizado = true;
        this.dialogRef.close("OK");
      }
    })
  }

  finalizar(){
    let form = this.formAtencao.value;
    form.userLogin = "ok";
    form.Status = "FINALIZADA";
    this.crudService.Save(form, this.data.novo, "/PharmaceuticalAttentions").subscribe(res => {
      if (res.Status =="OK"){
        this.snackBar.open("Atenção farmacêutica gravada com sucesso", "", { duration : 3000});
        this.dialogRef.close("OK");
      }
    })
  }  

}
