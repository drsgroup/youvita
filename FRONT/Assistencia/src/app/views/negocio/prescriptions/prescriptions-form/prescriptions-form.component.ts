import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatInput } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { WebContract } from 'app/models/base/Contrato';
import { PrescriptionService } from 'app/services/negocio/prescription/prescription.service';
import { PatientsService } from 'app/services/negocio/patients/patients.service';
import { LoginService } from 'app/services/negocio/login/login.service';
import { StateService } from 'app/services/state.service';
import { ClientService } from 'app/services/negocio/client/client.service';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';

@Component({
  selector: 'app-prescriptions-form',
  templateUrl: './prescriptions-form.component.html',
  styleUrls: ['./prescriptions-form.component.css']
})
export class PrescriptionsFormComponent implements OnInit {
  public prescriptionForm: FormGroup;
  public medicinesForm: FormGroup;
  public findMedicinesForm: FormGroup;
  public ClientId: number;
  public telaCliente: boolean;
  public fileContent: string = "";
  public fileContentType: string = "";
  public fileContentName: string = "";
  @ViewChild("ean") private ean: ElementRef;
  @ViewChild("dailyDose") private dailyDose: ElementRef;



  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<PrescriptionsFormComponent>,
    public loader: AppLoaderService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private prescriptionService: PrescriptionService,
    private patientService: PatientsService,
    private loginService: LoginService,
    private stateService: StateService,
    private clientService: ClientService,
    private crud: CRUDService
  ) { }

  listPatients = [];
  listStates = [];
  ListPrescriptionItem = [];
  listMedicines = [];
  selected = [];
  public prescriptionFiles: any;

  ngOnInit() {
    let usuario = this.loginService.getNomeUsuarioDetalhes();
    this.ClientId = usuario.ClientId;

    if (usuario.userType > 1) {
      this.telaCliente = true;
    } else {
      this.telaCliente = false;
    }
    this.montarForm(this.data.payload);
  }

  ftelaCliente(): Boolean {
    let usr = this.loginService.getNomeUsuarioDetalhes();
    if (usr.userType > 1) {
      return true;
    } else {
      return false;
    }
  }

  montarForm(item) {
    this.consultarPatients();
    this.consultarStates();
    this.prescriptionForm = new FormGroup({
      PrescriptionId: new FormControl(item.prescriptionId),
      PatientId: new FormControl(item.patientId, [Validators.required]),
      PrescriptionDate: new FormControl(item.prescriptionDate, [Validators.required]),
      ExpirationDate: new FormControl(item.expirationDate, [Validators.required]),
      Crm: new FormControl(item.crm, [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
      UfCrm: new FormControl(item.ufCrm, [Validators.required]),
      DoctorName: new FormControl(item.doctorName, [Validators.required, Validators.minLength(1), Validators.maxLength(60)]),
      PathImagePrescription: new FormControl(item.pathImagePrescription),
      fileContent: new FormControl("", [Validators.required]),
      fileType: new FormControl("", [Validators.required])

    });

    this.medicinesForm = new FormGroup({
      MedicineId: new FormControl("", [Validators.required, Validators.minLength(1)]),
      MedicineName : new FormControl("", [Validators.required]),
      LabName : new FormControl(""),
      Presentation : new FormControl(""),
      dailyDose: new FormControl("", [Validators.required, Validators.minLength(1)]),
      EAN: new FormControl("", [Validators.required, Validators.minLength(1)]),
      Dispensation : new FormControl("", [Validators.required, Validators.min(0)]),
      // termDays: new FormControl("", [Validators.required, Validators.minLength(1)])

    });

    this.findMedicinesForm = new FormGroup({
      MedicineFind: new FormControl("", [Validators.required, Validators.minLength(3)])
    });

    if (!this.data.novo) {
      this.prescriptionForm.disable();
      this.ListPrescriptionItem = item.ListPrescriptionItem;

      let objPrescriptionFiles: any = new Object();
      objPrescriptionFiles.PrescriptionId = item.prescriptionId;
      objPrescriptionFiles.login = "login";

      this.crud.GetParams(objPrescriptionFiles, "/prescriptionFiles").subscribe(res => {
        if (res.ObjectDTO.length > 0) {
          this.prescriptionFiles = res.ObjectDTO[0];
          if (this.prescriptionFiles.fileType == "jpeg" || this.prescriptionFiles.fileType == "jpg") {
            this.fileContent = "data:image/jpeg;base64," + this.prescriptionFiles.fileContent;
          } else if (this.prescriptionFiles.fileType == "png") {
            this.fileContent = "data:image/png;base64," + this.prescriptionFiles.fileContent;
          }
        }
      });

    }
  }

  downloadReceita() {
    let objPrescriptionFiles: any = new Object();
    objPrescriptionFiles.PrescriptionId = this.data.payload.prescriptionId;
    objPrescriptionFiles.login = "login";
    let linkSource = "";

    this.crud.GetParams(objPrescriptionFiles, "/prescriptionFiles").subscribe(res => {
      if (res.ObjectDTO.length > 0) {
        this.prescriptionFiles = res.ObjectDTO[0];
        if (this.prescriptionFiles.fileType == "pdf") {
          linkSource = "data:application/pdf;base64," + this.prescriptionFiles.fileContent
        } else if (this.prescriptionFiles.fileType == "jpeg" || this.prescriptionFiles.fileType == "jpg") {
          linkSource = "data:image/jpeg;base64," + this.prescriptionFiles.fileContent
        } else if (this.prescriptionFiles.fileType == "png") {
          linkSource = "data:image/png;base64," + this.prescriptionFiles.fileContent
        } else {
          linkSource = "data:application/octet-stream;base64," + this.prescriptionFiles.fileContent
        }

        let downloadLink = document.createElement("a");
        let fileName = this.data.payload.pathImagePrescription;

        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();

      }
    });
  }


  consultarPatients() {
    let params: any = new Object();
    params.patientName  ="";
    params.cpf = "";
    params.identificationOnClient = "";
    params.clientId = 0;
    
    //this.patientService.getPatients(params, this.ClientId).subscribe(res => {
    this.crud.GetParams(params, "/patient").subscribe(res =>{
      this.listPatients = [];
      this.listPatients = res.ObjectDTO;
    });
  }

  addPatientMedicine() {
    let newMedicine = this.medicinesForm.value;
    this.ListPrescriptionItem.push(newMedicine);
    this.ListPrescriptionItem = [...this.ListPrescriptionItem];
    // this.medicinesForm.controls["EAN"].setValue('');
    // this.medicinesForm.controls["dailyDose"].setValue('');
    // this.medicinesForm.controls["termDays"].setValue('');
    // this.medicinesForm.controls["MedicineId"].setValue('');

    this.medicinesForm.reset();
    this.medicinesForm.controls["EAN"].setValue('');
    this.medicinesForm.controls["dailyDose"].setValue('');

  }

  consultarStates() {
    this.stateService.listarEstados().subscribe(res => {
      this.listStates = [];
      this.listStates = res.ObjectDTO;
    });
  }

  onSelect({ selected }) {
    this.medicinesForm.controls["EAN"].setValue(selected[0].EAN);
    this.medicinesForm.controls["MedicineId"].setValue(selected[0].MedicineId);
    this.medicinesForm.controls["MedicineName"].setValue(selected[0].MedicineName);
    this.medicinesForm.controls["LabName"].setValue(selected[0].LabName);
    this.medicinesForm.controls["Presentation"].setValue(selected[0].Presentation);
  }

  procurarMedicamento() {
    let filter = this.findMedicinesForm.controls["MedicineFind"].value;
    this.clientService.buscarMedicamentos(filter).subscribe(res => {
      this.listMedicines = [];
      this.listMedicines = res.ObjectDTO;
    });
  }

  removeMedicine(medicine: any) {
    let aux = this.ListPrescriptionItem.filter(item => {
      if (item === medicine) {
        return false;
      } else {
        return true;
      }
    });
    this.ListPrescriptionItem = [...aux]

  }

  savePrescriptionPatient() {
    let formPrescription = this.prescriptionForm.value;
    formPrescription.ListPrescriptionItem = this.ListPrescriptionItem;

    this.prescriptionService.savePrescription(formPrescription, this.data.novo).subscribe(res => {
      var cnt = new WebContract();
      cnt = res;
      if (cnt.Status == "OK") {
        this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });

      } else {
        this.snackBar.open(cnt.Message, "", { duration: 3000 });
      }
      this.dialogRef.close("OK");
    });

  }

  formValido(): boolean {
    let v = this.prescriptionForm.invalid;
    
    if (this.ListPrescriptionItem.length == 0) {
      v = true; //invalido
    }
    return v;
  }

  selectFile(evento) {
    if (evento.target.files.length > 0) {
      let file: File = evento.target.files[0];
      let fileNameComplete = evento.target.files[0].name;
      let fileName = fileNameComplete.split(".")[0];
      let fileType = fileNameComplete.split(".")[1];
      this.fileContentType = fileType;
      this.fileContentName = fileNameComplete;
      let reader: FileReader = new FileReader();
      const self = this;

      reader.onloadend = (e) => {
        this.fileContent = reader.result.toString();
        let fileContentSplit = this.fileContent.split(",")[1];


        this.prescriptionForm.controls['fileContent'].setValue(fileContentSplit);
        this.prescriptionForm.controls['PathImagePrescription'].setValue(fileName);
        this.prescriptionForm.controls['fileType'].setValue(fileType);

      }
      reader.readAsDataURL(file);
    }
  }



}
