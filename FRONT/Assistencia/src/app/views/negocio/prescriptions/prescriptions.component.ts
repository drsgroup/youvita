import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { PrescriptionService } from 'app/services/negocio/prescription/prescription.service';
import { PrescriptionsFormComponent } from './prescriptions-form/prescriptions-form.component';
import { CampoBusca } from 'app/models/base/negocio/CampoBusca';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';

@Component({
  selector: 'app-prescriptions',
  templateUrl: './prescriptions.component.html',
  styleUrls: ['./prescriptions.component.css']
})
export class PrescriptionsComponent implements OnInit {

  private PrescriptionForm: FormGroup;
  constructor(
    private dialog: MatDialog,
    private prescriptionsService: PrescriptionService,
    private loader: AppLoaderService,
    private confirm: AppConfirmService,
    private snackBar: MatSnackBar,
    private appInformationService: AppInformationService,
    private crudService: CRUDService
  ) { }

  colunas = [
    {
      Propriedade: 'prescriptionId',
      Titulo: 'Código da Receita',
      Visivel: true,
      Largura: 50
    },
    {
      Propriedade: 'patientName',
      Titulo: 'Nome do Paciente',
      Visivel: true,
      Largura: 300
    },
    {
      Propriedade: 'doctorName',
      Titulo: 'Nome do Médico',
      Visivel: true,
      Largura: 80
    },
    {
      Propriedade: 'crm',
      Titulo: 'CRM',
      Visivel: true,
      Largura: 50
    },
    {
      Propriedade: 'stateUF',
      Titulo: 'UF CRM',
      Visivel: true,
      Largura: 50
    },
    {
      Propriedade: 'expirationDate',
      Titulo: 'Validade',
      Visivel: true,
      Largura: 50,
      Tipo: 'DATA'
    }
  ];

  linhas = [];
  public configBusca = [
    new CampoBusca("prescriptionDate", "Data Receita", 10, "", "DATA"),
    new CampoBusca("expirationDate", "Validade Receita", 10, "", "DATA"),
    new CampoBusca("patientName", "Nome Paciente", 50, "", "string"),
    new CampoBusca("CRM", "CRM", 6, "000000", "string"),
    new CampoBusca("UFCRM", "UF CRM", 2, "", "string"),
    new CampoBusca("DoctorName", "Nome Médico", 50, "", "string")
  ];
  public lastFind: any;

  ngOnInit() {
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Novo Cadastro de Receita";
    } else {
      titulo = "Visualização da Receita : " + dados.prescriptionId;
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(PrescriptionsFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.consultarPrescription(this.lastFind)
        return;
      });
  }

  consultarPrescription(parametro: any) {
    this.lastFind = parametro;
    //this.prescriptionsService.getPrescriptionByFilter(parametro).subscribe(res => {
    this.crudService.GetParams(parametro, "/Prescription").subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO
    })
  }


  deletePrescription(parametro: any) {
    this.confirm.confirm("Exclusão de Receita", "Tem certeza que deseja excluir a receita " + parametro.prescriptionId + "?").subscribe(result => {
      if (result === true) {
        this.loader.open("Excluindo receita");
        this.prescriptionsService.deletePrescription(parametro).subscribe(res => {
          if (res.Status == "OK") {
            this.snackBar.open("Receita excluída com sucesso!", "", { duration: 3000 });
            this.consultarPrescription(this.lastFind);
          }
          else {
            this.appInformationService.information("YouVita", res.Message);
          }
          this.loader.close();
        })
      }
    })
  }

}
