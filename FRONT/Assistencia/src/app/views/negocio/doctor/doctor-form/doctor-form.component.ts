import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { LoginService } from 'app/services/negocio/login/login.service';
import { CRUDService } from './../../../../services/negocio/CRUDService/CRUDService';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';
import { DoctorService } from './../../../../services/negocio/doctor/doctor.service';
import { RepresentativeService } from './../../../../services/negocio/representative/representative.service';
import { StateService } from 'app/services/state.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { WebContract } from 'app/models/base/Contrato';
import { HttpParams } from '@angular/common/http';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';
import { Md5 } from "md5-typescript";

@Component({
  selector: 'app-doctor-form',
  templateUrl: './doctor-form.component.html',
  styleUrls: ['./doctor-form.component.css']
})
export class DoctorFormComponent implements OnInit {
  public doctorForm: FormGroup;
  public addressForm: FormGroup;
  linhasStates = [];
  listState = [];
  listAddress = [];
  listRepresentatives = [];
  showAddress = false;
  campoObrigatorio = "Campo obrigatório";
  emailInvalido = "Email inválido";
  labFantasyName  ="";
  user : any;


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DoctorFormComponent>,
    public serviceStates: StateService,
    public serviceRepresentative: RepresentativeService,
    public doctorServide: DoctorService,
    public snackBar: MatSnackBar,
    public confirmation: AppConfirmService,
    public information: AppInformationService,
    public crud: CRUDService,
    public login: LoginService,
    public loader: AppLoaderService) { }

  ngOnInit() {
    this.getStates();
    this.getRepresentatives();
    this.montarForm(this.data.payload);
    this.getLab();
  }

  montarForm(item) {
    this.doctorForm = new FormGroup({
      doctorID: new FormControl(item.doctorID),
      doctorName: new FormControl(item.doctorName, [Validators.required]),
      crm: new FormControl(item.crm, [Validators.required]),
      uf_crm: new FormControl(item.uf_crm, [Validators.required]),
      cpf: new FormControl(item.cpf, [Validators.required]),
      address: new FormControl(item.address, [Validators.required]),
      number: new FormControl(item.number, [Validators.required]),
      complement: new FormControl(item.complement),
      city: new FormControl(item.city, [Validators.required]),
      neighborhood: new FormControl(item.neighborhood, [Validators.required]),
      stateId: new FormControl(item.stateId, [Validators.required]),
      zipCode: new FormControl(item.zipCode, [Validators.required]),
      phoneComercial: new FormControl(item.phoneComercial, [Validators.required]),
      ramal: new FormControl(item.ramal || ""),
      cellPhone: new FormControl(item.cellPhone),
      focalPoint: new FormControl(item.focalPoint, [Validators.required]),
      email: new FormControl(item.email, [Validators.required, Validators.email]),
      login: new FormControl(item.login, [Validators.required]),
      password: new FormControl("*****", [Validators.required]),
      confirmPassword: new FormControl("*****", [Validators.required]),
      comments: new FormControl(item.comments),
      status: new FormControl(item.status, [Validators.required]),
      representativeID: new FormControl(item.representativeID, [Validators.required]),
      laboratoryID: new FormControl(item.laboratoryID, [Validators.required])

    });

    if (!this.data.novo) { //desabilitar alterar login
      this.doctorForm.controls["login"].disable();
      this.doctorForm.controls["password"].clearValidators();
      this.doctorForm.controls["confirmPassword"].clearValidators();
    }else{
      this.doctorForm.controls["password"].setValue("");
      this.doctorForm.controls["confirmPassword"].setValue("");

    }
    // this.addressForm = new FormGroup({
    //   doctorID: new FormControl(""),
    //   address: new FormControl("", [Validators.required]),
    //   addressNumber: new FormControl("", [Validators.required]),
    //   city: new FormControl("", [Validators.required]),
    //   neighborhood: new FormControl("", [Validators.required]),
    //   stateId: new FormControl("", [Validators.required]),
    //   zipCode: new FormControl("", [Validators.required]),
    //   complement: new FormControl(""),
    // });

    // if ((item.ListDoctorAddresses != null) && (item.ListDoctorAddresses != undefined)) {
    //   this.listAddress = [...item.ListDoctorAddresses];
    // }
  }

  ToggleAddress() {
    this.showAddress = !this.showAddress;
  }

  selectRepres(event: any) {
    console.log(event);
    for (let i = 0; i < this.listRepresentatives.length; i++) {
      if (this.listRepresentatives[i].representativeID == event.value) {
        this.doctorForm.controls["laboratoryID"].setValue(this.listRepresentatives[i].laboratoryID);
        break;
      }
    }
  }

  SaveDoctor() {
    this.loader.open();
    let formulario = this.doctorForm.value;
    if (this.data.novo) {
      this.login.IsExists(formulario.login).subscribe(res => {
        if (res.Status == "OK" && res.ObjectDTO.length > 0) {
          this.information.information("YouVita", "Este login já existe, por favor informar um diferente");
          this.loader.close();
          return false;
        }
        if (this.IsValid(formulario)) {
          formulario.ListDoctorAddresses = this.listAddress;
          this.doctorServide.saveDoctor(formulario, this.data.novo).subscribe(res => {
            var cnt = new WebContract();
            cnt = res;
            if (cnt.Status == "OK") {
              this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });
            } else {
              this.snackBar.open(cnt.Message, "", { duration: 3000 });
            }
            this.dialogRef.close("OK");
            this.loader.close();
          });
        }
        this.loader.close();
      });
    } else {
      if (this.IsValid(formulario)) {
        formulario.ListDoctorAddresses = this.listAddress;
        if (formulario.password != "*****") {
          formulario.password = Md5.init(formulario.password);
        }else{
          formulario.password =null;
        }
        formulario.login = this.data.payload.login;

        this.doctorServide.saveDoctor(formulario, this.data.novo).subscribe(res => {
          var cnt = new WebContract();
          cnt = res;
          if (cnt.Status == "OK") {
            this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });
          } else {
            this.snackBar.open(cnt.Message, "", { duration: 3000 });
          }
          this.dialogRef.close("OK");
          this.loader.close();
        });
      }
      this.loader.close();

    }
  }

  IsValid(form: any) {
    if (form.password !== form.confirmPassword) {
      this.information.information("YouVita", "Confirmação de senha incorreta.");
      return false;
    }
    return true;

  }

  getStates() {
    this.serviceStates.listarEstados().subscribe(res => {
      this.linhasStates = [];
      this.linhasStates = res.ObjectDTO;
      //console.log(res.ObjectDTO);
    })
  }

  getRepresentatives() {
    let user = this.login.getNomeUsuarioDetalhes();
    let params: any = new Object();
    params.userLogin = "login";
    params.laboratoryID = user.laboratoryID;
    params.ok = "ok";
    this.crud.GetParams(params, "/Representative").subscribe(res => {
      this.listRepresentatives = [];
      this.listRepresentatives = res.ObjectDTO;
    });
  }

  getLab(){
    this.user = this.login.getNomeUsuarioDetalhes();
    this.crud.List("", "/laboratory").subscribe(res => {
      let list :any[]= res.ObjectDTO;
      for(let i =0; i < list.length;i++){
        if (this.user.laboratoryID == list[i].laboratoryID){
          this.labFantasyName = list[i].laboratoryFantasyName;
          break;
        }
      }

    });
  }

  // addAddress() {
  //   if (this.data.novo == true) {
  //     let newAddress = this.addressForm.value;
  //     this.listAddress.push(newAddress);
  //     this.listAddress = [...this.listAddress];
  //   } else {
  //     let newAddress = this.addressForm.value;
  //     newAddress.doctorID = this.data.payload.doctorID;
  //     this.doctorServide.addAddress(newAddress).subscribe(res => {
  //       if (res.Status == "OK") {
  //         this.listAddress.push(newAddress);
  //         this.listAddress = [...this.listAddress];
  //         this.snackBar.open("Endereço adicionado com sucesso.", "", { duration: 3000 });

  //       }
  //     });
  //   }
  //   this.addressForm.reset();
  // }

  // removeAddress(registro) {
  //   if (this.data.novo == false) {
  //     this.confirmation.confirm("YouVita", "Tem certeza que deseja exlcuir este endereço?").subscribe(res => {
  //       if (res === true) {
  //         this.doctorServide.deleteAddress(registro.doctorAddressesId).subscribe(res => {
  //           if (res.Status == "OK") {
  //             let aux = this.listAddress.filter(item => {
  //               if (item === registro) {
  //                 return false;
  //               } else {
  //                 return true;
  //               }
  //             });
  //             this.listAddress = [...aux];
  //             this.snackBar.open("Endereço excluído com sucesso", "", { duration: 3000 });
  //           }
  //         })
  //       }
  //     })
  //   } else {
  //     let aux = this.listAddress.filter(item => {
  //       if (item === registro) {
  //         return false;
  //       } else {
  //         return true;
  //       }
  //     });
  //     this.listAddress = [...aux];
  //   }
  // }



}
