import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { DoctorService } from './../../../services/negocio/doctor/doctor.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { DoctorFormComponent } from './doctor-form/doctor-form.component';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';
import { LoginService } from 'app/services/negocio/login/login.service';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.css']
})
export class DoctorComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    public serviceDoctor : DoctorService,
    public confirm : AppConfirmService,
    public loader : AppLoaderService,
    public snackBar : MatSnackBar,
    public appInformationService : AppInformationService,
    public login : LoginService,
    public crud : CRUDService
    ) { }

  colunas = [
    {
      Propriedade: 'doctorID',
      Titulo: 'ID Médico',
      Visivel: true,
      Largura : 50
     },
    {
      Propriedade: 'doctorName',
      Titulo: 'Nome do Médico',
      Visivel: true,
      Largura : 300
    },
    {
      Propriedade: 'crm',
      Titulo: 'CRM',
      Visivel: true,
      Largura : 100
    },
    {
      Propriedade: 'stateUF',
      Titulo: 'UF CRM',
      Visivel: true,
      Largura : 100
    },
    {
      Propriedade: 'laboratoryFantasyName',
      Titulo: 'Nome Laboratório',
      Visivel: true,
      Largura : 200 
    }

  ];

  linhas = [];  

  ngOnInit() {
  }

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Novo Médico";
    } else {
      titulo = "Editar Médico : "+dados.doctorID;
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(DoctorFormComponent, {
      width: '90%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {    
        this.consultarMedico("");    
        return;
      });
  }  

  consultarMedico(parametro : string) {
    let user = this.login.getNomeUsuarioDetalhes();
    let params: any = new Object();
    params.userLogin = "login";
    params.laboratoryID = user.laboratoryID;
    params.doctor = parametro;
    params.nok ="nok";
    this.crud.GetParams(params, "/doctor").subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO      

    })
  }

  deleteMedico(doctor: any) {
    this.confirm.confirm("Exclusão do Médico", "Tem certeza que deseja excluir o Médico " + doctor.doctorName + "?").subscribe(result => {
      if (result === true) {
        this.loader.open("Excluindo Médico");
        this.serviceDoctor.deleteDoctor(doctor).subscribe(res => {
          if (res.Status == "OK") {
            this.snackBar.open("Médico excluído com sucesso!", "", { duration: 3000 });
            this.consultarMedico("");
          }
          else {
            this.appInformationService.information("YouVita", res.Message);
          }
          this.loader.close();
        })
      }
    })
  }  

}
