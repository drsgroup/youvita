import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar, MatDialog } from '@angular/material';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';

@Component({
  selector: 'app-interacao-medicamentosa-form',
  templateUrl: './interacao-medicamentosa-form.component.html',
  styleUrls: ['./interacao-medicamentosa-form.component.css']
})
export class InteracaoMedicamentosaFormComponent implements OnInit {
  public formInteracao: FormGroup;
  public findMedicinesForm: FormGroup;
  public interacao : FormGroup;
  public listPatients = [];
  public listMedicines = [];
  public listMedicinesFind = [];
  public listResult = [];
  public controle : boolean = false;
  public MedicineInteractionPatientID : number = 0;
  

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<InteracaoMedicamentosaFormComponent>,
    private snackBar: MatSnackBar,
    private loader: AppLoaderService,
    private crudService: CRUDService,
    private dialog: MatDialog,
    private appInformation : AppInformationService) { }

  ngOnInit() {
    this.montarForm(this.data.payload);    
    
  }

  montarForm(item: any) {
    this.formInteracao = new FormGroup({
      patientId: new FormControl(item.PatientID)
    });

    this.interacao = new FormGroup({
      Comments : new FormControl(item.Comments, [Validators.required])
    });

    if (!this.data.novo){
      this.formInteracao.controls['patientId'].disable();
    }

    this.findMedicinesForm = new FormGroup({
      MedicineFind: new FormControl("",[Validators.required, Validators.minLength(3)])
    });
    this.getPatients();

    if (!this.data.novo){
      let par : any = new Object();
      par.InteractionMedicineID = item.MedicineInteractionPatientID;
      this.crudService.GetParams(par, "/MedicineInteractionPatientItem").subscribe(res => {
        this.listMedicines = [];
        this.listMedicines = res.ObjectDTO;
      });
      this.getResults(item.MedicineInteractionPatientID);
    }    
  }

  getPatients() {
    let params: any = new Object();
    params.patientName  ="";
    params.cpf = "";
    params.identificationOnClient = "";
    params.clientId = 0;    

    this.crudService.GetParams(params, "/patient").subscribe(res => {
      this.listPatients = [];
      this.listPatients = res.ObjectDTO;
    })
  }

  trocaPaciente(evento) {
    //carregar os medicamentos
    let params: any = new Object();
    params.userLogin = "ok";
    params.patientId = evento.value;
    this.listMedicines = [];
    // this.crudService.GetParams(params, "/InterationMedicine").subscribe(res => {
    //   this.listMedicines = res.ObjectDTO;
    // });
  }

  procurarMedicamento() {
    let params: any = new Object();
    params.userLogin = "ok";
    params.filter = this.findMedicinesForm.controls['MedicineFind'].value;
    if (params.filter != undefined && params.filter != null && params.filter != "") {
      if (params.filter.length >= 3) {
        this.crudService.GetParams(params, "/medicine").subscribe(res => {
          this.listMedicinesFind = [];
          this.listMedicinesFind = res.ObjectDTO;
        });
      }else{
        this.appInformation.information("YouVita", "Para efetuar busca de medicamento precisa de pelos menos 3 caracteres");
      }
    }
  }

  addMedicine(medicine){
    this.listMedicines.push(medicine);
    this.listMedicines = [...this.listMedicines];
    this.listMedicinesFind = [];
  }

  delMedicine(medicine){
    let x = this.listMedicines.filter( o => {
      if (o != medicine){
        return o;
      }
    });
    this.listMedicines=  [];
    this.listMedicines = x;
  }

  SaveInteraction(){
    let form = this.formInteracao.value;
    form.items = [];
    for(let i= 0; i < this.listMedicines.length; i++){
      let item : any = new Object();
      item.MedicineInteractionPatientItemID = 0;
      item.InteractionMedicineID = 0 ;
      item.ean = this.listMedicines[i].EAN;      
      form.items.push(item);
    }
    

    this.crudService.Save(form, true, "/InterationMedicine").subscribe(res => {
      if (res.Status == "OK"){        
        this.snackBar.open("Registro gravado com sucesso", "", {duration : 3000});        
        let ret : any = new Object();
        ret = res.ObjectDTO;    
        this.MedicineInteractionPatientID = ret.MedicineInteractionPatientID;
        this.getResults(ret.MedicineInteractionPatientID);
      }
    });
  }

  getResults(MedicineInteractionPatientID : number){
    let params : any = new Object();
    params.MedicineInteractionPatientItemId = MedicineInteractionPatientID;
    this.crudService.GetParams(params, "/InteractionMedicineResults").subscribe(res => {
      this.listResult = [];
      this.listResult = res.ObjectDTO; 
      this.controle = true;         
    });   
  }

  saveActions(){
    let form = this.interacao.value;
    form.MedicineInteractionPatientID = this.MedicineInteractionPatientID;
    this.crudService.Save(form, false, "/InterationMedicine").subscribe(res => {
      if (res.Status =="OK"){
        this.snackBar.open("Ações gravadas com sucesso", "", {duration : 4000});
      }
    });
    
  }
}
