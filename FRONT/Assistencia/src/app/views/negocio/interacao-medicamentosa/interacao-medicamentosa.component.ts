import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { InteracaoMedicamentosaFormComponent } from './interacao-medicamentosa-form/interacao-medicamentosa-form.component';
import { CampoBusca } from 'app/models/base/negocio/CampoBusca';
import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';

@Component({
  selector: 'app-interacao-medicamentosa',
  templateUrl: './interacao-medicamentosa.component.html',
  styleUrls: ['./interacao-medicamentosa.component.css']
})
export class InteracaoMedicamentosaComponent implements OnInit {
  public lastFind : any;

  linhas = [];
  colunas = [
    {
      Propriedade: 'MedicineInteractionPatientID',
      Titulo: 'Id.Interação',
      Visivel: true,
      Largura: 50
    },
    {
      Propriedade: 'patientName',
      Titulo: 'Paciente',
      Visivel: true,
      Largura: 100
    },
    {
      Propriedade: 'DateInteraction',
      Titulo: 'Data Interação',
      Visivel: true,
      Largura: 80,
      Tipo : "DATA"
    },
    {
      Propriedade: 'clientName',
      Titulo: 'Cliente',
      Visivel: true,
      Largura: 200
    }
  ];
  public configBusca =[
                       new CampoBusca("patientName","Nome Paciente", 50, "", "string"),
                       new CampoBusca("clientName", "Nome Cliente", 50, "", "string"),
                       new CampoBusca("dtIni", "Data inicial", 20, "", "DATA"),
                       new CampoBusca("dtFim", "Data final", 20, "", "DATA")];

  constructor(private dialog: MatDialog,
              private crudService : CRUDService) { }

  ngOnInit() {
  }


  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Nova Interação Medicamentosa" ;
    } else {
      titulo = "Visualiar Interação Medicamentosa : " + dados.ClientId;
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(InteracaoMedicamentosaFormComponent, {
      width: '90%', 
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.consultarInteracao(this.lastFind);
        return;
      });
  }

  consultarInteracao(parametro){
    this.lastFind = parametro;
    this.crudService.GetParams(parametro, "/InterationMedicine").subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO;
    });
  }

  visualizarInteracao(evento){

  }
}
