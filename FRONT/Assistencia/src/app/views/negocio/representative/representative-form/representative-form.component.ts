import { CRUDService } from 'app/services/negocio/CRUDService/CRUDService';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { AppInformationService } from './../../../../services/dialogs/app-information/app-information.service';
import { RepresentativeService } from './../../../../services/negocio/representative/representative.service';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { WebContract } from 'app/models/base/Contrato';
import { LoginService } from 'app/services/negocio/login/login.service';
import { Md5 } from 'md5-typescript';

@Component({
  selector: 'app-representative-form',
  templateUrl: './representative-form.component.html',
  styleUrls: ['./representative-form.component.css']
})
export class RepresentativeFormComponent implements OnInit {
  public represrForm: FormGroup;
  campoObrigatorio = "Campo obrigatório";
  emailInvalido = "Email inválido";
  listLabs = [];
  listState = [];
  user: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<RepresentativeFormComponent>,
    public representativeService: RepresentativeService,
    private snackBar: MatSnackBar,
    private information: AppInformationService,
    private login: LoginService,
    private loader: AppLoaderService,
    private crudServices: CRUDService) { }

  ngOnInit() {
    this.montarForm(this.data.payload);
  }

  montarForm(item) {
    this.getStates();
    this.represrForm = new FormGroup({
      representativeID: new FormControl(item.representativeID),
      representativeName: new FormControl(item.representativeName, [Validators.required, Validators.minLength(5)]),
      email: new FormControl(item.email, [Validators.required, Validators.email]),
      login: new FormControl(item.login, [Validators.required]),
      password: new FormControl("*****", [Validators.required]),
      confirmPassword: new FormControl("*****", [Validators.required]),
      status: new FormControl(item.status, [Validators.required]),
      comments: new FormControl(item.comments),
      laboratoryID: new FormControl(item.laboratoryID, [Validators.required]),
      cpf : new FormControl(item.cpf, [Validators.required]),
      address: new FormControl(item.address, [Validators.required]),
      addressNumber: new FormControl(item.addressNumber, [Validators.required]),
      complement : new FormControl(item.complement),
      city: new FormControl(item.city, [Validators.required]),
      neighborhood: new FormControl(item.neighborhood, [Validators.required]),
      stateId: new FormControl(item.stateId, [Validators.required]),
      zipCode: new FormControl(item.zipCode, [Validators.required]),
      phoneComercial: new FormControl(item.phoneComercial),
      ramal: new FormControl(item.ramal),
      cellPhone: new FormControl(item.cellPhone)      
    });


    if (!this.data.novo) { //desabilitar alterar login
      this.represrForm.controls["login"].disable();
      this.represrForm.controls["password"].clearValidators();
      this.represrForm.controls["confirmPassword"].clearValidators();
    }else{
      this.represrForm.controls["password"].setValue("");
      this.represrForm.controls["confirmPassword"].setValue("");
    }

    this.crudServices.List("", "/Laboratory").subscribe(res => {
      this.listLabs = [];
      this.listLabs = res.ObjectDTO;
    });

    this.user = this.login.getNomeUsuarioDetalhes();
    if (this.user.userType == 3) {
      this.represrForm.controls["laboratoryID"].setValue(this.user.laboratoryID);
      this.represrForm.controls["laboratoryID"].disable();
    }
    
  }

  SaveRepresentative() { 
    this.loader.open();
    let formulario = this.represrForm.value;
    if (this.user.userType == 3) {
      formulario.laboratoryID = this.user.laboratoryID;
    }
    if (this.data.novo) {
      this.login.IsExists(formulario.login).subscribe(res => {
        if (res.Status == "OK" && res.ObjectDTO.length > 0) {
          this.information.information("YouVita", "Este login já existe, por favor informar um diferente");
          this.loader.close();
          return false;
        }
        if (this.IsValid(formulario)) {
          formulario.password = Md5.init(formulario.password);
          this.representativeService.saveRepresentative(formulario, this.data.novo).subscribe(res => {
            var cnt = new WebContract();
            cnt = res;
            if (cnt.Status == "OK") {
              this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });
            } else {
              this.snackBar.open(cnt.Message, "", { duration: 3000 });
            }
            this.dialogRef.close("OK");
            this.loader.close();
          });
        };
        this.loader.close();
      });
    } else {
      if (this.IsValid(formulario)) {
        if (formulario.password != "*****") {
          formulario.password = Md5.init(formulario.password);
        }else{
          formulario.password =null;
        }
        formulario.login = this.data.payload.login;
        this.representativeService.saveRepresentative(formulario, this.data.novo).subscribe(res => {
          var cnt = new WebContract();
          cnt = res;
          if (cnt.Status == "OK") {
            this.snackBar.open("Registro gravado com sucesso", "", { duration: 3000 });
          } else {
            this.snackBar.open(cnt.Message, "", { duration: 3000 });
          }
          this.dialogRef.close("OK");
          this.loader.close();
        });
      };
      this.loader.close();
    }
  }

  IsValid(form) {
    if (form.password !== form.confirmPassword) {
      this.information.information("YouVita", "Confirmação de senha incorreta.");
      return false;
    }
    return true;
  }

  getStates(){
    this.crudServices.List("", "/state").subscribe(res => {
      this.listState = [];
      this.listState = res.ObjectDTO;
    })
  }

}
