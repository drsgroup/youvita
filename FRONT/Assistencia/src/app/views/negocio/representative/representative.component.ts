import { AppConfirmModule } from './../../../services/dialogs/app-confirm/app-confirm.module';
import { Component, OnInit } from '@angular/core';
import { RepresentativeFormComponent } from './representative-form/representative-form.component';
import { MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { RepresentativeService } from 'app/services/negocio/representative/representative.service';
import { AppConfirmService } from 'app/services/dialogs/app-confirm/app-confirm.service';
import { AppLoaderService } from 'app/services/dialogs/app-loader/app-loader.service';
import { AppInformationService } from 'app/services/dialogs/app-information/app-information.service';

@Component({
  selector: 'app-representative',
  templateUrl: './representative.component.html',
  styleUrls: ['./representative.component.css']
})
export class RepresentativeComponent implements OnInit {
  colunas = [
    {
      Propriedade: 'representativeID',
      Titulo: 'ID Rep.',
      Visivel: true,
      Largura : 60
     },
    {
      Propriedade: 'representativeName',
      Titulo: 'Nome do Representante',
      Visivel: true,
      Largura : 300
    },
    {
      Propriedade: 'email',
      Titulo: 'Email',
      Visivel: true,
      Largura : 300
    },
    {
      Propriedade: 'login',
      Titulo: 'login',
      Visivel: true,
      Largura : 100
    }
  ];

  linhas = [];    

  constructor(
    private dialog: MatDialog,
    public representativeService : RepresentativeService,
    private confirm: AppConfirmService,
    private loader: AppLoaderService,
    private snackBar: MatSnackBar,
    private appInformationService: AppInformationService) { }

  ngOnInit() {
  }

  consultarRepresentante(parametro : string){
    this.representativeService.getRepresentatives(parametro).subscribe(res => {
      this.linhas = [];
      this.linhas = res.ObjectDTO      
    })   
  }  


  deleteRepresentante(representant: any) {
    this.confirm.confirm("Exclusão do Paciente", "Tem certeza que deseja excluir o Paciente " +
    representant.representativeName + "?").subscribe(result => {

        if (result === true) {
          this.loader.open("Excluindo Paciente");
          this.representativeService.deleteRepresentative(representant).subscribe(res => {
            if (res.Status == "OK") {
              this.representativeService.getRepresentatives("");
              this.snackBar.open("Representante Excluído com sucesso!", "", { duration: 3000 });
              this.consultarRepresentante("");
            }
            else {
              this.appInformationService.information("YouVita", res.Message);
            }
            this.loader.close();
          })
        }
      })
  }  

  openForm(dados: any = {}, novo: Boolean) {
    var titulo;
    if (novo) {
      titulo = "Novo Representante";
    } else {
      titulo = "Editar Representante : " + dados.representativeID;
    }
    let dialogRef: MatDialogRef<any> = this.dialog.open(RepresentativeFormComponent, {
      width: '80%',
      disableClose: true,
      data: { title: titulo, payload: dados, novo: novo }
    });

    dialogRef.afterClosed()
      .subscribe(res => {        
        this.consultarRepresentante("");
        return;
      });
  }  

}
